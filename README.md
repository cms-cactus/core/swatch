# SWATCH

SWATCH (SoftWare for Automating conTrol of Common Hardware) is a software framework that is used to control and monitor electronics for upgrades of the CMS experiment; it was originally developed as part of the phase-1 upgrade to the level-1 trigger system.

Specifically, this repository contains:
 1. The SWATCH `core` & `action` libraries, which provide a framework for the registration of arbitrary system-/board-specific control/configuration procedures and monitoring data. Once registered to the framework, these system-/board-specific control procedures and monitoring data can then be accessed from generic system-/board-independent applications using the library's abstract interfaces. This is achieved by users creating plugin libraries that declare board-/system-specific control procedures; these plugins are then loaded by a generic board-/system-independent control application.

 2. Libraries that build on top of the `core` and `action` libraries in order to implement abstract interfaces that represent the common structure of boards, firmware and monitoring data in the level-1 trigger:
   * `swatch/processor`: A data processor board (non-hub MicroTCA blade)
   * `swatch/dtm`: A DAQ-TTC manager (DTM) board (i.e. abstract interface for an AMC13)
   * `swatch/system`: A trigger subsystem (i.e. collection of processor and DTM boards in MicroTCA crates)

 3. Three plugin libraries, based on this model:
   * Dummy plugin: `swatch/dummy`
   * MP7 plugin: `swatch/mp7`
   * AMC13 plugin: `swatch/amc13`

 4. SWATCH cell: The XDAQ application (a Trigger Supervisor cell) that is used to control subsystems of the level-1 trigger. 

*Note:* Much of the information in this README is meant for developers of the SWATCH libraries. Subsystem developers should instead read the user's guide which can be found here: http://cactus.web.cern.ch/cactus/release/swatch/latest_doc/index.html


## Dependencies

The main dependencies are:
 * Build utilities: make
 * [boost](https://boost.org)
 * [log4cplus](https://github.com/log4cplus/log4cplus)
 * [XDAQ15](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CMSOS)
 * [Prometheus CPP library](https://gitlab.cern.ch/cms-cactus/core/extern/prometheus-cpp) to export OpenMetrics

The [Trigger Supervisor](https://gitlab.cern.ch/cms-cactus/core/ts) library is also required to build the SWATCH cell.

The MP7 and AMC13 plugins also depend on:
 * [uHAL](https://ipbus.web.cern.ch/)
 * [MP7 software](https://gitlab.cern.ch/cms-cactus/boards/mp7)
 * [AMC13 software](https://gitlab.cern.ch/cms-cactus/boards/amc13)


## Build instructions

 1. Install the dependencies listed above

 2. Build the software. You can select which libraries to build through the `Set` variable:

    * Only non-cell libraries and dummy plugin: `make Set=core`  (default value if `Set` not specified)
    * Non-cell libraries + all plugins: `make Set=hwdevel`
    * All packages, excluding MP7 & AMC13 plugins: `make Set=no_hw`
    * All packages: `make Set=all`

    Add ` rpm` to the above commands if you want to build the RPMs (e.g. `make Set=all rpm`).


## Test suite

The functionality of the library's core classes and functions are are validated by an extensive suite of unit tests (implemented using the boost test framework, in the `*/test` directories). The test suite can be run as follows:

```
source swatch/test/env.sh
./swatch/test/bin/boostTest.exe --log_level=test_suite
```

If you want to run a single test, use the `--run_test` flag, e.g: `--run_test=SomeTestSuite/MyTest`


## Run the example SWATCH cell

The scripts and configuration files for running the example SWATCH cell can be found under `swatch/cell/example/test`

The example SWATCH cell can be run as follows:

~~~
./swatch/cell/test/runStandalone.sh
~~~

This script accepts a few options (that can be listed by adding the `--help` argument):

 * `--gdb` : Run cell under gdb
 * `--valgrind` : Run cell under valgrind
 * `--hw` : Loads SWATCH plugin libraries for AMC13 & MP7 

