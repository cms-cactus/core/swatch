#!/bin/bash

# Utility script for running clang-format over the source code 
# Tom Williams, July 2020

set -e


# Parse arguments
if [ $# -eq "0" ]
then
  echo "WARNING: This script will edit all of the source code in this repository"
  read -p "Continue (y/n)? " choice
  case "$choice" in 
    [yY] | [yY][eE][sS] )
      ;;
    [nN] | [nN][oO] )
      echo "Aborting script before editing files." 
      exit 0
      ;;
    * )
      echo "Invalid input. Aporting script before editing files."
      exit 0
      ;;
  esac
elif [ $# -ne "1" ] || [ "$1" != "-y" ]
then
  echo "ERROR: Invalid usage!"
  echo "usage: $0 [-y]"
  exit 1
fi  


# Run clang-format
echo "Formatting source code files"
find . -name "*.hpp" -or -name "*.hxx" -or -name "*.cpp" -or -name "*.cxx" | xargs clang-format --style=file -i