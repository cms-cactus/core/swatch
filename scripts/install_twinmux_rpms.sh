#!/bin/bash
set -e

if [ "$1" == "-h" ] ; then
    # Display Help
    echo "This script needs two arguments, your subsystem id the tag corresponding to the subsystem installation (update)"
    echo
    echo "example: install_subsystem_rpms.sh calol2 swatch-v1.6.0"
    echo   
    exit 1
fi

if [ "$#" != 2 ] ; then
    echo "number of arguments must be 2: subsystem-id tag/branch"
    echo "example: install_subsystem_rpms.sh calol2 swatch-v1.6.0"
    exit 1
fi

case $1 in
    twinmux)  # known subsystem
        ;;
    *)
        echo 'first argument should be a known subsystem'
	echo "bmtf|omtf|emtf|cppf|twinmux|calol1|calol2|ugt|ugmt"
        exit 1
esac

export subsystem=`echo $1`
export tag=`echo $2`
export scratch_folder=/cmsnfsscratch/globalscratch/l1t_subsystem_deployment

export group_token=`cat $scratch_folder/to.k`
export id=`cat $scratch_folder/map_name_id.txt | grep $subsystem | awk '{print $2}'`
export job=`cat $scratch_folder/map_name_id.txt | grep $subsystem | awk '{print $3}'`
export dropbox=`cat $scratch_folder/map_name_id.txt | grep $subsystem | awk '{print $4}'`

echo ""
echo "Current version of subsystem libraries installed on this node:"
#rpm -q --qf '%-50{NAME} %{VERSION}\n' $(rpm -qa | grep -i $subsystem | sort)
sleep 2

if [ -d "$scratch_folder/$subsystem" ]; then rm -Rf $scratch_folder/$subsystem; fi
mkdir $scratch_folder/$subsystem/

echo "downloading artifacts..."
curl --output $scratch_folder/$subsystem/artifacts.zip --header "PRIVATE-TOKEN: $group_token" "https://gitlab.cern.ch/api/v4/projects/$id/jobs/artifacts/$tag/download?job=$job"

curl --output $scratch_folder/$subsystem/artifacts_driver.zip --header "PRIVATE-TOKEN: $group_token" "https://gitlab.cern.ch/api/v4/projects/$id/jobs/artifacts/$tag/download?job=driver_rpms_builder"

case $1 in
    twinmux|cppf)  # specific jobs
	echo "getting omtf AND BOARD rpms"
	curl --output $scratch_folder/$subsystem/artifacts_driver_specific.zip --header "PRIVATE-TOKEN: $group_token" "https://gitlab.cern.ch/api/v4/projects/$id/jobs/artifacts/$tag/download?job=omtf_rpms_builder"
	curl --output $scratch_folder/$subsystem/artifacts_driver_specific2.zip --header "PRIVATE-TOKEN: $group_token" "https://gitlab.cern.ch/api/v4/projects/$id/jobs/artifacts/$tag/download?job=board_rpms_builder"
        ;;
    *)
esac

export size_artifacts=`du -sh $scratch_folder/$subsystem/artifacts.zip | awk '{print $1}'`
export size_artifacts_driver=`du -sh $scratch_folder/$subsystem/artifacts_driver.zip | awk '{print $1}'`
export size_artifacts_driver_specific=`du -sh $scratch_folder/$subsystem/artifacts_driver_specific.zip | awk '{print $1}'`
export size_artifacts_driver_specific2=`du -sh $scratch_folder/$subsystem/artifacts_driver_specific2.zip | awk '{print $1}'`

echo "size artifacts.zip = "$size_artifacts
echo "size artifacts_driver.zip = "$size_artifacts_driver
echo "size artifacts_driver_specific.zip = "$size_artifacts_driver_specific
echo "size artifacts_driver_specific2.zip = "$size_artifacts_driver_specific2

echo "  |"
echo "  |"
echo "  V"

if [ "$size_artifacts" == "0" ] ; then
    echo "artifact corresponding to subsystem:$subsystem tag/branch:$tag was not found. Please check the arguments are corrects and that corresponding CI/CD gitlab-pipelines worked"
    exit 1
else
    unzip -q $scratch_folder/$subsystem/artifacts.zip -d $scratch_folder/$subsystem/
    ls $scratch_folder/$subsystem/ci_rpms/
fi

if [ "$size_artifacts_driver" == "0" ] ; then
    echo "artifact_driver corresponding to subsystem:$subsystem tag/branch:$tag was not found. Please check the arguments are corrects and that corresponding CI/CD gitlab-pipelines worked"
else
    unzip -q $scratch_folder/$subsystem/artifacts_driver.zip -d $scratch_folder/$subsystem/
    ls $scratch_folder/$subsystem/ci_dependencies/
fi

if [ -f "$scratch_folder/$subsystem/artifacts_driver_specific.zip" ] ; then
    unzip -q $scratch_folder/$subsystem/artifacts_driver_specific.zip -d $scratch_folder/$subsystem/
    ls $scratch_folder/$subsystem/ci_omtf_dependencies/
else
    echo "artifact_driver_specific corresponding to subsystem:$subsystem tag/branch:$tag was not found. Please check the arguments are corrects and that corresponding CI/CD gitlab-pipelines worked"
fi

if [ -f "$scratch_folder/$subsystem/artifacts_driver_specific2.zip" ] ; then
    unzip -q $scratch_folder/$subsystem/artifacts_driver_specific2.zip -d $scratch_folder/$subsystem/
    ls $scratch_folder/$subsystem/ci_board_dependencies/
else
    echo "artifact_driver_specific corresponding to subsystem:$subsystem tag/branch:$tag was not found. Please check the arguments are corrects and that corresponding CI/CD gitlab-pipelines worked"
fi


chmod -R a+w $scratch_folder/$subsystem/



read -p "Please confirm: Would you like to deploy this package(s) via yum/dropbox? (y/n) " -n 1 -r
echo
if [[ $REPLY =~  ^[Yy]$ ]]
then
    echo "stopping trigger.target"
    sudo systemctl stop trigger.target
    #all subsystem contacts should be able to exectue drobox. TBC
    if [[ $dropbox -eq 1 ]]
    then
	#for the moment only calol1 has a dropbox, a central request for each subsystem dropbox will be send soon.
	echo "$subsystem installs via dropbox" 
	ssh cmsdropbox 'sudo dropbox2 -s l1ts-$subsystem -z cms -u $scratch_folder/$subsystem/ci_rpms/'    
	ssh cmsdropbox 'sudo dropbox2 -s l1ts-$subsystem -z cms -u $scratch_folder/$subsystem/ci_dependencies/'    
    else
	echo "$subsystem installs via yum"
	#sudo yum remove $(rpm -q --qf '%{NAME} ' $(rpm -qa | grep -i $subsystem | sort))
	sudo yum --disablerepo=* localinstall $scratch_folder/$subsystem/ci_omtf_dependencies/*.rpm
	sudo yum --disablerepo=* localinstall $scratch_folder/$subsystem/ci_board_dependencies/*.rpm
	sudo yum --disablerepo=* localinstall $scratch_folder/$subsystem/ci_dependencies/*.rpm 
	sudo yum --disablerepo=* localinstall $scratch_folder/$subsystem/ci_rpms/*.rpm
    fi
    echo "Please check that the version of the subsystem libraries installed on your node are the expected ones:"
    rpm -q --qf '%-50{NAME} %{VERSION}\n' $(rpm -qa | grep -i $subsystem | sort)
    echo "starting trigger.target"
    sudo systemctl start trigger.target
    sudo systemctl status trigger.target
else
    echo "Subsystem install not done."
fi



