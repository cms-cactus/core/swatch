
#ifndef __SWATCH_ACTION_PROPERTYSNAPSHOT_HPP__
#define __SWATCH_ACTION_PROPERTYSNAPSHOT_HPP__


#include <string>
#include <typeinfo>

#include "boost/any.hpp"

#include "swatch/core/TimePoint.hpp"
#include "swatch/core/exception.hpp"


namespace swatch {
namespace action {


class PropertySnapshot {
public:
  template <typename T>
  struct Type {
  };

  template <typename T>
  PropertySnapshot(const std::string& aName,
                   const std::string& aGroup,
                   const std::string& aDescription,
                   const T& aValue,
                   const core::TimePoint&);

  template <typename T>
  PropertySnapshot(const std::string& aName,
                   const std::string& aGroup,
                   const std::string& aDescription,
                   Type<T> aType,
                   const core::TimePoint&);

  ~PropertySnapshot();

  const std::string& getName() const;

  const std::string& getGroup() const;

  const std::string& getDescription() const;

  //! Returns whether or not value is known
  bool isValueKnown() const;

  /*!
   * @brief Returns the property's value; throws if the value is not known, or if the incorrect type is given
   *
   * @throw PropertyValueNotKnown If value is not known
   * @throw PropertyValueFailedCast If value is known, but incorrect type given
   */
  template <typename DataType>
  const DataType& getValue() const;

  const std::type_info& getType() const;

  //! Returns time at which metric's value was last updated
  const core::TimePoint& getUpdateTimestamp() const;

private:
  std::string mName, mGroup, mDescription;
  boost::any mValue;
  const std::type_info* mType;
  core::TimePoint mUpdateTime;
};


SWATCH_DEFINE_EXCEPTION(PropertyValueNotKnown)
SWATCH_DEFINE_EXCEPTION(PropertyValueFailedCast)


} // namespace action
} // namespace swatch

#include "swatch/action/PropertySnapshot.hxx"

#endif /* __SWATCH_ACTION_PROPERTYSNAPSHOT_HPP__ */
