
#ifndef __SWATCH_ACTION_PROPERTY_HPP__
#define __SWATCH_ACTION_PROPERTY_HPP__


#include <mutex>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include "boost/optional.hpp"

#include "swatch/action/AbstractProperty.hpp"
#include "swatch/core/TimePoint.hpp"
#include "swatch/core/exception.hpp"
#include "swatch/core/format.hpp"


namespace swatch {
namespace action {

class PropertyHolder;
class PropertySnapshot;


namespace detail {

template <typename DataType, class Enable = void>
class _Property : public AbstractProperty {
protected:
  _Property(const std::string& aName);
  _Property(const std::string& aName, const std::string& aGroup);
  ~_Property();

public:
  //! Returns value, update timestamp, description etc, within a swatch::core::PropertySnapshot object. (THREAD SAFE)
  PropertySnapshot getSnapshot() const;

  //! Returns time at which property's value was last updated. (THREAD SAFE)
  core::TimePoint getUpdateTime() const;

private:
  //! Set the value of the property
  void setValue(const DataType&);

  //! Set the value of the property to being unknown
  void setValueUnknown();

  //! Mutex used to stop corruption of mValue / mUpdateTime
  mutable std::mutex mMutex;

  //! Latest retrieved value of property; set to NULL if value is unknown.
  std::shared_ptr<const DataType> mValue;

  core::TimePoint mUpdateTime;

  friend class action::PropertyHolder;
};

}

////////////////////////////////
//   GENERIC CLASS TEMPLATE   //
////////////////////////////////


template <typename DataType, class Enable = void>
class Property : public detail::_Property<DataType> {
public:
  Property(const std::string& aName);
  Property(const std::string& aName, const std::string& aGroup);
  ~Property();
};


/////////////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Floating point types   //
/////////////////////////////////////////////////////////////


template <typename DataType>
class Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type> : public detail::_Property<DataType> {
public:
  Property(const std::string& aName);
  Property(const std::string& aName, const std::string& aGroup);
  ~Property();

  const std::string& getUnit() const;

  core::format::FloatingPointNotation getNotation() const;

  boost::optional<size_t> getPrecision() const;

  //! Sets unit if not already set, otherwise throws
  void setUnit(const std::string&);

  //! Sets precision if not already set, otherwise throws
  void setFormat(core::format::FloatingPointNotation);

  //! Sets precision and notation if not already set, otherwise throws
  void setFormat(core::format::FloatingPointNotation, const size_t);

private:
  std::string mUnit;

  boost::optional<std::pair<core::format::FloatingPointNotation, boost::optional<size_t>>> mFormat;

  static const std::pair<core::format::FloatingPointNotation, boost::optional<size_t>> kDefaultFormat;
};


///////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Integral types   //
///////////////////////////////////////////////////////


template <typename DataType>
class Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type> : public detail::_Property<DataType> {
public:
  Property(const std::string& aName);
  Property(const std::string& aName, const std::string& aGroup);
  ~Property();

  const std::string& getUnit() const;

  core::format::Base getBase() const;

  boost::optional<size_t> getWidth() const;

  char getFillChar() const;

  //! Sets unit if not already set, otherwise throws
  void setUnit(const std::string&);

  //! Sets base if not already set, otherwise throws
  void setFormat(core::format::Base);

  //! Sets base and width if not already set, otherwise throws
  void setFormat(core::format::Base, const size_t, const char);

private:
  std::string mUnit;

  boost::optional<std::tuple<core::format::Base, boost::optional<size_t>, char>> mFormat;

  static const std::tuple<core::format::Base, boost::optional<size_t>, const char> kDefaultFormat;
};


SWATCH_DEFINE_EXCEPTION(PropertyValueUnknown)


} // namespace action
} // namespace swatch


#endif /* __SWATCH_ACTION_PROPERTY_HPP__ */
