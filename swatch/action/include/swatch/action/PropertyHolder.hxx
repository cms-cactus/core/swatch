
#ifndef __SWATCH_ACTION_PROPERTYHOLDER_HXX__
#define __SWATCH_ACTION_PROPERTYHOLDER_HXX__


// IWYU pragma: private, include "swatch/core/PropertyHolder.hpp"

#include "swatch/action/Property.hpp"
#include "swatch/core/Object.hpp"
#include "swatch/core/utilities.hpp"


namespace swatch {
namespace action {


template <typename DataType>
Property<DataType>& PropertyHolder::getProperty(const std::string& aName)
{
  if (Property<DataType>* lProperty = dynamic_cast<Property<DataType>*>(&getProperty(aName)))
    return *lProperty;
  else
    SWATCH_THROW(core::ObjectFailedCast("Could not cast property '" + aName + "' to type " + core::demangleName(typeid(DataType).name()) + " (real type is " + core::demangleName(typeid(getProperty(aName)).name()) + ")"));
}


template <typename DataType>
const Property<DataType>& PropertyHolder::getProperty(const std::string& aName) const
{
  if (Property<DataType>* lProperty = dynamic_cast<Property<DataType>*>(&getProperty(aName)))
    return *lProperty;
  else
    SWATCH_THROW(core::ObjectFailedCast("Could not cast property '" + aName + "' to type " + core::demangleName(typeid(DataType).name()) + " (real type is " + core::demangleName(typeid(getProperty(aName)).name()) + ")"));
}


template <typename DataType>
Property<DataType>& PropertyHolder::registerProperty(const std::string& aName)
{
  if (mProperties.count(aName) > 0)
    SWATCH_THROW(PropertyAlreadyExists("Property with name '" + aName + "' already exists"));

  Property<DataType>* lProperty = new Property<DataType>(aName);
  mProperties[aName] = std::shared_ptr<AbstractProperty>(lProperty);

  return *lProperty;
}


template <typename DataType>
Property<DataType>& PropertyHolder::registerProperty(const std::string& aName, const std::string& aGroup)
{
  if (mProperties.count(aName) > 0)
    SWATCH_THROW(PropertyAlreadyExists("Property with name '" + aName + "' already exists"));

  if (mPropertyGroups.count(aGroup) == 0)
    SWATCH_THROW(PropertyGroupNotFound("Property group '" + aGroup + "' (for property '" + aName + "') has not been added yet"));

  Property<DataType>* lProperty = new Property<DataType>(aName, aGroup);
  mProperties[aName] = std::shared_ptr<AbstractProperty>(lProperty);

  return *lProperty;
}


template <typename DataType>
void PropertyHolder::set(Property<DataType>& aProperty, const DataType& aValue)
{
  aProperty.setValue(aValue);
}


template <typename DataType>
void PropertyHolder::setProperty(const std::string& aName, const DataType& aValue)
{
  getProperty<DataType>(aName).setValue(aValue);
}


} // namespace action
} // namespace swatch

#endif /* __SWATCH_ACTION_PROPERTYHOLDER_HXX__ */
