
#ifndef __SWATCH_ACTION_OBJECTFUNCTIONOID_HXX__
#define __SWATCH_ACTION_OBJECTFUNCTIONOID_HXX__

// IWYU pragma: private, include "swatch/action/ObjectFunctionoid.hpp"


#include <type_traits>


namespace swatch {
namespace action {


template <typename T>
const T& ObjectFunctionoid::getActionable() const
{
  static_assert((std::is_base_of<swatch::action::ActionableObject, T>::value), "class T must be a descendant of swatch::action::ActionableObject");

  return dynamic_cast<const T&>(mActionable);
}


template <typename T>
T& ObjectFunctionoid::getActionable()
{
  static_assert((std::is_base_of<swatch::action::ActionableObject, T>::value), "class T must be a descendant of swatch::action::ActionableObject");

  return dynamic_cast<T&>(mActionable);
}


}
}

#endif /* __SWATCH_ACTION_OBJECTFUNCTIONOID_HXX__ */
