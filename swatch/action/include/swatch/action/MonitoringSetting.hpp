/*
 * MetricSettings.hpp
 *
 *  Created on: 26 Oct 2015
 *      Author: kreczko
 */

#ifndef __SWATCH_ACTION_MONITORINGSETTING_HPP__
#define __SWATCH_ACTION_MONITORINGSETTING_HPP__


#include <map>
#include <memory>
#include <vector>

#include "swatch/core/monitoring/Status.hpp"


namespace swatch {
namespace action {

class MonitoringSetting {
public:
  MonitoringSetting(const std::string aId, const swatch::core::monitoring::Status aMonStatus);

  swatch::core::monitoring::Status getStatus() const;
  const std::string& getId() const;

private:
  std::string mId;
  swatch::core::monitoring::Status mMonitoringStatus;
};

typedef std::vector<MonitoringSetting> MonitoringSettings_t;
typedef std::shared_ptr<const MonitoringSetting> MonitoringSettingPtr_t;

} // namespace action
} // namespace swatch


#endif /* __SWATCH_ACTION_MONITORINGSETTING_HPP__ */
