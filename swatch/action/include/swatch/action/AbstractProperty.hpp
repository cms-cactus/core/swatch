#ifndef __SWATCH_ACTION_ABSTRACTPROPERTY_HPP__
#define __SWATCH_ACTION_ABSTRACTPROPERTY_HPP__


#include <string>
#include <unordered_map>
#include <unordered_set>

#include "swatch/core/TimePoint.hpp"
#include "swatch/core/exception.hpp"


namespace swatch {
namespace action {


class PropertyHolder;
class PropertySnapshot;


class AbstractProperty {
public:
  AbstractProperty(const std::string& aName);
  AbstractProperty(const std::string& aName, const std::string& aGroup);
  virtual ~AbstractProperty();

  const std::string& getName() const;

  const std::string& getGroup() const;

  const std::string& getDescription() const;

  //! Sets description if not set already, throws otherwise
  void setDescription(const std::string&);

  //! Returns value, update timestamp, description etc, within a swatch::action::PropertySnapshot object. (THREAD SAFE)
  virtual PropertySnapshot getSnapshot() const = 0;

  //! Returns time at which property's value was last updated. (THREAD SAFE)
  virtual core::TimePoint getUpdateTime() const = 0;

  virtual void setValueUnknown() = 0;

private:
  const std::string mName;
  const std::string mGroup;
  std::string mDescription;

  friend class PropertyHolder;
};


SWATCH_DEFINE_EXCEPTION(PropertyFieldAlreadySet)


} // namespace action
} // namespace swatch


#endif /* __SWATCH_ACTION_ABSTRACTPROPERTY_HPP__ */
