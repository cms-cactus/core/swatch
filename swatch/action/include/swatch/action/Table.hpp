
#ifndef __SWATCH_ACTION_TABLE_HPP__
#define __SWATCH_ACTION_TABLE_HPP__


#include <string>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "boost/any.hpp"

#include "swatch/core/exception.hpp"
#include "swatch/core/utilities.hpp"


namespace swatch {
namespace action {

class Table {
public:
  struct Column {
    Column(const std::string&, const std::type_info&);

    std::string name;
    const std::type_info* type;
  };

  Table(const std::vector<Column>&);

  ~Table();

  void addRow(const std::vector<boost::any>&);

  const std::vector<Column>& getColumns() const;

  size_t getNumberOfRows() const;

  template <typename T>
  const T& at(const size_t aColumnIndex, const size_t aRowIndex) const;

  template <typename T>
  const T& at(const std::string& aColumnName, const size_t aRowIndex) const;

private:
  const std::vector<Column> mColumns;
  std::unordered_map<std::string, size_t> mColumnMap;

  // Outer vector for rows, inner vector for columns
  std::vector<std::vector<boost::any>> mData;
};


SWATCH_DEFINE_EXCEPTION(ColumnDoesNotExist);
SWATCH_DEFINE_EXCEPTION(DuplicateColumnName);
SWATCH_DEFINE_EXCEPTION(TableTypeMismatch);
SWATCH_DEFINE_EXCEPTION(RowDoesNotExist);


template <typename T>
const T& Table::at(const size_t aColumnIndex, const size_t aRowIndex) const
{
  if (aColumnIndex >= mColumns.size())
    SWATCH_THROW(ColumnDoesNotExist("Column of index " + std::to_string(aColumnIndex) + " does not exist (table has " + std::to_string(mColumns.size()) + " columns)"));

  if (aRowIndex >= mData.size())
    SWATCH_THROW(RowDoesNotExist("Row of index " + std::to_string(aRowIndex) + " does not exist (table has " + std::to_string(mData.size()) + " rows)"));

  if (typeid(T) != *mColumns.at(aColumnIndex).type)
    SWATCH_THROW(TableTypeMismatch("Incorrect type (" + core::demangleName(typeid(T).name()) + ") given when accessing data for column " + std::to_string(aColumnIndex) + " ('" + mColumns.at(aColumnIndex).name + "', type " + core::demangleName(mColumns.at(aColumnIndex).type->name()) + ")"));

  return *boost::any_cast<T>(&mData.at(aRowIndex).at(aColumnIndex));
}


template <typename T>
const T& Table::at(const std::string& aColumnName, const size_t aRowIndex) const
{
  if (mColumnMap.count(aColumnName) == 0)
    SWATCH_THROW(ColumnDoesNotExist("Column of name '" + aColumnName + "' does not exist"));

  if (aRowIndex >= mData.size())
    SWATCH_THROW(RowDoesNotExist("Row of index " + std::to_string(aRowIndex) + " does not exist (table has " + std::to_string(mData.size()) + " rows)"));

  const size_t lColumnIndex = mColumnMap.at(aColumnName);
  if (typeid(T) != *mColumns.at(lColumnIndex).type)
    SWATCH_THROW(TableTypeMismatch("Incorrect type (" + core::demangleName(typeid(T).name()) + ") given when accessing data for column '" + aColumnName + "' (type " + core::demangleName(mColumns.at(lColumnIndex).type->name()) + ")"));

  return *boost::any_cast<T>(&mData.at(aRowIndex).at(lColumnIndex));
}


} // namespace action
} // namespace swatch

#endif /* __SWATCH_ACTION_TABLE_HPP__ */