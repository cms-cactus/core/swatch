
#ifndef __SWATCH_ACTION_PROPERTYHOLDER_HPP__
#define __SWATCH_ACTION_PROPERTYHOLDER_HPP__


#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include <log4cplus/logger.h>

#include "swatch/action/Property.hpp"
#include "swatch/core/MonitorableObject.hpp"
#include "swatch/core/TimePoint.hpp"
#include "swatch/core/exception.hpp"


namespace swatch {

namespace core {
struct ErrorInfo;
}

namespace action {

class AbstractProperty;


class PropertyHolder : public core::MonitorableObject {
public:
  typedef std::unordered_map<std::string, std::shared_ptr<AbstractProperty>> PropertyMap_t;

  PropertyHolder(const std::string& aId);

  PropertyHolder(const std::string& aId, const std::string& aAlias);

  ~PropertyHolder();

  bool isPresent() const;

  AbstractProperty& getProperty(const std::string&);

  template <typename T>
  Property<T>& getProperty(const std::string&);

  const AbstractProperty& getProperty(const std::string&) const;

  template <typename T>
  const Property<T>& getProperty(const std::string&) const;

  const PropertyMap_t& getProperties() const;

  const std::unordered_set<std::string>& getPropertyGroups() const;

  // TODO: Add updatePresence overload with Guard argument
  void updatePresence();

  // TODO: Add updateProperties overload with Guard argument
  void updateProperties();

  //! Returns time taken by retrievePropertyValues (inside updateProperties), in seconds
  core::Duration_t getPropertyUpdateDuration() const;

  //! Returns information about any exception caught when updating
  std::shared_ptr<const core::ErrorInfo> getPropertyUpdateErrorInfo() const;

protected:
  void addPropertyGroup(const std::string&);

  template <typename DataType>
  Property<DataType>& registerProperty(const std::string& aName);

  template <typename DataType>
  Property<DataType>& registerProperty(const std::string& aName, const std::string& aGroup);

  template <typename DataType>
  void set(Property<DataType>&, const DataType&);

  template <typename DataType>
  void setProperty(const std::string&, const DataType&);

  virtual void retrievePropertyValues() = 0;

  // true means present, false means absent
  virtual bool checkPresence() const = 0;

private:
  PropertyMap_t mProperties;
  std::unordered_set<std::string> mPropertyGroups;
  bool mIsPresent;
  log4cplus::Logger mLogger;

  //! Used to ensure thread-safe access to mPropertyUpdateDuration & mPropertyUpdateError
  mutable std::mutex mPropertyUpdateMutex;

  core::Duration_t mPropertyUpdateDuration;

  //! Stores details in case error occurs when updating the property values
  std::shared_ptr<core::ErrorInfo> mPropertyUpdateError;
};


SWATCH_DEFINE_EXCEPTION(PropertyAlreadyExists)
SWATCH_DEFINE_EXCEPTION(PropertyGroupNotFound)
SWATCH_DEFINE_EXCEPTION(PropertyNotFound)


} // namespace action
} // namespace swatch

#include "swatch/action/PropertyHolder.hxx"

#endif /* __SWATCH_ACTION_PROPERTY_HPP__ */
