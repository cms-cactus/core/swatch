/**
 * @file    GateKeeperView.hxx
 * @author  Luke Kreczko
 * @date    February 2016
 */

#ifndef __SWATCH_ACTION_GATEKEEPERVIEW_HXX__
#define __SWATCH_ACTION_GATEKEEPERVIEW_HXX__


// IWYU pragma: private, include "swatch/action/GateKeeperView.hpp"

#include <string>

#include "swatch/action/GateKeeperView.hpp"


namespace swatch {
namespace action {

template <typename T>
const std::vector<std::string> GateKeeperView::extractMapKeys(const std::unordered_map<std::string, T>& aMap) const
{
  std::vector<std::string> lKeys;
  for (const std::pair<std::string, T> lIt : aMap) {
    lKeys.push_back(lIt.first);
  }

  return lKeys;
}

} // namespace action
} // namespace swatch

#endif /* __SWATCH_ACTION_GATEKEEPERVIEW_HPP__ */
