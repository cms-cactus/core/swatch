
#ifndef __SWATCH_ACTION_THREADPOOL_HXX__
#define __SWATCH_ACTION_THREADPOOL_HXX__

// IWYU pragma: private, include "swatch/action/ThreadPool.hpp"

#include <functional>
#include <future>

#include "swatch/core/ParameterSet.hpp"


namespace swatch {
namespace action {

template <class OBJECT, class ResourceGuardType>
void ThreadPool::addTask(OBJECT* aCmd,
                         std::function<void(OBJECT*, std::shared_ptr<ResourceGuardType>, const core::ParameterSet&)> aFunction,
                         const std::shared_ptr<ResourceGuardType>& aResourceGuard,
                         const core::ParameterSet& aParamSet)
{
  // create packed_task
  std::packaged_task<void()> lTask(std::bind(aFunction, aCmd, aResourceGuard, std::ref(aParamSet)));
  {
    // lock mutex
    std::lock_guard<std::mutex> lGuard(mQueueMutex);
    if (mStop) {
      SWATCH_THROW(OperationOnStoppedThreadPool("ThreadPool is stopped, cannot schedule tasks."));
    }
    mTasks.push_back(std::move(lTask));
  }
  mCondition.notify_one();
}

template <class OBJECT, class ResourceGuardType>
void ThreadPool::addTask(OBJECT* aCmd, std::function<void(OBJECT*, std::shared_ptr<ResourceGuardType>)> aFunction, const std::shared_ptr<ResourceGuardType>& aResourceGuard)
{
  // create packed_task
  std::packaged_task<void()> lTask(std::bind(aFunction, aCmd, aResourceGuard));
  {
    // lock mutex
    std::lock_guard<std::mutex> lGuard(mQueueMutex);
    if (mStop) {
      SWATCH_THROW(OperationOnStoppedThreadPool("ThreadPool is stopped, cannot schedule tasks."));
    }
    mTasks.push_back(std::move(lTask));
  }
  mCondition.notify_one();
}


} // namespace action
} // namespace swatch

#endif /* __SWATCH_ACTION_THREADPOOL_HXX__ */
