
#ifndef __SWATCH_ACTION_PROPERTYSNAPSHOT_HXX__
#define __SWATCH_ACTION_PROPERTYSNAPSHOT_HXX__


#include "swatch/core/utilities.hpp"

// IWYU pragma: private, include "swatch/core/PropertySnapshot.hpp"


namespace swatch {
namespace action {



template <typename DataType>
PropertySnapshot::PropertySnapshot(const std::string& aName,
                                   const std::string& aGroup,
                                   const std::string& aDescription,
                                   const DataType& aValue,
                                   const core::TimePoint& aUpdateTime) :
  mName(aName),
  mGroup(aGroup),
  mDescription(aDescription),
  mValue(aValue),
  mType(&typeid(DataType)),
  mUpdateTime(aUpdateTime)
{
}


template <typename DataType>
PropertySnapshot::PropertySnapshot(const std::string& aName,
                                   const std::string& aGroup,
                                   const std::string& aDescription,
                                   Type<DataType>,
                                   const core::TimePoint& aUpdateTime) :
  mName(aName),
  mGroup(aGroup),
  mDescription(aDescription),
  mValue(),
  mType(&typeid(DataType)),
  mUpdateTime(aUpdateTime)
{
}


template <typename DataType>
const DataType& PropertySnapshot::getValue() const
{
  if (mValue.empty())
    SWATCH_THROW(PropertyValueNotKnown("Value of property '" + getName() + "' is not known"));
  else {
    try {
      return *boost::any_cast<DataType>(&mValue);
    }
    catch (const boost::bad_any_cast& lExc) {
      SWATCH_THROW(PropertyValueFailedCast("Cannot cast value of property '" + getName() + "' from type '" + swatch::core::demangleName(mValue.type().name()) + "' to type '" + swatch::core::demangleName(typeid(DataType).name()) + "'"));
    }
  }
}


} // namespace action
} // namespace swatch


#endif /* __SWATCH_ACTION_PROPERTYSNAPSHOT_HXX__ */