
#ifndef __SWATCH_ACTION_ACTIONABLEOBJECT_HXX__
#define __SWATCH_ACTION_ACTIONABLEOBJECT_HXX__

// IWYU pragma: private, include "swatch/action/ActionableObject.hpp"


#include <type_traits>


namespace swatch {
namespace action {


template <typename T>
T& ActionableObject::registerCommand(const std::string& aId)
{
  static_assert((std::is_base_of<swatch::action::Command, T>::value), "class T must be a descendant of swatch::action::Command");
  T* lObj(new T(aId, *this));
  registerCommand(aId, lObj);
  return *lObj;
}


template <typename T>
T& ActionableObject::registerCommand(const std::string& aId, const std::string& aAlias)
{
  static_assert((std::is_base_of<swatch::action::Command, T>::value), "class T must be a descendant of swatch::action::Command");
  T* lObj(new T(aId, aAlias, *this));
  registerCommand(aId, lObj);
  return *lObj;
}


}
}

#endif /* __SWATCH_ACTION_ACTIONABLEOBJECT_HXX__ */
