#include "swatch/action/test/DummyCommand.hpp"


#include <chrono>
#include <thread>
#include <typeinfo>

#include "swatch/action/test/DummyActionableObject.hpp"
#include "swatch/test/types.hpp"


using namespace swatch::test;

namespace swatch {
namespace action {
namespace test {

DummyConstraint::DummyConstraint(const std::map<std::string, std::type_index>& aParameters)
{
  for (const auto& lItem : aParameters) {
    if (lItem.second == std::type_index(typeid(Int_t)))
      require<Int_t>(lItem.first);
    else if (lItem.second == std::type_index(typeid(UInt_t)))
      require<UInt_t>(lItem.first);
    else if (lItem.second == std::type_index(typeid(Float_t)))
      require<Float_t>(lItem.first);
    else if (lItem.second == std::type_index(typeid(String_t)))
      require<String_t>(lItem.first);
    else
      assert(false);
  }
}

const std::string DummyConstraint::sDescription = "Dummy constraint for unit tests";
core::Match DummyConstraint::sResult = true;


const std::string DummyCommand::kDescription("Dummy description of the dummy command");

const Int_t DummyCommand::kDefaultResult(-1);

const std::string DummyCommand::kParamX("x");
const std::string DummyCommand::kParamToDo("todo");

const std::string DummyCommand::kParamDescriptionX("Dummy parameter: x");
const std::string DummyCommand::kParamDescriptionToDo("Another dummy parameter - things to do");

const std::string DummyCommand::kFinalMsgUseResource("Dummy command successfully used resource");
const std::string DummyCommand::kFinalMsgSleep("Dummy command finished sleeping");


DummyCommand::DummyCommand(const std::string& aId, ActionableObject& aActionable) :
  Command(aId, "", DummyCommand::kDescription, aActionable, kDefaultResult)
{
  registerParameter({ kParamX, kParamDescriptionX.c_str() }, Int_t(15), DummyRule<Int_t>());
  registerParameter({ kParamToDo, kParamDescriptionToDo.c_str() }, String_t(""), DummyRule<String_t>());

  addConstraint("constraintA", DummyConstraint({ { DummyCommand::kParamX, std::type_index(typeid(Int_t)) } }));
  addConstraint("constraintB", DummyConstraint({ { DummyCommand::kParamX, std::type_index(typeid(Int_t)) }, { DummyCommand::kParamToDo, std::type_index(typeid(String_t)) } }));
}


DummyCommand::~DummyCommand()
{
}


void DummyCommand::setExecutionDetails(const core::ParameterSet& aInfoSet)
{
  mNextExecutionDetails = aInfoSet;
}


Command::State DummyCommand::code(const core::ParameterSet& aParams)
{
  // Filling the detailed info ParameterSet: Fill up each entry with a dummy value, then reset to the requested value
  // (set each entry twice so unit tests will check that initial value is used )
  std::set<std::string> lItemNames = mNextExecutionDetails.keys();
  for (auto lIt = lItemNames.begin(); lIt != lItemNames.end(); lIt++) {
    this->addExecutionDetails(*lIt, Int_t(-99));
    const std::type_info& lDataType = mNextExecutionDetails.getType(*lIt);
    if (lDataType == typeid(Bool_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Bool_t>(*lIt));
    else if (lDataType == typeid(UInt_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<UInt_t>(*lIt));
    else if (lDataType == typeid(Int_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Int_t>(*lIt));
    else if (lDataType == typeid(String_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<String_t>(*lIt));
    else
      assert(false);
  }

  // Now actually use the resource ...
  DummyActionableObject& res = getActionable<DummyActionableObject>();

  String_t todo = aParams.get<String_t>("todo");
  setResult(aParams.get<Int_t>("x"));

  if (todo == "useResource") {

    res.setNumber(54);
    setProgress(0.1);

    setProgress(0.99, kFinalMsgUseResource);
    return State::kDone;
  }
  if (todo == "sleep") {
    setProgress(0.01, "Dummy command just started");
    unsigned int milliseconds(aParams.get<Int_t>("milliseconds"));

    for (unsigned int i = 0; i < milliseconds; ++i) {
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
      setProgress(0.01 + 0.99 * float(i) / milliseconds, "Dummy command progressed");
    }

    setStatusMsg(kFinalMsgSleep);
    return State::kDone;
  }
  else {
    return State::kDone;
  }
}


//-----------------------//
/*  DummyWarningCommand  */

const Int_t DummyWarningCommand::kDefaultResult(-1);
const std::string DummyWarningCommand::kFinalMsg("Dummy command did something, but ended up in warning");

DummyWarningCommand::DummyWarningCommand(const std::string& aId, ActionableObject& aActionable) :
  Command(aId, aActionable, kDefaultResult)
{
}


DummyWarningCommand::~DummyWarningCommand()
{
}


void DummyWarningCommand::setExecutionDetails(const core::ParameterSet& aInfoSet)
{
  mNextExecutionDetails = aInfoSet;
}


Command::State DummyWarningCommand::code(const core::ParameterSet& aParams)
{
  // Filling the detailed info ParameterSet: Fill up each entry with a dummy value, then reset to the requested value
  // (set each entry twice so unit tests will check that initial value is used )
  std::set<std::string> lItemNames = mNextExecutionDetails.keys();
  for (auto lIt = lItemNames.begin(); lIt != lItemNames.end(); lIt++) {
    this->addExecutionDetails(*lIt, Int_t(-99));
    const std::type_info& lDataType = mNextExecutionDetails.getType(*lIt);
    if (lDataType == typeid(Bool_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Bool_t>(*lIt));
    else if (lDataType == typeid(UInt_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<UInt_t>(*lIt));
    else if (lDataType == typeid(Int_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Int_t>(*lIt));
    else if (lDataType == typeid(String_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<String_t>(*lIt));
    else
      assert(false);
  }

  // Now actually use the resource ...
  setProgress(0.5049, kFinalMsg);
  return State::kWarning;
}



//---------------------//
/*  DummyErrorCommand  */

const Int_t DummyErrorCommand::kDefaultResult(-1);
const std::string DummyErrorCommand::kFinalMsg("Dummy command did something, but ended up in error");
const float DummyErrorCommand::kFinalProgress(0.5049);

DummyErrorCommand::DummyErrorCommand(const std::string& aId, ActionableObject& aActionable) :
  Command(aId, aActionable, kDefaultResult)
{
}

DummyErrorCommand::~DummyErrorCommand()
{
}

void DummyErrorCommand::setExecutionDetails(const core::ParameterSet& aInfoSet)
{
  mNextExecutionDetails = aInfoSet;
}

Command::State DummyErrorCommand::code(const core::ParameterSet& aParams)
{
  // Filling the detailed info ParameterSet: Fill up each entry with a dummy value, then reset to the requested value
  // (set each entry twice so unit tests will check that initial value is used )
  std::set<std::string> lItemNames = mNextExecutionDetails.keys();
  for (auto lIt = lItemNames.begin(); lIt != lItemNames.end(); lIt++) {
    this->addExecutionDetails(*lIt, Int_t(-99));
    const std::type_info& lDataType = mNextExecutionDetails.getType(*lIt);
    if (lDataType == typeid(Bool_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Bool_t>(*lIt));
    else if (lDataType == typeid(UInt_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<UInt_t>(*lIt));
    else if (lDataType == typeid(Int_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Int_t>(*lIt));
    else if (lDataType == typeid(String_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<String_t>(*lIt));
    else
      assert(false);
  }

  // Now actually use the resource ...
  setProgress(kFinalProgress, kFinalMsg);
  return State::kError;
}


//---------------------//
/*  DummyThrowCommand  */

const Int_t DummyThrowCommand::kDefaultResult(-1);
const std::string DummyThrowCommand::kExceptionMsg("My test exception message");
const float DummyThrowCommand::kFinalProgress(0.4032);

DummyThrowCommand::DummyThrowCommand(const std::string& aId, ActionableObject& aActionable) :
  Command(aId, aActionable, kDefaultResult)
{
}

DummyThrowCommand::~DummyThrowCommand()
{
}

void DummyThrowCommand::setExecutionDetails(const core::ParameterSet& aInfoSet)
{
  mNextExecutionDetails = aInfoSet;
}

Command::State DummyThrowCommand::code(const core::ParameterSet& aParams)
{
  // Filling the detailed info ParameterSet: Fill up each entry with a dummy value, then reset to the requested value
  // (set each entry twice so unit tests will check that initial value is used )
  std::set<std::string> lItemNames = mNextExecutionDetails.keys();
  for (auto lIt = lItemNames.begin(); lIt != lItemNames.end(); lIt++) {
    this->addExecutionDetails(*lIt, Int_t(-99));
    const std::type_info& lDataType = mNextExecutionDetails.getType(*lIt);
    if (lDataType == typeid(Bool_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Bool_t>(*lIt));
    else if (lDataType == typeid(UInt_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<UInt_t>(*lIt));
    else if (lDataType == typeid(Int_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<Int_t>(*lIt));
    else if (lDataType == typeid(String_t))
      addExecutionDetails(*lIt, mNextExecutionDetails.get<String_t>(*lIt));
    else
      assert(false);
  }

  // Now actually use the resource ...
  setProgress(kFinalProgress);
  SWATCH_THROW(core::RuntimeError(kExceptionMsg));
}


} /* namespace test */
} /* namespace action */
} /* namespace swatch */
