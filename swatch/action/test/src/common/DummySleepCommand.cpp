
#include "swatch/action/test/DummySleepCommand.hpp"


#include <chrono>
#include <thread>

#include "swatch/action/test/DummyActionableObject.hpp"
#include "swatch/test/types.hpp"


using namespace swatch::test;
using namespace swatch;

namespace swatch {
namespace action {
namespace test {

DummySleepCommand::DummySleepCommand(const std::string& aId, ActionableObject& aActionable) :
  Command(aId, aActionable, Int_t(-33))
{
  registerParameter("n", UInt_t(50));
  registerParameter("millisecPerSleep", UInt_t(100));
}


DummySleepCommand::~DummySleepCommand()
{
}


Command::State DummySleepCommand::code(const core::ParameterSet& aParams)
{
  DummyActionableObject& res = getActionable<DummyActionableObject>();

  setStatusMsg("Dummy sleep command just started");

  unsigned int n(aParams.get<UInt_t>("n"));
  unsigned int millisecPerSleep(aParams.get<UInt_t>("millisecPerSleep"));

  for (unsigned int i = 0; i < n; i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(millisecPerSleep));

    std::ostringstream oss;
    oss << "Dummy sleep command for object '" << res.getPath() << "' progressed. " << i << " of " << n << " sleeps done; " << (n - i) * millisecPerSleep << " milli sec remain";
    setProgress(float(i) / n, oss.str());
  }

  setStatusMsg("Dummy sleep command for object '" + res.getPath() + "' completed");
  return State::kDone;
}

} /* namespace test */
} /* namespace action */
} /* namespace swatch */
