#include <boost/test/unit_test.hpp>

// SWATCH headers
#include "swatch/action/Table.hpp"


namespace swatch {
namespace action {
namespace test {


BOOST_AUTO_TEST_SUITE(TableTestSuite)

BOOST_AUTO_TEST_CASE(TestGetters)
{
  // Example table for storing build information
  Table lTable({ { "Repository name", typeid(std::string) }, { "Branch/Tag", typeid(std::string) }, { "SHA", typeid(std::string) }, { "Uncommitted changes?", typeid(bool) } });

  BOOST_CHECK_EQUAL(lTable.getColumns().size(), 4);
  BOOST_CHECK_EQUAL(lTable.getColumns().at(0).name, "Repository name");
  BOOST_CHECK_EQUAL(lTable.getColumns().at(0).type, &typeid(std::string));
  BOOST_CHECK_EQUAL(lTable.getColumns().at(1).name, "Branch/Tag");
  BOOST_CHECK_EQUAL(lTable.getColumns().at(1).type, &typeid(std::string));
  BOOST_CHECK_EQUAL(lTable.getColumns().at(2).name, "SHA");
  BOOST_CHECK_EQUAL(lTable.getColumns().at(2).type, &typeid(std::string));
  BOOST_CHECK_EQUAL(lTable.getColumns().at(3).name, "Uncommitted changes?");
  BOOST_CHECK_EQUAL(lTable.getColumns().at(3).type, &typeid(bool));

  BOOST_CHECK_EQUAL(lTable.getNumberOfRows(), 0);
  BOOST_CHECK_THROW(lTable.at<std::string>(4, 0), ColumnDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<std::string>("Bob", 0), ColumnDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<std::string>(0, 0), RowDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<std::string>("Repository name", 0), RowDoesNotExist);

  // Try to add row with data of incorrect types
  BOOST_CHECK_THROW(lTable.addRow({ { false }, { std::string("some-branch") }, { std::string("0x12345678") }, { false } }), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.addRow({ { std::string("my-repo") }, { std::string("some-branch") }, { std::string("0x12345678") }, { 42 } }), TableTypeMismatch);

  // Add first row. Check that data is correct
  lTable.addRow({ { std::string("my-repo") }, { std::string("some-branch") }, { std::string("0x12345678") }, { false } });
  BOOST_CHECK_EQUAL(lTable.getNumberOfRows(), 1);
  BOOST_CHECK_THROW(lTable.at<std::string>(0, 1), RowDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<std::string>("Repository name", 1), RowDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<bool>(0, 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<bool>("Repository name", 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>(3, 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>("Uncommitted changes?", 0), TableTypeMismatch);

  BOOST_CHECK_EQUAL(lTable.at<std::string>(0, 0), "my-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Repository name", 0), "my-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(1, 0), "some-branch");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Branch/Tag", 0), "some-branch");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(2, 0), "0x12345678");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("SHA", 0), "0x12345678");
  BOOST_CHECK_EQUAL(lTable.at<bool>(3, 0), false);
  BOOST_CHECK_EQUAL(lTable.at<bool>("Uncommitted changes?", 0), false);

  // Add second row. Check that data in both rows is correct.
  lTable.addRow({ { std::string("other-repo") }, { std::string("vX.Y.Z") }, { std::string("0x90abcdef") }, { true } });
  BOOST_CHECK_EQUAL(lTable.getNumberOfRows(), 2);
  BOOST_CHECK_THROW(lTable.at<std::string>(0, 2), RowDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<std::string>("Repository name", 2), RowDoesNotExist);
  BOOST_CHECK_THROW(lTable.at<bool>(0, 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<bool>(0, 1), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<bool>("Repository name", 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<bool>("Repository name", 1), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>(3, 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>(3, 1), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>("Uncommitted changes?", 0), TableTypeMismatch);
  BOOST_CHECK_THROW(lTable.at<std::string>("Uncommitted changes?", 1), TableTypeMismatch);

  BOOST_CHECK_EQUAL(lTable.at<std::string>(0, 0), "my-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Repository name", 0), "my-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(1, 0), "some-branch");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Branch/Tag", 0), "some-branch");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(2, 0), "0x12345678");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("SHA", 0), "0x12345678");
  BOOST_CHECK_EQUAL(lTable.at<bool>(3, 0), false);
  BOOST_CHECK_EQUAL(lTable.at<bool>("Uncommitted changes?", 0), false);

  BOOST_CHECK_EQUAL(lTable.at<std::string>(0, 1), "other-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Repository name", 1), "other-repo");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(1, 1), "vX.Y.Z");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("Branch/Tag", 1), "vX.Y.Z");
  BOOST_CHECK_EQUAL(lTable.at<std::string>(2, 1), "0x90abcdef");
  BOOST_CHECK_EQUAL(lTable.at<std::string>("SHA", 1), "0x90abcdef");
  BOOST_CHECK_EQUAL(lTable.at<bool>(3, 1), true);
  BOOST_CHECK_EQUAL(lTable.at<bool>("Uncommitted changes?", 1), true);
}

BOOST_AUTO_TEST_SUITE_END()


} /* namespace test */
} /* namespace action */
} /* namespace swatch */
