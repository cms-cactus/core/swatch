#include <boost/test/unit_test.hpp>

// SWATCH headers
#include "swatch/action/File.hpp"


namespace swatch {
namespace action {
namespace test {


BOOST_AUTO_TEST_SUITE(FileTestSuite)

BOOST_AUTO_TEST_CASE(TestGetters)
{
  File lFile("/path/to/someFile.tgz", "SomeType", "Format1");

  BOOST_CHECK_EQUAL(lFile.getPath(), "/path/to/someFile.tgz");
  BOOST_CHECK_EQUAL(lFile.getName(), "someFile.tgz");
  BOOST_CHECK_EQUAL(lFile.getContentType(), "SomeType");
  BOOST_CHECK_EQUAL(lFile.getContentFormat(), "Format1");

  File lFile2("/path/to/anotherFile.tgz", "A non-default name", "TypeB", "FormatY");

  BOOST_CHECK_EQUAL(lFile2.getPath(), "/path/to/anotherFile.tgz");
  BOOST_CHECK_EQUAL(lFile2.getName(), "A non-default name");
  BOOST_CHECK_EQUAL(lFile2.getContentType(), "TypeB");
  BOOST_CHECK_EQUAL(lFile2.getContentFormat(), "FormatY");
}

BOOST_AUTO_TEST_SUITE_END()


} /* namespace test */
} /* namespace action */
} /* namespace swatch */
