// Boost Unit Test includes
#define BOOST_NO_CXX11_NUMERIC_LIMITS 1
#include <boost/test/unit_test.hpp>

// boost headers
#include "boost/any.hpp"

// swatch headers
#include "swatch/action/GateKeeper.hpp"
#include "swatch/action/GateKeeperView.hpp"
#include "swatch/action/test/DummyGateKeeper.hpp"
#include "swatch/test/types.hpp"


using namespace swatch::test;

namespace swatch {
namespace action {
namespace test {

struct TestGateKeeperViewSetup {
  TestGateKeeperViewSetup() :
    gk()
  {
    typedef std::shared_ptr<const boost::any> ParamPtr_t;
    typedef GateKeeper::Parameters_t Parameters_t;
    typedef std::shared_ptr<MonitoringSetting> MonSettingPtr_t;
    typedef GateKeeper::MonitoringSettings_t MonSettings_t;
    typedef GateKeeper::Masks_t Masks_t;

    GateKeeper::ParametersContext_t lCommonParams(new Parameters_t {
        { "hello", ParamPtr_t(new boost::any(String_t("World"))) },
        { "answer", ParamPtr_t(new boost::any(Int_t(42))) } });
    gk.addContext("dummy_sys.common", lCommonParams);

    GateKeeper::ParametersContext_t lChildA1Params(new Parameters_t {
        { "hello", ParamPtr_t(new boost::any(String_t("World! (childA1)"))) } });
    gk.addContext("dummy_sys.childA1", lChildA1Params);

    GateKeeper::ParametersContext_t lChildTypeAParams(new GateKeeper::Parameters_t {
        { "sequence_1.command_1.parameter_1", ParamPtr_t(new boost::any(String_t("sequence"))) },
        { "command_1.parameter_1", ParamPtr_t(new boost::any(String_t("command"))) } });
    gk.addContext("dummy_sys.childTypeA", lChildTypeAParams);

    // monitoring status
    MonSettings_t lMetricSettings {
      { "criticalMetric", MonSettingPtr_t(new MonitoringSetting("criticalMetric", core::monitoring::kEnabled)) },
      { "weird_state.criticalMetric", MonSettingPtr_t(new MonitoringSetting("weird_state.criticalMetric", core::monitoring::kDisabled)) },
      { "nonCriticalMetric", MonSettingPtr_t(new MonitoringSetting("nonCriticalMetric", core::monitoring::kNonCritical)) }
    };
    gk.addSettingsContext("dummy_sys.common", lMetricSettings);

    MonSettings_t lA1Settings {
      { "criticalMetric", MonSettingPtr_t(new MonitoringSetting("criticalMetric", core::monitoring::kEnabled)) }
    };
    gk.addSettingsContext("dummy_sys.childA1", lA1Settings);

    // Masks
    gk.addMasksContext("dummy_sys.common", Masks_t { "componentA" });
    gk.addMasksContext("dummy_sys.childTypeA", Masks_t { "componentB" });
    gk.addMasksContext("dummy_sys.childA1", Masks_t { "componentC" });

    // Disable some IDs
    gk.addDisabledId("dummy_sys.childA1");
    gk.addDisabledId("dummy_sys.child42");
  }

  DummyGateKeeper gk;
};

BOOST_AUTO_TEST_SUITE(TestGateKeeperView)

BOOST_FIXTURE_TEST_CASE(TestDisabledFlagsView, TestGateKeeperViewSetup)
{
  GateKeeperView lView(gk);
  BOOST_REQUIRE_EQUAL(lView.disabledFlagsSize(), size_t(2));
  unsigned int lIdx = 0;
  for (GateKeeperView::DisabledFlags_t::const_iterator lIt = lView.disabledFlagsBegin(); lIt != lView.disabledFlagsEnd();
       ++lIt, ++lIdx) {
    BOOST_CHECK_EQUAL(lIt->getContext(), "none");
    if (lIdx == 0) {
      BOOST_CHECK_EQUAL(lIt->getObjectId(), "dummy_sys.childA1");
      BOOST_CHECK_EQUAL(lIt->getObject(), "dummy_sys.childA1");
    }
    if (lIdx == 1) {
      BOOST_CHECK_EQUAL(lIt->getObjectId(), "dummy_sys.child42");
      BOOST_CHECK_EQUAL(lIt->getObject(), "dummy_sys.child42");
    }
  }
}

BOOST_FIXTURE_TEST_CASE(TestMasksView, TestGateKeeperViewSetup)
{
  GateKeeperView lView(gk);
  BOOST_REQUIRE_EQUAL(lView.masksSize(), size_t(3));

  std::unordered_map<std::string, std::string> lExpected = {
    { "dummy_sys.common", "componentA" },
    { "dummy_sys.childTypeA", "componentB" },
    { "dummy_sys.childA1", "componentC" }
  };

  for (GateKeeperView::Masks_t::const_iterator it = lView.masksBegin(); it != lView.masksEnd(); ++it) {
    BOOST_CHECK(lExpected.find(it->getContext()) != lExpected.end());
    BOOST_CHECK_EQUAL(it->getObjectId(), lExpected.at(it->getContext()));
  }
}

BOOST_FIXTURE_TEST_CASE(TestMasksViewWithContext, TestGateKeeperViewSetup)
{
  std::vector<std::string> lContexts, lMasks;
  lContexts.push_back("dummy_sys.common");
  lMasks.push_back("componentA");

  GateKeeperView lView(gk, lContexts);
  BOOST_REQUIRE_EQUAL(lView.masksSize(), size_t(1));
  unsigned int i = 0;
  for (GateKeeperView::Masks_t::const_iterator it = lView.masksBegin(); it != lView.masksEnd(); ++it, ++i) {
    BOOST_CHECK_EQUAL(it->getContext(), lContexts.at(i));
    BOOST_CHECK_EQUAL(it->getObjectId(), lMasks.at(i));
  }
}

BOOST_FIXTURE_TEST_CASE(TestMonitoringSettingsView, TestGateKeeperViewSetup)
{
  typedef std::tuple<std::string, std::string, core::monitoring::Status> Tuple_t;

  const std::vector<Tuple_t> lExpected = {
    { "dummy_sys.childA1", "criticalMetric", core::monitoring::kEnabled },
    { "dummy_sys.common", "criticalMetric", core::monitoring::kEnabled },
    { "dummy_sys.common", "nonCriticalMetric", core::monitoring::kNonCritical },
    { "dummy_sys.common", "weird_state.criticalMetric", core::monitoring::kDisabled }
  };

  GateKeeperView lView(gk);
  BOOST_REQUIRE_EQUAL(lView.monitoringSettingsSize(), lExpected.size());

  std::vector<Tuple_t> lReturned;
  for (auto it = lView.monitoringSettingsBegin(); it != lView.monitoringSettingsEnd(); ++it)
    lReturned.push_back({ it->getContext(), it->getObjectId(), it->getObject()->getStatus() });

  std::sort(lReturned.begin(), lReturned.end());

  BOOST_REQUIRE_EQUAL(lReturned.size(), lExpected.size());
  for (size_t i = 0; i < lReturned.size(); i++) {
    BOOST_CHECK_EQUAL(std::get<0>(lReturned.at(i)), std::get<0>(lExpected.at(i)));
    BOOST_CHECK_EQUAL(std::get<1>(lReturned.at(i)), std::get<1>(lExpected.at(i)));
    BOOST_CHECK_EQUAL(std::get<2>(lReturned.at(i)), std::get<2>(lExpected.at(i)));
  }
}

BOOST_FIXTURE_TEST_CASE(TestMonitoringSettingsViewWithContext, TestGateKeeperViewSetup)
{
  std::vector<std::string> lContexts, lSettings;
  std::vector<core::monitoring::Status> lValues;
  lContexts.push_back("dummy_sys.childA1");

  lSettings.push_back("criticalMetric");
  lSettings.push_back("weird_state.criticalMetric");
  lSettings.push_back("nonCriticalMetric");
  lSettings.push_back("criticalMetric");

  lValues.push_back(core::monitoring::kEnabled);
  lValues.push_back(core::monitoring::kDisabled);
  lValues.push_back(core::monitoring::kNonCritical);
  lValues.push_back(core::monitoring::kEnabled);

  GateKeeperView lView(gk, lContexts);
  BOOST_REQUIRE_EQUAL(lView.monitoringSettingsSize(), size_t(1));

  unsigned int i = 0;
  for (GateKeeperView::MonitoringSettings_t::const_iterator it = lView.monitoringSettingsBegin();
       it != lView.monitoringSettingsEnd(); ++it, ++i) {
    BOOST_CHECK_EQUAL(it->getContext(), lContexts.at(i));
    BOOST_CHECK_EQUAL(it->getObjectId(), lSettings.at(i));
    BOOST_CHECK_EQUAL(it->getObject()->getStatus(), lValues.at(i));
  }
}

BOOST_FIXTURE_TEST_CASE(TestParameterView, TestGateKeeperViewSetup)
{
  // Expected result: context, parameter name; parameter value
  const std::vector<std::array<std::string, 3>> lExpected = {
    { "dummy_sys.childA1", "hello", "World! (childA1)" },
    { "dummy_sys.childTypeA", "command_1.parameter_1", "command" },
    { "dummy_sys.childTypeA", "sequence_1.command_1.parameter_1", "sequence" },
    { "dummy_sys.common", "answer", "42" },
    { "dummy_sys.common", "hello", "World" }
  };

  GateKeeperView lView(gk);
  BOOST_REQUIRE_EQUAL(lView.parametersSize(), lExpected.size());

  typedef std::tuple<std::string, std::string, GateKeeper::Parameter_t> Tuple_t;
  std::vector<Tuple_t> lReturned;
  for (auto it = lView.parametersBegin(); it != lView.parametersEnd(); ++it)
    lReturned.push_back({ it->getContext(), it->getObjectId(), it->getObject() });

  std::sort(lReturned.begin(), lReturned.end(),
            [](const Tuple_t& t1, const Tuple_t& t2) { return (std::get<0>(t1) != std::get<0>(t2)) ? (std::get<0>(t1) < std::get<0>(t2)) : (std::get<1>(t1) < std::get<1>(t2)); });

  BOOST_REQUIRE_EQUAL(lReturned.size(), lExpected.size());

  auto lItA = lReturned.begin();
  auto lItB = lExpected.begin();
  for (; lItA != lReturned.end(); lItA++, lItB++) {
    BOOST_CHECK_EQUAL(std::get<0>(*lItA), lItB->at(0));
    BOOST_CHECK_EQUAL(std::get<1>(*lItA), lItB->at(1));

    const boost::any& lValue = *std::get<2>(*lItA);
    if (lValue.type() == typeid(String_t))
      BOOST_CHECK_EQUAL(boost::any_cast<String_t>(lValue), lItB->at(2));
    else
      BOOST_CHECK_EQUAL(boost::any_cast<Int_t>(lValue), std::stoi(lItB->at(2)));
  }
}

BOOST_FIXTURE_TEST_CASE(TestParameterViewWithContext, TestGateKeeperViewSetup)
{
  std::vector<std::string> lContexts, lParameters, lValues;
  lContexts.push_back("dummy_sys.childA1");

  lParameters.push_back("hello");

  lValues.push_back("World! (childA1)");

  GateKeeperView lView(gk, lContexts);
  BOOST_REQUIRE_EQUAL(lView.parametersSize(), size_t(1));

  unsigned int i = 0;
  for (GateKeeperView::Parameters_t::const_iterator it = lView.parametersBegin(); it != lView.parametersEnd();
       ++it, ++i) {
    BOOST_CHECK_EQUAL(it->getContext(), lContexts.at(i));
    BOOST_CHECK_EQUAL(it->getObjectId(), lParameters.at(i));
    if (it->getObject()->type() == typeid(String_t))
      BOOST_CHECK_EQUAL(boost::any_cast<String_t>(*it->getObject()), lValues.at(i));
    else
      BOOST_CHECK_EQUAL(boost::any_cast<Int_t>(*it->getObject()), std::stoi(lValues.at(i)));
  }
}


BOOST_AUTO_TEST_SUITE_END() // TestGateKeeper

} // ns: test
} // ns: action
} // ns: swatch
