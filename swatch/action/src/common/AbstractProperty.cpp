
#include "swatch/action/AbstractProperty.hpp"


namespace swatch {
namespace action {

AbstractProperty::AbstractProperty(const std::string& aName) :
  mName(aName)
{
}


AbstractProperty::AbstractProperty(const std::string& aName, const std::string& aGroup) :
  mName(aName),
  mGroup(aGroup)
{
}


AbstractProperty::~AbstractProperty()
{
}


const std::string& AbstractProperty::getName() const
{
  return mName;
}


const std::string& AbstractProperty::getGroup() const
{
  return mGroup;
}


const std::string& AbstractProperty::getDescription() const
{
  return mDescription;
}


void AbstractProperty::setDescription(const std::string& aDescription)
{
  if (not mDescription.empty())
    SWATCH_THROW(PropertyFieldAlreadySet("Description of property '" + mName + "' has already been set."));

  mDescription = aDescription;
}

} // namespace action
} // namespace swatch