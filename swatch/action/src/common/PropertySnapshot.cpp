
#include "swatch/action/PropertySnapshot.hpp"


namespace swatch {
namespace action {


PropertySnapshot::~PropertySnapshot()
{
}


const std::string& PropertySnapshot::getName() const
{
  return mName;
}


const std::string& PropertySnapshot::getGroup() const
{
  return mGroup;
}


const std::string& PropertySnapshot::getDescription() const
{
  return mDescription;
}


bool PropertySnapshot::isValueKnown() const
{
  return (not mValue.empty());
}


const std::type_info& PropertySnapshot::getType() const
{
  return *mType;
}


const core::TimePoint& PropertySnapshot::getUpdateTimestamp() const
{
  return mUpdateTime;
}



} // namespace action
} // namespace swatch