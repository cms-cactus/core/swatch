
#include "swatch/action/ObjectFunctionoid.hpp"


#include "log4cplus/loggingmacros.h"

#include "swatch/action/ActionableObject.hpp"
#include "swatch/action/PropertyHolder.hpp"


namespace swatch {
namespace action {


//------------------------------------------------------------------------------------
ObjectFunctionoid::ObjectFunctionoid(const std::string& aId, const std::string& aAlias, const std::string& aDescription, ActionableObject& aActionable) :
  Functionoid(aId, aAlias, aDescription),
  mActionable(aActionable)
{
}


//------------------------------------------------------------------------------------
const ActionableObject& ObjectFunctionoid::getActionable() const
{
  return mActionable;
}


//------------------------------------------------------------------------------------
ActionableObject& ObjectFunctionoid::getActionable()
{
  return mActionable;
}


void ObjectFunctionoid::updateProperties()
{
  LOG4CPLUS_INFO(mActionable.getLogger(), "Updating properties of '" << mActionable.getPath() << "' and subcomponents");
  const auto lStart = std::chrono::steady_clock::now();

  // Crawl through property holders in object hierarchy, calling updatePresence on all of them, and updateProperties on present, non-disabled ones.
  std::string lDisabledObjPath;
  for (swatch::core::Object::iterator it = mActionable.begin(); it != mActionable.end(); it++) {
    if (PropertyHolder* lPropHolder = dynamic_cast<PropertyHolder*>(&*it)) {
      LOG4CPLUS_DEBUG(mActionable.getLogger(), "Updating present/absent flag of '" << lPropHolder->getPath() << "'");
      lPropHolder->updatePresence();

      // Skip this sub-object if under a disabled or absent object
      if ((!lDisabledObjPath.empty()) && (lPropHolder->getPath().find(lDisabledObjPath) == size_t(0)))
        continue;
      else if ((not lPropHolder->isPresent()) or (lPropHolder->getMonitoringStatus() == swatch::core::monitoring::kDisabled)) {
        lDisabledObjPath = lPropHolder->getPath();
        continue;
      }
      else {
        LOG4CPLUS_DEBUG(mActionable.getLogger(), "Updating properties of '" << lPropHolder->getPath() << "'");
        lPropHolder->updateProperties();
      }
    }
  }

  const std::chrono::duration<float, std::milli> lDurationInMs(std::chrono::steady_clock::now() - lStart);
  LOG4CPLUS_INFO(mActionable.getLogger(), "Properties of '" << mActionable.getPath() << "' and subcomponents updated. Took " << lDurationInMs.count() << "ms");
}


} // namespace action
} // namespace swatch
