
#include "swatch/action/File.hpp"

#include <boost/filesystem.hpp>


namespace swatch {
namespace action {


File::File(const std::string& aPath, const std::string& aContentType, const std::string& aContentFormat) :
  mPath(aPath),
  mName(boost::filesystem::path(aPath).filename().native()),
  mContentType(aContentType),
  mContentFormat(aContentFormat)
{
}


File::File(const std::string& aPath, const std::string& aName, const std::string& aContentType, const std::string& aContentFormat) :
  mPath(aPath),
  mName(aName),
  mContentType(aContentType),
  mContentFormat(aContentFormat)
{
}


File::~File()
{
}


const std::string& File::getPath() const
{
  return mPath;
}


const std::string& File::getName() const
{
  return mName;
}


const std::string& File::getContentType() const
{
  return mContentType;
}


const std::string& File::getContentFormat() const
{
  return mContentFormat;
}

} // namespace action
} // namespace swatch
