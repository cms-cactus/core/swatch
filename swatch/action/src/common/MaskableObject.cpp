
#include "swatch/action/MaskableObject.hpp"


namespace swatch {
namespace action {


MaskableObject::MaskableObject(const std::string& aId, const std::string& aAlias) :
  PropertyHolder(aId, aAlias),
  mMasked(false)
{
}


bool MaskableObject::isMasked() const
{
  return mMasked;
}


void MaskableObject::setMasked(bool aMask)
{
  mMasked = aMask;
}


bool MaskableObject::checkPresence() const
{
  return true;
}


void MaskableObject::retrievePropertyValues()
{
}


} // namespace action
} // namespace swatch
