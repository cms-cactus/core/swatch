
#include "swatch/action/PropertyHolder.hpp"

#include <log4cplus/loggingmacros.h>

#include "swatch/action/Property.hpp"
#include "swatch/core/ErrorInfo.hpp"
#include "swatch/core/LeafObject.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/logger/Logger.hpp"


namespace swatch {
namespace action {


PropertyHolder::PropertyHolder(const std::string& aId) :
  MonitorableObject(aId),
  mIsPresent(true),
  mLogger(swatch::logger::Logger::getInstance("swatch.core.PropertyHolder")),
  mPropertyUpdateDuration(0)
{
}


PropertyHolder::PropertyHolder(const std::string& aId, const std::string& aAlias) :
  MonitorableObject(aId, aAlias),
  mIsPresent(true),
  mLogger(swatch::logger::Logger::getInstance("swatch.core.PropertyHolder")),
  mPropertyUpdateDuration(0)
{
}


PropertyHolder::~PropertyHolder()
{
}


bool PropertyHolder::isPresent() const
{
  return mIsPresent;
}


AbstractProperty& PropertyHolder::getProperty(const std::string& aName)
{
  auto lIt = mProperties.find(aName);

  if (lIt == mProperties.end())
    SWATCH_THROW(PropertyNotFound("Property '" + aName + "' does not exist"));

  return *lIt->second;
}


const AbstractProperty& PropertyHolder::getProperty(const std::string& aName) const
{
  auto lIt = mProperties.find(aName);

  if (lIt == mProperties.end())
    SWATCH_THROW(PropertyNotFound("Property '" + aName + "' does not exist"));

  return *lIt->second;
}


const PropertyHolder::PropertyMap_t& PropertyHolder::getProperties() const
{
  return mProperties;
}


const std::unordered_set<std::string>& PropertyHolder::getPropertyGroups() const
{
  return mPropertyGroups;
}


void PropertyHolder::updatePresence()
{
  try {
    mIsPresent = this->checkPresence();
  }
  catch (const std::exception& e) {
    LOG4CPLUS_ERROR(mLogger, core::demangleName(typeid(e).name()) << " exception was thrown by the checkPresence() method of '" << this->getPath() << "'. Exception message: " << e.what());
  }
}


void PropertyHolder::updateProperties()
{
  core::SteadyTimePoint_t lStartTime = core::SteadyTimePoint_t::clock::now();

  try {
    this->retrievePropertyValues();
    const auto lEndTime = core::SteadyTimePoint_t::clock::now();

    std::unique_lock<std::mutex> lUpdateInfoLock(mPropertyUpdateMutex);
    mPropertyUpdateError.reset();
    mPropertyUpdateDuration = lEndTime - lStartTime;
  }
  catch (const std::exception& e) {
    const auto lEndTime = core::SteadyTimePoint_t::clock::now();
    LOG4CPLUS_WARN(mLogger, core::demangleName(typeid(e).name()) << " exception was thrown by the retrievePropertyValues() method of '" << this->getPath() << "'. Exception message: " << e.what());

    std::unique_lock<std::mutex> lUpdateInfoLock(mPropertyUpdateMutex);
    mPropertyUpdateError.reset(new core::ErrorInfo(e));
    mPropertyUpdateDuration = lEndTime - lStartTime;
  }

  for (auto& x : mProperties) {
    if (x.second->getUpdateTime().steady < lStartTime)
      x.second->setValueUnknown();
  }
}


core::Duration_t PropertyHolder::getPropertyUpdateDuration() const
{
  std::unique_lock<std::mutex> lLock(mPropertyUpdateMutex);
  return mPropertyUpdateDuration;
}


std::shared_ptr<const core::ErrorInfo> PropertyHolder::getPropertyUpdateErrorInfo() const
{
  std::unique_lock<std::mutex> lLock(mPropertyUpdateMutex);
  return mPropertyUpdateError;
}


void PropertyHolder::addPropertyGroup(const std::string& aGroup)
{
  mPropertyGroups.insert(aGroup);
}


} // namespace action
} // namespace swatch