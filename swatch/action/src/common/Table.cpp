
#include "swatch/action/Table.hpp"


namespace swatch {
namespace action {


Table::Column::Column(const std::string& aName, const std::type_info& aType) :
  name(aName),
  type(&aType)
{
}


Table::Table(const std::vector<Column>& aColumns) :
  mColumns(aColumns)
{
  for (size_t i = 0; i < mColumns.size(); i++) {
    if (mColumnMap.count(mColumns.at(i).name) > 0)
      SWATCH_THROW(DuplicateColumnName("Multiple columns have the same name, '" + mColumns.at(i).name + "'"));
    mColumnMap[mColumns.at(i).name] = i;
  }
}


Table::~Table()
{
}


void Table::addRow(const std::vector<boost::any>& aRowData)
{
  if (aRowData.size() != mColumns.size())
    SWATCH_THROW(ColumnDoesNotExist("Number of entries in supplied row (" + std::to_string(aRowData.size()) + ") does not match number of columns (" + std::to_string(mColumns.size()) + ")"));

  for (size_t i = 0; i < aRowData.size(); i++) {
    if (aRowData.at(i).type() != *mColumns.at(i).type)
      SWATCH_THROW(TableTypeMismatch("Data supplied for column " + std::to_string(i) + " ('" + mColumns.at(i).name + "', type " + core::demangleName(mColumns.at(i).type->name()) + ") has incorrect type, " + core::demangleName(aRowData.at(i).type().name())));
  }

  mData.push_back(aRowData);
}


const std::vector<Table::Column>& Table::getColumns() const
{
  return mColumns;
}


size_t Table::getNumberOfRows() const
{
  return mData.size();
}


} // namespace action
} // namespace swatch
