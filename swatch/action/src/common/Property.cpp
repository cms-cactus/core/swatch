
#include "swatch/action/Property.hpp"


#include <type_traits>

#include "swatch/action/PropertySnapshot.hpp"
#include "swatch/action/Table.hpp"
#include "swatch/core/utilities.hpp"


namespace swatch {
namespace action {

namespace detail {

template <typename DataType, class Enable>
_Property<DataType, Enable>::_Property(const std::string& aName) :
  AbstractProperty(aName)
{
}


template <typename DataType, class Enable>
_Property<DataType, Enable>::_Property(const std::string& aName, const std::string& aGroup) :
  AbstractProperty(aName, aGroup)
{
}


template <typename DataType, class Enable>
_Property<DataType, Enable>::~_Property()
{
}


template <typename DataType, class Enable>
PropertySnapshot _Property<DataType, Enable>::getSnapshot() const
{
  std::lock_guard<std::mutex> lLock(mMutex);

  if (mValue != NULL)
    return PropertySnapshot(getName(), getGroup(), getDescription(), *mValue, mUpdateTime);
  else
    return PropertySnapshot(getName(), getGroup(), getDescription(), PropertySnapshot::Type<DataType>(), mUpdateTime);
}


template <typename DataType, class Enable>
core::TimePoint _Property<DataType, Enable>::getUpdateTime() const
{
  // Note: In order to ensure thread safety, must return copy rather than const ref
  return mUpdateTime;
}


template <typename DataType, class Enable>
void _Property<DataType, Enable>::setValue(const DataType& aValue)
{
  std::lock_guard<std::mutex> lLock(this->mMutex);
  this->mUpdateTime = core::TimePoint::now();
  this->mValue.reset(new DataType(aValue));
}


template <typename DataType, class Enable>
void _Property<DataType, Enable>::setValueUnknown()
{
  std::lock_guard<std::mutex> lLock(mMutex);
  mUpdateTime = core::TimePoint::now();
  mValue.reset((const DataType*)NULL);
}

} // namespace detail


////////////////////////////////
//   GENERIC CLASS TEMPLATE   //
////////////////////////////////


template <typename DataType, class Enable>
Property<DataType, Enable>::Property(const std::string& aName) :
  detail::_Property<DataType>(aName)
{
}


template <typename DataType, class Enable>
Property<DataType, Enable>::Property(const std::string& aName, const std::string& aGroup) :
  detail::_Property<DataType>(aName, aGroup)
{
}


template <typename DataType, class Enable>
Property<DataType, Enable>::~Property()
{
}


/////////////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Floating point types   //
/////////////////////////////////////////////////////////////


template <typename DataType>
const std::pair<core::format::FloatingPointNotation, boost::optional<size_t>>
    Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::kDefaultFormat { core::format::kFixedPoint, {} };


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::Property(const std::string& aName) :
  detail::_Property<DataType>(aName)
{
}


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::Property(const std::string& aName, const std::string& aGroup) :
  detail::_Property<DataType>(aName, aGroup)
{
}


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::~Property()
{
}


template <typename DataType>
const std::string& Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getUnit() const
{
  return mUnit;
}


template <typename DataType>
core::format::FloatingPointNotation Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getNotation() const
{
  if (mFormat)
    return mFormat->first;
  else
    return kDefaultFormat.first;
}


template <typename DataType>
boost::optional<size_t> Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getPrecision() const
{
  if (mFormat)
    return mFormat->second;
  else
    return kDefaultFormat.second;
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setUnit(const std::string& aUnit)
{
  if (not mUnit.empty())
    SWATCH_THROW(PropertyFieldAlreadySet("Unit of property '" + this->getName() + "' has already been set."));

  mUnit = aUnit;
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setFormat(core::format::FloatingPointNotation aNotation)
{
  if (mFormat)
    SWATCH_THROW(PropertyFieldAlreadySet("Format of property '" + this->getName() + "' has already been set."));

  mFormat = std::pair<core::format::FloatingPointNotation, boost::optional<size_t>> { aNotation, kDefaultFormat.second };
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setFormat(core::format::FloatingPointNotation aNotation, const size_t aPrecision)
{
  if (mFormat)
    SWATCH_THROW(PropertyFieldAlreadySet("Format of property '" + this->getName() + "' has already been set."));

  mFormat = std::pair<core::format::FloatingPointNotation, boost::optional<size_t>> { aNotation, aPrecision };
}


///////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Integral types   //
///////////////////////////////////////////////////////


template <typename DataType>
const std::tuple<core::format::Base, boost::optional<size_t>, const char>
    Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::kDefaultFormat { core::format::kDecimal, {}, '0' };


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::Property(const std::string& aName) :
  detail::_Property<DataType>(aName)
{
}


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::Property(const std::string& aName, const std::string& aGroup) :
  detail::_Property<DataType>(aName, aGroup)
{
}


template <typename DataType>
Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::~Property()
{
}


template <typename DataType>
const std::string& Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getUnit() const
{
  return mUnit;
}


template <typename DataType>
core::format::Base Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getBase() const
{
  if (mFormat)
    return std::get<0>(*mFormat);
  else
    return std::get<0>(kDefaultFormat);
}


template <typename DataType>
boost::optional<size_t> Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getWidth() const
{
  if (mFormat)
    return std::get<1>(*mFormat);
  else
    return std::get<1>(kDefaultFormat);
}


template <typename DataType>
char Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getFillChar() const
{
  if (mFormat)
    return std::get<2>(*mFormat);
  else
    return std::get<2>(kDefaultFormat);
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setUnit(const std::string& aUnit)
{
  if (not mUnit.empty())
    SWATCH_THROW(PropertyFieldAlreadySet("Unit of property '" + this->getName() + "' has already been set."));

  mUnit = aUnit;
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setFormat(core::format::Base aBase)
{
  if (mFormat)
    SWATCH_THROW(PropertyFieldAlreadySet("Format of property '" + this->getName() + "' has already been set."));

  mFormat = std::tuple<core::format::Base, boost::optional<size_t>, char> { aBase, boost::none, std::get<2>(kDefaultFormat) };
}


template <typename DataType>
void Property<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setFormat(core::format::Base aBase, const size_t aWidth, const char aFillChar)
{
  if (mFormat)
    SWATCH_THROW(PropertyFieldAlreadySet("Format of property '" + this->getName() + "' has already been set."));

  mFormat = std::tuple<core::format::Base, boost::optional<size_t>, char> { aBase, aWidth, aFillChar };
}


////////////////////////////////
//   EXPLICIT INSTANTIATION   //
////////////////////////////////


template class detail::_Property<bool>;
template class detail::_Property<uint16_t>;
template class detail::_Property<uint32_t>;
template class detail::_Property<uint64_t>;
template class detail::_Property<int>;
template class detail::_Property<int64_t>;
template class detail::_Property<float>;
template class detail::_Property<double>;
template class detail::_Property<std::string>;
template class detail::_Property<Table>;

template class Property<bool>;
template class Property<uint16_t>;
template class Property<uint32_t>;
template class Property<uint64_t>;
template class Property<int>;
template class Property<int64_t>;
template class Property<float>;
template class Property<double>;
template class Property<std::string>;
template class Property<Table>;


} // namespace action
} // namespace swatch