#include "swatch/action/CommandVec.hpp"


#include <exception>
#include <iostream>
#include <math.h>
#include <set>
#include <vector>

#include "log4cplus/loggingmacros.h"

#include "swatch/action/ActionableObject.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/action/GateKeeper.hpp"
#include "swatch/core/ParameterSet.hpp"


namespace swatch {
namespace action {


CommandVec::CommandVec(const std::string& aId, const std::string& aAlias, const std::string& aDescription, ActionableObject& aActionable) :
  ObjectFunctionoid(aId, aAlias, aDescription, aActionable),
  mCommands(),
  mCachedParameters(),
  mState(kInitial),
  mCommandIt(mCommands.end())
{
}


void CommandVec::addCommand(Command& aCommand, const std::string& aNamespace)
{
  if (&aCommand.getActionable() != &getActionable()) {
    SWATCH_THROW(InvalidResource("Cannot add command '" + aCommand.getId() + "' (resource: " + aCommand.getActionable().getPath() + ") ' to sequence of resource '" + getActionable().getPath() + "'"));
  }
  mCommands.push_back(Element(aCommand, aNamespace));
  mCommandIt = mCommands.end();
}


void CommandVec::addCommand(const std::string& aCommand, const std::string& aNamespace)
{
  Command& lCommand = getActionable().getCommand(aCommand);
  mCommands.push_back(Element(lCommand, aNamespace));
  mCommandIt = mCommands.end();
}


CommandVec::~CommandVec()
{
}


CommandVec::Element::Element(Command& aCommand, const std::string& aNamespace) :
  mCmd(&aCommand),
  mNamespace(aNamespace)
{
}


CommandVec::Element::~Element()
{
}

const std::string& CommandVec::Element::getNamespace() const
{
  return mNamespace;
}

const Command& CommandVec::Element::get() const
{
  return *mCmd;
}

Command& CommandVec::Element::get()
{
  return *mCmd;
}


size_t CommandVec::size() const
{
  return mCommands.size();
}


CommandVec::const_iterator CommandVec::begin() const
{
  return mCommands.begin();
}


CommandVec::const_iterator CommandVec::end() const
{
  return mCommands.end();
}


std::vector<Command*> CommandVec::getCommands()
{
  std::vector<Command*> lCmds;
  lCmds.reserve(size());
  for (auto lIt = mCommands.begin(); lIt != mCommands.end(); lIt++)
    lCmds.push_back(&lIt->get());
  return lCmds;
}


Functionoid::State CommandVec::getState() const
{
  std::unique_lock<std::mutex> lock(mMutex);
  return mState;
}


CommandVecSnapshot CommandVec::getStatus() const
{
  std::unique_lock<std::mutex> lock(mMutex);

  std::chrono::duration<float> waitingTime(0.0);
  if (mScheduledTime != core::SteadyTimePoint_t())
    waitingTime = (mState == kScheduled ? core::SteadyTimePoint_t::clock::now() : mExecStartTime.steady) - mScheduledTime;

  std::chrono::duration<float> runningTime(0.0);
  switch (mState) {
    case kInitial:
    case kScheduled:
      break;
    case kRunning:
      runningTime = core::SteadyTimePoint_t::clock::now() - mExecStartTime.steady;
      break;
    default:
      runningTime = mExecEndTime - mExecStartTime.steady;
      break;
  }

  const Command* currentCommand = (((mCommandIt == mCommands.end()) || (mState == State::kError)) ? NULL : &mCommandIt->get());

  return CommandVecSnapshot(IdAliasPair(*this), IdAliasPair(getActionable()), mState, mExecStartTime.system, waitingTime.count(), runningTime.count(), currentCommand, mStatusOfCompletedCommands, mCommands.size());
}


void CommandVec::runCommands(std::shared_ptr<BusyGuard> aGuard)
{
  // 1) Declare that I'm running
  {
    std::unique_lock<std::mutex> lock(mMutex);
    mExecStartTime = core::TimePoint::now();
    // Finish straight away if there aren't any commands to run
    if (mCommands.empty()) {
      mState = kDone;
      mCommandIt = mCommands.end();
      mExecEndTime = mExecStartTime.steady;
      return;
    }
    else {
      mState = kRunning;
      mCommandIt = mCommands.begin();
    }
  }

  // 2) Run the commands
  State lEndState;
  try {
    ParameterSets_t::iterator lIt(mCachedParameters.begin());

    while (true) {
      mCommandIt->get().exec(aGuard.get(), *lIt, false); // False = run the commands in this thread!
      // FIXME: Make exec method return CommandStatus to remove any possibility of race condition ?

      CommandSnapshot status = mCommandIt->get().getStatus();
      std::unique_lock<std::mutex> lock(mMutex);
      mStatusOfCompletedCommands.push_back(status);

      // Don't execute any more commands if there was an error
      if (status.getState() == kError) {
        lEndState = kError;
        mExecEndTime = core::SteadyTimePoint_t::clock::now();
        break;
      }

      // Increment the "current command" iterator
      ++mCommandIt;
      ++lIt;

      // Exit the loop if no more commands remain
      if (mCommandIt == mCommands.end()) {
        lEndState = kDone;
        for (std::vector<CommandSnapshot>::const_iterator statusIt = mStatusOfCompletedCommands.begin(); statusIt != mStatusOfCompletedCommands.end(); statusIt++) {
          if (statusIt->getState() == kWarning)
            lEndState = kWarning;
        }
        mExecEndTime = core::SteadyTimePoint_t::clock::now();
        break;
      }
    }
  }
  catch (const std::exception& e) {
    std::cout << "An exception occurred in CommandVec::runCommands(): " << e.what() << std::endl;

    std::unique_lock<std::mutex> lock(mMutex);
    lEndState = kError;
    mExecEndTime = core::SteadyTimePoint_t::clock::now();
  }

  // 3) Update properties of non-disabled PropertyHolder-derived objects
  updateProperties();

  // 4) Run post-action callback if defined
  if (getActionable().getPostActionCallback()) {
    LOG4CPLUS_INFO(getActionable().getLogger(), "Executing post-action callback on " << getActionable().getId() << ", at the end of " << getId());
    try {
      getActionable().getPostActionCallback()(*this);
    }
    catch (const std::exception& e) {
      LOG4CPLUS_ERROR(getActionable().getLogger(), "An exception [" << core::demangleName(typeid(e).name()) << "] was thrown by the post-action callback: " << e.what());
    }
  }

  std::unique_lock<std::mutex> lock(mMutex);
  mState = lEndState;

  // 5) The resource is released by destruction of BusyGuard
}


CommandVec::MissingParam::MissingParam(const std::string& aNamespace, const std::string& aCommand, const std::string& aParam) :
  nspace(aNamespace),
  command(aCommand),
  parameter(aParam)
{
}


void CommandVec::checkForMissingParameters(const GateKeeper& aGateKeeper, std::vector<core::ParameterSet>& aParamSets, std::vector<MissingParam>& aMissingParams) const
{
  extractParameters(aGateKeeper, aParamSets, aMissingParams, false);
}


CommandVec::ParamRuleViolationList::ParamRuleViolationList(const std::string& aCommand, const size_t aCommandIdx, const core::ParameterSet& aParamSet, const std::vector<Command::ParamRuleViolation>& aRuleViolations) :
  command(aCommand),
  commandIdx(aCommandIdx),
  paramSet(aParamSet),
  violations(aRuleViolations)
{
}


void CommandVec::checkForInvalidParameters(const std::vector<core::ParameterSet>& aParamSets, std::vector<ParamRuleViolationList>& aRuleViolations) const
{
  if (aParamSets.size() < size()) {
    std::ostringstream lMsgStream;
    lMsgStream << "Parameters for only " << aParamSets.size() << " commands supplied";
    SWATCH_THROW(ParameterNotFound(lMsgStream.str()));
  }

  auto lParamSetIt = aParamSets.begin();
  for (size_t i = 0; i < size(); i++, lParamSetIt++) {
    const Command& lCommand = mCommands.at(i).get();

    std::vector<Command::ParamRuleViolation> lRuleViolations;
    lCommand.checkRulesAndConstraints(*lParamSetIt, lRuleViolations);
    if (!lRuleViolations.empty())
      aRuleViolations.push_back(ParamRuleViolationList(lCommand.getId(), i, *lParamSetIt, lRuleViolations));
  }
}


void CommandVec::reset(const ParameterSets_t& aParamSets)
{
  std::unique_lock<std::mutex> lock(mMutex);

  mState = kInitial;
  mScheduledTime = core::SteadyTimePoint_t();
  mExecStartTime = core::TimePoint();
  mExecEndTime = core::SteadyTimePoint_t();
  mCommandIt = mCommands.end();
  mCachedParameters = aParamSets;
  mStatusOfCompletedCommands.clear();
  mStatusOfCompletedCommands.reserve(mCommands.size());
}


void CommandVec::extractParameters(const GateKeeper& aGateKeeper, std::vector<core::ParameterSet>& aParamSets, std::vector<MissingParam>& aMissingParams, bool aThrowOnMissing) const
{
  aParamSets.clear();
  aParamSets.reserve(mCommands.size());

  aMissingParams.clear();

  for (CommandVector_t::const_iterator lIt(mCommands.begin()); lIt != mCommands.end(); ++lIt) {
    const Command& lCommand(lIt->get());
    //    const std::string& lCommandAlias = (lIt->getAlias().empty() ? lCommand.getId() : lIt->getAlias());

    core::ParameterSet lParams;
    std::set<std::string> lKeys(lCommand.getDefaultParams().keys());
    for (std::set<std::string>::iterator lIt2(lKeys.begin()); lIt2 != lKeys.end(); ++lIt2) {
      GateKeeper::Parameter_t lData(aGateKeeper.get(lIt->getNamespace(), lCommand.getId(), *lIt2, getActionable().getGateKeeperContexts()));
      if (lData.get() != NULL) {
        lParams.adopt(*lIt2, lData);
      }
      else if (aThrowOnMissing) {
        std::ostringstream oss;
        oss << "Could not find value of parameter '" << *lIt2 << "' for command '" << lCommand.getId() << "' in namespace '" << lIt->getNamespace() << "' of resource '" << getActionable().getId() << "'";
        //        LOG4CPLUS_ERROR(getActionable().getLogger(), oss.str());
        SWATCH_THROW(ParameterNotFound(oss.str()));
      }
      else {
        aMissingParams.push_back(MissingParam(lIt->getNamespace(), lCommand.getId(), *lIt2));
      }
    }
    aParamSets.push_back(lParams);
  }
}

std::ostream& operator<<(std::ostream& aOstream, const CommandVec::MissingParam& aMissingParam)
{
  return (aOstream << aMissingParam.nspace << "." << aMissingParam.command << "." << aMissingParam.parameter);
}


bool operator!=(const CommandVec::MissingParam& aParam1, const CommandVec::MissingParam& aParam2)
{
  return !((aParam1.nspace == aParam2.nspace) && (aParam1.command == aParam2.command) && (aParam1.parameter == aParam2.parameter));
}


std::ostream& operator<<(std::ostream& aOstream, const CommandVec::ParamRuleViolationList& aViolationList)
{
  aOstream << "{" << aViolationList.commandIdx << ", " << aViolationList.command << ", pSet = " << aViolationList.paramSet << ", [";
  for (size_t i = 0; i < aViolationList.violations.size(); i++) {
    const auto& lViolation = aViolationList.violations.at(i);
    if (i != 0)
      aOstream << ", ";
    aOstream << "{params [" << core::join(lViolation.parameters, ",") << "],";
    aOstream << " '" << lViolation.ruleDescription << "', '" << lViolation.details << "'}";
  }
  aOstream << "]}";
  return aOstream;
}


bool operator!=(const CommandVec::ParamRuleViolationList& aList1, const CommandVec::ParamRuleViolationList& aList2)
{
  if (aList1.command != aList2.command)
    return true;
  if (aList1.commandIdx != aList2.commandIdx)
    return true;

  if (aList1.paramSet != aList2.paramSet)
    return true;

  if (aList1.violations.size() != aList2.violations.size())
    return true;

  auto lIt1 = aList1.violations.begin();
  for (auto lIt2 = aList2.violations.begin(); lIt2 != aList2.violations.end(); lIt1++, lIt2++) {
    if (lIt1->ruleDescription != lIt2->ruleDescription)
      return true;
    if (lIt1->parameters != lIt2->parameters)
      return true;
    if (lIt1->details != lIt2->details)
      return true;
  }

  return false;
}



CommandVecSnapshot::CommandVecSnapshot(const IdAliasPair& aAction, const IdAliasPair& aActionable, State aState, const core::SystemTimePoint_t& aStartTime, float aWaitingTime, float aRunningTime, const Command* aCurrentCommand, const std::vector<CommandSnapshot>& aStatusOfCompletedCommands, size_t aTotalNumberOfCommands) :
  ActionSnapshot(aAction, aActionable, aState, aStartTime, aWaitingTime, aRunningTime),
  mTotalNumberOfCommands(aTotalNumberOfCommands),
  mCommandStatuses(aStatusOfCompletedCommands)
{
  if (aCurrentCommand != NULL) {
    mCommandStatuses.push_back(aCurrentCommand->getStatus());
  }

  for (auto it = aStatusOfCompletedCommands.begin(); it != aStatusOfCompletedCommands.end(); it++)
    mResults.push_back(&it->getResult());
}


float CommandVecSnapshot::getProgress() const
{
  if ((mTotalNumberOfCommands == 0) && (getState() == State::kDone))
    return 1.0;
  else if (mCommandStatuses.empty())
    return 0.0;
  else if ((getState() == State::kDone) || (getState() == State::kWarning))
    return 1.0;
  else
    return (float(mCommandStatuses.size() - 1) + mCommandStatuses.back().getProgress()) / float(mTotalNumberOfCommands);
}


size_t CommandVecSnapshot::getNumberOfCompletedCommands() const
{
  return mResults.size();
}


size_t CommandVecSnapshot::getTotalNumberOfCommands() const
{
  return mTotalNumberOfCommands;
}


const std::vector<const boost::any*>& CommandVecSnapshot::getResults() const
{
  return mResults;
}


CommandVecSnapshot::const_iterator CommandVecSnapshot::begin() const
{
  return mCommandStatuses.begin();
}


CommandVecSnapshot::const_iterator CommandVecSnapshot::end() const
{
  return mCommandStatuses.end();
}


const CommandSnapshot& CommandVecSnapshot::at(size_t aIndex) const
{
  return mCommandStatuses.at(aIndex);
}


size_t CommandVecSnapshot::size() const
{
  return mCommandStatuses.size();
}


} /* namespace action */
} /* namespace swatch */
