#include "swatch/action/Command.hpp"


// Standard headers
#include <exception> // for exception
#include <mutex>
#include <set> // for set
#include <sstream> // for operator<<, basic_ostream, etc
#include <stddef.h> // for NULL
#include <stdexcept> // for out_of_range
#include <typeinfo> // for type_info

// boost headers
#include "boost/lexical_cast.hpp"

// SWATCH headers
#include "swatch/action/ActionableObject.hpp"
#include "swatch/action/BusyGuard.hpp"
#include "swatch/action/PropertyHolder.hpp"
#include "swatch/action/ThreadPool.hpp"
#include "swatch/core/ErrorInfo.hpp"
#include "swatch/core/Match.hpp"
#include "swatch/core/utilities.hpp"

// log4cplus headers
#include <log4cplus/loggingmacros.h>



namespace swatch {
namespace action {


//------------------------------------------------------------------------------------
Command::~Command()
{
}


//------------------------------------------------------------------------------------
void Command::exec(const core::ParameterSet& aParams, bool aUseThreadPool)
{
  exec(NULL, aParams, aUseThreadPool);
}


//------------------------------------------------------------------------------------
void Command::exec(const BusyGuard* aOuterBusyGuard, const core::ParameterSet& aParams, bool aUseThreadPool)
{
  // Merge user-supplied parameter values with default values
  core::ParameterSet lRunningParams = mergeParametersWithDefaults(aParams);

  // Validate the new running parameters against rules and constraints
  std::vector<ParamRuleViolation> lRuleViolations;
  checkRulesAndConstraints(lRunningParams, lRuleViolations);
  if (!lRuleViolations.empty()) {
    std::ostringstream lMsgStream;
    lMsgStream << "Parameters [" << core::join(lRuleViolations.at(0).parameters, ", ") << "] failed rule/constraint '"
               << lRuleViolations.at(0).ruleDescription << "': " << lRuleViolations.at(0).details;
    SWATCH_THROW(CommandParameterCheckFailed(lMsgStream.str()));
  }

  std::shared_ptr<BusyGuard> lBusyGuard(new BusyGuard(*this, mActionableStatus, aOuterBusyGuard));

  // Reset the command's state before doing anything else
  resetForRunning(lRunningParams);

  // Execute the command protected by a very generic try/catch
  try {
    // if threadpool is to be used
    if (aUseThreadPool) {
      std::unique_lock<std::mutex> lock(mMutex);
      mState = kScheduled;
      mScheduledTime = core::SteadyTimePoint_t::clock::now();

      ThreadPool& pool = ThreadPool::getInstance();
      pool.addTask<Command, BusyGuard>(this, &Command::runCode, lBusyGuard, mRunningParams);
    }
    else {
      // otherwise execute in same thread
      this->runCode(lBusyGuard, mRunningParams);
    }
  }
  catch (const std::exception& e) {
    LOG4CPLUS_ERROR(getActionable().getLogger(), "Exception thrown when executing command '" << getId() << "' : " << (e.what() != 0x0 ? e.what() : ""));

    // Then rethrow the exception on to the higher layers of code.
    throw;
  }
}


//------------------------------------------------------------------------------------
void Command::runCode(std::shared_ptr<BusyGuard> aActionGuard, const core::ParameterSet& aParams)
{
  // 1) Declare that I'm running
  {
    std::unique_lock<std::mutex> lock(mMutex);
    mState = kRunning;
    mExecStartTime = core::TimePoint::now();
  }

  // 2) Run the code, handling any exceptions
  State lEndState;
  try {
    State s = this->code(aParams);

    std::unique_lock<std::mutex> lock(mMutex);
    mExecEndTime = core::SteadyTimePoint_t::clock::now();
    const std::string lLastStatusMsg(mStatusMessages.empty() ? "" : mStatusMessages.back().second);
    switch (s) {
      case State::kWarning:
        LOG4CPLUS_WARN(getActionable().getLogger(), "Command '" << getId() << "' returned warning. Last status message: " << lLastStatusMsg);
        lEndState = s;
        mProgress = 1.0;
        break;
      case State::kDone:
        LOG4CPLUS_INFO(getActionable().getLogger(), "Command '" << getId() << "' completed successfully. Last status message: " << lLastStatusMsg);
        lEndState = s;
        mProgress = 1.0;
        break;
      case State::kError:
        LOG4CPLUS_ERROR(getActionable().getLogger(), "Command '" << getId() << "' returned error. Progress = " << mProgress << "; last status message: " << lLastStatusMsg);
        lEndState = s;
        break;
      default:
        lEndState = State::kError;
        const std::string lExtraMessage("Command::code() method returned invalid Status enum value '" + boost::lexical_cast<std::string>(s) + "'");
        mStatusMessages.push_back({ std::chrono::duration<float>(mExecEndTime - mExecStartTime.steady).count(), lExtraMessage });
        LOG4CPLUS_ERROR(getActionable().getLogger(), "Command '" << getId() << "' : " << lExtraMessage);
    }
  }
  catch (const std::exception& e) {
    std::unique_lock<std::mutex> lock(mMutex);
    mExecEndTime = core::SteadyTimePoint_t::clock::now();
    lEndState = State::kError;
    mErrorInfo.reset(new core::ErrorInfo(e));
    const std::string lExtraMessage("An exception of type '" + core::demangleName(typeid(e).name()) + "' was thrown in Command::code(): " + e.what());
    mStatusMessages.push_back({ std::chrono::duration<float>(mExecEndTime - mExecStartTime.steady).count(), lExtraMessage });
    LOG4CPLUS_ERROR(getActionable().getLogger(), "Command '" << getId() << " : " << lExtraMessage);
  }

  // If this command isn't called inside sequence/transition: Update properties and run post-action callback
  if (getActionable().getStatus().getRunningActions().size() == 1) {
    // 3) Update properties of non-disabled PropertyHolder-derived objects
    updateProperties();

    // 4) Run post-action callback if defined
    if (getActionable().getPostActionCallback()) {
      LOG4CPLUS_INFO(getActionable().getLogger(), "Executing post-action callback on " << getActionable().getId() << ", at the end of " << getId());
      try {
        getActionable().getPostActionCallback()(*this);
      }
      catch (const std::exception& e) {
        LOG4CPLUS_ERROR(getActionable().getLogger(), "An exception [" << core::demangleName(typeid(e).name()) << "] was thrown by the post-action callback: " << e.what());
      }
    }
  }

  std::unique_lock<std::mutex> lock(mMutex);
  mState = lEndState;

  // 5) Release control of the resource - by destruction of the ActionGuard.
}


//------------------------------------------------------------------------------------
void Command::resetForRunning(const core::ParameterSet& aParams)
{
  std::unique_lock<std::mutex> lock(mMutex);
  mState = kScheduled;
  mScheduledTime = core::SteadyTimePoint_t();
  mExecStartTime = core::TimePoint();
  mExecEndTime = core::SteadyTimePoint_t();
  mProgress = 0.0;
  mStatusMessages.clear();

  mErrorInfo.reset();

  mExecutionDetails = core::ParameterSet();
  mResult.reset(new boost::any(mDefaultResult));

  mRunningParams = aParams;
}


//------------------------------------------------------------------------------------
Functionoid::State Command::getState() const
{
  std::unique_lock<std::mutex> lock(mMutex);
  return mState;
}


//------------------------------------------------------------------------------------
CommandSnapshot Command::getStatus() const
{
  std::unique_lock<std::mutex> lock(mMutex);

  // Only let user see the result once the command has completed (since its contents can change before then) ...
  std::shared_ptr<const boost::any> result(new boost::any());
  if ((mState == kDone) || (mState == kWarning) || (mState == kError))
    result = mResult;

  std::chrono::duration<float> waitingTime(0.0);
  if (mScheduledTime != core::SteadyTimePoint_t())
    waitingTime = (mState == kScheduled ? core::SteadyTimePoint_t::clock::now() : mExecStartTime.steady) - mScheduledTime;

  std::chrono::duration<float> runningTime(0.0);
  switch (mState) {
    case kInitial:
    case kScheduled:
      break;
    case kRunning:
      runningTime = core::SteadyTimePoint_t::clock::now() - mExecStartTime.steady;
      break;
    default:
      runningTime = mExecEndTime - mExecStartTime.steady;
      break;
  }

  return CommandSnapshot(IdAliasPair(*this), IdAliasPair(getActionable()), mState, mExecStartTime.system, waitingTime.count(), runningTime.count(), mProgress, mStatusMessages, mRunningParams, mExecutionDetails, result, mErrorInfo);
}


//------------------------------------------------------------------------------------
void Command::setProgress(float aProgress)
{
  if (aProgress < 0. or aProgress > 1.) {
    std::ostringstream err;
    err << "Progress must be in the [0.,1.] range. " << aProgress;
    SWATCH_THROW(OutOfRange(err.str()));
  }

  std::unique_lock<std::mutex> lock(mMutex);
  mProgress = aProgress;
}


//------------------------------------------------------------------------------------
void Command::setProgress(float aProgress, const std::string& aMsg)
{
  if (aProgress < 0. or aProgress > 1.) {
    std::ostringstream lExc;
    lExc << "Progress must be in the [0.,1.] range. " << aProgress;
    // TODO: should this not throw a SWATCH exception?
    SWATCH_THROW(OutOfRange(lExc.str()));
  }

  std::unique_lock<std::mutex> lock(mMutex);
  mProgress = aProgress;
  const std::chrono::duration<float> lTimeSinceStart(core::SteadyTimePoint_t::clock::now() - mExecStartTime.steady);
  mStatusMessages.push_back({ lTimeSinceStart.count(), aMsg });

  LOG4CPLUS_DEBUG(getActionable().getLogger(), "Command '" << getId() << "' : " << aMsg);
}


//------------------------------------------------------------------------------------
void Command::setStatusMsg(const std::string& aMsg)
{
  std::unique_lock<std::mutex> lock(mMutex);
  const std::chrono::duration<float> lTimeSinceStart(core::SteadyTimePoint_t::clock::now() - mExecStartTime.steady);
  mStatusMessages.push_back({ lTimeSinceStart.count(), aMsg });

  LOG4CPLUS_DEBUG(getActionable().getLogger(), "Command '" << getId() << "' : " << aMsg);
}


//------------------------------------------------------------------------------------
void Command::unregisterParameter(const std::string aName)
{
  if (getPath() != getId()) {
    // The Command has already been registered. Parameters list cannot be modified
    SWATCH_THROW(CommandParameterRegistrationFailed("Revoking parameter outside constructors is not allowed"));
  }

  for (const auto& lConstr : mConstraints) {
    std::set<std::string> lDependants = lConstr.second->getParameterNames();
    if (std::find(lDependants.begin(), lDependants.end(), aName) == lDependants.end())
      continue;
    std::ostringstream lExc;
    lExc << "Command '" << getId() << "' - Parameter '" << aName << "' is used by constraint "
         << lConstr.first << "[" << *(lConstr.second) << "]. The constraint must be removed before the parameter.";

    SWATCH_THROW(CommandParameterRegistrationFailed(lExc.str()));
  }

  mDefaultParams.erase(aName);
  mParameters.erase(aName);
}


//------------------------------------------------------------------------------------
void Command::removeConstraint(const std::string& aName)
{
  if (getPath() != getId()) {
    // The Command has already been registered. Parameters list cannot be modified
    SWATCH_THROW(CommandConstraintRegistrationFailed("Revoking constraints outside constructors is not allowed"));
  }

  if (mConstraints.erase(aName) == 0) {
    SWATCH_THROW(CommandConstraintRegistrationFailed("Constraint removal failed: Constraint '" + aName + "' not found"));
  }
}


//------------------------------------------------------------------------------------
const Command::ConstraintMap_t& Command::getConstraints() const
{
  return mConstraints;
}


//------------------------------------------------------------------------------------
Command::ParamRuleViolation::ParamRuleViolation(const std::vector<std::string>& aParams, const std::string& aDescription, const std::string& aDetails) :
  parameters(aParams),
  ruleDescription(aDescription),
  details(aDetails)
{
}

//------------------------------------------------------------------------------------
void Command::checkRulesAndConstraints(const core::ParameterSet& aParams, std::vector<ParamRuleViolation>& aRuleViolations) const
{
  for (const auto& lParName : aParams.keys()) {
    const core::AbstractRule& lRule = *mParameters.at(lParName).rule;
    if (!(aParams.getType(lParName) == lRule.type())) {
      SWATCH_THROW(core::InvalidArgument("C++ type '" + core::demangleName(aParams.getType(lParName).name()) + "' of parameter '" + lParName + "' received for command '" + this->getActionable().getId() + "." + getId() + " does not match registered type '" + core::demangleName(lRule.type().name()) + "'"));
    }

    core::Match lResult = lRule(aParams.get(lParName));
    if (!lResult.ok)
      aRuleViolations.push_back(ParamRuleViolation({ lParName }, boost::lexical_cast<std::string>(lRule), lResult.details));
  }

  for (const auto& lConstrPair : mConstraints) {

    core::Match lResult = (*lConstrPair.second)(aParams);
    if (lResult.ok)
      continue;

    aRuleViolations.push_back(ParamRuleViolation({}, boost::lexical_cast<std::string>(*lConstrPair.second), lResult.details));
    for (const std::string& lParName : lConstrPair.second->getParameterNames())
      aRuleViolations.back().parameters.push_back(lParName);
  }
}


//------------------------------------------------------------------------------------
const Command::ParameterMap_t& Command::getParameters() const
{
  return mParameters;
}


//------------------------------------------------------------------------------------
const core::ParameterSet& Command::getDefaultParams() const
{
  return mDefaultParams;
}


//------------------------------------------------------------------------------------
const boost::any& Command::getDefaultResult() const
{
  return mDefaultResult;
}


//------------------------------------------------------------------------------------
core::ParameterSet Command::mergeParametersWithDefaults(const core::ParameterSet& aParams) const
{
  core::ParameterSet merged = core::ParameterSet(aParams);

  std::set<std::string> keys = mDefaultParams.keys();
  for (const std::string& k : keys) {
    if (!merged.has(k))
      merged.adopt(k, mDefaultParams);
  }

  return merged;
}


void Command::enableMonitoring(const std::string& aId)
{
  getActionable().getObj<core::MonitorableObject>(aId).setMonitoringStatus(core::monitoring::kEnabled);
}


void Command::enableMonitoring(const std::vector<std::string>& aIds)
{
  for (const std::string& x : aIds)
    getActionable().getObj<core::MonitorableObject>(x).setMonitoringStatus(core::monitoring::kEnabled);
}


void Command::enableMonitoring(const std::string& aComponentId, const std::vector<std::string>& aSubComponentIds)
{
  core::MonitorableObject& lComponent = getActionable().getObj<core::MonitorableObject>(aComponentId);
  for (const std::string& x : aSubComponentIds)
    lComponent.getObj<core::MonitorableObject>(x).setMonitoringStatus(core::monitoring::kEnabled);
}


void Command::disableMonitoring(const std::string& aId)
{
  getActionable().getObj<core::MonitorableObject>(aId).setMonitoringStatus(core::monitoring::kDisabled);
}


void Command::disableMonitoring(const std::vector<std::string>& aIds)
{
  for (const std::string& x : aIds)
    getActionable().getObj<core::MonitorableObject>(x).setMonitoringStatus(core::monitoring::kDisabled);
}


void Command::disableMonitoring(const std::string& aComponentId, const std::vector<std::string>& aSubComponentIds)
{
  core::MonitorableObject& lComponent = getActionable().getObj<core::MonitorableObject>(aComponentId);
  for (const std::string& x : aSubComponentIds)
    lComponent.getObj<core::MonitorableObject>(x).setMonitoringStatus(core::monitoring::kDisabled);
}


//------------------------------------------------------------------------------------
CommandSnapshot::CommandSnapshot(const IdAliasPair& aCommand, const IdAliasPair& aActionable, State aState, const core::SystemTimePoint_t& aStartTime, float aWaitingTime, float aRunningTime, float aProgress, const std::vector<std::pair<float, std::string>>& aMessages, const core::ParameterSet& aParams, const core::ParameterSet& aExecutionDetails, const std::shared_ptr<const boost::any>& aResult, const std::shared_ptr<const core::ErrorInfo>& aErrorInfo) :
  ActionSnapshot(aCommand, aActionable, aState, aStartTime, aWaitingTime, aRunningTime),
  mProgress(aProgress),
  mMessages(aMessages),
  mParams(aParams),
  mExecutionDetails(aExecutionDetails),
  mResult(aResult),
  mErrorInfo(aErrorInfo)
{
}


//------------------------------------------------------------------------------------
float CommandSnapshot::getProgress() const
{
  return mProgress;
}


//------------------------------------------------------------------------------------
const std::vector<std::pair<float, std::string>>& CommandSnapshot::getMessages() const
{
  return mMessages;
}


//------------------------------------------------------------------------------------
const core::ParameterSet& CommandSnapshot::getParameters() const
{
  return mParams;
}


//------------------------------------------------------------------------------------
const core::ParameterSet& CommandSnapshot::getExecutionDetails() const
{
  return mExecutionDetails;
}

//------------------------------------------------------------------------------------
const boost::any& CommandSnapshot::getResult() const
{
  return *mResult;
}

//------------------------------------------------------------------------------------
const std::shared_ptr<const core::ErrorInfo>& CommandSnapshot::getErrorInfo() const
{
  return mErrorInfo;
}


} // namespace action
} // namespace swatch
