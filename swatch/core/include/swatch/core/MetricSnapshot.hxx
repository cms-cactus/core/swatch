/**
 * @file    MetricSnapshot.hxx
 * @author  Tom Williams
 * @date    May 2016
 */

#ifndef __SWATCH_CORE_METRICSNAPSHOT_HXX__
#define __SWATCH_CORE_METRICSNAPSHOT_HXX__


// IWYU pragma: private, include "swatch/core/MetricSnapshot.hpp"


#include <typeinfo>

#include "swatch/core/utilities.hpp"


namespace swatch {
namespace core {


template <typename DataType>
MetricSnapshot::MetricSnapshot(const std::string& aIdPath,
                               const std::string& aAlias,
                               swatch::core::StatusFlag flag,
                               const DataType& aValue,
                               const TimePoint& updateTime,
                               std::shared_ptr<AbstractMetricCondition> errCond,
                               boost::optional<TimePoint> aErrorStartTime,
                               boost::optional<std::chrono::seconds> aMinDurationForError,
                               std::shared_ptr<AbstractMetricCondition> warnCond,
                               boost::optional<TimePoint> aWarnStartTime,
                               boost::optional<std::chrono::seconds> aMinDurationForWarning,
                               swatch::core::monitoring::Status m_status) :
  mIdPath(aIdPath),
  mAlias(aAlias),
  mFlag(flag),
  mValue(aValue),
  mType(&typeid(DataType)),
  mUpdateTime(updateTime),
  mErrorCondition(errCond),
  mWarnCondition(warnCond),
  mErrorStartTime(aErrorStartTime),
  mWarningStartTime(aWarnStartTime),
  mMinDurationForError(aMinDurationForError),
  mMinDurationForWarning(aMinDurationForWarning),
  mMonitoringStatus(m_status),
  mValueStringConverter(&convertValueToString<DataType>)
{
}


template <typename DataType>
MetricSnapshot::MetricSnapshot(const std::string& aIdPath,
                               const std::string& aAlias,
                               swatch::core::StatusFlag aFlag,
                               Type<DataType>,
                               const TimePoint& aUpdateTime,
                               std::shared_ptr<AbstractMetricCondition> aErrCond,
                               boost::optional<TimePoint> aErrorStartTime,
                               boost::optional<std::chrono::seconds> aMinDurationForError,
                               std::shared_ptr<AbstractMetricCondition> aWarnCond,
                               boost::optional<TimePoint> aWarnStartTime,
                               boost::optional<std::chrono::seconds> aMinDurationForWarning,
                               swatch::core::monitoring::Status aMonStatus) :
  mIdPath(aIdPath),
  mAlias(aAlias),
  mFlag(aFlag),
  mValue(),
  mType(&typeid(DataType)),
  mUpdateTime(aUpdateTime),
  mErrorCondition(aErrCond),
  mWarnCondition(aWarnCond),
  mErrorStartTime(aErrorStartTime),
  mWarningStartTime(aWarnStartTime),
  mMinDurationForError(aMinDurationForError),
  mMinDurationForWarning(aMinDurationForWarning),
  mMonitoringStatus(aMonStatus)
{
}


template <typename DataType>
DataType MetricSnapshot::getValue() const
{
  if (mValue.empty())
    SWATCH_THROW(MetricValueNotKnown("Value of metric '" + getPath() + "' is not known"));
  else {
    try {
      return boost::any_cast<DataType>(mValue);
    }
    catch (const boost::bad_any_cast& lExc) {
      SWATCH_THROW(MetricValueFailedCast("Cannot cast value of metric '" + getPath() + "' from type '" + swatch::core::demangleName(mValue.type().name()) + "' to type '" + swatch::core::demangleName(typeid(DataType).name()) + "'"));
    }
  }
}


template <typename DataType>
std::string MetricSnapshot::convertValueToString(const boost::any& aValue)
{
  std::ostringstream lOSStream;
  lOSStream << boost::any_cast<DataType>(aValue);
  return lOSStream.str();
}

} // namespace core
} // namespace swatch



#endif /* __SWATCH_CORE_METRICSNAPSHOT_HXX__ */
