
#ifndef __SWATCH_CORE_ERRORINFO_HPP__
#define __SWATCH_CORE_ERRORINFO_HPP__


#include <exception>
#include <memory>
#include <stddef.h>
#include <string>
#include <typeinfo>


namespace swatch {
namespace core {

struct ErrorInfo {
  struct Location {
    std::string function;
    std::string file;
    size_t line;
  };

  ErrorInfo(const std::exception&);

  const std::type_info& type;
  const std::string message;
  const std::shared_ptr<const Location> location;
  const std::shared_ptr<const ErrorInfo> innerError;

private:
  static const std::type_info& extractType(const std::exception&);
  static std::shared_ptr<const Location> extractLocation(const std::exception&);
  static std::shared_ptr<const ErrorInfo> extractNestedException(const std::exception&);
};

} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_ERRORINFO_HPP__ */
