/**
 * @file    BaseMetric.hxx
 * @author  Tom Williams
 * @date    July 2015
 */

#ifndef __SWATCH_CORE_METRIC_HXX__
#define __SWATCH_CORE_METRIC_HXX__


// IWYU pragma: private, include "swatch/core/BaseMetric.hpp"

#include <exception>
#include <mutex>

#include "swatch/core/MetricSnapshot.hpp"


namespace swatch {
namespace core {
namespace detail {


template <typename DataType>
template <class ConditionType>
void _Metric<DataType>::setErrorCondition(const ConditionType& aErrorCondition)
{
  static_assert((std::is_base_of<MetricCondition<DataType>, ConditionType>::value), "class ConditionType must be a descendant of MetricCondtion<DataType>");

  std::lock_guard<std::mutex> lLock(mMutex);
  mErrorCondition.reset(new ConditionType(aErrorCondition));
  mMinDurationForError = boost::none;
}


template <typename DataType>
template <class ConditionType>
void _Metric<DataType>::setErrorCondition(const ConditionType& aErrorCondition, size_t aMinDurationInSeconds)
{
  static_assert((std::is_base_of<MetricCondition<DataType>, ConditionType>::value), "class ConditionType must be a descendant of MetricCondtion<DataType>");

  std::lock_guard<std::mutex> lLock(mMutex);
  mErrorCondition.reset(new ConditionType(aErrorCondition));
  mMinDurationForError = std::chrono::seconds(aMinDurationInSeconds);
}


template <typename DataType>
template <class ConditionType>
void _Metric<DataType>::setWarningCondition(const ConditionType& aWarningCondition)
{
  static_assert((std::is_base_of<MetricCondition<DataType>, ConditionType>::value), "class ConditionType must be a descendant of MetricCondtion<DataType>");

  std::lock_guard<std::mutex> lLock(mMutex);
  mWarnCondition.reset(new ConditionType(aWarningCondition));
  mMinDurationForWarning = boost::none;
}


template <typename DataType>
template <class ConditionType>
void _Metric<DataType>::setWarningCondition(const ConditionType& aWarningCondition, size_t aMinDurationInSeconds)
{
  static_assert((std::is_base_of<MetricCondition<DataType>, ConditionType>::value), "class ConditionType must be a descendant of MetricCondtion<DataType>");

  std::lock_guard<std::mutex> lLock(mMutex);
  mWarnCondition.reset(new ConditionType(aWarningCondition));
  mMinDurationForWarning = std::chrono::seconds(aMinDurationInSeconds);
}


} // namespace detail
} // namespace core
} // namespace swatch



#endif /* __SWATCH_CORE_METRIC_HXX__ */
