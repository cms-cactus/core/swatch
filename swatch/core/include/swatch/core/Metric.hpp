/**
 * @file    BaseMetric.hpp
 * @author  Tom Williams
 * @date    July 2015
 */

#ifndef __SWATCH_CORE_METRIC_HPP__
#define __SWATCH_CORE_METRIC_HPP__


#include <mutex>
#include <stddef.h> // for NULL
#include <string> // for string
#include <sys/time.h> // for timeval

#include "boost/optional.hpp"

#include "swatch/core/AbstractMetric.hpp"
#include "swatch/core/format.hpp"
#include "swatch/core/fwd.hpp"
#include "swatch/core/monitoring/Status.hpp" // for monitoring::Status


namespace swatch {
namespace core {

class MonitorableObject;

template <typename DataType>
class MetricCondition;

namespace detail {

/**
 * @brief      Metric encapsulating monitoring data of given type.
 *
 * @tparam     DataType  type of the monitoring data.
 */
template <typename DataType>
class _Metric : public AbstractMetric {

protected:
  /*!
   * @brief      Construct a metric with no error/warning limits
   *
   * @param[in]  aId   Id of the new metric
   * @param[in]  aId   Alias for the new metric (no restrictions on characters)
   */
  _Metric(const std::string& aId, const std::string& aAlias);

  /*!
   * @brief      Construct the Metric
   *
   * @param[in]  aId              A identifier
   * @param[in]  aId              Alias for the new metric (no restrictions on characters)
   * @param      aErrorCondition  Functor used to determine if the metric's
   *                              value indicates an error; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no error condition.
   * @param      aWarnCondition   Functor used to determine if the metric's
   *                              value indicates a warning; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no warning condition.
   */
  _Metric(const std::string& aId, const std::string& aAlias, MetricCondition<DataType>* aErrorCondition, MetricCondition<DataType>* aWarnCondition = NULL);

  virtual ~_Metric();

public:
  /**
   * @brief      Returns metric value, status flag, and error/warning
   *             conditions, within a swatch::core::MetricSnapshot object.
   *             (THREAD SAFE)
   *
   * @return     The snapshot.
   */
  MetricSnapshot getSnapshot() const;

  /**
   * @brief      Returns the status flag and monitoring mask/setting
   *
   * @return     The status.
   */
  std::pair<StatusFlag, monitoring::Status> getStatus() const;

  /**
   * @brief      Returns time at which metric's value was last updated. (THREAD
   *             SAFE)
   *
   * @return     The update time.
   */
  SteadyTimePoint_t getUpdateTime() const;

  /**
   * @brief      Sets the monitoring status of this metric.
   *
   * @param[in]  aStatus  A monitoring status object.
   */
  void setMonitoringStatus(monitoring::Status aStatus);

  void resetHistory();

private:
  std::pair<StatusFlag, monitoring::Status> getStatus(const std::lock_guard<std::mutex>& aGuard) const;

  //! Set the value of the metric to being unknown
  void setValueUnknown();

  template <class ConditionType>
  void setErrorCondition(const ConditionType& aErrorCondition);

  template <class ConditionType>
  void setErrorCondition(const ConditionType& aErrorCondition, size_t aMinDurationInSeconds);

  template <class ConditionType>
  void setWarningCondition(const ConditionType& aWarningCondition);

  template <class ConditionType>
  void setWarningCondition(const ConditionType& aWarningCondition, size_t aMinDurationInSeconds);

  void addDependantMetric(const AbstractMetric& aMetric, const std::function<void()>& aUpdateFunction)
  {
    mDependantMetrics.push_back(std::make_pair(&aMetric, aUpdateFunction));
  }

protected:
  const std::vector<std::pair<const AbstractMetric*, std::function<void()>>>& getDependantMetrics() const
  {
    return mDependantMetrics;
  }

  // TODO: Maybe eventually update to read-write mutex if needed ???
  //! Mutex used to stop corruption of value_
  mutable std::mutex mMutex;

  //! Latest retrieved value of metric; set to NULL if metric value is unknown.
  std::shared_ptr<const DataType> mValue;

  TimePoint mUpdateTime;

  //! Time point since which warning/error conditions have been continuously met
  boost::optional<TimePoint> mErrorStartTime, mWarningStartTime;

  std::shared_ptr<MetricCondition<DataType>> mErrorCondition;
  std::shared_ptr<MetricCondition<DataType>> mWarnCondition;

private:
  boost::optional<std::chrono::seconds> mMinDurationForError;
  boost::optional<std::chrono::seconds> mMinDurationForWarning;

  swatch::core::monitoring::Status mMonitoringStatus;

  std::vector<std::pair<const AbstractMetric*, std::function<void()>>> mDependantMetrics;
  friend class core::MonitorableObject;
};

} // namespace detail


////////////////////////////////
//   GENERIC CLASS TEMPLATE   //
////////////////////////////////

/**
 * @brief      Metric encapsulating monitoring data of given type.
 *
 * @tparam     DataType  type of the monitoring data.
 */
template <typename DataType, class Enable>
class Metric : public detail::_Metric<DataType> {
public:
  /*!
   * @brief      Construct a metric with no error/warning limits
   *
   * @param[in]  aId   Id of the new metric
   * @param[in]  aId   Alias for the new metric (no restrictions on characters)
   */
  Metric(const std::string& aId, const std::string& aAlias);

  /*!
   * @brief      Construct the Metric
   *
   * @param[in]  aId              A identifier
   * @param[in]  aId              Alias for the new metric (no restrictions on characters)
   * @param      aErrorCondition  Functor used to determine if the metric's
   *                              value indicates an error; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no error condition.
   * @param      aWarnCondition   Functor used to determine if the metric's
   *                              value indicates a warning; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no warning condition.
   */
  Metric(const std::string& aId, const std::string& aAlias, MetricCondition<DataType>* aErrorCondition, MetricCondition<DataType>* aWarnCondition = NULL);

  virtual ~Metric();
};


/////////////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Floating point types   //
/////////////////////////////////////////////////////////////

/**
 * @brief      Metric encapsulating monitoring data of given type.
 *
 * @tparam     DataType  type of the monitoring data.
 */
template <typename DataType>
class Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type> : public detail::_Metric<DataType> {
public:
  /*!
   * @brief      Construct a metric with no error/warning limits
   *
   * @param[in]  aId   Id of the new metric
   * @param[in]  aId   Alias for the new metric (no restrictions on characters)
   */
  Metric(const std::string& aId, const std::string& aAlias);

  /*!
   * @brief      Construct the Metric
   *
   * @param[in]  aId              A identifier
   * @param[in]  aId              Alias for the new metric (no restrictions on characters)
   * @param      aErrorCondition  Functor used to determine if the metric's
   *                              value indicates an error; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no error condition.
   * @param      aWarnCondition   Functor used to determine if the metric's
   *                              value indicates a warning; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no warning condition.
   */
  Metric(const std::string& aId, const std::string& aAlias, MetricCondition<DataType>* aErrorCondition, MetricCondition<DataType>* aWarnCondition = NULL);

  virtual ~Metric();

  const std::string& getUnit() const;

  format::FloatingPointNotation getNotation() const;

  boost::optional<size_t> getPrecision() const;

  //! Sets unit if not already set, otherwise throws
  void setUnit(const std::string&);

  //! Sets precision if not already set, otherwise throws
  void setFormat(core::format::FloatingPointNotation);

  //! Sets precision and notation if not already set, otherwise throws
  void setFormat(core::format::FloatingPointNotation, const size_t);

private:
  std::string mUnit;

  boost::optional<std::pair<format::FloatingPointNotation, boost::optional<size_t>>> mFormat;

  static const std::pair<format::FloatingPointNotation, boost::optional<size_t>> kDefaultFormat;
};


///////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Integral types   //
///////////////////////////////////////////////////////

/**
 * @brief      Metric encapsulating monitoring data of given type.
 *
 * @tparam     DataType  type of the monitoring data.
 */
template <typename DataType>
class Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type> : public detail::_Metric<DataType> {
public:
  /*!
   * @brief      Construct a metric with no error/warning limits
   *
   * @param[in]  aId   Id of the new metric
   * @param[in]  aId   Alias for the new metric (no restrictions on characters)
   */
  Metric(const std::string& aId, const std::string& aAlias);

  /*!
   * @brief      Construct the Metric
   *
   * @param[in]  aId              A identifier
   * @param[in]  aId              Alias for the new metric (no restrictions on characters)
   * @param      aErrorCondition  Functor used to determine if the metric's
   *                              value indicates an error; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no error condition.
   * @param      aWarnCondition   Functor used to determine if the metric's
   *                              value indicates a warning; the Metric takes
   *                              ownership of this condition object. NULL value
   *                              represents no warning condition.
   */
  Metric(const std::string& aId, const std::string& aAlias, MetricCondition<DataType>* aErrorCondition, MetricCondition<DataType>* aWarnCondition = NULL);

  virtual ~Metric();

  const std::string& getUnit() const;

  format::Base getBase() const;

  boost::optional<size_t> getWidth() const;

  char getFillChar() const;

  //! Sets unit if not already set, otherwise throws
  void setUnit(const std::string&);

  //! Sets base if not already set, otherwise throws
  void setFormat(format::Base);

  //! Sets base and width if not already set, otherwise throws
  void setFormat(format::Base, const size_t, const char);

private:
  std::string mUnit;

  boost::optional<std::tuple<format::Base, boost::optional<size_t>, char>> mFormat;

  static const std::tuple<format::Base, boost::optional<size_t>, const char> kDefaultFormat;
};


/**
 * Metric condition for monitoring data of given type
 *
 * @tparam DataType type of the monitoring data
 */
template <typename DataType>
class MetricCondition : public AbstractMetricCondition {
public:
  MetricCondition();

  virtual ~MetricCondition();

  virtual bool operator()(const DataType&) const = 0;
};


SWATCH_DEFINE_EXCEPTION(MetricFieldAlreadySet)


} // namespace core
} // namespace swatch



#include "swatch/core/Metric.hxx"

#endif /* __SWATCH_CORE_METRIC_HPP__ */
