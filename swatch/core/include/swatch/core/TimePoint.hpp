
#ifndef __SWATCH_CORE_TIMEPOINT_HPP__
#define __SWATCH_CORE_TIMEPOINT_HPP__


#include <chrono>


namespace swatch {
namespace core {


typedef std::chrono::steady_clock::time_point SteadyTimePoint_t;
typedef std::chrono::system_clock::time_point SystemTimePoint_t;
typedef std::chrono::system_clock::duration Duration_t;


struct TimePoint {
public:
  TimePoint();
  ~TimePoint();

  static TimePoint now();

  SteadyTimePoint_t steady;
  SystemTimePoint_t system;
};


} // end ns: core
} // end ns: swatch

#endif /* __SWATCH_CORE_TIMEPOINT_HPP__ */
