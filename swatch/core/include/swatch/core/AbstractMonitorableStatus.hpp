/**
 * @file    AbstractMonitorableStatus.hpp
 * @author  Tom Williams
 * @date    November 2015
 */

#ifndef __SWATCH_CORE_ABSTRACTMONITORABLESTATUS_HPP__
#define __SWATCH_CORE_ABSTRACTMONITORABLESTATUS_HPP__


#include <mutex>

#include "swatch/core/fwd.hpp"


namespace swatch {
namespace core {


class AbstractMonitorableStatus {
protected:
  AbstractMonitorableStatus();

public:
  AbstractMonitorableStatus(const AbstractMonitorableStatus&) = delete;
  AbstractMonitorableStatus& operator=(const AbstractMonitorableStatus&) = delete;

  virtual ~AbstractMonitorableStatus();

  virtual bool isUpdatingMetrics(const MonitorableStatusGuard& aGuard) const = 0;

  virtual void waitUntilReadyToUpdateMetrics(MonitorableStatusGuard& aGuard) = 0;

  virtual void finishedUpdatingMetrics(const MonitorableStatusGuard& aGuard) = 0;

  virtual void waitUntilReadyToReadMetrics(MonitorableStatusGuard& aGuard) = 0;

  virtual void finishedReadingMetrics(const MonitorableStatusGuard& aGuard) = 0;

protected:
  std::unique_lock<std::mutex>& getUniqueLock(MonitorableStatusGuard& aGuard) const;

protected:
  mutable std::mutex mMutex;

  friend class MonitorableStatusGuard;
};


class MonitorableStatusGuard {
public:
  MonitorableStatusGuard(const AbstractMonitorableStatus& aStatus);
  MonitorableStatusGuard(const AbstractMonitorableStatus& aStatus, std::adopt_lock_t);
  virtual ~MonitorableStatusGuard();

  bool isCorrectGuard(const AbstractMonitorableStatus& aStatus) const;

private:
  MonitorableStatusGuard(); // no default CTOR
  MonitorableStatusGuard(const MonitorableStatusGuard&); // non-copyable
  MonitorableStatusGuard& operator=(const MonitorableStatusGuard& aGuard); // non-assignable

  const AbstractMonitorableStatus& mStatus;
  std::unique_lock<std::mutex> mLockGuard;

  friend class AbstractMonitorableStatus;
};


} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_ABSTRACTMONITORABLESTATUS_HPP__ */
