/**
 * @file    MetricSnapshot.hpp
 * @author  Tom Williams
 * @date    May 2016
 */

#ifndef __SWATCH_CORE_METRICSNAPSHOT_HPP__
#define __SWATCH_CORE_METRICSNAPSHOT_HPP__


#include <functional>
#include <memory>
#include <string>
#include <sys/time.h>
#include <typeinfo>

#include "boost/any.hpp"
#include "boost/optional.hpp"

#include "swatch/core/LeafObject.hpp"
#include "swatch/core/StatusFlag.hpp"
#include "swatch/core/TTSUtils.hpp"
#include "swatch/core/TimePoint.hpp"
#include "swatch/core/exception.hpp"
#include "swatch/core/fwd.hpp"
#include "swatch/core/monitoring/Status.hpp"


namespace swatch {
namespace core {

class MetricSnapshot {
public:
  template <typename T>
  struct Type {
  };

  template <typename T>
  MetricSnapshot(const std::string& aIdPath, const std::string& aAlias,
                 swatch::core::StatusFlag, const T& aValue,
                 const TimePoint&,
                 std::shared_ptr<AbstractMetricCondition> aErrCond,
                 boost::optional<TimePoint> aErrorStartTime,
                 boost::optional<std::chrono::seconds> aMinDurationForError,
                 std::shared_ptr<AbstractMetricCondition> aWarnCond,
                 boost::optional<TimePoint> aWarnStartTime,
                 boost::optional<std::chrono::seconds> aMinDurationForWarning,
                 swatch::core::monitoring::Status aMonStatus = monitoring::kEnabled);

  template <typename T>
  MetricSnapshot(const std::string& aIdPath, const std::string& aAlias,
                 swatch::core::StatusFlag, Type<T> aType,
                 const TimePoint&,
                 std::shared_ptr<AbstractMetricCondition> aErrCond,
                 boost::optional<TimePoint> aErrorStartTime,
                 boost::optional<std::chrono::seconds> aMinDurationForError,
                 std::shared_ptr<AbstractMetricCondition> aWarnCond,
                 boost::optional<TimePoint> aWarnStartTime,
                 boost::optional<std::chrono::seconds> aMinDurationForWarning,
                 swatch::core::monitoring::Status aMonStatus = monitoring::kEnabled);

  //! Returns metric's ID path
  const std::string& getPath() const;

  //! Returns metric's ID
  std::string getMetricId() const;

  //! Returns alias to metric
  const std::string& getAlias() const;

  //! Returns ID path of monitorable object (the metric's parent)
  std::string getMonitorableObjectPath() const;

  //! Returns ID string of monitorable object (the metric's parent)
  std::string getMonitorableObjectId() const;

  //! Returns status flag deduced from comparing the stored value with limits
  swatch::core::StatusFlag getStatusFlag() const;

  //! Returns whether or not value is known
  bool isValueKnown() const;

  /*!
   * @brief Returns the metric's value; throws if the value is not known, or if the incorrect type is given
   *
   * @throw MetricValueNotKnown If value is not known
   * @throw MetricValueFailedCast If value is known, but incorrect type given
   */
  template <typename DataType>
  DataType getValue() const;

  std::string getValueAsString() const;

  const std::type_info& getType() const;

  //! Returns time at which metric's value was last updated
  const TimePoint& getUpdateTimestamp() const;

  //! Returns metric's warning condition; NULL returned if metric doesn't have any warning condition
  const AbstractMetricCondition* getWarningCondition() const;

  //! Returns metric's error condition; NULL returned if metric doesn't have any error condition
  const AbstractMetricCondition* getErrorCondition() const;

  swatch::core::monitoring::Status getMonitoringStatus() const;

private:
  std::string mIdPath;
  std::string mAlias;
  swatch::core::StatusFlag mFlag;
  boost::any mValue;
  const std::type_info* mType;
  TimePoint mUpdateTime;
  std::shared_ptr<AbstractMetricCondition> mErrorCondition;
  std::shared_ptr<AbstractMetricCondition> mWarnCondition;

  //! Time point since which warning/error conditions have been continuously met
  boost::optional<TimePoint> mErrorStartTime, mWarningStartTime;
  boost::optional<std::chrono::seconds> mMinDurationForError;
  boost::optional<std::chrono::seconds> mMinDurationForWarning;

  swatch::core::monitoring::Status mMonitoringStatus;
  std::function<std::string(const boost::any&)> mValueStringConverter;

  template <typename DataType>
  static std::string convertValueToString(const boost::any& aValue);
};


template <>
std::string MetricSnapshot::convertValueToString<bool>(const boost::any& aValue);


SWATCH_DEFINE_EXCEPTION(MetricValueNotKnown)
SWATCH_DEFINE_EXCEPTION(MetricValueFailedCast)


} // namespace core
} // namespace swatch


#include "swatch/core/MetricSnapshot.hxx"


#endif /* __SWATCH_CORE_METRICSNAPSHOT_HPP__ */
