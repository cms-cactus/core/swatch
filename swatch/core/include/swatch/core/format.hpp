
#ifndef __SWATCH_CORE_FORMAT_HPP__
#define __SWATCH_CORE_FORMAT_HPP__


namespace swatch {
namespace core {
namespace format {


enum FloatingPointNotation {
  kFixedPoint,
  kScientific
};


enum Base {
  kBinary,
  kOctal,
  kHeximal,
  kDecimal
};


} // namespace format
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_FORMAT_HPP__ */
