#ifndef __SWATCH_CORE_RULES_ALL_HPP__
#define __SWATCH_CORE_RULES_ALL_HPP__


#include <memory>
#include <vector>

#include "swatch/core/Rule.hpp"
#include "swatch/core/utilities.hpp"


namespace swatch {
namespace core {
namespace rules {

template <typename T>
class All : public Rule<std::vector<T>> {
public:
  /**
   * @brief      All rule constructor
   *
   * @param[in]  aRule   Element rule
   *
   * @tparam     ElementRule    Element rule class
   */
  template <typename ElementRule>
  All(const ElementRule& aRule);

  /**
   * @brief      Copy constructor
   *
   * @param[in]  aOther  A copy of me
   */
  All(const All& aOther);

  virtual Match verify(const std::vector<T>& aValue) const final;

  const Rule<T>& getElementRule() const;

private:
  virtual void describe(std::ostream& aStream) const final;

  // Element rule
  std::unique_ptr<Rule<T>> mElementRule;

  //! Element rule cloner function to be used in the Copy Constructor
  RuleCloner_t mElementRuleCloner;
};


template <typename T>
template <typename ElementRule>
All<T>::All(const ElementRule& aRule)
{
  static_assert((std::is_base_of<Rule<T>, ElementRule>::value), "class ElementRule in All(const ElementRule& aRule) must be a descendant of Rule<T>");

  mElementRule = std::unique_ptr<Rule<T>>(new ElementRule(aRule));

  mElementRuleCloner = RuleCloner<ElementRule>;
}


template <typename T>
All<T>::All(const All& aOther) :
  mElementRule(dynamic_cast<Rule<T>*>((*aOther.mElementRuleCloner)(*aOther.mElementRule))),
  mElementRuleCloner(aOther.mElementRuleCloner)
{
}


template <typename T>
Match All<T>::verify(const std::vector<T>& aValue) const
{
  Match lMatch(true);

  // Apply the sub rules and merge details
  std::vector<std::string> lDetails;

  for (size_t i = 0; i < aValue.size(); i++) {
    Match lElementMatch = mElementRule->verify(aValue.at(i));
    lMatch.ok = lMatch.ok && lElementMatch.ok;
    if (not lElementMatch.details.empty())
      lDetails.push_back("Element " + std::to_string(i) + ": " + lElementMatch.details);
  }

  lMatch.details = core::join(lDetails, "; ");

  // Return the merged result
  return lMatch;
}


template <typename T>
const Rule<T>& All<T>::getElementRule() const
{
  return *mElementRule;
}


template <typename T>
void All<T>::describe(std::ostream& aStream) const
{
  aStream << "All(x, " << *mElementRule << ")";
}

} // namespace rules
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULES_ALL_HPP__ */
