#ifndef __SWATCH_CORE_RULES_FINITENUMBER_HXX__
#define __SWATCH_CORE_RULES_FINITENUMBER_HXX__


#include <ostream>
#include <type_traits>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
void FiniteNumber<T>::describe(std::ostream& aStream) const
{
  aStream << "isFinite(x)";
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_FINITENUMBER_HXX__ */