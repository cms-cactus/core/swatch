#ifndef __SWATCH_CORE_RULES_LESSERTHAN_HPP__
#define __SWATCH_CORE_RULES_LESSERTHAN_HPP__


#include "swatch/core/Rule.hpp"


namespace swatch {
namespace core {
namespace rules {

template <typename T>
class LesserThan : public Rule<T> {

public:
  LesserThan(const T& aUpperBound) :
    lUpperBound(aUpperBound) {}
  virtual ~LesserThan() {}

  /**
   * @brief      Checks if aValue is greater than .
   *
   * @param[in]  aValue  Input value
   *
   * @return     True if aValue is finite
   */
  virtual Match verify(const T& aValue) const;

  const T& getUpperBound() const;

private:
  virtual void describe(std::ostream& aStream) const;

  const T lUpperBound;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/LesserThan.hxx"


#endif /* __SWATCH_CORE_RULES_LESSERTHAN_HPP__ */