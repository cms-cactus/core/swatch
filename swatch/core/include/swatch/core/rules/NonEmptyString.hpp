#ifndef __SWATCH_CORE_RULES_NONEMPTYSTRING_HPP__
#define __SWATCH_CORE_RULES_NONEMPTYSTRING_HPP__


#include "swatch/core/Rule.hpp"


namespace xdata {
class Serializable;
}

namespace swatch {
namespace core {
namespace rules {

template <typename T>
class NonEmptyString : public Rule<T> {

public:
  NonEmptyString() {}
  ~NonEmptyString() {}

  virtual Match verify(const T& aValue) const;

  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return !aValue.value_.empty();
  }

  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return not aValue.empty();
  }

private:
  virtual void describe(std::ostream& aStream) const;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/NonEmptyString.hxx"


#endif /* __SWATCH_CORE_RULES_NONEMPTYSTRING_HPP__ */