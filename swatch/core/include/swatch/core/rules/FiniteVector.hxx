#ifndef __SWATCH_CORE_RULES_FINITEVECTOR_HXX__
#define __SWATCH_CORE_RULES_FINITEVECTOR_HXX__


#include <algorithm>
#include <ostream>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match FiniteVector<T>::verify(const T& aValue) const
{
  return verify<T>(aValue);
}

template <typename T>
void FiniteVector<T>::describe(std::ostream& aStream) const
{
  aStream << "all(x,isFinite)";
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_FINITEVECTOR_HXX__ */