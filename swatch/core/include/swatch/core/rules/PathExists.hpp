#ifndef __SWATCH_CORE_RULES_PATHEXISTS_HPP__
#define __SWATCH_CORE_RULES_PATHEXISTS_HPP__


#include "swatch/core/Rule.hpp"


namespace xdata {
class Serializable;
}

namespace swatch {
namespace core {
namespace rules {

template <typename T>
class PathExists : public Rule<T> {

public:
  PathExists(const std::string& aPrefix = "", const std::string& aExtension = "") :
    mPrefix(aPrefix),
    mExtension(aExtension)
  {
  }
  ~PathExists() {}

  virtual Match verify(const T& aValue) const;

private:
  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  static bool isEmpty(const U& aValue)
  {
    return aValue.value_.empty();
  }


  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  static bool isEmpty(const U& aValue)
  {
    return aValue.empty();
  }

  virtual void describe(std::ostream& aStream) const;

  const std::string mPrefix;
  const std::string mExtension;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/PathExists.hxx"


#endif /* __SWATCH_CORE_RULES_PATHEXISTS_HPP__ */