
#ifndef __SWATCH_CORE_FWD_HPP__
#define __SWATCH_CORE_FWD_HPP__


namespace swatch {
namespace core {

template <typename T>
class AbstractFactory;
class AbstractMetric;
class AbstractMetricCondition;
class AbstractMonitorableStatus;
class AbstractRule;
class AbstractStub;
template <typename A, typename D>
struct ClassRegistrationHelper;
template <typename DataType>
class ComplexMetric;
template <typename A, typename K>
struct CreatorRegistrationHelper;
struct ErrorInfo;
class Exception;
template <typename T>
class GenericView;
class LeafObject;
class Match;
template <typename DataType, class Enable = void>
class Metric;
template <typename DataType>
class MetricCondition;
class MetricReadGuard;
class MetricSnapshot;
class MetricUpdateGuard;
class MetricView;
class MonitorableObject;
class MonitorableObjectSnapshot;
class MonitorableStatusGuard;
class Object;
class ObjectView;
class ParameterSet;
template <typename T>
class Rule;
template <typename DataType>
class SimpleMetric;

} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_FWD_HPP__ */
