
#include "swatch/core/MonitorableObject.hpp"


#include <exception> // for exception
#include <stddef.h> // for NULL
#include <stdexcept> // for runtime_error, out_of_range
#include <sys/time.h> // for gettimeofday, timeval, etc
#include <typeinfo> // for type_info

// log4cplus headers
#include "log4cplus/logger.h" // for Logger
#include <log4cplus/loggingmacros.h>

// SWATCH headers
#include "swatch/core/AbstractMonitorableStatus.hpp"
#include "swatch/core/ErrorInfo.hpp"
#include "swatch/core/LeafObject.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/logger/Logger.hpp"


using namespace std;

namespace swatch {
namespace core {


MonitorableObjectSnapshot::MonitorableObjectSnapshot(const std::string& aPath,
                                                     swatch::core::StatusFlag aFlag,
                                                     swatch::core::monitoring::Status aMonStatus,
                                                     const Duration_t& aUpdateDuration,
                                                     const std::shared_ptr<const ErrorInfo>& aErrorInfo) :
  mPath(aPath),
  mFlag(aFlag),
  mMonitoringStatus(aMonStatus),
  mUpdateDuration(aUpdateDuration),
  mErrorInfo(aErrorInfo)
{
}


MonitorableObjectSnapshot::~MonitorableObjectSnapshot()
{
}


const std::string& MonitorableObjectSnapshot::getPath() const
{
  return mPath;
}


std::string MonitorableObjectSnapshot::getId() const
{
  std::size_t lIdxLastDot = mPath.rfind('.');
  if (lIdxLastDot == std::string::npos)
    return mPath;
  else
    return mPath.substr(lIdxLastDot + 1);
}


swatch::core::StatusFlag MonitorableObjectSnapshot::getStatusFlag() const
{
  return mFlag;
}


swatch::core::monitoring::Status MonitorableObjectSnapshot::getMonitoringStatus() const
{
  return mMonitoringStatus;
}


const Duration_t& MonitorableObjectSnapshot::getUpdateDuration() const
{
  return mUpdateDuration;
}


const std::shared_ptr<const ErrorInfo>& MonitorableObjectSnapshot::getErrorInfo() const
{
  return mErrorInfo;
}


// TODO (when migrated to full C++11) : Call other MonitorableObject constructor to minimise duplication
MonitorableObject::MonitorableObject(const std::string& aId) :
  Object(aId),
  mMetricsUpdatedByMe(),
  mMetrics(),
  mUpdateDuration(0),
  mUpdateError(),
  mMonitoringStatus(monitoring::kEnabled),
  mStatus(NULL),
  mLogger(swatch::logger::Logger::getInstance("swatch.core.MonitorableObject"))
{
}


MonitorableObject::MonitorableObject(const std::string& aId, const std::string& aAlias) :
  Object(aId, aAlias),
  mMetricsUpdatedByMe(),
  mMetrics(),
  mUpdateDuration(0),
  mUpdateError(),
  mMonitoringStatus(monitoring::kEnabled),
  mStatus(NULL),
  mLogger(swatch::logger::Logger::getInstance("swatch.core.MonitorableObject"))
{
}


MonitorableObject::~MonitorableObject()
{
  mMetricsUpdatedByMe.clear();
  mMetrics.clear();
}


std::vector<std::string> MonitorableObject::getMetrics() const
{
  std::vector<std::string> lNames;
  for (MetricMap_t::value_type p : mMetrics) {
    lNames.push_back(p.first);
  }
  return lNames;
}


const AbstractMetric& MonitorableObject::getMetric(const std::string& aId) const
{
  try {
    return *mMetrics.at(aId);
  }
  catch (const std::out_of_range& e) {
    SWATCH_THROW(MetricNotFoundInMonitorableObject("MonitorableObject \"" + getPath() + "\" does not contain metric of ID \"" + aId + "\""));
  }
}



AbstractMetric& MonitorableObject::getMetric(const std::string& aId)
{
  try {
    return *mMetrics.at(aId);
  }
  catch (const std::out_of_range& e) {
    SWATCH_THROW(MetricNotFoundInMonitorableObject("MonitorableObject \"" + getPath() + "\" does not contain metric of ID \"" + aId + "\""));
  }
}


StatusFlag MonitorableObject::getStatusFlag() const
{
  StatusFlag result = kNoLimit;

  // If this object is disabled, then return kNoLimit as status
  if (mMonitoringStatus == swatch::core::monitoring::kDisabled)
    return kNoLimit;

  for (auto lIt = mMonObjChildren.begin(); lIt != mMonObjChildren.end(); lIt++) {
    const MonitorableObject& lMonChild = *(lIt->second);
    // only enabled children contribute to the status
    if (lMonChild.getMonitoringStatus() == monitoring::kEnabled)
      result = result & lMonChild.getStatusFlag();
  }

  for (MetricMap_t::value_type p : mMetrics) {
    std::pair<StatusFlag, monitoring::Status> lMetricStatus = p.second->getStatus();
    // only enabled metrics contribute to the status
    if (lMetricStatus.second == monitoring::kEnabled)
      result = result & lMetricStatus.first;
  }

  return result;
}


MonitorableObjectSnapshot MonitorableObject::getStatus() const
{
  std::unique_lock<std::mutex> lUpdateErrorLock(mUpdateErrorMutex);
  return MonitorableObjectSnapshot(getPath(), getStatusFlag(), mMonitoringStatus, mUpdateDuration, mUpdateError);
}


void MonitorableObject::updateMetrics()
{
  MetricUpdateGuard lGuard(*this);

  updateMetrics(lGuard);
}


void MonitorableObject::updateMetrics(const MetricUpdateGuard& aGuard)
{
  if (!aGuard.isCorrectGuard(*this))
    SWATCH_THROW(RuntimeError("Metric write guard for incorrect object given to monitorable object '" + getId() + "'"));

  SteadyTimePoint_t startTime = SteadyTimePoint_t::clock::now();

  try {
    this->retrieveMetricValues();
    SteadyTimePoint_t endTime = SteadyTimePoint_t::clock::now();

    std::unique_lock<std::mutex> lUpdateErrorLock(mUpdateErrorMutex);
    mUpdateError.reset();
    mUpdateDuration = endTime - startTime;
  }
  catch (const std::exception& e) {
    SteadyTimePoint_t endTime = SteadyTimePoint_t::clock::now();
    LOG4CPLUS_WARN(mLogger, "Exception of type '" << demangleName(typeid(e).name()) << "' was thrown by retrieveMetricValues() method of monitorable object '" << this->getPath() << "'. Exception message: " << e.what());

    std::unique_lock<std::mutex> lUpdateErrorLock(mUpdateErrorMutex);
    mUpdateError.reset(new ErrorInfo(e));
    mUpdateDuration = endTime - startTime;
  }

  for (auto* lMetric : mMetricsUpdatedByMe) {
    // last update before start time equals failure
    bool failedUpdate = (lMetric->getUpdateTime() < startTime);
    bool isEnabled = lMetric->getStatus().second
        != monitoring::kDisabled;
    // only set the value to unknown for enabled metrics
    if (failedUpdate && isEnabled)
      lMetric->setValueUnknown();
  }

  for (auto lIt = mDependantMetrics.begin(); lIt != mDependantMetrics.end(); lIt++)
    lIt->second();
}


void MonitorableObject::setMonitoringStatus(const swatch::core::monitoring::Status aMonStatus)
{
  mMonitoringStatus = aMonStatus;
}


monitoring::Status MonitorableObject::getMonitoringStatus() const
{
  return mMonitoringStatus;
}


void MonitorableObject::resetMetricHistory()
{
  for (auto lObjChild : mMonObjChildren)
    lObjChild.second->resetMetricHistory();

  for (MetricMap_t::value_type p : mMetrics)
    p.second->resetHistory();
}


void MonitorableObject::addMonitorable(MonitorableObject* aMonObj)
{
  addObj(aMonObj);
  finishAddingMonitorable(aMonObj);
}


void MonitorableObject::delegateMetricUpdate(const std::string& aId, MonitorableObject& aMonObj)
{
  AbstractMetric& lMetric = getMetric(aId);

  const auto lIt = mMetricsUpdatedByMe.find(&lMetric);
  if (lIt == mMetricsUpdatedByMe.end())
    SWATCH_THROW(RuntimeError("Cannot delegate updates of metric '" + lMetric.getPath() + "' to '" + aMonObj.getPath() + "': Update has already been delegated"));

  if (not(aMonObj.isAncestorOf(*this) or this->isAncestorOf(aMonObj)))
    SWATCH_THROW(RuntimeError("Cannot delegate updates of metric '" + lMetric.getPath() + "' to '" + aMonObj.getPath() + "': Updater must be either an ancestor or descendant"));

  lMetric.mResponsibleIdPathIfDelegated = aMonObj.getPath();
  this->mMetricsUpdatedByMe.erase(lIt);
  aMonObj.mMetricsUpdatedByMe.insert(&lMetric);
}


void MonitorableObject::setMonitorableStatus(AbstractMonitorableStatus& aStatus, log4cplus::Logger& aLogger)
{
  if ((mStatus == NULL) || (mStatus == &aStatus)) {
    mStatus = &aStatus;
    mLogger = aLogger;
  }
  else
    SWATCH_THROW(RuntimeError("Status of monitorable object '" + getId() + "' has already been set"));
}


void MonitorableObject::finishAddingMonitorable(MonitorableObject* aMonObj)
{
  mMonObjChildren.insert(MonObjMap_t::value_type(aMonObj->getId(), aMonObj));

  // Set status of new child, and all its monitorable descendants
  // (use setStatus method to check that descendants aren't already using custom status instances defined by end user)
  if (mStatus != NULL) {
    aMonObj->setMonitorableStatus(*mStatus, mLogger);

    for (Object::iterator lIt = aMonObj->begin(); lIt != aMonObj->end(); lIt++) {
      if (MonitorableObject* lChildMonObj = dynamic_cast<MonitorableObject*>(&*lIt)) {
        lChildMonObj->setMonitorableStatus(*mStatus, mLogger);
        for (const auto* lMetric : lChildMonObj->mMetricsUpdatedByMe) {
          if (not isAncestorOf(*lMetric))
            SWATCH_THROW(RuntimeError("Updates for metric '" + lMetric->getPath() + "' were delegated to '" + lChildMonObj->getPath() + "', which is under a different resource"));
        }
      }
    }
  }
}


MetricUpdateGuard::MetricUpdateGuard(MonitorableObject& aMonObj) :
  mObjStatus(*aMonObj.mStatus)
{
  if (aMonObj.mStatus == NULL)
    SWATCH_THROW(RuntimeError("Status not defined for monitorable object " + aMonObj.getId()));

  MonitorableStatusGuard lLockGuard(mObjStatus);
  mObjStatus.waitUntilReadyToUpdateMetrics(lLockGuard);
}


MetricUpdateGuard::~MetricUpdateGuard()
{
  MonitorableStatusGuard lLockGuard(mObjStatus);
  mObjStatus.finishedUpdatingMetrics(lLockGuard);
}


bool MetricUpdateGuard::isCorrectGuard(const MonitorableObject& aMonObj) const
{
  return (aMonObj.mStatus == &mObjStatus);
}



MetricReadGuard::MetricReadGuard(const MonitorableObject& aMonObj) :
  mObjStatus(*aMonObj.mStatus)
{
  if (aMonObj.mStatus == NULL)
    SWATCH_THROW(RuntimeError("Status not defined for monitorable object " + aMonObj.getId()));

  MonitorableStatusGuard lLockGuard(mObjStatus);
  mObjStatus.waitUntilReadyToReadMetrics(lLockGuard);
}


MetricReadGuard::~MetricReadGuard()
{
  MonitorableStatusGuard lLockGuard(mObjStatus);
  mObjStatus.finishedReadingMetrics(lLockGuard);
}


bool MetricReadGuard::isCorrectGuard(const MonitorableObject& aMonObj) const
{
  return (aMonObj.mStatus == &mObjStatus);
}


}
}
