
#include "swatch/core/ErrorInfo.hpp"


#include "boost/exception/errinfo_nested_exception.hpp"
#include "boost/exception/error_info.hpp"
#include "boost/exception_ptr.hpp"


namespace swatch {
namespace core {


ErrorInfo::ErrorInfo(const std::exception& e) :
  type(extractType(e)),
  message(e.what()),
  location(extractLocation(e)),
  innerError(extractNestedException(e))
{
}


const std::type_info& ErrorInfo::extractType(const std::exception& e)
{
  if (boost::get_error_info<boost::original_exception_type>(e) != NULL)
    return **boost::get_error_info<boost::original_exception_type>(e);
  else
    return typeid(e);
}


std::shared_ptr<const ErrorInfo::Location> ErrorInfo::extractLocation(const std::exception& e)
{
  if ((boost::get_error_info<boost::throw_function>(e) == NULL) or (boost::get_error_info<boost::throw_file>(e) == NULL) or (boost::get_error_info<boost::throw_line>(e) == NULL))
    return NULL;
  else {
    Location lLocation;
    lLocation.function = *boost::get_error_info<boost::throw_function>(e);
    lLocation.file = *boost::get_error_info<boost::throw_file>(e);
    lLocation.line = *boost::get_error_info<boost::throw_line>(e);
    return std::make_shared<const Location>(lLocation);
  }
}


std::shared_ptr<const ErrorInfo> ErrorInfo::extractNestedException(const std::exception& e)
{
  if (boost::get_error_info<boost::errinfo_nested_exception>(e) == NULL)
    return NULL;
  else {
    try {
      boost::rethrow_exception(*boost::get_error_info<boost::errinfo_nested_exception>(e));
    }
    catch (const std::exception& lNestedException) {
      return std::make_shared<const ErrorInfo>(lNestedException);
    }
  }
}


} // namespace core
} // namespace swatch