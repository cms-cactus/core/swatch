
#include "swatch/core/Metric.hpp"


#include <ostream> // for ostringstream, etc


namespace swatch {
namespace core {
namespace detail {

template <typename DataType>
_Metric<DataType>::_Metric(const std::string& aId, const std::string& aAlias) :
  AbstractMetric(aId, aAlias),
  mValue((const DataType*)NULL),
  mUpdateTime(),
  mErrorCondition(),
  mWarnCondition(),
  mMonitoringStatus(monitoring::kEnabled)
{
}


template <typename DataType>
_Metric<DataType>::_Metric(const std::string& aId, const std::string& aAlias,
                           MetricCondition<DataType>* aErrorCondition,
                           MetricCondition<DataType>* aWarnCondition)
try : AbstractMetric(aId, aAlias),
    mValue((const DataType*)NULL),
    mUpdateTime(),
    mErrorCondition(aErrorCondition),
    mWarnCondition(aWarnCondition),
    mMonitoringStatus(monitoring::kEnabled) {
}
catch (...) {
  delete aErrorCondition;
  delete aWarnCondition;
  // (re)throw; is implicit in CTOR try-catch block
}


template <typename DataType>
_Metric<DataType>::~_Metric()
{
}


template <typename DataType>
MetricSnapshot _Metric<DataType>::getSnapshot() const
{
  std::lock_guard<std::mutex> lLock(mMutex);

  swatch::core::StatusFlag lFlag = getStatus(lLock).first;

  if (mValue != NULL)
    return MetricSnapshot(getPath(), getAlias(), lFlag, *mValue, mUpdateTime, mErrorCondition, mErrorStartTime, mMinDurationForError, mWarnCondition, mWarningStartTime, mMinDurationForWarning, mMonitoringStatus);
  else
    return MetricSnapshot(getPath(), getAlias(), lFlag, MetricSnapshot::Type<DataType>(), mUpdateTime, mErrorCondition, mErrorStartTime, mMinDurationForError, mWarnCondition, mWarningStartTime, mMinDurationForWarning, mMonitoringStatus);
}


template <typename DataType>
std::pair<StatusFlag, monitoring::Status> _Metric<DataType>::getStatus() const
{
  std::lock_guard<std::mutex> lLock(mMutex);
  return getStatus(lLock);
}


template <typename DataType>
std::pair<StatusFlag, monitoring::Status> _Metric<DataType>::getStatus(const std::lock_guard<std::mutex>& aLock) const
{
  if (mMonitoringStatus == monitoring::kDisabled)
    return std::make_pair(kNoLimit, mMonitoringStatus); // disabled metrics always return kNoLimit

  if (this->mValue != NULL) {
    if ((mErrorCondition == NULL) and (mWarnCondition == NULL))
      return std::make_pair(kNoLimit, mMonitoringStatus);

    if (mErrorCondition and (*mErrorCondition)(*mValue)) {
      if (not mMinDurationForError)
        return std::make_pair(kError, mMonitoringStatus);
      else if (mErrorStartTime and ((this->mUpdateTime.steady - mErrorStartTime->steady) >= *mMinDurationForError))
        return std::make_pair(kError, mMonitoringStatus);
    }

    if (mWarnCondition and (*mWarnCondition)(*mValue)) {
      if (not mMinDurationForWarning)
        return std::make_pair(kWarning, mMonitoringStatus);
      else if (mWarningStartTime and ((this->mUpdateTime.steady - mWarningStartTime->steady) >= *mMinDurationForWarning))
        return std::make_pair(kWarning, mMonitoringStatus);
    }

    return std::make_pair(kGood, mMonitoringStatus);
  }

  return std::make_pair(kUnknown, mMonitoringStatus);
}


template <typename DataType>
SteadyTimePoint_t _Metric<DataType>::getUpdateTime() const
{
  std::lock_guard<std::mutex> lLock(mMutex);
  return mUpdateTime.steady;
}


template <typename DataType>
void _Metric<DataType>::setValueUnknown()
{
  std::lock_guard<std::mutex> lLock(mMutex);
  mUpdateTime = TimePoint::now();
  mValue.reset((const DataType*)NULL);
  for (auto lIt = this->getDependantMetrics().begin(); lIt != this->getDependantMetrics().end(); lIt++)
    lIt->second();
}


template <typename DataType>
void _Metric<DataType>::setMonitoringStatus(monitoring::Status aMonStatus)
{
  std::lock_guard<std::mutex> lLock(mMutex);
  mMonitoringStatus = aMonStatus;
}


template <typename DataType>
void _Metric<DataType>::resetHistory()
{
  this->mWarningStartTime = boost::none;
  this->mErrorStartTime = boost::none;
}

} // namespace detail


////////////////////////////////
//   GENERIC CLASS TEMPLATE   //
////////////////////////////////


template <typename DataType, class Enable>
Metric<DataType, Enable>::Metric(const std::string& aId, const std::string& aAlias) :
  detail::_Metric<DataType>(aId, aAlias)
{
}


template <typename DataType, class Enable>
Metric<DataType, Enable>::Metric(const std::string& aId, const std::string& aAlias,
                                 MetricCondition<DataType>* aErrorCondition,
                                 MetricCondition<DataType>* aWarnCondition) :
  detail::_Metric<DataType>(aId, aAlias, aErrorCondition, aWarnCondition)
{
}


template <typename DataType, class Enable>
Metric<DataType, Enable>::~Metric()
{
}


/////////////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Floating point types   //
/////////////////////////////////////////////////////////////


template <typename DataType>
const std::pair<core::format::FloatingPointNotation, boost::optional<size_t>>
    Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::kDefaultFormat { format::kFixedPoint, {} };


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::Metric(const std::string& aId, const std::string& aAlias) :
  detail::_Metric<DataType>(aId, aAlias)
{
}


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::Metric(const std::string& aId, const std::string& aAlias,
                                                                                                 MetricCondition<DataType>* aErrorCondition,
                                                                                                 MetricCondition<DataType>* aWarnCondition) :
  detail::_Metric<DataType>(aId, aAlias, aErrorCondition, aWarnCondition)
{
}


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::~Metric()
{
}


template <typename DataType>
const std::string& Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getUnit() const
{
  return mUnit;
}


template <typename DataType>
core::format::FloatingPointNotation Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getNotation() const
{
  if (mFormat)
    return mFormat->first;
  else
    return kDefaultFormat.first;
}


template <typename DataType>
boost::optional<size_t> Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::getPrecision() const
{
  if (mFormat)
    return mFormat->second;
  else
    return kDefaultFormat.second;
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setUnit(const std::string& aUnit)
{
  if (not mUnit.empty())
    SWATCH_THROW(MetricFieldAlreadySet("Unit of metric '" + this->getPath() + "' has already been set."));

  mUnit = aUnit;
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setFormat(core::format::FloatingPointNotation aNotation)
{
  if (mFormat)
    SWATCH_THROW(MetricFieldAlreadySet("Format of metric '" + this->getPath() + "' has already been set."));

  mFormat = std::pair<core::format::FloatingPointNotation, boost::optional<size_t>> { aNotation, kDefaultFormat.second };
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_floating_point<DataType>::value>::type>::setFormat(core::format::FloatingPointNotation aNotation, const size_t aPrecision)
{
  if (mFormat)
    SWATCH_THROW(MetricFieldAlreadySet("Format of metric '" + this->getPath() + "' has already been set."));

  mFormat = std::pair<core::format::FloatingPointNotation, boost::optional<size_t>> { aNotation, aPrecision };
}


///////////////////////////////////////////////////////
//   CLASS TEMPLATE SPECIALISATION: Integral types   //
///////////////////////////////////////////////////////


template <typename DataType>
const std::tuple<core::format::Base, boost::optional<size_t>, const char>
    Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::kDefaultFormat { core::format::kDecimal, {}, '0' };


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::Metric(const std::string& aId, const std::string& aAlias) :
  detail::_Metric<DataType>(aId, aAlias)
{
}


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::Metric(const std::string& aId, const std::string& aAlias,
                                                                                                                                       MetricCondition<DataType>* aErrorCondition,
                                                                                                                                       MetricCondition<DataType>* aWarnCondition) :
  detail::_Metric<DataType>(aId, aAlias, aErrorCondition, aWarnCondition)
{
}


template <typename DataType>
Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::~Metric()
{
}


template <typename DataType>
const std::string& Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getUnit() const
{
  return mUnit;
}


template <typename DataType>
core::format::Base Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getBase() const
{
  if (mFormat)
    return std::get<0>(*mFormat);
  else
    return std::get<0>(kDefaultFormat);
}


template <typename DataType>
boost::optional<size_t> Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getWidth() const
{
  if (mFormat)
    return std::get<1>(*mFormat);
  else
    return std::get<1>(kDefaultFormat);
}


template <typename DataType>
char Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::getFillChar() const
{
  if (mFormat)
    return std::get<2>(*mFormat);
  else
    return std::get<2>(kDefaultFormat);
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setUnit(const std::string& aUnit)
{
  if (not mUnit.empty())
    SWATCH_THROW(MetricFieldAlreadySet("Unit of property '" + this->getPath() + "' has already been set."));

  mUnit = aUnit;
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setFormat(core::format::Base aBase)
{
  if (mFormat)
    SWATCH_THROW(MetricFieldAlreadySet("Format of property '" + this->getPath() + "' has already been set."));

  mFormat = std::tuple<core::format::Base, boost::optional<size_t>, char> { aBase, boost::none, std::get<2>(kDefaultFormat) };
}


template <typename DataType>
void Metric<DataType, typename std::enable_if<std::is_integral<DataType>::value and not std::is_same<DataType, bool>::value>::type>::setFormat(core::format::Base aBase, const size_t aWidth, const char aFillChar)
{
  if (mFormat)
    SWATCH_THROW(MetricFieldAlreadySet("Format of property '" + this->getPath() + "' has already been set."));

  mFormat = std::tuple<core::format::Base, boost::optional<size_t>, char> { aBase, aWidth, aFillChar };
}


////////////////////////////////
//   EXPLICIT INSTANTIATION   //
////////////////////////////////


template class detail::_Metric<bool>;
template class detail::_Metric<uint16_t>;
template class detail::_Metric<uint32_t>;
template class detail::_Metric<uint64_t>;
template class detail::_Metric<int>;
template class detail::_Metric<int64_t>;
template class detail::_Metric<float>;
template class detail::_Metric<double>;
template class detail::_Metric<std::string>;
template class detail::_Metric<tts::State>;


template class Metric<bool>;
template class Metric<uint16_t>;
template class Metric<uint32_t>;
template class Metric<uint64_t>;
template class Metric<int>;
template class Metric<int64_t>;
template class Metric<float>;
template class Metric<double>;
template class Metric<std::string>;
template class Metric<tts::State>;

template <typename DataType>
MetricCondition<DataType>::MetricCondition()
{
}


template <typename DataType>
MetricCondition<DataType>::~MetricCondition()
{
}


template class MetricCondition<bool>;
template class MetricCondition<uint16_t>;
template class MetricCondition<uint32_t>;
template class MetricCondition<uint64_t>;
template class MetricCondition<int>;
template class MetricCondition<int64_t>;
template class MetricCondition<float>;
template class MetricCondition<double>;
template class MetricCondition<std::string>;
template class MetricCondition<tts::State>;


}
}
