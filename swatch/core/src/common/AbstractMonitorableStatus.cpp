
#include "swatch/core/AbstractMonitorableStatus.hpp"


namespace swatch {
namespace core {


//------------------------------------------------------------------------------------
AbstractMonitorableStatus::AbstractMonitorableStatus()
{
}

//------------------------------------------------------------------------------------
AbstractMonitorableStatus::~AbstractMonitorableStatus()
{
}

//------------------------------------------------------------------------------------
std::unique_lock<std::mutex>& AbstractMonitorableStatus::getUniqueLock(MonitorableStatusGuard& aGuard) const
{
  return aGuard.mLockGuard;
}


//------------------------------------------------------------------------------------
MonitorableStatusGuard::MonitorableStatusGuard(const AbstractMonitorableStatus& aStatus) :
  mStatus(aStatus),
  mLockGuard(aStatus.mMutex)
{
}

//------------------------------------------------------------------------------------
MonitorableStatusGuard::MonitorableStatusGuard(const AbstractMonitorableStatus& aStatus, std::adopt_lock_t) :
  mStatus(aStatus),
  mLockGuard(aStatus.mMutex, std::adopt_lock_t())
{
}

//------------------------------------------------------------------------------------
MonitorableStatusGuard::~MonitorableStatusGuard()
{
}

//------------------------------------------------------------------------------------
bool MonitorableStatusGuard::isCorrectGuard(const AbstractMonitorableStatus& aStatus) const
{
  return (&mStatus == &aStatus);
}


}
}
