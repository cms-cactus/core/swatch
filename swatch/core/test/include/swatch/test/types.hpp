
#ifndef __SWATCH_TEST_TYPES_HPP__
#define __SWATCH_TEST_TYPES_HPP__


#include <limits>
#include <ostream>

#ifndef SWATCH_NO_XDATA
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Vector.h"
#endif


namespace swatch {
namespace test {

// clang-format off
#ifdef SWATCH_NO_XDATA

typedef bool         Bool_t;
typedef int          Int_t;
typedef int32_t      Int32_t;
typedef int64_t      Int64_t;
typedef unsigned int UInt_t;
typedef uint32_t     UInt32_t;
typedef uint64_t     UInt64_t;
typedef short        Short_t;
typedef long         Long_t;
typedef float        Float_t;
typedef double       Double_t;
typedef std::string  String_t;

typedef std::vector<bool>         VectorBool_t;
typedef std::vector<int>          VectorInt_t;
typedef std::vector<int32_t>      VectorInt32_t;
typedef std::vector<int64_t>      VectorInt64_t;
typedef std::vector<unsigned int> VectorUInt_t;
typedef std::vector<uint32_t>     VectorUInt32_t;
typedef std::vector<uint64_t>     VectorUInt64_t;
typedef std::vector<short>        VectorShort_t;
typedef std::vector<long>         VectorLong_t;
typedef std::vector<float>        VectorFloat_t;
typedef std::vector<double>       VectorDouble_t;
typedef std::vector<std::string>  VectorString_t;

#else

typedef xdata::Boolean           Bool_t;
typedef xdata::Integer           Int_t;
typedef xdata::Integer32         Int32_t;
typedef xdata::Integer64         Int64_t;
typedef xdata::UnsignedInteger   UInt_t;
typedef xdata::UnsignedInteger32 UInt32_t;
typedef xdata::UnsignedInteger64 UInt64_t;
typedef xdata::UnsignedShort     Short_t;
typedef xdata::UnsignedLong      Long_t;
typedef xdata::Float             Float_t;
typedef xdata::Double            Double_t;
typedef xdata::String            String_t;

typedef xdata::Vector<xdata::Boolean>           VectorBool_t;
typedef xdata::Vector<xdata::Integer>           VectorInt_t;
typedef xdata::Vector<xdata::Integer32>         VectorInt32_t;
typedef xdata::Vector<xdata::Integer64>         VectorInt64_t;
typedef xdata::Vector<xdata::UnsignedInteger>   VectorUInt_t;
typedef xdata::Vector<xdata::UnsignedInteger32> VectorUInt32_t;
typedef xdata::Vector<xdata::UnsignedInteger64> VectorUInt64_t;
typedef xdata::Vector<xdata::UnsignedShort>     VectorShort_t;
typedef xdata::Vector<xdata::UnsignedLong>      VectorLong_t;
typedef xdata::Vector<xdata::Float>             VectorFloat_t;
typedef xdata::Vector<xdata::Double>            VectorDouble_t;
typedef xdata::Vector<xdata::String>            VectorString_t;

typedef xdata::Table Table_t;

#endif
// clang-format on

} // namespace test
} // namespace swatch


#ifndef SWATCH_NO_XDATA
namespace xdata {

template <typename T>
inline std::ostream& operator<<(std::ostream& aStream, const xdata::SimpleType<T>& aSerializable)
{
  aStream << T(aSerializable);
  return aStream;
}

inline std::ostream& operator<<(std::ostream& aStream, const xdata::Serializable& aSerializable)
{
  aStream << aSerializable.toString();
  return aStream;
}

inline bool operator==(const Serializable& x, const Serializable& y)
{
  return x.equals(y);
}

} // namespace xdata

namespace std {
inline bool isnan(const swatch::test::UInt_t aValue)
{
  return aValue.isNaN();
}

}
#endif


#endif /* __SWATCH_TEST_TYPES_HPP__ */
