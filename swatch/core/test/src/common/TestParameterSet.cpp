// Boost Unit Test includes
#define BOOST_NO_CXX11_NUMERIC_LIMITS 1
#include <boost/test/unit_test.hpp>


// boost headers
#include "boost/lexical_cast.hpp"
#include <boost/test/test_tools.hpp>

// SWATCH headers
#include "swatch/core/ParameterSet.hpp"
#include "swatch/test/types.hpp"


using namespace swatch::test;

namespace swatch {
namespace core {
namespace test {


BOOST_AUTO_TEST_SUITE(ParSetsTestSuite)

BOOST_AUTO_TEST_CASE(EmptySetTest)
{
  ParameterSet lPSet;

  BOOST_CHECK_EQUAL(lPSet.size(), size_t(0));
  BOOST_CHECK_EQUAL(lPSet.keys().size(), size_t(0));
  BOOST_CHECK_THROW(lPSet.get<String_t>("aKey"), ParameterNotFound);
  BOOST_CHECK_EQUAL(lPSet.has("aKey"), false);
}


struct ParTestSetup {
  ParTestSetup();
  ~ParTestSetup();

  std::shared_ptr<const boost::any> str;
  std::shared_ptr<const boost::any> intA;
  std::shared_ptr<const boost::any> intB;

  const String_t* pStr;
  const Int_t* pIntA;
  const Int_t* pIntB;
};

ParTestSetup::ParTestSetup() :
  str(new boost::any(String_t("aStringValue"))),
  intA(new boost::any(Int_t(42))),
  intB(new boost::any(Int_t(-1))),
  pStr(boost::any_cast<String_t>(str.get())),
  pIntA(boost::any_cast<Int_t>(intA.get())),
  pIntB(boost::any_cast<Int_t>(intB.get()))
{
}

ParTestSetup::~ParTestSetup() {}


BOOST_FIXTURE_TEST_CASE(SimpleAdoptGetEraseTest, ParTestSetup)
{
  /*  ---  adopt method  ---  */
  {
    ParameterSet pset;

    pset.adopt("aString", str);
    pset.adopt("intA", intA);
    pset.adopt("intB", intB);

    BOOST_CHECK_EQUAL(pset.size(), size_t(3));
    std::set<std::string> expectedKeys = { "aString", "intA", "intB" };
    std::set<std::string> returnedKeys = pset.keys();
    BOOST_CHECK_EQUAL_COLLECTIONS(returnedKeys.begin(), returnedKeys.end(), expectedKeys.begin(), expectedKeys.end());

    BOOST_CHECK_EQUAL(pset.has("aString"), true);
    BOOST_CHECK_EQUAL(pset.has("intA"), true);
    BOOST_CHECK_EQUAL(pset.has("intB"), true);
    BOOST_CHECK_EQUAL(pset.has("someOtherItem"), false);

    // Check that parameter values are correct
    BOOST_CHECK_EQUAL(pset.get<String_t>("aString"), *pStr);
    BOOST_CHECK_EQUAL(pset.get<Int_t>("intA"), *pIntA);
    BOOST_CHECK_EQUAL(pset.get<Int_t>("intB"), *pIntB);

    // Check that parameter values weren't copied
    BOOST_CHECK_EQUAL(&pset.get<String_t>("aString"), pStr);
    BOOST_CHECK_EQUAL(&pset.get<Int_t>("intA"), pIntA);
    BOOST_CHECK_EQUAL(&pset.get<Int_t>("intB"), pIntB);

    // Check that exception thrown when cast to incorrect type
    BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
  }


  /*  ---  add method ---  */
  {
    ParameterSet pset;
    pset.add("aString", boost::any_cast<String_t>(*str));
    pset.add("intA", boost::any_cast<Int_t>(*intA));
    pset.add("intB", boost::any_cast<Int_t>(*intB));

    BOOST_CHECK_EQUAL(pset.size(), size_t(3));
    std::set<std::string> expectedKeys = { "aString", "intA", "intB" };
    std::set<std::string> returnedKeys = pset.keys();
    BOOST_CHECK_EQUAL_COLLECTIONS(returnedKeys.begin(), returnedKeys.end(), expectedKeys.begin(), expectedKeys.end());

    BOOST_CHECK_EQUAL(pset.has("aString"), true);
    BOOST_CHECK_EQUAL(pset.has("intA"), true);
    BOOST_CHECK_EQUAL(pset.has("intB"), true);
    BOOST_CHECK_EQUAL(pset.has("someOtherItem"), false);

    // Check that cannot overwrite existing entries using 'add' method
    BOOST_CHECK_THROW(pset.add("aString", String_t("A string that should not enter the set")), ParameterExists);
    BOOST_CHECK_THROW(pset.add("intA", Int_t(boost::any_cast<Int_t>(*intA) + 1)), ParameterExists);
    BOOST_CHECK_THROW(pset.add("intB", Int_t(boost::any_cast<Int_t>(*intB) + 1)), ParameterExists);

    // Check that parameter values are correct
    BOOST_CHECK_EQUAL(pset.get<String_t>("aString"), *pStr);
    BOOST_CHECK_EQUAL(pset.get<Int_t>("intA"), *pIntA);
    BOOST_CHECK_EQUAL(pset.get<Int_t>("intB"), *pIntB);

    // Check that parameter values WERE copied
    BOOST_CHECK_NE(&pset.get<String_t>("aString"), pStr);
    BOOST_CHECK_NE(&pset.get<Int_t>("intA"), pIntA);
    BOOST_CHECK_NE(&pset.get<Int_t>("intB"), pIntB);

    // Check that exception thrown when cast to incorrect type
    BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
    BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
  }
}


BOOST_FIXTURE_TEST_CASE(ShallowCopyTest, ParTestSetup)
{
  // Create the "source" set
  std::unique_ptr<ParameterSet> originalPSet(new ParameterSet());
  originalPSet->adopt("aString", str);
  originalPSet->adopt("intA", intA);
  originalPSet->adopt("intB", intB);

  // Copy the "source" set - creating instance that will be tested
  ParameterSet pset(*originalPSet);

  // Delete the original set before testing ...
  originalPSet.reset(NULL);

  // Check the basics
  BOOST_CHECK_EQUAL(pset.size(), size_t(3));
  std::set<std::string> expectedKeys = { "aString", "intA", "intB" };
  std::set<std::string> returnedKeys = pset.keys();
  BOOST_CHECK_EQUAL_COLLECTIONS(returnedKeys.begin(), returnedKeys.end(), expectedKeys.begin(), expectedKeys.end());

  BOOST_CHECK_EQUAL(pset.has("aString"), true);
  BOOST_CHECK_EQUAL(pset.has("intA"), true);
  BOOST_CHECK_EQUAL(pset.has("intB"), true);
  BOOST_CHECK_EQUAL(pset.has("someOtherItem"), false);

  // Check that parameter values are correct
  BOOST_CHECK_EQUAL(pset.get<String_t>("aString"), *pStr);
  BOOST_CHECK_EQUAL(pset.get<Int_t>("intA"), *pIntA);
  BOOST_CHECK_EQUAL(pset.get<Int_t>("intB"), *pIntB);

  // Check that parameter values weren't copied
  BOOST_CHECK_EQUAL(&pset.get<String_t>("aString"), pStr);
  BOOST_CHECK_EQUAL(&pset.get<Int_t>("intA"), pIntA);
  BOOST_CHECK_EQUAL(&pset.get<Int_t>("intB"), pIntB);

  // Check that exception thrown when cast to incorrect type
  BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Int_t>("aString"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<String_t>("intA"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Bool_t>("intB"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
}



BOOST_FIXTURE_TEST_CASE(DeepCopyTest, ParTestSetup)
{
  // Create the "source" set
  std::unique_ptr<ParameterSet> originalPSet(new ParameterSet());
  originalPSet->adopt("aString", str);
  originalPSet->adopt("intA", intA);
  originalPSet->adopt("intB", intB);

  // Copy the "source" set - creating instance that will be tested
  ParameterSet pset;
  pset.deepCopyFrom(*originalPSet);

  // Delete the original set before testing ...
  originalPSet.reset(NULL);

  // Check the basics
  BOOST_CHECK_EQUAL(pset.size(), size_t(3));
  std::set<std::string> expectedKeys = { "aString", "intA", "intB" };
  std::set<std::string> returnedKeys = pset.keys();
  BOOST_CHECK_EQUAL_COLLECTIONS(returnedKeys.begin(), returnedKeys.end(), expectedKeys.begin(), expectedKeys.end());

  BOOST_CHECK_EQUAL(pset.has("aString"), true);
  BOOST_CHECK_EQUAL(pset.has("intA"), true);
  BOOST_CHECK_EQUAL(pset.has("intB"), true);
  BOOST_CHECK_EQUAL(pset.has("someOtherItem"), false);

  // Check that parameter values are correct
  BOOST_CHECK_EQUAL(pset.get<String_t>("aString"), *pStr);
  BOOST_CHECK_EQUAL(pset.get<Int_t>("intA"), *pIntA);
  BOOST_CHECK_EQUAL(pset.get<Int_t>("intB"), *pIntB);

  // Check that parameter values WERE copied
  BOOST_CHECK_NE(&pset.get<String_t>("aString"), pStr);
  BOOST_CHECK_NE(&pset.get<Int_t>("intA"), pIntA);
  BOOST_CHECK_NE(&pset.get<Int_t>("intB"), pIntB);

  // Check that exception thrown when cast to incorrect type
  BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Bool_t>("aString"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<Bool_t>("intA"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
  BOOST_CHECK_THROW(pset.get<String_t>("intB"), ParameterFailedCast);
}


BOOST_AUTO_TEST_SUITE_END() // ParSetsTestSuite


} // ns: test
} // ns: core
} // ns: swatch
