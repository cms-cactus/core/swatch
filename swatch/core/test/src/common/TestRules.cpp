#include <boost/test/unit_test.hpp>


#include <iostream>

// boost headers
#include "boost/lexical_cast.hpp"
#include <boost/filesystem.hpp>

#ifndef SWATCH_NO_XDATA
#include "xdata/TableIterator.h"
#endif

// SWATCH headers
#include "swatch/core/rules/And.hpp"
#include "swatch/core/rules/FiniteNumber.hpp"
#include "swatch/core/rules/FiniteVector.hpp"
#include "swatch/core/rules/GreaterThan.hpp"
#include "swatch/core/rules/InRange.hpp"
#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/LesserThan.hpp"
#ifndef SWATCH_NO_XDATA
#include "swatch/core/rules/NoEmptyCells.hpp"
#endif
#include "swatch/core/rules/None.hpp"
#include "swatch/core/rules/Not.hpp"
#include "swatch/core/rules/OfSize.hpp"
#include "swatch/core/rules/Or.hpp"
#include "swatch/core/rules/OutOfRange.hpp"
#include "swatch/core/rules/PathExists.hpp"
#include "swatch/test/types.hpp"


using namespace swatch::test;

namespace swatch {
namespace core {
namespace test {

struct RuleTestSetup {

  Int_t intNaN;
  Int_t intOK;
  UInt_t uintNaN;
  UInt_t uintOK;

  Float_t floatNaN;
  Float_t floatOK;
  Double_t doubleNaN;
  Double_t doubleOK;

  //---
  VectorInt_t vIntEmpty;
  VectorInt_t vIntNaN;
  VectorInt_t vIntOK;

  VectorUInt_t vUintEmpty;
  VectorUInt_t vUintNaN;
  VectorUInt_t vUintOK;

  VectorFloat_t vFloatEmpty;
  VectorFloat_t vFloatNaN;
  VectorFloat_t vFloatOK;

  VectorDouble_t vDoubleEmpty;
  VectorDouble_t vDoubleNaN;
  VectorDouble_t vDoubleOK;

  String_t strEmpty;
  String_t strFull;
  String_t strOptA;
  String_t strOptD;

#ifndef SWATCH_NO_XDATA
  Table_t tableEmpty;
  Table_t tableEmptyCell;
  Table_t tableEmptyCellBig;
  Table_t tableUnsupColumn;
  Table_t tableUnsupColumnAndEmpty;
  Table_t tableOK;
#endif

  RuleTestSetup() :
    intNaN(),
    intOK(-123),
    uintNaN(),
    uintOK(56.),
    floatNaN(),
    floatOK(-5.),
    doubleNaN(),
    doubleOK(90.),

    vIntEmpty(),
    vUintEmpty(),
    vFloatEmpty(),
    vDoubleEmpty(),

    strEmpty(),
    strFull("I am a string"),
    strOptA("A"),
    strOptD("D")
  {
    for (int i : { 1, -2, 3 }) {
      vIntNaN.push_back(Int_t(i));
      vIntOK.push_back(Int_t(i));
    }
    vIntNaN.push_back(Int_t());

    for (int u : { 10, 20, 30 }) {
      vUintNaN.push_back(UInt_t(u));
      vUintOK.push_back(UInt_t(u));
    }
    vUintNaN.push_back(UInt_t());

    for (int f : { 5., 6., -7. }) {
      vFloatNaN.push_back(Float_t(f));
      vFloatOK.push_back(Float_t(f));
    }
    vFloatNaN.push_back(Float_t());

    for (int d : { 50., 60., -70. }) {
      vDoubleNaN.push_back(Double_t(d));
      vDoubleOK.push_back(Double_t(d));
    }
    vDoubleNaN.push_back(Double_t());

#ifndef SWATCH_NO_XDATA
    auto initTable = [](Table_t& t) {
      t.addColumn("col_float", "float");
      t.addColumn("col_int", "int");
      t.addColumn("col_uint", "unsigned int");
      t.addColumn("col_str", "string");

      t.append();

      *(t.getValueAt(0, "col_float")) = Float_t(5.);
      *(t.getValueAt(0, "col_int")) = Int_t(-87);
      *(t.getValueAt(0, "col_uint")) = UInt_t(1900);
      *(t.getValueAt(0, "col_str")) = String_t("Lorem ipsum");

      t.append();
      *(t.getValueAt(1, "col_float")) = Float_t(10.);
      *(t.getValueAt(1, "col_int")) = Int_t(-847);
      *(t.getValueAt(1, "col_uint")) = UInt_t(3900);
      *(t.getValueAt(1, "col_str")) = String_t("Ipsum lorem");

      t.append();
      *(t.getValueAt(2, "col_float")) = Float_t(13.);
      *(t.getValueAt(2, "col_int")) = Int_t(-27);
      *(t.getValueAt(2, "col_uint")) = UInt_t(40);
      *(t.getValueAt(2, "col_str")) = String_t("Dixit");
    };

    initTable(tableOK);
    initTable(tableEmptyCell);
    initTable(tableUnsupColumn);
    initTable(tableEmptyCellBig);
    initTable(tableUnsupColumnAndEmpty);

    // Insert an empty cell
    *(tableEmptyCell.getValueAt(1, "col_float")) = Float_t();
    // Add a extra, empty row
    tableEmptyCell.append();

    // Add many more columns
    tableEmptyCellBig.addColumn("col_float_B", "float");
    tableEmptyCellBig.addColumn("col_int_B", "int");
    tableEmptyCellBig.addColumn("col_uint_B", "unsigned int");
    tableEmptyCellBig.addColumn("col_str_B", "string");
    // Add a extra, empty row
    tableEmptyCellBig.append();

    // And a complex column type
    tableUnsupColumn.addColumn("col_unk_A", "table");
    tableUnsupColumn.addColumn("col_unk_B", "table");
    tableUnsupColumn.addColumn("col_unk_C", "table");
    tableUnsupColumn.addColumn("col_unk_D", "table");
    tableUnsupColumn.addColumn("col_unk_E", "table");
    tableUnsupColumn.addColumn("col_unk_F", "table");

    // tableUnknownColumn.addColumn("col_unk3", "table");
    tableUnsupColumnAndEmpty.addColumn("col_unk_A", "table");
    tableUnsupColumnAndEmpty.append();
#endif
  }

  //---
};


BOOST_AUTO_TEST_SUITE(RuleTestSuite)

BOOST_FIXTURE_TEST_CASE(SimpleNumericTest, RuleTestSetup)
{
  rules::FiniteNumber<UInt_t> lUintRule;
  BOOST_CHECK(lUintRule.type() == typeid(UInt_t));
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lUintRule), "isFinite(x)");

#ifndef SWATCH_NO_XDATA
  BOOST_CHECK_EQUAL(lUintRule(uintNaN), false);
#endif
  BOOST_CHECK_EQUAL(lUintRule(uintOK), true);

  BOOST_CHECK_THROW(lUintRule(floatNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintRule(floatOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintRule(doubleNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintRule(doubleOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintRule(strFull), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintRule(vIntEmpty), RuleTypeMismatch);
}


BOOST_FIXTURE_TEST_CASE(SimpleVectorTest, RuleTestSetup)
{
  rules::FiniteVector<VectorUInt_t> lUintVecRule;
  BOOST_CHECK(lUintVecRule.type() == typeid(VectorUInt_t));
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lUintVecRule), "all(x,isFinite)");

  BOOST_CHECK_EQUAL(lUintVecRule(vUintEmpty), true);
#ifndef SWATCH_NO_XDATA
  BOOST_CHECK_EQUAL(lUintVecRule(vUintNaN), false);
#endif
  BOOST_CHECK_EQUAL(lUintVecRule(vUintOK), true);

  BOOST_CHECK_THROW(lUintVecRule(floatNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintVecRule(floatOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintVecRule(doubleNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintVecRule(doubleOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintVecRule(strFull), RuleTypeMismatch);
  BOOST_CHECK_THROW(lUintVecRule(vIntEmpty), RuleTypeMismatch);

  rules::OfSize<VectorUInt_t> lUintVecOsSizeRule(3);


  // vUintOK has size 3
  BOOST_CHECK_EQUAL(lUintVecOsSizeRule(vUintOK), true);
  // vUintNaN has size 4
  BOOST_CHECK_EQUAL(lUintVecOsSizeRule(vUintNaN), false);
}


BOOST_FIXTURE_TEST_CASE(NoneTest, RuleTestSetup)
{
  rules::None<Double_t> lNoDoubleRule;
  BOOST_CHECK(lNoDoubleRule.type() == typeid(Double_t));
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lNoDoubleRule), "true");

  BOOST_CHECK_EQUAL(lNoDoubleRule(doubleNaN), true);
  BOOST_CHECK_EQUAL(lNoDoubleRule(doubleOK), true);

  BOOST_CHECK_THROW(lNoDoubleRule(floatNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lNoDoubleRule(floatOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lNoDoubleRule(strFull), RuleTypeMismatch);
  BOOST_CHECK_THROW(lNoDoubleRule(vIntEmpty), RuleTypeMismatch);
  BOOST_CHECK_THROW(lNoDoubleRule(vDoubleEmpty), RuleTypeMismatch);
#ifndef SWATCH_NO_XDATA
  BOOST_CHECK_THROW(lNoDoubleRule(tableOK), RuleTypeMismatch);
#endif
}


BOOST_FIXTURE_TEST_CASE(IsAmongTest, RuleTestSetup)
{
  rules::IsAmong<String_t> lRule({ "A", "B", "C" });
  BOOST_CHECK(lRule.type() == typeid(String_t));
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lRule), "x in {A, B, C}");

  BOOST_CHECK_EQUAL(lRule(strOptA), true);
  BOOST_CHECK_EQUAL(lRule(strOptD), false);
}


#ifndef SWATCH_NO_XDATA
BOOST_FIXTURE_TEST_CASE(NoEmptyCellsTest, RuleTestSetup)
{
  rules::NoEmptyCells lRule;
  BOOST_CHECK(lRule.type() == typeid(Table_t));
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lRule), "!any(x,isEmpty)");

  BOOST_CHECK_EQUAL(lRule(tableEmpty), true);
  BOOST_CHECK_EQUAL(lRule(tableEmptyCell), Match(false, "5 empty cell(s) found in 4 columns. 'col_float': [1,3], 'col_int': [3], 'col_str': [3], 'col_uint': [3]."));
  BOOST_CHECK_EQUAL(lRule(tableEmptyCellBig), Match(false, "20 empty cell(s) found in 8 columns (only first 5 columns shown). 'col_float': [3], 'col_float_B': [0-3], 'col_int': [3], 'col_int_B': [0-3], 'col_str': [3], ..."));
  BOOST_CHECK_EQUAL(lRule(tableOK), true);
  BOOST_CHECK_EQUAL(lRule(tableUnsupColumn), Match(false, "6 column(s) have unsupported types (only first 5 columns shown). 'col_unk_A': table, 'col_unk_B': table, 'col_unk_C': table, 'col_unk_D': table, 'col_unk_E': table, ..."));
  BOOST_CHECK_EQUAL(lRule(tableUnsupColumnAndEmpty), Match(false, "1 column(s) have unsupported types. 'col_unk_A': table. 4 empty cell(s) found in 4 columns. 'col_float': [3], 'col_int': [3], 'col_str': [3], 'col_uint': [3]."));

  BOOST_CHECK_THROW(lRule(floatNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(floatOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(doubleNaN), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(doubleOK), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(strFull), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(vIntEmpty), RuleTypeMismatch);
  BOOST_CHECK_THROW(lRule(vDoubleEmpty), RuleTypeMismatch);
}
#endif


BOOST_FIXTURE_TEST_CASE(GreaterThanTest, RuleTestSetup)
{

  rules::GreaterThan<UInt_t> lUintGT(100);

  BOOST_CHECK(lUintGT.type() == typeid(UInt_t));
  BOOST_CHECK_EQUAL(lUintGT(UInt_t(120)), true);
  BOOST_CHECK_EQUAL(lUintGT(UInt_t(80)), false);

  rules::GreaterThan<Int_t> lIntGT(-100);

  BOOST_CHECK(lIntGT.type() == typeid(Int_t));
  BOOST_CHECK_EQUAL(lIntGT(Int_t(-80)), true);
  BOOST_CHECK_EQUAL(lIntGT(Int_t(-120)), false);

  rules::GreaterThan<Float_t> lFloatGT(-100);

  BOOST_CHECK(lFloatGT.type() == typeid(Float_t));
  BOOST_CHECK_EQUAL(lFloatGT(Float_t(-80)), true);
  BOOST_CHECK_EQUAL(lFloatGT(Float_t(-120)), false);

  rules::GreaterThan<Double_t> lDoubleGT(-100);

  BOOST_CHECK(lDoubleGT.type() == typeid(Double_t));
  BOOST_CHECK_EQUAL(lDoubleGT(Double_t(-80)), true);
  BOOST_CHECK_EQUAL(lDoubleGT(Double_t(-120)), false);
}


BOOST_FIXTURE_TEST_CASE(LesserThanTest, RuleTestSetup)
{

  rules::LesserThan<UInt_t> lUintLT(100);

  BOOST_CHECK(lUintLT.type() == typeid(UInt_t));
  BOOST_CHECK_EQUAL(lUintLT(UInt_t(120)), false);
  BOOST_CHECK_EQUAL(lUintLT(UInt_t(80)), true);

  rules::LesserThan<Int_t> lIntLT(-100);

  BOOST_CHECK(lIntLT.type() == typeid(Int_t));
  BOOST_CHECK_EQUAL(lIntLT(Int_t(-80)), false);
  BOOST_CHECK_EQUAL(lIntLT(Int_t(-120)), true);

  rules::LesserThan<Float_t> lFloatLT(-100);

  BOOST_CHECK(lFloatLT.type() == typeid(Float_t));
  BOOST_CHECK_EQUAL(lFloatLT(Float_t(-80)), false);
  BOOST_CHECK_EQUAL(lFloatLT(Float_t(-120)), true);

  rules::LesserThan<Double_t> lDoubleLT(-100);

  BOOST_CHECK(lDoubleLT.type() == typeid(Double_t));
  BOOST_CHECK_EQUAL(lDoubleLT(Double_t(-80)), false);
  BOOST_CHECK_EQUAL(lDoubleLT(Double_t(-120)), true);
}

BOOST_FIXTURE_TEST_CASE(InRangeTest, RuleTestSetup)
{

  rules::InRange<UInt_t> lUintRng(100, 200);

  BOOST_CHECK(lUintRng.type() == typeid(UInt_t));
  BOOST_CHECK_EQUAL(lUintRng(UInt_t(120)), true);
  BOOST_CHECK_EQUAL(lUintRng(UInt_t(80)), false);

  rules::InRange<Int_t> lIntRng(-200, -100);

  BOOST_CHECK(lIntRng.type() == typeid(Int_t));
  BOOST_CHECK_EQUAL(lIntRng(Int_t(-80)), false);
  BOOST_CHECK_EQUAL(lIntRng(Int_t(-120)), true);

  rules::InRange<Float_t> lFloatRng(-200, -100);

  BOOST_CHECK(lFloatRng.type() == typeid(Float_t));
  BOOST_CHECK_EQUAL(lFloatRng(Float_t(-80)), false);
  BOOST_CHECK_EQUAL(lFloatRng(Float_t(-120)), true);

  rules::InRange<Double_t> lDoubleRng(-200, -100);

  BOOST_CHECK(lDoubleRng.type() == typeid(Double_t));
  BOOST_CHECK_EQUAL(lDoubleRng(Double_t(-80)), false);
  BOOST_CHECK_EQUAL(lDoubleRng(Double_t(-120)), true);
}


// ----------------------------------------------------------------------------
BOOST_FIXTURE_TEST_CASE(OutOfRangeTest, RuleTestSetup)
{

  rules::OutOfRange<UInt_t> lUintRng(100, 200);

  BOOST_CHECK(lUintRng.type() == typeid(UInt_t));
  BOOST_CHECK_EQUAL(lUintRng(UInt_t(120)), false);
  BOOST_CHECK_EQUAL(lUintRng(UInt_t(80)), true);

  rules::OutOfRange<Int_t> lIntRng(-200, -100);

  BOOST_CHECK(lIntRng.type() == typeid(Int_t));
  BOOST_CHECK_EQUAL(lIntRng(Int_t(-80)), true);
  BOOST_CHECK_EQUAL(lIntRng(Int_t(-120)), false);

  rules::OutOfRange<Float_t> lFloatRng(-200, -100);

  BOOST_CHECK(lFloatRng.type() == typeid(Float_t));
  BOOST_CHECK_EQUAL(lFloatRng(Float_t(-80)), true);
  BOOST_CHECK_EQUAL(lFloatRng(Float_t(-120)), false);

  rules::OutOfRange<Double_t> lDoubleRng(-200, -100);

  BOOST_CHECK(lDoubleRng.type() == typeid(Double_t));
  BOOST_CHECK_EQUAL(lDoubleRng(Double_t(-80)), true);
  BOOST_CHECK_EQUAL(lDoubleRng(Double_t(-120)), false);
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
BOOST_FIXTURE_TEST_CASE(AndTest, RuleTestSetup)
{
  rules::GreaterThan<UInt_t> lUintGT100(100);
  rules::LesserThan<UInt_t> lUintLT200(200);

  rules::And<UInt_t> lAnd(lUintGT100, lUintLT200);
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lAnd), "(x > 100 && x < 200)");

  BOOST_CHECK_EQUAL(lAnd(UInt_t(120)), true);
  BOOST_CHECK_EQUAL(lAnd(UInt_t(80)), false);
  BOOST_CHECK_EQUAL(lAnd(UInt_t(250)), false);

  // TODO: Improve this test
  rules::And<UInt_t> lAnd2(lAnd, lAnd);
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lAnd2), "((x > 100 && x < 200) && (x > 100 && x < 200))");
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
BOOST_FIXTURE_TEST_CASE(OrTest, RuleTestSetup)
{
  rules::GreaterThan<UInt_t> lUintGT100(100);
  rules::LesserThan<UInt_t> lUintLT200(200);

  rules::Or<UInt_t> lOr(lUintGT100, lUintLT200);
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lOr), "(x > 100 || x < 200)");

  BOOST_CHECK_EQUAL(lOr(UInt_t(120)), true);
  BOOST_CHECK_EQUAL(lOr(UInt_t(80)), true);
  BOOST_CHECK_EQUAL(lOr(UInt_t(250)), true);

  // TODO: Improve this test
  rules::Or<UInt_t> lOr2(lOr, lOr);
  BOOST_CHECK_EQUAL(boost::lexical_cast<std::string>(lOr2), "((x > 100 || x < 200) || (x > 100 || x < 200))");
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
BOOST_FIXTURE_TEST_CASE(NotTest, RuleTestSetup)
{
  rules::GreaterThan<UInt_t> lUintGT100(100);
  rules::Not<UInt_t> lNotUintGT100(lUintGT100);

  BOOST_CHECK_EQUAL(lNotUintGT100(UInt_t(120)), false);
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
BOOST_FIXTURE_TEST_CASE(PathExistsTest, RuleTestSetup)
{
  // char* lEnvVar = std::getenv("SWATCH_ROOT");
  BOOST_REQUIRE(bool(std::getenv("SWATCH_CORE_INCLUDE_PATH")));
  boost::filesystem::path lSwatchCoreIncludeDir = std::getenv("SWATCH_CORE_INCLUDE_PATH");
  boost::filesystem::path lSwatchCoreTestIncludeDir(lSwatchCoreIncludeDir);
  lSwatchCoreTestIncludeDir /= "test";

  std::cout << "core test include directory " << lSwatchCoreTestIncludeDir << std::endl;
  rules::PathExists<String_t> lExistsPlain, lExistsInTest(lSwatchCoreTestIncludeDir.string()), lHeaderExistsInTest(lSwatchCoreTestIncludeDir.string(), "hpp");

  // Existing paths
  for (const auto& lPath : { lSwatchCoreIncludeDir, lSwatchCoreIncludeDir / "test", lSwatchCoreIncludeDir / "test" / "DummyMetric.hpp" }) {
    BOOST_CHECK_EQUAL(lExistsPlain(String_t(lPath.string())), true);
  }

  // Non-existing paths
  for (const auto& lPath : { lSwatchCoreIncludeDir / "nowhere/", lSwatchCoreIncludeDir / "nothing" }) {
    BOOST_CHECK_EQUAL(lExistsPlain(String_t(lPath.string())), false);
  }

  // Existing in test
  for (const auto& lPath : { "DummyMetric.hpp" }) {
    BOOST_CHECK_EQUAL(lExistsInTest(String_t(lPath)), true);
  }

  // Non existing in test
  for (const auto& lPath : { "nowhere/", "nothing" }) {
    BOOST_CHECK_EQUAL(lExistsInTest(String_t(lPath)), false);
  }

  // Existing in test with extension sh
  for (const auto& lPath : { "DummyMetric" }) {
    BOOST_CHECK_EQUAL(lHeaderExistsInTest(String_t(lPath)), true);
  }
}
// ----------------------------------------------------------------------------


BOOST_AUTO_TEST_SUITE_END() // RuleTestSuite
// ----------------------------------------------------------------------------

} /* namespace test */
} /* namespace core */
} /* namespace swatch */
