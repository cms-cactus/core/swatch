

#include "swatchcell/framework/tools/utilities.h"


#include <typeindex>

#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/SimpleType.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Vector.h"

#include "swatch/action/ActionableObject.hpp"
#include "swatch/core/MetricSnapshot.hpp"
#include "swatch/core/ParameterSet.hpp"
#include "swatch/dtm/DaqTTCManager.hpp"
#include "swatch/processor/Processor.hpp"
#include "swatch/system/System.hpp"


namespace swatchcellframework {
namespace tools {


void updateMetricsOfEnabledObjects(swatch::system::System& aSystem)
{
  typedef std::deque<swatch::processor::Processor*>::const_iterator ProcessorIt_t;
  typedef std::deque<swatch::dtm::DaqTTCManager*>::const_iterator DaqTTCMgrIt_t;

  // Loop over processors; only update status of enabled ones
  for (ProcessorIt_t lIt = aSystem.getProcessors().begin(); lIt != aSystem.getProcessors().end(); lIt++) {
    if ((*lIt)->getStatus().isEnabled())
      updateMetrics(**lIt);
  }

  // Loop over AMC13s; only update status of enabled ones
  for (DaqTTCMgrIt_t lIt = aSystem.getDaqTTCs().begin(); lIt != aSystem.getDaqTTCs().end(); lIt++) {
    if ((*lIt)->getStatus().isEnabled())
      updateMetrics(**lIt);
  }
}


void updateMetrics(swatch::action::ActionableObject& aObj)
{
  //  std::cout << "updateMetrics(ActObj '" << aObj.getPath() << "')" << std::endl;
  // Create one metric update guard to be used for all descendant monitorable objects, to avoid the actionable
  // ... object's status continually flip-flopping between "updating metrics" & "no uptdating metrics"
  swatch::core::MetricUpdateGuard lUpdateGuard(aObj);

  // Crawl through all non-disabled monitorable objects in hierarchy, and call update method of their child metrics
  std::string lDisabledObjPath;
  for (swatch::core::Object::iterator it = aObj.begin(); it != aObj.end(); it++) {
    if (swatch::core::MonitorableObject* lMonSubObj = dynamic_cast<swatch::core::MonitorableObject*>(&*it)) {
      //      std::cout << "MonObj '" << lMonSubObj->getPath() << "'" << std::endl;
      // Skip this sub-object if under a disabled object
      if ((!lDisabledObjPath.empty()) && (lMonSubObj->getPath().find(lDisabledObjPath) == size_t(0)))
        continue;
      else if (lMonSubObj->getMonitoringStatus() == swatch::core::monitoring::kDisabled) {
        lDisabledObjPath = lMonSubObj->getPath();
        continue;
      }
      else
        lMonSubObj->updateMetrics(lUpdateGuard);
      //      std::cout << "   metrics were updated!" << std::endl;
    }
  }
}


template <typename DataType, class SerializableType>
std::shared_ptr<xdata::Serializable> convertMetricValue(const swatch::core::MetricSnapshot& aSnapshot)
{
  if (aSnapshot.isValueKnown())
    return std::shared_ptr<xdata::Serializable>(new SerializableType(aSnapshot.getValue<DataType>()));
  else
    return std::shared_ptr<xdata::Serializable>(new SerializableType());
}


std::shared_ptr<xdata::Serializable> getMetricValueAsSerializable(const swatch::core::MetricSnapshot& aSnapshot)
{
  using namespace swatch::core;

  typedef std::unordered_map<std::type_index, std::function<std::shared_ptr<xdata::Serializable>(const swatch::core::MetricSnapshot&)>> ConverterMap_t;
  static const ConverterMap_t kMetricConverterMap = {
    {std::type_index(typeid(bool)), convertMetricValue<bool, xdata::Boolean>},
    {std::type_index(typeid(uint16_t)), convertMetricValue<uint16_t, xdata::UnsignedShort>},
    {std::type_index(typeid(uint32_t)), convertMetricValue<uint32_t, xdata::UnsignedInteger32>},
    {std::type_index(typeid(uint64_t)), convertMetricValue<uint64_t, xdata::UnsignedLong>},
    {std::type_index(typeid(int)), convertMetricValue<int, xdata::Integer>},
    {std::type_index(typeid(float)), convertMetricValue<float, xdata::Float>},
    {std::type_index(typeid(double)), convertMetricValue<double, xdata::Double>},
    {std::type_index(typeid(std::string)), convertMetricValue<std::string, xdata::String>},
    {std::type_index(typeid(tts::State)), convertMetricValue<tts::State, xdata::UnsignedShort>}};

  auto lIt = kMetricConverterMap.find(std::type_index(aSnapshot.getType()));
  if (lIt == kMetricConverterMap.end())
    SWATCH_THROW(RuntimeError("Cannot covert metric '" + aSnapshot.getPath() + "' (type: " + demangleName(aSnapshot.getType().name()) + ") to xdata Serializable"));

  return (lIt->second)(aSnapshot);
}


template <class T>
const xdata::Serializable& getAsSerializable(const swatch::core::ParameterSet& aParamSet, const std::string& aKey)
{
  return aParamSet.get<T>(aKey);
}

const xdata::Serializable& getSerializable(const swatch::core::ParameterSet& aParamSet, const std::string& aKey)
{
  typedef std::unordered_map<std::type_index, std::function<const xdata::Serializable&(const swatch::core::ParameterSet&, const std::string&)>> ConverterMap_t;
  static const ConverterMap_t kConverterMap = {
    {std::type_index(typeid(xdata::Boolean)), getAsSerializable<xdata::Boolean>},
    {std::type_index(typeid(xdata::Integer)), getAsSerializable<xdata::Integer>},
    {std::type_index(typeid(xdata::Integer32)), getAsSerializable<xdata::Integer32>},
    {std::type_index(typeid(xdata::Integer64)), getAsSerializable<xdata::Integer64>},
    {std::type_index(typeid(xdata::UnsignedInteger)), getAsSerializable<xdata::UnsignedInteger>},
    {std::type_index(typeid(xdata::UnsignedInteger32)), getAsSerializable<xdata::UnsignedInteger32>},
    {std::type_index(typeid(xdata::UnsignedInteger64)), getAsSerializable<xdata::UnsignedInteger64>},
    {std::type_index(typeid(xdata::UnsignedShort)), getAsSerializable<xdata::UnsignedShort>},
    {std::type_index(typeid(xdata::UnsignedLong)), getAsSerializable<xdata::UnsignedLong>},
    {std::type_index(typeid(xdata::Float)), getAsSerializable<xdata::Float>},
    {std::type_index(typeid(xdata::Double)), getAsSerializable<xdata::Double>},
    {std::type_index(typeid(xdata::String)), getAsSerializable<xdata::String>},
    {std::type_index(typeid(xdata::Vector<xdata::Boolean>)), getAsSerializable<xdata::Vector<xdata::Boolean>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer>)), getAsSerializable<xdata::Vector<xdata::Integer>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer32>)), getAsSerializable<xdata::Vector<xdata::Integer32>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer64>)), getAsSerializable<xdata::Vector<xdata::Integer64>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger>)), getAsSerializable<xdata::Vector<xdata::UnsignedInteger>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger32>)), getAsSerializable<xdata::Vector<xdata::UnsignedInteger32>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger64>)), getAsSerializable<xdata::Vector<xdata::UnsignedInteger64>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedShort>)), getAsSerializable<xdata::Vector<xdata::UnsignedShort>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedLong>)), getAsSerializable<xdata::Vector<xdata::UnsignedLong>>},
    {std::type_index(typeid(xdata::Vector<xdata::Float>)), getAsSerializable<xdata::Vector<xdata::Float>>},
    {std::type_index(typeid(xdata::Vector<xdata::Double>)), getAsSerializable<xdata::Vector<xdata::Double>>},
    {std::type_index(typeid(xdata::Vector<xdata::String>)), getAsSerializable<xdata::Vector<xdata::String>>},
    {std::type_index(typeid(xdata::Table)), getAsSerializable<xdata::Table>}};

  auto lIt = kConverterMap.find(std::type_index(aParamSet.getType(aKey)));
  if (lIt == kConverterMap.end())
    SWATCH_THROW(swatch::core::RuntimeError("Could not convert parameter '" + aKey + "' of type " + swatch::core::demangleName(aParamSet.getType(aKey).name()) + " to a xdata::Serializable-derived type"));

  return (lIt->second)(aParamSet, aKey);
}


template <class T>
const xdata::Serializable& getSerializable(const boost::any& aAny)
{
  return *boost::any_cast<T>(&aAny);
}

const xdata::Serializable& getSerializable(const boost::any& aAny)
{
  typedef std::unordered_map<std::type_index, std::function<const xdata::Serializable&(const boost::any&)>> ConverterMap_t;
  static const ConverterMap_t kConverterMap = {
    {std::type_index(typeid(xdata::Boolean)), getSerializable<xdata::Boolean>},
    {std::type_index(typeid(xdata::Integer)), getSerializable<xdata::Integer>},
    {std::type_index(typeid(xdata::Integer32)), getSerializable<xdata::Integer32>},
    {std::type_index(typeid(xdata::Integer64)), getSerializable<xdata::Integer64>},
    {std::type_index(typeid(xdata::UnsignedInteger)), getSerializable<xdata::UnsignedInteger>},
    {std::type_index(typeid(xdata::UnsignedInteger32)), getSerializable<xdata::UnsignedInteger32>},
    {std::type_index(typeid(xdata::UnsignedInteger64)), getSerializable<xdata::UnsignedInteger64>},
    {std::type_index(typeid(xdata::UnsignedShort)), getSerializable<xdata::UnsignedShort>},
    {std::type_index(typeid(xdata::UnsignedLong)), getSerializable<xdata::UnsignedLong>},
    {std::type_index(typeid(xdata::Float)), getSerializable<xdata::Float>},
    {std::type_index(typeid(xdata::Double)), getSerializable<xdata::Double>},
    {std::type_index(typeid(xdata::String)), getSerializable<xdata::String>},
    {std::type_index(typeid(xdata::Vector<xdata::Boolean>)), getSerializable<xdata::Vector<xdata::Boolean>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer>)), getSerializable<xdata::Vector<xdata::Integer>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer32>)), getSerializable<xdata::Vector<xdata::Integer32>>},
    {std::type_index(typeid(xdata::Vector<xdata::Integer64>)), getSerializable<xdata::Vector<xdata::Integer64>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger>)), getSerializable<xdata::Vector<xdata::UnsignedInteger>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger32>)), getSerializable<xdata::Vector<xdata::UnsignedInteger32>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger64>)), getSerializable<xdata::Vector<xdata::UnsignedInteger64>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedShort>)), getSerializable<xdata::Vector<xdata::UnsignedShort>>},
    {std::type_index(typeid(xdata::Vector<xdata::UnsignedLong>)), getSerializable<xdata::Vector<xdata::UnsignedLong>>},
    {std::type_index(typeid(xdata::Vector<xdata::Float>)), getSerializable<xdata::Vector<xdata::Float>>},
    {std::type_index(typeid(xdata::Vector<xdata::Double>)), getSerializable<xdata::Vector<xdata::Double>>},
    {std::type_index(typeid(xdata::Vector<xdata::String>)), getSerializable<xdata::Vector<xdata::String>>},
    {std::type_index(typeid(xdata::Table)), getSerializable<xdata::Table>}};

  auto lIt = kConverterMap.find(std::type_index(aAny.type()));
  if (lIt == kConverterMap.end())
    SWATCH_THROW(swatch::core::RuntimeError("Could not extract a xdata::Serializable-derived type from boost::any containing type " + swatch::core::demangleName(aAny.type().name())));

  return (lIt->second)(aAny);
}


template <class T>
void addSerializable(swatch::core::ParameterSet& aParamSet, const std::string& aKey, const std::type_info& aType, const std::string& aValue)
{
  T lParam;
  lParam.fromString(aValue);
  aParamSet.add(aKey, lParam);
}

void addSerializable(swatch::core::ParameterSet& aParamSet, const std::string& aKey, const std::type_info& aType, const std::string& aValue)
{
  typedef std::unordered_map<std::type_index, std::function<void(swatch::core::ParameterSet&, const std::string&, const std::type_info&, const std::string&)>> FunctionMap_t;
  static const FunctionMap_t kFunctionMap = {
    { std::type_index(typeid(xdata::Boolean)), addSerializable<xdata::Boolean> },
    { std::type_index(typeid(xdata::Integer)), addSerializable<xdata::Integer> },
    { std::type_index(typeid(xdata::Integer32)), addSerializable<xdata::Integer32> },
    { std::type_index(typeid(xdata::Integer64)), addSerializable<xdata::Integer64> },
    { std::type_index(typeid(xdata::UnsignedInteger)), addSerializable<xdata::UnsignedInteger> },
    { std::type_index(typeid(xdata::UnsignedInteger32)), addSerializable<xdata::UnsignedInteger32> },
    { std::type_index(typeid(xdata::UnsignedInteger64)), addSerializable<xdata::UnsignedInteger64> },
    { std::type_index(typeid(xdata::UnsignedShort)), addSerializable<xdata::UnsignedShort> },
    { std::type_index(typeid(xdata::UnsignedLong)), addSerializable<xdata::UnsignedLong> },
    { std::type_index(typeid(xdata::Float)), addSerializable<xdata::Float> },
    { std::type_index(typeid(xdata::Double)), addSerializable<xdata::Double> },
    { std::type_index(typeid(xdata::Vector<xdata::Boolean>)), addSerializable<xdata::Vector<xdata::Boolean>> },
    { std::type_index(typeid(xdata::Vector<xdata::Integer>)), addSerializable<xdata::Vector<xdata::Integer>> },
    { std::type_index(typeid(xdata::Vector<xdata::Integer32>)), addSerializable<xdata::Vector<xdata::Integer32>> },
    { std::type_index(typeid(xdata::Vector<xdata::Integer64>)), addSerializable<xdata::Vector<xdata::Integer64>> },
    { std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger>)), addSerializable<xdata::Vector<xdata::UnsignedInteger>> },
    { std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger32>)), addSerializable<xdata::Vector<xdata::UnsignedInteger32>> },
    { std::type_index(typeid(xdata::Vector<xdata::UnsignedInteger64>)), addSerializable<xdata::Vector<xdata::UnsignedInteger64>> },
    { std::type_index(typeid(xdata::Vector<xdata::UnsignedShort>)), addSerializable<xdata::Vector<xdata::UnsignedShort>> },
    { std::type_index(typeid(xdata::Vector<xdata::UnsignedLong>)), addSerializable<xdata::Vector<xdata::UnsignedLong>> },
    { std::type_index(typeid(xdata::Vector<xdata::Float>)), addSerializable<xdata::Vector<xdata::Float>> },
    { std::type_index(typeid(xdata::Vector<xdata::Double>)), addSerializable<xdata::Vector<xdata::Double>> },
    { std::type_index(typeid(xdata::Vector<xdata::String>)), addSerializable<xdata::Vector<xdata::String>> },
    { std::type_index(typeid(xdata::String)), addSerializable<xdata::String> },
    { std::type_index(typeid(xdata::Table)), addSerializable<xdata::Table> }
  };

  using namespace swatch::core;

  auto lIt = kFunctionMap.find(std::type_index(aType));
  if (lIt == kFunctionMap.end())
    SWATCH_THROW(swatch::core::RuntimeError("Cannot add serializable of type " + swatch::core::demangleName(aType.name()) + " to parameter set"));

  return (lIt->second)(aParamSet, aKey, aType, aValue);
}

} // end ns: tools
} // end ns: swatchcellframework
