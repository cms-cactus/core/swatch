/*
 * File:   MonitoringThread.cpp
 * Author: tom
 * Date:   February 2016
 */

#include "swatchcell/framework/MonitoringThread.h"

#include <prometheus/counter.h>


#include <unordered_map>
#include <map>
#include <regex>

// SWATCH cell headers
#include "swatchcell/framework/CellAbstract.h"
#include "swatchcell/framework/tools/panelFunctions.h"
#include "swatchcell/framework/tools/utilities.h"

#include "swatchcell/framework/InfoSpaceBuilder.h"

// boost headers
#include "boost/algorithm/string/case_conv.hpp"

// SWATCH headers
#include "swatch/dtm/DaqTTCManager.hpp"
#include "swatch/processor/Processor.hpp"
#include "swatch/system/System.hpp"

#include "swatch/dtm/AMCPort.hpp"
#include "swatch/dtm/AMCPortCollection.hpp"
#include "swatch/processor/Port.hpp"
#include "swatch/processor/PortCollection.hpp"

#include <prometheus/gauge.h>
#include <prometheus/exposer.h>
#include <prometheus/registry.h>
#include <prometheus/text_serializer.h>

#include <memory>
#include <algorithm>

namespace swatchcellframework {

double convertMetricValue(const swatch::core::MetricSnapshot& aSnapshot);
const std::string MonitoringThread::kSwatchCellDisableMonitoringDBFlag = "SWATCHCELL_DISABLE_WRITE_TO_MON_DB";
const std::string MonitoringThread::kSwatchCellDisableOpenMetricsExporterFlag = "SWATCHCELL_DISABLE_OPENMETRICS_EXPORTER";

const std::string MonitoringThread::kSwatchMetricValueFamily = "swatch_metric_value";

const std::string MonitoringThread::kSwatchMetricStatusFamily = "swatch_metric_status";
const std::string MonitoringThread::kSwatchComponentStatusFamily = "swatch_component_status";
const std::string MonitoringThread::kSwatchBoardStatusFamily = "swatch_board_status";
const std::string MonitoringThread::kSwatchSystemStatusFamily = "swatch_system_status";

const std::string MonitoringThread::kSwatchMetricMonitoringStatusFamily = "swatch_metric_monitoring_status";
const std::string MonitoringThread::kSwatchComponentMonitoringStatusFamily = "swatch_component_monitoring_status";
const std::string MonitoringThread::kSwatchBoardMonitoringStatusFamily = "swatch_board_monitoring_status";
const std::string MonitoringThread::kSwatchSystemMonitoringStatusFamily = "swatch_system_monitoring_status";

MonitoringThread::MonitoringThread(const std::string& aParentLoggerName, CellContext& aContext, size_t aPeriodInSeconds) :
  PeriodicWorkerThread(aParentLoggerName + ".MonitoringThread", aPeriodInSeconds),
  mContext(aContext),
  mInfospace(getLowercaseCopy(mContext.getSubsystemId() + "_cell"), aContext, getLogger()),
  mWriteToMonitoringDB(!getenv(this -> kSwatchCellDisableMonitoringDBFlag.c_str())),
  mExportOpenMetrics(!getenv(this -> kSwatchCellDisableOpenMetricsExporterFlag.c_str()))
{
  // 'static' db tables
  mMoniMap["RunNumber"] = new xdata::UnsignedLong;
  
  if (this -> mExportOpenMetrics)
  {
    std::string lPrometheusAddress = "0.0.0.0:";
    int lSWATCHPort = this -> extractPortFromURL(mContext.getLocalUrl());
    lPrometheusAddress += std::to_string(lSWATCHPort + 1);
    this -> mExposer.reset(new prometheus::Exposer(lPrometheusAddress));
  } else
  {
    std::ostringstream lMsg;
    lMsg << this -> kSwatchCellDisableOpenMetricsExporterFlag << " was found. OpenMetrics exporter is disabled.";
    LOG4CPLUS_INFO(getLogger(), lMsg.str());
  }
  
  if (!this -> mWriteToMonitoringDB)
  {
    std::ostringstream lMsg;
    lMsg << this -> kSwatchCellDisableMonitoringDBFlag << " was found. Writing to monitoring DB is disabled.";
    LOG4CPLUS_INFO(getLogger(), lMsg.str());
  }
}

void MonitoringThread::start()
{
  PeriodicWorkerThread::start();
  CellContext::SharedGuard_t lGuard(mContext);
  swatch::system::System& lSystem = mContext.getSystem(lGuard);

  if (this -> mExportOpenMetrics)
  {
    LOG4CPLUS_INFO(getLogger(), "Monitoring thread: creating Prometheus metric registry");
    initPrometheusMetrics(lSystem);
  }
}

void MonitoringThread::initPrometheusMetrics(swatch::system::System& aSystem)
{

  // metrics init
  // first, create a registry
  // second, create swatch metric family and assign it to the registry
  // third, add swatch metrics to the swatch metric family
  this -> mMetricsRegistry = std::make_shared<prometheus::Registry>();
  // initialising metric families
  this -> mExposer -> RegisterCollectable(this -> mMetricsRegistry, "/metrics");
  
  this -> mMetricFamilyMap[kSwatchMetricValueFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_metric_value")
                                                      .Help("Value of a SWATCH metric")
                                                      .Register( *(this -> mMetricsRegistry) );
  
  this -> mMetricFamilyMap[kSwatchMetricStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_metric_status")
                                                      .Help("Status of a SWATCH metric. 0 = Non-monitored, 1 = NoLimit, 2 = Good, 3 = Unknown, 4 = Warning, 5 = Error")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchComponentStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_component_status")
                                                      .Help("Status of a SWATCH component. 0 = Non-monitored, 1 = NoLimit, 2 = Good , 3 = Unknown, 4 = Warning, 5 = Error")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchBoardStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_board_status")
                                                      .Help("Status of a SWATCH board. 0 = Non-monitored, 1 = NoLimit, 2 = Good, 3 = Unknown, 4 = Warning, 5 = Error")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchSystemStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_system_status")
                                                      .Help("Status of SWATCH system. 0 = Non-monitored, 1 = NoLimit, 2 = Good, 3 = Unknown, 4 = Warning, 5 = Error")
                                                      .Register( *(this -> mMetricsRegistry) );
  
  this -> mMetricFamilyMap[kSwatchMetricMonitoringStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_metric_monitoring_status")
                                                      .Help("Monitoring status of a SWATCH metric. 0 = Disabled; 1 = Non critical; 2 = Enabled")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchComponentMonitoringStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_component_monitoring_status")
                                                      .Help("Monitoring status of a SWATCH component. 0 = Disabled; 1 = Non critical; 2 = Enabled")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchBoardMonitoringStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_board_monitoring_status")
                                                      .Help("Monitoring status of a SWATCH board. 0 = Disabled; 1 = Non critical; 2 = Enabled")
                                                      .Register( *(this -> mMetricsRegistry) );
  this -> mMetricFamilyMap[kSwatchSystemMonitoringStatusFamily] = & prometheus::BuildGauge()
                                                      .Name("swatch_system_monitoring_status")
                                                      .Help("Monitoring status of SWATCH system. 0 = Disabled; 1 = Non critical; 2 = Enabled")
                                                      .Register( *(this -> mMetricsRegistry) );
}


void MonitoringThread::stop()
{
  
  PeriodicWorkerThread::stop();
  // Prometheus registry clean up should not be required
}


MonitoringThread::~MonitoringThread()
{
  // TODO: Need to ensure that the thread is no longer active by this point
  //  - will likely require adding new method to base class

  for (auto& moni : mMoniMap)
    delete moni.second;
}


void MonitoringThread::doWork()
{
  CellContext::SharedGuard_t lGuard(mContext);
  swatch::system::System& lSys = mContext.getSystem(lGuard);

  swatchcellframework::tools::updateMetricsOfEnabledObjects(lSys);

  if (this -> mWriteToMonitoringDB)
  {
    try {
      fillInfospace(lSys);
    }
    catch (const std::exception& aException) {
      LOG4CPLUS_ERROR(getLogger(), "Exception of type '" << swatch::core::demangleName(typeid(aException).name()) << "' thrown within monitoring thread when filling infospace. Message: " << aException.what());
    }
    catch (...) {
      LOG4CPLUS_ERROR(getLogger(), "Object of unknown type thrown within monitoring thread when filling infospace");
    }
  }

  if (this -> mExportOpenMetrics)
  {
    try {
      updatePrometheusMetrics(lSys);
    }
    catch (const std::exception& aException) {
      LOG4CPLUS_ERROR(getLogger(), "Exception of type '" << swatch::core::demangleName(typeid(aException).name()) << "' thrown within monitoring thread when updating prometheus metric values. Message: " << aException.what());
    }
    catch (...) {
      LOG4CPLUS_ERROR(getLogger(), "Object of unknown type thrown within monitoring thread when updating prometheus metric values");
    }
  }
}



std::regex MonitoringThread::kColNameRegex("[a-zA-Z0-9_$#]+");


double convertMetricValue(const swatch::core::MetricSnapshot& aSnapshot)
{
  // Declare map of conversion functions
  typedef swatch::core::MetricSnapshot Snapshot_t;
  typedef std::unordered_map<std::type_index, std::function<double(const Snapshot_t&)>> FunctionMap_t;
  static const FunctionMap_t kFunctionMap = {
    {std::type_index(typeid(bool)),     [] (const Snapshot_t& x) { return double(x.getValue<bool>());} },
    {std::type_index(typeid(uint16_t)), [] (const Snapshot_t& x) { return double(x.getValue<uint16_t>());} },
    {std::type_index(typeid(uint32_t)), [] (const Snapshot_t& x) { return double(x.getValue<uint32_t>());} },
    {std::type_index(typeid(uint64_t)), [] (const Snapshot_t& x) { return double(x.getValue<uint64_t>());} },
    {std::type_index(typeid(int)),      [] (const Snapshot_t& x) { return double(x.getValue<int>());} },
    {std::type_index(typeid(float)),    [] (const Snapshot_t& x) { return double(x.getValue<float>());} },
    {std::type_index(typeid(double)),   [] (const Snapshot_t& x) { return double(x.getValue<double>());} }
  };

  // Apply relevant conversion function
  const std::type_info& lType = aSnapshot.getType();
  if (kFunctionMap.count(lType) == 0)
    throw std::runtime_error("Could not convert metric of type '" + swatch::core::demangleName(lType.name()) + "' to 'double'");
  return kFunctionMap.at(lType)(aSnapshot);
}

// 0 = disabled, 1 = Non-critical, 2 = Enabled
// whether a metric or object contributes to the overall status of a system depends on the "most restrictive" monitoring status of its parents, grandparents, etc - i.e:
// * If monitoring state = disabled for that object/metric or any of its ancestors -> monitoring is disabled for that object.
// * Otherwise, if monitoring state = non-critical for that object/metric or any of its ancestors -> monitoring is enabled for that object, but it doesn't contribute to system status
// * Otherwise, the object/metric contributes to the overall system status#
// in practice, the function should just return the min between aStatus and monitoringStatusLevel
double MonitoringThread::computeMonitoringStatusLevel(
  const swatch::core::monitoring::Status & aStatus,
  const double inheritedMonitoringStatusLevel
)
{
  double lMonitoringStatus = 2;
  using namespace swatch::core::monitoring;
  switch (aStatus) {
    case Status::kDisabled : lMonitoringStatus = 0; break;
    case Status::kNonCritical : lMonitoringStatus = 1; break;
    case Status::kEnabled : lMonitoringStatus = 2; break;
  }
  return std::min(lMonitoringStatus, inheritedMonitoringStatusLevel);
}

// recursive function
// goes through object children
// if children is a mon object, then inspect it by calling again the function
// if a children is a metric, add it to metric registry 
// The parameter aMonitoringStatusLevel is used to propagate if higher-level objects contribute to the overall status of the system
void MonitoringThread::inspectMonitorableComponent (
  const std::string & aSystemName,
  const std::string & aBoardName,
  const swatch::core::MonitorableObject & aMonitorableObject,
  double aMonitoringStatusLevel
) 
{
  aMonitoringStatusLevel = this -> computeMonitoringStatusLevel(aMonitorableObject.getMonitoringStatus(), aMonitoringStatusLevel);

  // parsing path of aMonitorableObject

  // board path is aSystemName.aBoardName, unless aMonitorableObject is a System (i.e. aBoardName == ""), then it is just aSystemName
  const size_t lBoardPathLength = (aBoardName == "") ?
    aSystemName.size() :
  // the +1 considers the first dot between system and board name
    aSystemName.size() + 1 + aBoardName.size();
  const std::string& lMonitorableObjectPath = aMonitorableObject.getPath();
  // if the component path is longer than the board path then we are handling a subcomponent
  const bool lMonitorableObjectIsSystem = (aBoardName == "") ;
  const bool lMonitorableObjectIsComponent = lMonitorableObjectPath.size() > lBoardPathLength ;
  // +1 considers the dot after board name, which must be removed from the component path
  const std::string lComponentPath = (lMonitorableObjectIsComponent) ? lMonitorableObjectPath.substr(lBoardPathLength + 1) : "";
  
  std::map<std::string, std::string> lMetricLabels = {
    {"system", aSystemName}
  };
  if (aBoardName != "") lMetricLabels["board"] = aBoardName;
  if (lComponentPath != "") lMetricLabels["component"] = lComponentPath;

  // browsing through sub components and metrics
  for (const std::string& lChildrenName : aMonitorableObject.getChildren())
  {
    // if mon obj go in the object; if metric serialise metric values and statuses
    // if else, skip
    if (const swatch::core::MonitorableObject* lChildren = aMonitorableObject.getObjPtr<swatch::core::MonitorableObject> (lChildrenName)) 
    {
      // browse children, if aMonitorableObject is a System, then lChildren is a board and lBoardName is the children name
      const std::string & lBoardName = lMonitorableObjectIsSystem ? lChildrenName : aBoardName;
      this -> inspectMonitorableComponent(aSystemName, lBoardName, *lChildren, aMonitoringStatusLevel);

    } else if (const swatch::core::AbstractMetric* lMetric = aMonitorableObject.getObjPtr<swatch::core::AbstractMetric> (lChildrenName)) 
    {
      // serialise metric and its status
      lMetricLabels["metric"] = lChildrenName;
      const swatch::core::MetricSnapshot lSnapshot = lMetric -> getSnapshot();

      // do not export the value of string-/enum-type SWATCH metrics
      if ( (lSnapshot.getType() == typeid(std::string)) or (lSnapshot.getType() == typeid(swatch::core::tts::State)) ) continue;
      
      this -> exportMonitorableStatus<swatch::core::MetricSnapshot>(
        lSnapshot,
        lMetricLabels,
        aMonitoringStatusLevel,
        this -> mMetricFamilyMap[kSwatchMetricStatusFamily],
        this -> mMetricFamilyMap[kSwatchMetricMonitoringStatusFamily]
      );

      if (lSnapshot.isValueKnown()) 
      {
        double lMetricValue = convertMetricValue(lSnapshot);
        this -> mMetricFamilyMap[kSwatchMetricValueFamily] -> Add(lMetricLabels).Set(lMetricValue);
      }
    }
  }

  // building the alert for the component itself
  // based on the object type we have 4 types of monitorable object statuses
  // 1) system status, raised by systems. aMonitorableObject is a system when board name == ""
  // 2) board status, raised by boards (i.e. processors and DTMs), aMonitorableObject is a board when aComponentPath == ""
  // 3) component status, raised by components (e.g. algo, inputPorts, inputPorts.Rx666 ), aMonitorableObject is a component when aComponentPath != ""
  swatch::core::MonitorableObjectSnapshot lSnapshot = aMonitorableObject.getStatus();
  prometheus::Family<prometheus::Gauge>* lMonitorableStatusFamily = NULL;
  prometheus::Family<prometheus::Gauge>* lMonitoringStatusFamily = NULL;

  if (lComponentPath != "") 
  {
    lMonitorableStatusFamily = this -> mMetricFamilyMap[kSwatchComponentStatusFamily];
    lMonitoringStatusFamily = this -> mMetricFamilyMap[kSwatchComponentMonitoringStatusFamily];
  }
  else if (aBoardName != "") 
  {
    lMonitorableStatusFamily = this -> mMetricFamilyMap[kSwatchBoardStatusFamily];
    lMonitoringStatusFamily = this -> mMetricFamilyMap[kSwatchBoardMonitoringStatusFamily];
  }
  else 
  {
    lMonitorableStatusFamily = this -> mMetricFamilyMap[kSwatchSystemStatusFamily];
    lMonitoringStatusFamily = this -> mMetricFamilyMap[kSwatchSystemMonitoringStatusFamily];
  }

  // removing the label metric if it remained from the previous metric export step, since we are re-using the same map
  lMetricLabels.erase("metric"); 
  this -> exportMonitorableStatus<swatch::core::MonitorableObjectSnapshot>(
    lSnapshot,
    lMetricLabels,
    aMonitoringStatusLevel,
    lMonitorableStatusFamily,
    lMonitoringStatusFamily
  );

}

template<class TMonitorableSnapshot>
void MonitoringThread::exportMonitorableStatus(const TMonitorableSnapshot & aSnapshot, 
                                               const std::map<std::string, std::string> & aMetricLabels,
                                               double monitoringStatusLevel,
                                               prometheus::Family<prometheus::Gauge>* aMonitorableStatusFamily,
                                               prometheus::Family<prometheus::Gauge>* aMonitoringStatusFamily)
{
  // alert exported if monitoring is not disabled for that specific metric
  // the value represents the alert level: 0 is non-monitored, 1 is No limit, 2 is Good, 3 is Unknown, 4 is Warning, 5 is Error
  double lAlertLevel = 0;
  double lMonitoringStatus = this -> computeMonitoringStatusLevel(aSnapshot.getMonitoringStatus(), monitoringStatusLevel);

  // if lMonitoringStatus is not disabled (i.e. == 0)
  if ( lMonitoringStatus > 0 )
  {
    using namespace swatch::core;
    
    const auto & lStatusFlag = aSnapshot.getStatusFlag();
    switch (lStatusFlag) {
      case StatusFlag::kNoLimit: lAlertLevel = 1; break;
      case StatusFlag::kGood: lAlertLevel = 2; break;
      case StatusFlag::kUnknown: lAlertLevel = 3; break;
      case StatusFlag::kWarning: lAlertLevel = 4; break;
      case StatusFlag::kError: lAlertLevel = 5; break;
    }
  }

  aMonitorableStatusFamily -> Add(aMetricLabels).Set(lAlertLevel);
  aMonitoringStatusFamily -> Add(aMetricLabels).Set(lMonitoringStatus);
}

void MonitoringThread::updatePrometheusMetrics(swatch::system::System& aSystem)
{
  LOG4CPLUS_DEBUG(getLogger(), "Monitoring thread: Updating prometheus metrics");
  // looping over all boards to retrieve their metrics
  // there is some parsing of metric path involved
  // to parse the path, I first get the length of the system name, the length of the board path and the length of the metric path, and of the metric name
  // from that, i can separate system name, board name, component path (e.g. "inputPorts.Rx00", or "readout"), and the metric name
  // those end up being the metric labels

  std::string lSystemName = aSystem.getId();
  this -> inspectMonitorableComponent(lSystemName, "", aSystem, this -> computeMonitoringStatusLevel(aSystem.getMonitoringStatus(), 2));
}


void MonitoringThread::fillInfospace(swatch::system::System& aSystem)
{
  LOG4CPLUS_DEBUG(getLogger(), "Monitoring thread: Filling infospace");

  typedef std::chrono::steady_clock SteadyClock_t;
  SteadyClock_t::time_point lStartTime = SteadyClock_t::now();

  resetInfospaceTables();
  std::chrono::milliseconds lResetTime = std::chrono::duration_cast<std::chrono::milliseconds>(SteadyClock_t::now() - lStartTime);
  lStartTime = SteadyClock_t::now();

  // 1) Run number: Set to 0 if out of run, actual value otherwise
  if (mContext.getRunControlOperation().getFSM().getState() == RunControl::kStateRunning)
    mMoniMap["RunNumber"]->setValue(mContext.getRunControlOperation().getRunNumber());
  else
    mMoniMap["RunNumber"]->setValue(xdata::UnsignedLong(0));

  createTables(aSystem);
  std::chrono::milliseconds lCreateTime = std::chrono::duration_cast<std::chrono::milliseconds>(SteadyClock_t::now() - lStartTime);
  lStartTime = SteadyClock_t::now();

  fillTables(aSystem);
  std::chrono::milliseconds lFillTime = std::chrono::duration_cast<std::chrono::milliseconds>(SteadyClock_t::now() - lStartTime);
  lStartTime = SteadyClock_t::now();

  LOG4CPLUS_DEBUG(getLogger(), "Monitoring thread: pushing updated data to infospace");
  mInfospace.push(mMoniMap);
  std::chrono::milliseconds lPushTime = std::chrono::duration_cast<std::chrono::milliseconds>(SteadyClock_t::now() - lStartTime);

  std::ostringstream lMsg;
  lMsg << "Monitoring thread: Infospace tables filled, and data pushed (";
  lMsg << lResetTime.count() << "ms reset + " << lCreateTime.count() << "ms create + " << lFillTime.count() << "ms fill + " << lPushTime.count() << "ms push)";

  LOG4CPLUS_INFO(getLogger(), lMsg.str());
}


void MonitoringThread::resetInfospaceTables()
{
  for (auto lIt = mMoniMap.begin(); lIt != mMoniMap.end(); lIt++) {
    if (xdata::Table* lTable = dynamic_cast<xdata::Table*>(lIt->second)) {
      // 1) Save the column definitions
      std::vector<std::pair<std::string, std::string>> lColumnDefinitions;
      for (const std::string& lColName : lTable->getColumns()) {
        lColumnDefinitions.push_back(std::make_pair(lColName, lTable->getColumnType(lColName)));
      }

      // 2) xdata::Table::clear() - clears both rows and columns from table
      lTable->clear();

      // 3) Add back the column definitions
      for (auto lColumnIt = lColumnDefinitions.begin(); lColumnIt != lColumnDefinitions.end(); lColumnIt++)
        lTable->addColumn(lColumnIt->first, lColumnIt->second);
    }
  }
}


void MonitoringThread::createTables(swatch::system::System& aSystem)
{
  std::map<std::string, std::vector<swatch::action::ActionableObject*>> lActionablesByTableName;
  std::map<std::string, std::vector<swatch::core::MonitorableObject*>> lPortsByTableName;

  // 1) Loop over the processors, and DAQ-TTC managers, determining which of them is of each type
  for (auto lIt = aSystem.getProcessors().begin(); lIt != aSystem.getProcessors().end(); lIt++) {
    swatch::processor::Processor& lProc = **lIt;

    // Determine this processor's table name
    const std::string lTableName = getInfospaceTableName("processor", lProc);

    // If this processor's table doesn't already exists, then add to the map of processors by type
    if (mMoniMap.count(lTableName) == 0) {
      if (lActionablesByTableName.count(lTableName) == 0)
        lActionablesByTableName.insert(std::make_pair(lTableName, std::vector<swatch::action::ActionableObject*>()));
      lActionablesByTableName.at(lTableName).push_back(&lProc);
    }

    auto lRxPorts = lProc.getInputPorts().getPorts();
    for (auto lPortIt = lRxPorts.begin(); lPortIt != lRxPorts.end(); lPortIt++) {
      const std::string lPortTableName = getInfospaceTableName("rxPort", **lPortIt);

      if (mMoniMap.count(lPortTableName) == 0) {

        if (lPortsByTableName.count(lPortTableName) == 0)
          lPortsByTableName.insert(std::make_pair(lPortTableName, std::vector<swatch::core::MonitorableObject*>()));
        lPortsByTableName.at(lPortTableName).push_back(*lPortIt);
      }
    }

    auto lTxPorts = lProc.getOutputPorts().getPorts();
    for (auto lPortIt = lTxPorts.begin(); lPortIt != lTxPorts.end(); lPortIt++) {
      const std::string lPortTableName = getInfospaceTableName("txPort", **lPortIt);

      if (mMoniMap.count(lPortTableName) == 0) {

        if (lPortsByTableName.count(lPortTableName) == 0)
          lPortsByTableName.insert(std::make_pair(lPortTableName, std::vector<swatch::core::MonitorableObject*>()));
        lPortsByTableName.at(lPortTableName).push_back(*lPortIt);
      }
    }
  }

  for (auto lIt = aSystem.getDaqTTCs().begin(); lIt != aSystem.getDaqTTCs().end(); lIt++) {
    swatch::dtm::DaqTTCManager& lDTM = **lIt;

    // Determine this processor's table name
    const std::string lTableName = getInfospaceTableName("amc13", lDTM);

    // If this processor's table doesn't already exists, then add to the map of processors by type
    if (mMoniMap.count(lTableName) == 0) {
      if (lActionablesByTableName.count(lTableName) == 0)
        lActionablesByTableName.insert(std::make_pair(lTableName, std::vector<swatch::action::ActionableObject*>()));
      lActionablesByTableName.at(lTableName).push_back(&lDTM);
    }

    auto lPorts = lDTM.getAMCPorts().getPorts();
    for (auto lPortIt = lPorts.begin(); lPortIt != lPorts.end(); lPortIt++) {
      const std::string lPortTableName = getInfospaceTableName("amc13", **lPortIt);

      if (mMoniMap.count(lPortTableName) == 0) {

        if (lPortsByTableName.count(lPortTableName) == 0)
          lPortsByTableName.insert(std::make_pair(lPortTableName, std::vector<swatch::core::MonitorableObject*>()));
        lPortsByTableName.at(lPortTableName).push_back(*lPortIt);
      }
    }
  }


  // 2) Loop over "new" actionable tables, determining column names for each table
  for (auto lIt = lActionablesByTableName.begin(); lIt != lActionablesByTableName.end(); lIt++) {
    if (!checkColumnName(lIt->first)) {
      LOG4CPLUS_WARN(getLogger(), "Actionable table name '" + lIt->first + "' is not a valid column name; cannot add this table.");
      continue;
    }

    std::ostringstream lLogMsg;
    lLogMsg << "Monitoring thread: creating table '" << lIt->first << "' for " << lIt->second.size() << " boards:";
    for (auto lActionableIt = lIt->second.begin(); lActionableIt != lIt->second.end(); lActionableIt++)
      lLogMsg << "  '" << (*lActionableIt)->getId() << "'";
    LOG4CPLUS_INFO(getLogger(), lLogMsg.str());

    mMoniMap[lIt->first] = createActionableTable(lIt->second);
  }

  // 3) Loop over "new" port tables, determining column names for each table
  for (auto lIt = lPortsByTableName.begin(); lIt != lPortsByTableName.end(); lIt++) {
    if (!checkColumnName(lIt->first)) {
      LOG4CPLUS_WARN(getLogger(), "Port table name '" + lIt->first + "' is not a valid column name; cannot add this table.");
      continue;
    }

    std::ostringstream lLogMsg;
    lLogMsg << "Monitoring thread: creating table '" << lIt->first << "' for " << lIt->second.size() << " ports:";
    for (auto lActionableIt = lIt->second.begin(); lActionableIt != lIt->second.end(); lActionableIt++)
      lLogMsg << "  '" << (*lActionableIt)->getId() << "'";
    LOG4CPLUS_INFO(getLogger(), lLogMsg.str());

    mMoniMap[lIt->first] = createPortTable(lIt->second);
  }
}


xdata::Table* MonitoringThread::createActionableTable(const std::vector<swatch::action::ActionableObject*>& lActionables)
{
  swatch::action::ActionableObject& lObj0 = *lActionables.at(0);

  xdata::Table* lTable = new xdata::Table();
  lTable->addColumn("id", xdata::String().type());

  for (swatch::core::Object::iterator lIt = lObj0.begin(); lIt != lObj0.end(); lIt++) {
    if (const swatch::core::AbstractMetric* lMetric = dynamic_cast<const swatch::core::AbstractMetric*>(&*lIt)) {
      // Skip metrics within ports
      if (const swatch::processor::Processor* lProc = dynamic_cast<swatch::processor::Processor*>(&lObj0)) {
        if (lProc->getInputPorts().isAncestorOf(*lMetric) || lProc->getOutputPorts().isAncestorOf(*lMetric)) {
          if (lObj0.getNumberOfGenerationsTo(*lMetric) > 2)
            continue;
        }
      }
      else {
        if (lObj0.getNumberOfGenerationsTo(*lMetric) > 2)
          continue;
      }

      const std::string lMetricRelPath = lMetric->getPath().substr(lObj0.getPath().size() + 1);
      bool lMetricExistsInAllProcessors = true;

      for (auto lAcIt = lActionables.begin(); lAcIt != lActionables.end(); lAcIt++) {
        try {
          (*lAcIt)->getObj<swatch::core::AbstractMetric>(lMetricRelPath);
        }
        catch (const std::exception& aException) {
          lMetricExistsInAllProcessors = false;
        }
      }

      if (lMetricExistsInAllProcessors) {
//        // Check that the metric doesn't contain any "_", since "." will be replaced by "_" for column names
//        if (lMetricRelPath.find('_') != std::string::npos) {
//          LOG4CPLUS_WARN(No , "Cannot create infospace table column for actionable metric '" << lMetricRelPath << "' since it contains one or more underscores, '_'");
//          continue;
//        }

        std::string lColName = lMetricRelPath;
        std::replace(lColName.begin(), lColName.end(), '.', '_');

        if (!checkColumnName(lColName)) {
          LOG4CPLUS_WARN(getLogger(), "Cannot create infospace table column '" << lColName << "' - invalid column name.");
          continue;
        }

        std::shared_ptr<xdata::Serializable> lData(tools::getMetricValueAsSerializable(lMetric->getSnapshot()));
        lTable->addColumn(lColName, lData->type());
      }
    }
  }

  return lTable;
}


xdata::Table* MonitoringThread::createPortTable(const std::vector<swatch::core::MonitorableObject*>& lPorts)
{
  swatch::core::MonitorableObject& lPort0 = *lPorts.at(0);

  xdata::Table* lTable = new xdata::Table();
  lTable->addColumn("boardId", xdata::String().type());
  lTable->addColumn("portId", xdata::String().type());
  if (dynamic_cast<swatch::action::MaskableObject*>(&lPort0))
    lTable->addColumn("isMasked", xdata::Boolean().type());

  for (swatch::core::Object::iterator lIt = lPort0.begin(); lIt != lPort0.end(); lIt++) {
    if (const swatch::core::AbstractMetric* lMetric = dynamic_cast<const swatch::core::AbstractMetric*>(&*lIt)) {
      // Skip metrics within ports -- currently achieved by excluding all metrics more than two generations below
      if (lPort0.getNumberOfGenerationsTo(*lMetric) > 2)
        continue;

      const std::string lMetricRelPath = lMetric->getPath().substr(lPort0.getPath().size() + 1);
      bool lMetricExistsInAllProcessors = true;

      for (auto lPortIt = lPorts.begin(); lPortIt != lPorts.end(); lPortIt++) {
        try {
          (*lPortIt)->getObj<swatch::core::AbstractMetric>(lMetricRelPath);
        }
        catch (const std::exception& aException) {
          lMetricExistsInAllProcessors = false;
        }
      }

      if (lMetricExistsInAllProcessors) {
//        // Check that the metric doesn't contain any "_", since "." will be replaced by "_" for column names
//        if (lMetricRelPath.find('_') != std::string::npos) {
//          LOG4CPLUS_WARN(mLogger, "Cannot create infospace table column for actionable metric '" << lMetricRelPath << "' since it contains one or more underscores, '_'");
//          continue;
//        }

        std::string lColName = lMetricRelPath;
        std::replace(lColName.begin(), lColName.end(), '.', '_');

        if (!checkColumnName(lColName)) {
          LOG4CPLUS_WARN(getLogger(), "Cannot create infospace table column '" << lColName << "' - invalid column name.");
          continue;
        }

        std::shared_ptr<xdata::Serializable> lData(tools::getMetricValueAsSerializable(lMetric->getSnapshot()));
        lTable->addColumn(lColName, lData->type());
      }
    }
  }

  return lTable;
}


void MonitoringThread::fillTables(swatch::system::System& aSystem)
{
  LOG4CPLUS_DEBUG(getLogger(), "Monitoring thread: Filling tables");

  for (auto lProcIt = aSystem.getProcessors().begin(); lProcIt != aSystem.getProcessors().end(); lProcIt++) {
    swatch::processor::Processor& lProc = **lProcIt;

    if (lProc.getStatus().isEnabled() && lProc.getMonitoringStatus() != swatch::core::monitoring::kDisabled) {
      // Determine this processor's table name, and fill that table
      fillActionableTable(getInfospaceTableName("processor", lProc), lProc);

      swatch::processor::InputPortCollection& lRxPorts = lProc.getInputPorts();
      if (lRxPorts.getMonitoringStatus() != swatch::core::monitoring::kDisabled) {
        for (auto lPortIt = lRxPorts.getPorts().begin(); lPortIt != lRxPorts.getPorts().end(); lPortIt++) {
          if ((*lPortIt)->getMonitoringStatus() != swatch::core::monitoring::kDisabled)
            fillPortTable(getInfospaceTableName("rxPort", **lPortIt), lProc.getId(), **lPortIt);
        }
      } // for: rx ports

      swatch::processor::OutputPortCollection& lTxPorts = lProc.getOutputPorts();
      if (lTxPorts.getMonitoringStatus() != swatch::core::monitoring::kDisabled) {
        for (auto lPortIt = lTxPorts.getPorts().begin(); lPortIt != lTxPorts.getPorts().end(); lPortIt++) {
          if ((*lPortIt)->getMonitoringStatus() != swatch::core::monitoring::kDisabled)
            fillPortTable(getInfospaceTableName("txPort", **lPortIt), lProc.getId(), **lPortIt);
        }
      } // for: tx ports
    }
  } // for: processors


  for (auto lAMC13It = aSystem.getDaqTTCs().begin(); lAMC13It != aSystem.getDaqTTCs().end(); lAMC13It++) {
    swatch::dtm::DaqTTCManager& lDTM = **lAMC13It;

    if (lDTM.getStatus().isEnabled() && lDTM.getMonitoringStatus() != swatch::core::monitoring::kDisabled) {
      // Determine this AMC13's table name, and fill that table
      fillActionableTable(getInfospaceTableName("amc13", lDTM), lDTM);

      // Iterate over backplane ports
      if (lDTM.getAMCPorts().getMonitoringStatus() != swatch::core::monitoring::kDisabled) {
        for (auto lPortIt = lDTM.getAMCPorts().getPorts().begin(); lPortIt != lDTM.getAMCPorts().getPorts().end(); lPortIt++) {
          swatch::dtm::AMCPort& lPort = **lPortIt;

          if (lPort.getMonitoringStatus() != swatch::core::monitoring::kDisabled)
            fillPortTable(getInfospaceTableName("amc13", lPort), lDTM.getId(), lPort);
        } // for: AMC ports
      }
    }
  } // for: AMC13s
}


void MonitoringThread::fillActionableTable(const std::string& aTableName, swatch::action::ActionableObject& aActionable)
{
  auto lTableIt = mMoniMap.find(aTableName);
  if (lTableIt == mMoniMap.end()) {
    LOG4CPLUS_WARN(getLogger(), "Could not find any infospace table of name '" + aTableName + "' for actionable '" + aActionable.getPath() + "'");
  }
  else {
    LOG4CPLUS_DEBUG(getLogger(), "Filling infospace table '" + aTableName + "' with info from actionable '" + aActionable.getPath() + "'");
    xdata::Table& lTable = dynamic_cast<xdata::Table&>(*lTableIt->second);
    size_t lRowIdx = lTable.getRowCount();

    // Set the id column
    xdata::String lId(aActionable.getId());
    lTable.setValueAt(lRowIdx, "id", lId);

    // Set the other columns to values of metrics
    std::vector<std::string> lColumnNamesVec = lTable.getColumns();
    std::unordered_map<std::string, const swatch::core::AbstractMetric*> lMetricColumns;
    for (auto lIt = lColumnNamesVec.begin(); lIt != lColumnNamesVec.end(); lIt++) {
      if (*lIt != "id")
        lMetricColumns.insert(std::make_pair(*lIt, (const swatch::core::AbstractMetric*)NULL));
    }
    for (swatch::core::Object::iterator lIt = aActionable.begin(); lIt != aActionable.end(); lIt++) {
      if (const swatch::core::AbstractMetric* lMetric = dynamic_cast<const swatch::core::AbstractMetric*>(&*lIt)) {

        // TODO: Replace with metric path -> col name conversion function
        std::string lColName = lMetric->getPath().substr(aActionable.getPath().size() + 1);
        std::replace(lColName.begin(), lColName.end(), '.', '_');

        if (lMetricColumns.find(lColName) != lMetricColumns.end()) {
          if (lMetricColumns[lColName] != NULL) {
            LOG4CPLUS_ERROR(getLogger(), "Infospace column '" << lColName << "' in current row of table '" << aTableName << "', has already been filled by another metric, '" << lMetricColumns[lColName]->getPath() << "'; cannot set this cell to value of metric '" << lMetric->getPath() << "'");
          }
          try {
            std::shared_ptr<xdata::Serializable> lData(tools::getMetricValueAsSerializable(lMetric->getSnapshot()));
            lTable.setValueAt(lRowIdx, lColName, const_cast<xdata::Serializable&>(*lData));
            lMetricColumns[lColName] = lMetric;
          }
          catch (const std::exception& aException) {
            LOG4CPLUS_ERROR(getLogger(), "Exception of type '" << swatch::core::demangleName(typeid(aException).name()) << "' thrown when filling infospace table '" << aTableName << "' column '" << lColName << "', from object '" << aActionable.getPath() << "'");
          }
        }
      }
    }

    for (auto lIt = lMetricColumns.begin(); lIt != lMetricColumns.end(); lIt++) {
      if (lIt->second == NULL) {
        LOG4CPLUS_WARN(getLogger(), "Infospace column '" << lIt->first << "' in table '" << aTableName << "' was not filled in row " << lRowIdx << " for actionable '" << aActionable.getPath() << "' because no corresponding metric was found.");
      }
    }
  }
}


void MonitoringThread::fillPortTable(const std::string& aTableName, const std::string& aActionableId, swatch::core::MonitorableObject& aPort)
{
  auto lTableIt = mMoniMap.find(aTableName);
  if (lTableIt == mMoniMap.end()) {
    LOG4CPLUS_WARN(getLogger(), "Could not find any infospace table of name '" + aTableName + "' for port '" + aPort.getPath() + "'");
  }
  else {

    LOG4CPLUS_DEBUG(getLogger(), "Filling infospace table '" + aTableName + "' with info from port '" + aPort.getPath() + "'");
    xdata::Table& lTable = dynamic_cast<xdata::Table&>(*lTableIt->second);
    size_t lRowIdx = lTable.getRowCount();

    // Set the fixed columns: boardId, id & isMasked column
    xdata::String lBoardId(aActionableId);
    lTable.setValueAt(lRowIdx, "boardId", lBoardId);
    xdata::String lPortId(aPort.getId());
    lTable.setValueAt(lRowIdx, "portId", lPortId);
    if (swatch::action::MaskableObject* lMaskable = dynamic_cast<swatch::action::MaskableObject*>(&aPort)) {
      xdata::Boolean lIsMasked(lMaskable->isMasked());
      lTable.setValueAt(lRowIdx, "isMasked", lIsMasked);
    }

    // Set the other columns to values of metrics
    std::vector<std::string> lColumnNamesVec = lTable.getColumns();
    std::unordered_map<std::string, const swatch::core::AbstractMetric*> lMetricColumns;
    for (auto lIt = lColumnNamesVec.begin(); lIt != lColumnNamesVec.end(); lIt++) {
      if ((*lIt != "portId") && (*lIt != "boardId") && (*lIt != "isMasked"))
        lMetricColumns.insert(std::make_pair(*lIt, (const swatch::core::AbstractMetric*)NULL));
    }
    for (swatch::core::Object::iterator lIt = aPort.begin(); lIt != aPort.end(); lIt++) {
      if (const swatch::core::AbstractMetric* lMetric = dynamic_cast<const swatch::core::AbstractMetric*>(&*lIt)) {

        // TODO: Replace with metric path -> col name conversion function
        std::string lColName = lMetric->getPath().substr(aPort.getPath().size() + 1);
        std::replace(lColName.begin(), lColName.end(), '.', '_');

        if (lMetricColumns.find(lColName) != lMetricColumns.end()) {
          if (lMetricColumns[lColName] != NULL) {
            LOG4CPLUS_ERROR(getLogger(), "Infospace column '" << lColName << "' in current row of table '" << aTableName << "', has already been filled by another metric, '" << lMetricColumns[lColName]->getPath() << "'; cannot set this cell to value of metric '" << lMetric->getPath() << "'");
          }
          try {
            std::shared_ptr<xdata::Serializable> lData(tools::getMetricValueAsSerializable(lMetric->getSnapshot()));
            lTable.setValueAt(lRowIdx, lColName, const_cast<xdata::Serializable&>(*lData));
            lMetricColumns[lColName] = lMetric;
          }
          catch (const std::exception& aException) {
            LOG4CPLUS_ERROR(getLogger(), "Exception of type '" << swatch::core::demangleName(typeid(aException).name()) << "' thrown when filling infospace table '" << aTableName << "' column '" << lColName << "', from object '" << aPort.getPath() << "'");
          }
        }
      }
    }

    for (auto lIt = lMetricColumns.begin(); lIt != lMetricColumns.end(); lIt++) {
      if (lIt->second == NULL) {
        LOG4CPLUS_WARN(getLogger(), "Infospace column '" << lIt->first << "' in table '" << aTableName << "' was not filled in row " << lRowIdx << " for port '" << aPort.getPath() << "' because no corresponding metric was found.");
      }
    }
  }
}


std::string MonitoringThread::getInfospaceTableName(const std::string& aPrefix, const swatch::core::Object& aObject)
{
  std::string lClassName = swatch::core::demangleName(typeid(aObject).name());
  std::string lTableName = aPrefix + "_" + lClassName.substr(lClassName.rfind(':') + 1);
  std::replace(lTableName.begin(), lTableName.end(), ':', '#');
  return lTableName;
}


bool MonitoringThread::checkColumnName(const std::string& aColName)
{
  std::cmatch lMatch;
  if (!std::regex_match(aColName.c_str(), lMatch, kColNameRegex))
    return false;
  else if (aColName.size() > 30)
    return false;
  else
    return true;
}


std::string MonitoringThread::getLowercaseCopy(const std::string& aString)
{
  std::string lInputString(aString);
  boost::algorithm::to_lower(lInputString);
  return lInputString;
}

int MonitoringThread::extractPortFromURL(const std::string& aURL)
{
  // stripping the protocol part if existing
  size_t lProtocolPosition = aURL.find("://");
  // lURL_new is the lURL excluding the http part
  std::string lURL_new = (lProtocolPosition != std::string::npos) ? aURL.substr(lProtocolPosition + 3) : aURL; 
  size_t lColonPosition = lURL_new.find_first_of(":");
  size_t lSlashPosition = lURL_new.find_first_of("/");

  // port spans from first : to first / or for the entire string after the first : if / is not present
  std::string lPort = lURL_new.substr(lColonPosition + 1, 
                                      (lSlashPosition != std::string::npos) ? 
                                      lSlashPosition - lColonPosition - 1 : 
                                      std::string::npos);
  return std::stoi(lPort);
}


} // ns: swatchcellframework
