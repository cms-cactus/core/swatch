/* 
 * File:   MonitoringThread.hpp
 * Author: tom
 * Date:   February 2016
 */

#ifndef __SWATCHCELL_FRAMEWORK_MONITORINGTHREAD_H__
#define __SWATCHCELL_FRAMEWORK_MONITORINGTHREAD_H__


// Standard headers
#include <memory>
#include <stddef.h>
#include <regex>


// xdata types
#include "xdata/xdata.h"
#include "xdata/TableIterator.h"
#include "xdata/Table.h"

///! GC THE SLAYER
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/Integer.h"
#include "xdata/TimeVal.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedShort.h"

#include "swatchcell/framework/InfoSpaceBuilder.h"
#include "swatchcell/framework/PeriodicWorkerThread.hpp"
#include "swatch/system/System.hpp"


// prometheus headers
#include <prometheus/counter.h>
#include <prometheus/exposer.h>
#include <prometheus/registry.h>
#include <prometheus/text_serializer.h>
#include <prometheus/exposer.h>
// metric map
#include <unordered_map>

// Forward declarations
namespace swatch {
namespace action {
class ActionableObject;
class MaskableObject;
}
}

namespace swatchcellframework {

// Forward declarations
class CellAbstract;
class CellContext;


class MonitoringThread : public PeriodicWorkerThread {
public:

  MonitoringThread(const std::string& aParentLoggerName, CellContext& aContext, size_t aPeriodInSeconds);

  ~MonitoringThread();

  inline bool getWriteToMonitoringDBFlag() const { return mWriteToMonitoringDB; };

  Stats getStats() const;
  
  //!build xml flashlist definition out of the Infospace
  inline void getFlashlist( std::ostream & flash ) { mInfospace.getFlashlist( flash ); };

  void start();
  void stop();

private:
  //! Iterates over the enabled processors & AMC13s in the system, and calls the SWATCH API's "update metrics" methods 
  void doWork(); 
  
  CellContext& mContext;


  //! ! GC THE SLAYER
  InfoSpaceBuilder mInfospace;
  std::map< std::string, xdata::Serializable * > mMoniMap;

  std::shared_ptr<prometheus::Registry> mMetricsRegistry;
  std::unique_ptr<prometheus::Exposer> mExposer;

  std::unordered_map< std::string, prometheus::Family<prometheus::Gauge>* > mMetricFamilyMap;

  static const std::string kSwatchMetricValueFamily;

  static const std::string kSwatchMetricStatusFamily;
  static const std::string kSwatchComponentStatusFamily;
  static const std::string kSwatchBoardStatusFamily;
  static const std::string kSwatchSystemStatusFamily;

  static const std::string kSwatchMetricMonitoringStatusFamily;
  static const std::string kSwatchComponentMonitoringStatusFamily;
  static const std::string kSwatchBoardMonitoringStatusFamily;
  static const std::string kSwatchSystemMonitoringStatusFamily;

  static double computeMonitoringStatusLevel(
    const swatch::core::monitoring::Status & aStatus,
    const double monitoringStatusLevel
  );

  template<class TMonitorableSnapshot>
  void exportMonitorableStatus(const TMonitorableSnapshot & aSnapshot, 
                               const std::map<std::string, std::string> & aMetricLabels,
                               double monitoringStatusLevel,
                               prometheus::Family<prometheus::Gauge>* aMonitorableStatusFamily,
                               prometheus::Family<prometheus::Gauge>* aMonitoringStatusFamily);
  
  static std::regex kColNameRegex;

  void initPrometheusMetrics(swatch::system::System& aSystem);
  void updatePrometheusMetrics(swatch::system::System& aSystem );
  void inspectMonitorableComponent (
    const std::string & aSystemName,
    const std::string & aBoardName,
    const swatch::core::MonitorableObject & aMonitorableObject,
    double aMonitoringStatusLevel
  );
  static const std::string kSwatchCellDisableMonitoringDBFlag;
  static const std::string kSwatchCellDisableOpenMetricsExporterFlag;
  
  //! Determines if cell writes to monitoring DB, false if env variable in kSwatchCellDisableMonitoringDBFlag is set
  bool mWriteToMonitoringDB;
  //! Determines if cell exports metrics in OpenMetrics format , false if env variable in kSwatchCellDisableOpenMetricsExporterFlag is set
  bool mExportOpenMetrics;

  void fillInfospace( swatch::system::System& aSystem );

  void resetInfospaceTables();

  void createTables(swatch::system::System& aSystem);

  xdata::Table* createActionableTable(const std::vector<swatch::action::ActionableObject*>& lActionables);

  xdata::Table* createPortTable(const std::vector<swatch::core::MonitorableObject*>& lPorts);

  void fillProcessorTables(swatch::system::System& aSystem);

  void fillTables(swatch::system::System& aSystem);
  
  void fillActionableTable(const std::string& aTableName, swatch::action::ActionableObject& aActionable);

  void fillPortTable(const std::string& aTableName, const std::string& aActionableId, swatch::core::MonitorableObject& aPort);

  static std::string getInfospaceTableName(const std::string& aPrefix, const swatch::core::Object& aObject);

  static bool checkColumnName(const std::string& aColName);

  static std::string getLowercaseCopy(const std::string& aString);
  
  static int extractPortFromURL(const std::string& aURL);
};
  
  
} // end ns: swatchcellframework


#endif	/* _SWATCHCELL_FRAMEWORK_MONITORINGTHREAD_HPP_ */

