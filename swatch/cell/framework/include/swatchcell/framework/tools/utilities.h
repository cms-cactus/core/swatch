
#ifndef __SWATCHCELL_FRAMEWORK_TOOLS_UTILITIES_H__
#define __SWATCHCELL_FRAMEWORK_TOOLS_UTILITIES_H__


#include <memory>
#include <typeinfo>


// Forward declarations 
namespace boost {
class any;
}
namespace xdata {
class Serializable;
}

namespace swatch {
namespace action {
class ActionableObject;
}
namespace core {
class MetricSnapshot;
class ParameterSet;
}
namespace system {
class System;
}
}


namespace swatchcellframework {
namespace tools {

  //! Calls updateMetrics function on all processors and AMC13s that are enabled 
void updateMetricsOfEnabledObjects(swatch::system::System& aSystem);

//! Calls update method of all metrics within an actionable object ((e.g. processor, system, ...), and all of its descendants that are non-disabled MonitorableObject  
void updateMetrics(swatch::action::ActionableObject& aObj);

std::shared_ptr<xdata::Serializable> getMetricValueAsSerializable(const swatch::core::MetricSnapshot& aSnapshot);

const xdata::Serializable& getSerializable(const swatch::core::ParameterSet&, const std::string&);

const xdata::Serializable& getSerializable(const boost::any&);

void addSerializable(swatch::core::ParameterSet&, const std::string&, const std::type_info&, const std::string&);

} // end ns: tools
} // end ns: swatchcellframework

#endif /* _swatchcell_framework_tools_utilities_h_ */

