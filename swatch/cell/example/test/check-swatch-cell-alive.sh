#!/usr/bin/env bash

# verifies the cell start up fine and responds to http requests

# starts cell
cell/example/test/runStandalone.sh 1> output 2> output  &
cell_pid=$!
# waits for the cell to start up
echo "Waiting for the cell to start up, sleeping 20s"
sleep 20s
# pings cell
echo "Pinging cell"
curl -L --fail --silent ${HOSTNAME}:3333/urn:xdaq-application:lid=13/ 1> /dev/null
curl_errcode=$?
killall xdaq.exe

# returns error if fail
if [[ ${curl_errcode} -eq 0 ]]; then
  echo "Cell was started up and successfully responded to HTTP requests."
  exit 0
else
  >&2 echo "Communication with cell failed. Verify that the cell successfully started up in the following log:\n\n"
  cat output
  exit ${curl_errcode}
fi