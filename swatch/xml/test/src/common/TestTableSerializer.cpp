// Boost Unit Test includes
#include <boost/test/unit_test.hpp>

// boost headers
#include "boost/any.hpp"

// XDAQ headers
#include <xdata/Table.h>

// swatch headers
#include "swatch/xml/TableSerializer.hpp"
//others
#include "pugixml.hpp"


namespace swatch {
namespace xml {
namespace test {

BOOST_AUTO_TEST_SUITE(TestTableSerializer)

BOOST_AUTO_TEST_CASE(TestTable)
{
  pugi::xml_document lDoc;
  lDoc.load_string(
      "<param id='test' type='table'>"
      "   <columns>aaa,bbb,ccc,ddd</columns>"
      "   <types>int,uint,uint64,float</types>"
      "   <rows>"
      "      <row>1,2,3,4</row>"
      "      <row>5,6,7,8</row>"
      "   </rows>"
      "</param>");
  pugi::xml_node lNode = lDoc.child("param");
  TableSerializer lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "table");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  // Check columns
  std::vector<std::string> lExpColumns = { "aaa", "bbb", "ccc", "ddd" };
  std::vector<std::string> lColumns = boost::any_cast<xdata::Table>(lResult.get())->getColumns();

  BOOST_CHECK_EQUAL_COLLECTIONS(lColumns.begin(), lColumns.end(), lExpColumns.begin(), lExpColumns.end());
  std::string lExpected = "{\"rows\":2,\"cols\":4,\"definition\":[[\"aaa\",\"int\"],[\"bbb\",\"unsigned int\"],[\"ccc\",\"unsigned int 64\"],[\"ddd\",\"float\"]],\"data\":[[1,2,3,4.00000e+00],[5,6,7,8.00000e+00]]}";

  BOOST_CHECK_EQUAL(boost::any_cast<xdata::Table>(lResult.get())->toString(), lExpected);
}

BOOST_AUTO_TEST_CASE(TestTableDelimiter)
{
  pugi::xml_document doc;
  doc.load_string(
      "<param id='test' type='table' delimiter='|'>"
      "   <columns>aaa|bbb|ccc</columns>"
      "   <types>uint|uint|uint</types>"
      "   <rows>"
      "      <row>1|2|4</row>"
      "      <row>5|6|7</row>"
      "   </rows>"
      "</param>");
  pugi::xml_node lNode = doc.child("param");
  TableSerializer lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "table");

  std::unique_ptr<const boost::any> result(lSerializer.import(lNode));

  std::string expected = "{\"rows\":2,\"cols\":3,\"definition\":[[\"aaa\",\"unsigned int\"],[\"bbb\",\"unsigned int\"],[\"ccc\",\"unsigned int\"]],\"data\":[[1,2,4],[5,6,7]]}";

  BOOST_CHECK_EQUAL(boost::any_cast<xdata::Table>(result.get())->toString(), expected);
}

BOOST_AUTO_TEST_CASE(TestIntruderTag)
{
  pugi::xml_document doc;
  doc.load_string(
      "<param id='test' type='table'>"
      "   <columns>aaa,bbb,ccc</columns>"
      "   <rows>"
      "      <row>1,2,4</row>"
      "      <not_a_row>5,6,7</not_a_row>"
      "   </rows>"
      "</param>");
  pugi::xml_node lNode = doc.child("param");
  TableSerializer lSerializer;

  BOOST_CHECK_THROW(lSerializer.import(lNode), swatch::xml::ValueError);
}


BOOST_AUTO_TEST_CASE(TestMissingTag)
{
  pugi::xml_document doc;
  doc.load_string(
      "<param id='test' type='table'>"
      "   <columns>aaa,bbb,ccc</columns>"
      "   <rows>"
      "      <row>1,2,4</row>"
      "      <row>5,6,7</row>"
      "   </rows>"
      "</param>");
  pugi::xml_node lNode = doc.child("param");
  TableSerializer lSerializer;

  BOOST_CHECK_THROW(lSerializer.import(lNode), swatch::xml::ValueError);
}

BOOST_AUTO_TEST_CASE(TestColRowMismatch)
{
  pugi::xml_document doc;
  doc.load_string(
      "<param id='test' type='table'>"
      "   <columns>aaa,bbb,ccc</columns>"
      "   <rows>"
      "      <row>1,2</row>"
      "      <row>5,6,7</row>"
      "   </rows>"
      "</param>");
  pugi::xml_node lNode = doc.child("param");
  TableSerializer lSerializer;

  BOOST_CHECK_THROW(lSerializer.import(lNode), swatch::xml::ValueError);
}

BOOST_AUTO_TEST_SUITE_END() // TestTableSerializer

} // namespace swatch
} // namespace xml
} // namespace test
