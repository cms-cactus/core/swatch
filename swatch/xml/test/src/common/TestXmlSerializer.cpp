// Boost Unit Test includes
#include <boost/test/unit_test.hpp>

// xdaq
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/SimpleType.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Vector.h"

// SWATCH headers
#include "swatch/core/exception.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/xml/VectorSerializer.hpp"
#include "swatch/xml/XmlSerializer.hpp"

//others
#include "pugixml.hpp"


namespace swatch {
namespace xml {
namespace test {

namespace {
const xdata::Serializable& asSerializable(const boost::any& aAny)
{
  const std::type_info& lType = aAny.type();
  const xdata::Serializable* lData = NULL;
  if (lType == typeid(xdata::Boolean))
    lData = boost::any_cast<xdata::Boolean>(&aAny);
  else if (lType == typeid(xdata::Integer))
    lData = boost::any_cast<xdata::Integer>(&aAny);
  else if (lType == typeid(xdata::Integer32))
    lData = boost::any_cast<xdata::Integer32>(&aAny);
  else if (lType == typeid(xdata::Integer64))
    lData = boost::any_cast<xdata::Integer64>(&aAny);
  else if (lType == typeid(xdata::UnsignedInteger))
    lData = boost::any_cast<xdata::UnsignedInteger>(&aAny);
  else if (lType == typeid(xdata::UnsignedInteger32))
    lData = boost::any_cast<xdata::UnsignedInteger32>(&aAny);
  else if (lType == typeid(xdata::UnsignedInteger64))
    lData = boost::any_cast<xdata::UnsignedInteger64>(&aAny);
  else if (lType == typeid(xdata::UnsignedShort))
    lData = boost::any_cast<xdata::UnsignedShort>(&aAny);
  else if (lType == typeid(xdata::UnsignedLong))
    lData = boost::any_cast<xdata::UnsignedLong>(&aAny);
  else if (lType == typeid(xdata::Float))
    lData = boost::any_cast<xdata::Float>(&aAny);
  else if (lType == typeid(xdata::Double))
    lData = boost::any_cast<xdata::Double>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Boolean>))
    lData = boost::any_cast<xdata::Vector<xdata::Boolean>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Integer>))
    lData = boost::any_cast<xdata::Vector<xdata::Integer>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Integer32>))
    lData = boost::any_cast<xdata::Vector<xdata::Integer32>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Integer64>))
    lData = boost::any_cast<xdata::Vector<xdata::Integer64>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::UnsignedInteger>))
    lData = boost::any_cast<xdata::Vector<xdata::UnsignedInteger>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::UnsignedInteger32>))
    lData = boost::any_cast<xdata::Vector<xdata::UnsignedInteger32>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::UnsignedInteger64>))
    lData = boost::any_cast<xdata::Vector<xdata::UnsignedInteger64>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::UnsignedShort>))
    lData = boost::any_cast<xdata::Vector<xdata::UnsignedShort>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::UnsignedLong>))
    lData = boost::any_cast<xdata::Vector<xdata::UnsignedLong>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Float>))
    lData = boost::any_cast<xdata::Vector<xdata::Float>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::Double>))
    lData = boost::any_cast<xdata::Vector<xdata::Double>>(&aAny);
  else if (lType == typeid(xdata::Vector<xdata::String>))
    lData = boost::any_cast<xdata::Vector<xdata::String>>(&aAny);
  else if (lType == typeid(xdata::String))
    lData = boost::any_cast<xdata::String>(&aAny);
  else if (lType == typeid(xdata::Table))
    lData = boost::any_cast<xdata::Table>(&aAny);
  else
    SWATCH_THROW(core::RuntimeError("Could not extract a xdata::Serializable-derived type from boost::any containing type " + core::demangleName(lType.name())));

  return *lData;
}

}


struct XmlSerializerTestSetup {
  std::unique_ptr<XmlSerializer> serializer;
  std::vector<std::string> inputs, outputs, types;


  XmlSerializerTestSetup() :
    serializer(new XmlSerializer()), inputs(), outputs(), types()
  {
    inputs = {
      // vectors
      "<param id='testUintV' type='vector:uint'>40, 30, 1, 2</param>",
      "<param id='testUint64V' type='vector:uint64'>0x2800000000, 30, 1, 2</param>",
      "<param id='testIntV' type='vector:int'>-2, 2, -42</param>",
      "<param id='testFloatV' type='vector:float'>-2.2, 2.3, -42.0</param>",
      "<param id='testBoolV' type='vector:bool'>false, false, true</param>",
      "<param id='testStringV' type='vector:string'>hello, is, it, me, you're, looking, for</param>",
      // single objects
      "<param id='testUint' type='uint'>40</param>",
      "<param id='testUint64' type='uint64'>0x2800000000</param>",
      "<param id='testInt' type='int'>-2</param>",
      "<param id='testFloat' type='float'>-2.2</param>",
      "<param id='testBool' type='bool'>false</param>",
      "<param id='testString' type='string'>hello</param>",
      "<param id='test' type='table' delimiter='|'>"
      "   <columns>aaa|bbb|ccc</columns>"
      "   <types>uint|uint|uint</types>"
      "   <rows>"
      "      <row>1|2|4</row>"
      "      <row>5|6|7</row>"
      "   </rows>"
      "</param>"
    };
    outputs = {
      "[40,30,1,2]",
      "[171798691840,30,1,2]",
      "[-2,2,-42]",
      "[-2.20000e+00,2.30000e+00,-4.20000e+01]",
      "[false,false,true]",
      "[hello,is,it,me,you're,looking,for]",
      "40",
      "171798691840",
      "-2",
      "-2.20000e+00",
      "false",
      "hello",
      "{\"rows\":2,\"cols\":3,\"definition\":[[\"aaa\",\"unsigned int\"],[\"bbb\",\"unsigned int\"],[\"ccc\",\"unsigned int\"]],\"data\":[[1,2,4],[5,6,7]]}"
    };
    types = {
      "vector",
      "vector",
      "vector",
      "vector",
      "vector",
      "vector",
      "unsigned int",
      "unsigned int 64",
      "int",
      "float",
      "bool",
      "string",
      "table"
    };
    BOOST_REQUIRE_EQUAL(inputs.size(), outputs.size());
    BOOST_REQUIRE_EQUAL(inputs.size(), types.size());
  }
};


BOOST_AUTO_TEST_SUITE(TestXmlSerializer)

BOOST_FIXTURE_TEST_CASE(TestImport, XmlSerializerTestSetup)
{
  for (unsigned int i = 0; i < inputs.size(); ++i) {
    pugi::xml_document lDoc;
    BOOST_REQUIRE_EQUAL(lDoc.load_string(inputs.at(i).c_str()), true);
    pugi::xml_node lNode = lDoc.child("param");
    BOOST_REQUIRE_EQUAL(lNode.empty(), false);
    std::unique_ptr<const boost::any> lResult(serializer->import(lNode));

    const xdata::Serializable& lData = asSerializable(*lResult);
    BOOST_CHECK_EQUAL(lData.type(), types.at(i));
    BOOST_CHECK_EQUAL(lData.toString(), outputs.at(i));
  }
}


BOOST_FIXTURE_TEST_CASE(TestInvalid, XmlSerializerTestSetup)
{
  std::string lInput = "<param id='test' type='vector:uint'>40, error, 1, 2</param>";
  pugi::xml_document lDoc;
  BOOST_REQUIRE_EQUAL(lDoc.load_string(lInput.c_str()), true);
  pugi::xml_node lNode = lDoc.child("param");

  BOOST_CHECK_THROW(serializer->import(lNode), swatch::xml::ValueError);
}


BOOST_FIXTURE_TEST_CASE(TestUnkownType, XmlSerializerTestSetup)
{
  std::string lInput = "<param id='test' type='unknown'>I can't do that Bob.</param>";
  pugi::xml_document lDoc;
  BOOST_REQUIRE_EQUAL(lDoc.load_string(lInput.c_str()), true);
  pugi::xml_node lNode = lDoc.child("param");

  BOOST_CHECK_THROW(serializer->import(lNode), swatch::xml::UnknownDataType);
}

BOOST_AUTO_TEST_SUITE_END() // TestXmlSerializer

} //ns: test
} //ns: xml
} //ns: swatch
