// Boost Unit Test includes
#include <boost/test/unit_test.hpp>

// XDAQ headers
#include <xdata/Boolean.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger.h>
#include <xdata/UnsignedInteger64.h>

// swatch headers
#include "swatch/xml/VectorSerializer.hpp"

//others
#include "pugixml.hpp"


namespace swatch {
namespace xml {
namespace test {

BOOST_AUTO_TEST_SUITE(TestVectorSerializer)

BOOST_AUTO_TEST_CASE(TestUInt)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:uint'>40, 30, 1, 2</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::UnsignedInteger> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::UnsignedInteger>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "unsigned int");

  BOOST_CHECK_EQUAL(lData.toString(), "[40,30,1,2]");
}

BOOST_AUTO_TEST_CASE(TestUInt64)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:uint64'>0x2800000000, 30, 1, 2</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::UnsignedInteger64> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::UnsignedInteger64>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "unsigned int 64");

  BOOST_CHECK_EQUAL(lData.toString(), "[171798691840,30,1,2]");
}

BOOST_AUTO_TEST_CASE(TestInt)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:int'>-2</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::Integer> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::Integer>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "int");
  BOOST_CHECK_EQUAL(lData.toString(), "[-2]");
}

BOOST_AUTO_TEST_CASE(TestBool)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:bool'>true,false</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::Boolean> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::Boolean>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "bool");
  BOOST_CHECK_EQUAL(lData.toString(), "[true,false]");
}

BOOST_AUTO_TEST_CASE(TestFloat)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:float'>2.0</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::Float> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::Float>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "float");
  BOOST_CHECK_EQUAL(lData.toString(), "[2.00000e+00]");
}

BOOST_AUTO_TEST_CASE(TestString)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:string'>hello, world, and, all</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::String> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::String>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "string");
  BOOST_CHECK_EQUAL(lData.toString(), "[hello,world,and,all]");
}

BOOST_AUTO_TEST_CASE(TestCustomDelimiter)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='vector:string' delimiter='|'>hello, world| and, all</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::String> lSerializer;

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  auto& lData = *boost::any_cast<xdata::Vector<xdata::String>>(lResult.get());
  BOOST_CHECK_EQUAL(lData.type(), "vector");
  BOOST_CHECK_EQUAL(lData.getElementType(), "string");
  BOOST_CHECK_EQUAL(lData.toString(), "[hello, world,and, all]");
}

BOOST_AUTO_TEST_CASE(TestInvalid)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='string'>hello, test</param>");
  pugi::xml_node lNode = lDoc.child("param");
  VectorSerializer<xdata::Integer> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "vector:int");

  BOOST_CHECK_THROW(lSerializer.import(lNode), swatch::xml::ValueError);
}


BOOST_AUTO_TEST_SUITE_END() // TestVectorSerializer

} //ns: test
} //ns: xml
} //ns: swatch
