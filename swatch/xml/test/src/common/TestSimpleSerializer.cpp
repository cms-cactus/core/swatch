// Boost Unit Test includes
#include <boost/test/unit_test.hpp>

// XDAQ headers
#include <xdata/Boolean.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger.h>
#include <xdata/UnsignedInteger64.h>

// swatch headers
#include "swatch/xml/SimpleSerializer.hpp"

//others
#include "pugixml.hpp"


namespace swatch {
namespace xml {
namespace test {


BOOST_AUTO_TEST_SUITE(TestSimpleSerializer)
// Test all supported types
BOOST_AUTO_TEST_CASE(TestUInt)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='clkErrorTimeout' type='uint'>40</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::UnsignedInteger> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "unsigned int");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::UnsignedInteger>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "unsigned int");
  BOOST_CHECK_EQUAL(lData->toString(), "40");
}

BOOST_AUTO_TEST_CASE(TestUInt64)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='clkErrorTimeout' type='uint64'>0x2800000000</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::UnsignedInteger64> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "unsigned int 64");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::UnsignedInteger64>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "unsigned int 64");
  BOOST_CHECK_EQUAL(lData->toString(), "171798691840");
}

BOOST_AUTO_TEST_CASE(TestInt)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='int'>-2</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::Integer> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "int");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::Integer>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "int");
  BOOST_CHECK_EQUAL(lData->toString(), "-2");
}

BOOST_AUTO_TEST_CASE(TestBool)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='bool'>true</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::Boolean> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "bool");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::Boolean>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "bool");
  BOOST_CHECK_EQUAL(lData->toString(), "true");
}

BOOST_AUTO_TEST_CASE(TestFloat)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='float'>2.0</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::Float> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "float");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::Float>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "float");
  // xdata::Float will change the string to a more generic format
  BOOST_CHECK_EQUAL(lData->toString(), "2.00000e+00");

  // further tests to understand xdata::Float
  std::unique_ptr<boost::any> test2(new boost::any(xdata::Float()));
  xdata::Serializable* lData2 = boost::any_cast<xdata::Float>(test2.get());
  lData2->fromString(lData->toString());
  BOOST_REQUIRE(lData->equals(*lData2));
  BOOST_CHECK_EQUAL(lData->toString(), lData2->toString());
}

BOOST_AUTO_TEST_CASE(TestString)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='string'>hello</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::String> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "string");

  std::unique_ptr<const boost::any> lResult(lSerializer.import(lNode));
  const xdata::Serializable* lData = boost::any_cast<xdata::String>(lResult.get());
  BOOST_CHECK_EQUAL(lData->type(), "string");
  BOOST_CHECK_EQUAL(lData->toString(), "hello");
}

BOOST_AUTO_TEST_CASE(TestInvalid)
{
  pugi::xml_document lDoc;
  lDoc.load_string("<param id='test' type='string'>hello</param>");
  pugi::xml_node lNode = lDoc.child("param");
  SimpleSerializer<xdata::Integer> lSerializer;
  BOOST_CHECK_EQUAL(lSerializer.type(), "int");

  BOOST_CHECK_THROW(lSerializer.import(lNode), swatch::xml::ValueError);
}


BOOST_AUTO_TEST_SUITE_END() // TestSimpleSerializer

} //ns: test
} //ns: xml
} //ns: swatch
