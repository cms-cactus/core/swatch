#ifndef __SWATCH_XML_ABSTRACTSERIALIZER_HPP__
#define __SWATCH_XML_ABSTRACTSERIALIZER_HPP__

// Standard headers

// boost headers

//xdata
#include "xdata/ObjectSerializer.h"
#include "xdata/Serializable.h"

// SWATCH headers
#include "swatch/core/exception.hpp"

// others
#include "pugixml.hpp"


namespace boost {
class any;
}

namespace swatch {
namespace xml {

/**
 * Abstract base XML serializer for xdata serializables
 */
class AbstractSerializer : public xdata::ObjectSerializer {
public:
  virtual ~AbstractSerializer()
  {
    // TODO: move this into hxx which currently does not work for some bizzare reason (multiple definitions)
  }

  //! Converts an XML element into a xdata::Serializable
  virtual const boost::any* import(const pugi::xml_node& aNode) = 0;
};

SWATCH_DEFINE_EXCEPTION(ValueError)

} // namespace xml
} // namespace swatch

#endif /* __SWATCH_XML_ABSTRACTSERIALIZER_HPP__ */
