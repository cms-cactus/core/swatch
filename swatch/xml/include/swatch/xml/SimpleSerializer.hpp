#ifndef __SWATCH_XML_SIMPLESERIALIZER_HPP__
#define __SWATCH_XML_SIMPLESERIALIZER_HPP__

// Standard headers

// boost headers

//xdata
#include "pugixml.hpp"
#include "swatch/xml/AbstractSerializer.hpp"
#include "xdata/Serializable.h"

namespace swatch {
namespace xml {

/**
 * Implementation of the XML object serializer for basic types and string
 */
template <class T>
class SimpleSerializer : public AbstractSerializer {
public:
  SimpleSerializer();
  virtual ~SimpleSerializer();

  //! Converts an XML element into a xdata::Serializable
  virtual const boost::any* import(const pugi::xml_node& aNode);

  //! Implementation of xdata::ObjectSerializer::type;
  std::string type() const;
};

} // namespace xml
} // namespace swatch

#include "swatch/xml/SimpleSerializer.hxx"

#endif /* __SWATCH_XML_SINGLEOBJECTSERIALIZER_HPP__ */
