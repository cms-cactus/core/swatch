#ifndef __SWATCH_XML_VECTORSERIALIZER_HXX__
#define __SWATCH_XML_VECTORSERIALIZER_HXX__

// IWYU pragma: private, include "swatch/xml/VectorSerializer.hpp"


// boost headers
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>

// SWATCH headers
#include "swatch/core/exception.hpp"
#include "swatch/xml/VectorSerializer.hpp"

// xdata
#include <xdata/Boolean.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger.h>

namespace swatch {
namespace xml {

template <class T>
VectorSerializer<T>::VectorSerializer()
{
}

template <class T>
VectorSerializer<T>::~VectorSerializer()
{
}


template <class T>
const boost::any* VectorSerializer<T>::import(const pugi::xml_node& aNode)
{
  std::string lType(aNode.attribute("type").value());
  std::string lValue(aNode.child_value());
  pugi::xml_attribute lDelimiterAttr(aNode.attribute("delimiter"));
  std::string lDelimiter(lDelimiterAttr.empty() ? "," : lDelimiterAttr.value());
  xdata::Vector<T> lVector = convertString(lValue, lDelimiter);

  return new boost::any(lVector);
}


template <class T>
xdata::Vector<T> VectorSerializer<T>::convertString(const std::string& aStr, const std::string& aDeliminter) const
{
  // split string by ","
  // remove spaces
  xdata::Vector<T> lVector;
  std::vector<std::string> lStrings;
  boost::split(lStrings, aStr, boost::is_any_of(aDeliminter));
  for (std::vector<std::string>::iterator lIt = lStrings.begin(); lIt != lStrings.end(); ++lIt) {
    std::string lTmp = boost::trim_copy(*lIt);
    T lSerializable;
    try {
      lSerializable.fromString(lTmp);
    }
    catch (const xdata::exception::Exception& lExc) {
      SWATCH_THROW(ValueError("Could not parse '" + lTmp + "'."));
    }
    lVector.push_back(lSerializable);
  }
  return lVector;
}


template <class T>
std::string VectorSerializer<T>::type() const
{
  T lTmp;
  return "vector:" + lTmp.type();
}


} // namespace xml
} // namespace swatch

#endif /* __SWATCH_XML_VECTORSERIALIZER_HXX__ */
