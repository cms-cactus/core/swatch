#ifndef __SWATCH_XML_SIMPLESERIALIZER_HXX__
#define __SWATCH_XML_SIMPLESERIALIZER_HXX__

// IWYU pragma: private, include "swatch/xml/SimpleSerializer.hpp"

// Standard headers

// boost headers
#include "boost/any.hpp"

// SWATCH headers
#include "swatch/core/exception.hpp"
// xdata
#include "swatch/xml/SimpleSerializer.hpp"
#include <xdata/Boolean.h>
#include <xdata/Float.h>
#include <xdata/Integer.h>
#include <xdata/String.h>
#include <xdata/UnsignedInteger.h>

namespace swatch {
namespace xml {

template <class T>
SimpleSerializer<T>::SimpleSerializer()
{
}

template <class T>
SimpleSerializer<T>::~SimpleSerializer()
{
}

template <class T>
const boost::any* SimpleSerializer<T>::import(const pugi::xml_node& aNode)
{
  T lSerializable;

  std::string lValue(aNode.child_value());
  try {
    lSerializable.fromString(lValue);
  }
  catch (const xdata::exception::Exception& lExc) {
    const std::string lType(aNode.attribute("type").value());
    SWATCH_THROW(ValueError("Could not parse '" + lValue + "' into type '" + lType + "'."));
  }

  return new boost::any(lSerializable);
}


template <class T>
std::string SimpleSerializer<T>::type() const
{
  T lTmp;
  return lTmp.type();
}


} // namespace xml
} // namespace swatch

#endif /* __SWATCH_XML_SIMPLESERIALIZER_HXX__ */
