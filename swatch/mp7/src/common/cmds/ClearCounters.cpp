#include "swatch/mp7/cmds/ClearCounters.hpp"


#include "mp7/TTCNode.hpp"

#include "xdata/String.h"

#include "swatch/mp7/MP7Processor.hpp"


namespace swatch {
namespace mp7 {
namespace cmds {

// ----------------------------------------------------------------------------
ClearCounters::ClearCounters(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, xdata::String())
{
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
action::Command::State ClearCounters::code(const core::ParameterSet& aParams)
{
  // Reset TTC counters
  MP7Processor& lProcessor = getActionable<MP7Processor>();
  ::mp7::MP7Controller& lDriver = lProcessor.driver();
  setProgress(0., "Clearing TTC counters");

  lDriver.getTTC().clear();

  setResult(xdata::String("Counters cleared"));
  return State::kDone;
}
// ----------------------------------------------------------------------------

} // namespace cmds
} // namespace mp7
} // namespace swatch