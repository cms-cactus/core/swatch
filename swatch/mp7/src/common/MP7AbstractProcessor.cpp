/**
 * @file    MP7AbstractProcessor.cpp
 * @author  Alessandro Thea
 * @brief   MP7 board processor implementation
 * @date    February 2016
 */

#include "swatch/mp7/MP7AbstractProcessor.hpp"


#include "swatch/mp7/ChannelDescriptorCollection.hpp"


namespace swatch {
namespace mp7 {


MP7AbstractProcessor::MP7AbstractProcessor(const swatch::core::AbstractStub& aStub) :
  swatch::processor::Processor(aStub),
  mRxDescriptors(new ChannelDescriptorCollection()),
  mTxDescriptors(new ChannelDescriptorCollection())
{
}


const ChannelDescriptorCollection& MP7AbstractProcessor::getRxDescriptors() const
{
  return *mRxDescriptors;
}


const ChannelDescriptorCollection& MP7AbstractProcessor::getTxDescriptors() const
{
  return *mTxDescriptors;
}

} // namespace mp7
} // namespace swatch
