

set(_SWATCH_VERSION_CPP_GENERATOR_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/swatch/create_version_file.sh)


function(swatch_embed_project_info package_name target)
    message(DEBUG "swatch_embed_project_info: START (package_name=${package_name}, target=${target})")

    # 1. Add rule for creating the version.cpp file
    add_custom_target(
        ${target}__version
        bash ${_SWATCH_VERSION_CPP_GENERATOR_SCRIPT} ${package_name} ${CMAKE_CURRENT_BINARY_DIR}/version.cpp ${PROJECT_VERSION_MAJOR} ${PROJECT_VERSION_MINOR} ${PROJECT_VERSION_PATCH}
        BYPRODUCTS
            ${CMAKE_CURRENT_BINARY_DIR}/version.cpp
        COMMENT
            "Auto-generating version.cpp for ${target}"
    )

    # 2. Add version.cpp to build of ${target}
    target_sources(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/version.cpp)

    message(DEBUG "swatch_embed_project_info: END")
endfunction()