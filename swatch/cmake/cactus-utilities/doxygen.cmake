
function(cactus_add_doxygen_target)

  find_package(Doxygen OPTIONAL_COMPONENTS dot mscgen dia)

  if (NOT DOXYGEN_FOUND)
    set(DOXYGEN_NOT_FOUND_COMMAND_ARGS
      # show error message
      COMMAND ${CMAKE_COMMAND} -E echo "Cannot run because doxygen not found"
      # fail build
      COMMAND ${CMAKE_COMMAND} -E false
    )
    if(NOT TARGET doxygen)
      add_custom_target(doxygen ${DOXYGEN_NOT_FOUND_COMMAND_ARGS})
    endif()
  else()
    # Set variables used to customise default cmake doxygen configuration
    _cactus_create_doxygen_include_path_list(${PROJECT_SOURCE_DIR} ${includePaths})
    get_property(DOXYGEN_STRIP_FROM_INC_PATH GLOBAL PROPERTY CACTUS_DOXY_INCLUDE_PATHS)
    set(DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doxygen)

    # Add doxygen target
    # (Only source code under directories added by 'add_subdirectory' will be included)
    get_directory_property(subDirs DIRECTORY ${PROJECT_SOURCE_DIR} SUBDIRECTORIES)
    doxygen_add_docs(doxygen
      ${subDirs}
      WORKING_DIRECTORY
        ${PROJECT_SOURCE_DIR}
      })
  endif()

endfunction()


function(_cactus_create_doxygen_include_path_list dir)

  if (IS_DIRECTORY ${dir}/include)
    get_property(CACTUS_DOXY_INCLUDE_PATHS GLOBAL PROPERTY CACTUS_DOXY_INCLUDE_PATHS)
    list(APPEND CACTUS_DOXY_INCLUDE_PATHS ${dir}/include)
    set_property(GLOBAL PROPERTY CACTUS_DOXY_INCLUDE_PATHS ${CACTUS_DOXY_INCLUDE_PATHS})
  endif ()

  get_directory_property(subDirs DIRECTORY ${dir} SUBDIRECTORIES)
  foreach(subDir ${subDirs})
    _cactus_create_doxygen_include_path_list(${subDir})
  endforeach()

endfunction()


##########################################################################
# OLD - PREVIOUS HOMEBREW IMPLEMENTATION

# Variables for customisation:
#   - DOXYGEN_EXECUTABLE:       Name/path of doxygen executable
#   - DOXYFILE:                 Path to config file
#   - DOXYGEN_MAINPAGE:         Path to main page
#   - DOXYGEN_EXCLUDE_PATTERNS: List of file patterns to ignore


# function(cactus_add_doxygen_target)

#   # 1) Find doxygen executable
#   if (NOT DOXYGEN_EXECUTABLE)
#     set(DOXYGEN_EXECUTABLE doxygen)
#   endif ()

#   if (NOT EXISTS ${DOXYGEN_EXECUTABLE})
#     find_program(DOXYGEN_PROGRAM doxygen)
#     if (DOXYGEN_PROGRAM)
#       set(DOXYGEN_EXECUTABLE ${DOXYGEN_PROGRAM})
#       unset(DOXYGEN_PROGRAM)
#     else ()
#       message(NOTICE "doxygen executable (${DOXYGEN_EXECUTABLE}) not found! Adding dummy 'doxygen' target")
#       unset(DOXYGEN_EXECUTABLE)
#     endif ()
#   endif ()

#   # 2) Define target
#   if (DOXYGEN_EXECUTABLE)
#     if(NOT TARGET doxygen)
#       _cactus_create_doxygen_include_path_list(${PROJECT_SOURCE_DIR} ${includePaths})
#       get_property(includePaths GLOBAL PROPERTY CACTUS_DOXY_INCLUDE_PATHS)

#       add_custom_target(doxygen
#         COMMAND
#           ${CMAKE_COMMAND} 
#             -E env
#             DOXYGEN_REPO_BASE_PATH=${PROJECT_SOURCE_DIR}
#             DOXYGEN_MAINPAGE=${DOXYGEN_MAINPAGE}
#             DOXYGEN_OUTPUT=${CMAKE_CURRENT_BINARY_DIR}/doxygen
#             DOXYGEN_STRIP_FROM_INC_PATH=${includePaths}
#             DOXYGEN_EXCLUDE_PATTERNS=${DOXYGEN_EXCLUDE_PATTERNS}
#             doxygen ${DOXYFILE}
#         COMMENT
#           Creating doxygen pages for
#         )
#     endif()
#   else()
#     set(DOXYGEN_NOT_FOUND_COMMAND_ARGS
#       # show error message
#       COMMAND ${CMAKE_COMMAND} -E echo "Cannot run because doxygen not found"
#       # fail build
#       COMMAND ${CMAKE_COMMAND} -E false
#     )
#     if(NOT TARGET doxygen)
#       add_custom_target(doxygen ${DOXYGEN_NOT_FOUND_COMMAND_ARGS})
#     endif()
#   endif()

# endfunction()

