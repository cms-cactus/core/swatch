
get_filename_component(@PROJECT_NAME@_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

@TEMPLATE_CONFIG_FIND_PACKAGE_STATEMENTS@

if(NOT TARGET @TEMPLATE_CONFIG_NAMESPACE_NAME@::@TEMPLATE_CONFIG_REQUIRED_TARGET@)
    include("${@PROJECT_NAME@_CMAKE_DIR}/@PROJECT_NAME@Targets.cmake")
endif()
