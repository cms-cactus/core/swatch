
function(cactus_add_project_subdirectory subdir)
  option(EXCLUDE_SUBPROJECTS_FROM_ALL "Set 'EXCLUDE_FROM_ALL' for directories that define projects" OFF)
  if(EXCLUDE_SUBPROJECTS_FROM_ALL)
    add_subdirectory(${subdir} EXCLUDE_FROM_ALL)
  else()
    add_subdirectory(${subdir})
  endif()
endfunction()
