
# Arguments = list of components to install (optional)

function(cactus_add_project_rpm_rules)

    list(APPEND CPACK_GENERATOR "RPM")

    set(CPACK_PACKAGE_NAME ${PROJECT_NAME})
    set(CPACK_PACKAGE_DESCRIPTION "")
    set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "")
    set(CPACK_PACKAGE_VENDOR "")

    set(CPACK_RPM_FILE_NAME "RPM-DEFAULT")

    # Force architecture to match output of 'rpm --eval "%{_target_cpu}"' - since cmake uses output of
    # 'uname' by default, and uname gives 'armv7l' on CentOS7 arm/v7, whereas RPM expects 'armv7hl
    execute_process(
        COMMAND rpm --eval "%{_target_cpu}"
        OUTPUT_STRIP_TRAILING_WHITESPACE
        OUTPUT_VARIABLE CPACK_RPM_PACKAGE_ARCHITECTURE
    )
    message(STATUS "RPM arch set to: ${CPACK_RPM_PACKAGE_ARCHITECTURE}")

    set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
    set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
    set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

    # Release = N[.autobuild_SHORTSHA].centosM.gccX_Y_Z
    if(NOT DEFINED CPACK_RPM_PACKAGE_RELEASE)
        set(CPACK_RPM_PACKAGE_RELEASE "1")
    endif()

    if (DEFINED ENV{GITLAB_CI})
        if ("$ENV{CI_COMMIT_TAG}" STREQUAL "")
            message(STATUS "Detected GitLab CI environment for non-tag build. Adding '.autobuild_$ENV{CI_COMMIT_SHORT_SHA}' to RPM release field.")
            string(APPEND CPACK_RPM_PACKAGE_RELEASE ".autobuild_")
            string(APPEND CPACK_RPM_PACKAGE_RELEASE $ENV{CI_COMMIT_SHORT_SHA})
        endif()
    endif()

    if(EXISTS /etc/centos-release)
        file(READ /etc/centos-release VERBOSE_CENTOS_RELEASE_STRING)
        string(STRIP "${VERBOSE_CENTOS_RELEASE_STRING}" VERBOSE_CENTOS_RELEASE_STRING)
        if (VERBOSE_CENTOS_RELEASE_STRING MATCHES "^CentOS Linux release ([0-9]+)\\.[0-9].+$")
            string(APPEND CPACK_RPM_PACKAGE_RELEASE ".centos${CMAKE_MATCH_1}")
        endif()
    endif()
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(RPM_RELEASE_COMPILER_SUFFIX "gcc")
    else()
        string(TOLOWER ${CMAKE_CXX_COMPILER_ID} RPM_RELEASE_COMPILER_SUFFIX)
    endif()
    string(APPEND RPM_RELEASE_COMPILER_SUFFIX "${CMAKE_CXX_COMPILER_VERSION}")
    string(REPLACE "." "_" RPM_RELEASE_COMPILER_SUFFIX "${RPM_RELEASE_COMPILER_SUFFIX}")
    string(APPEND CPACK_RPM_PACKAGE_RELEASE ".${RPM_RELEASE_COMPILER_SUFFIX}")


    set(CPACK_COMPONENTS_ALL ${ARGN})
    if (NOT CPACK_COMPONENTS_ALL)
        unset(CPACK_COMPONENTS_ALL)
    endif()

    set(CPACK_RPM_COMPONENT_INSTALL ON)
    if (NOT DEFINED CPACK_RPM_MAIN_COMPONENT)
        set(CPACK_RPM_MAIN_COMPONENT "cmake")
    endif()

    if((CMAKE_BUILD_TYPE STREQUAL "Debug") OR (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo"))
        set(CPACK_RPM_DEBUGINFO_PACKAGE ON)
        set(CPACK_RPM_DEBUGINFO_SINGLE_PACKAGE ON)
        set(CPACK_RPM_BUILD_SOURCE_DIRS_PREFIX "/usr/src/debug/${PROJECT_NAME}-${PROJECT_VERSION}")
    endif()

    include(CPack)

endfunction()
