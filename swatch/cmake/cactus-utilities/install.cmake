
include(GNUInstallDirs)


function(cactus_add_install_rules componentName)
  string(TOUPPER "INSTALL_${PROJECT_NAME}" INSTALL_SWITCH_VAR)
  message(DEBUG "cactus_add_install_rules: START (componentName=${componentName}, ${INSTALL_SWITCH_VAR}=${${INSTALL_SWITCH_VAR}})")
  if (${INSTALL_SWITCH_VAR} OR (NOT DEFINED ${INSTALL_SWITCH_VAR}))
    get_directory_property(targets DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} BUILDSYSTEM_TARGETS)

    foreach(target ${targets})
      get_target_property(targetType ${target} TYPE)

      if(targetType STREQUAL "SHARED_LIBRARY")
        list(APPEND libraries ${target})
      elseif(targetType STREQUAL "EXECUTABLE")
        list(APPEND executables ${target})
      endif()

    endforeach()

    file(GLOB scripts LIST_DIRECTORIES false "${CMAKE_CURRENT_SOURCE_DIR}/scripts/*")
    foreach(script ${scripts})
      message(DEBUG "cactus_add_install_rules: install script '${script}'")
      install(
        FILES ${script}
        DESTINATION ${CMAKE_INSTALL_BINDIR}
        COMPONENT ${componentName}
      )
    endforeach()

    if(libraries)
      message(DEBUG "cactus_add_install_rules: install libraries <${libraries}>")
      install(
        TARGETS ${libraries}
        EXPORT ${PROJECT_NAME}Targets
        LIBRARY
        DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT ${componentName}
      )
    endif()

    if(executables)
      message(DEBUG "cactus_add_install_rules: install executables <${executables}>")
      install(
        TARGETS ${executables}
        DESTINATION ${CMAKE_INSTALL_BINDIR}
        COMPONENT ${componentName}
      )
    endif()

    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include)
      message(DEBUG "cactus_add_install_rules: install headers from ${CMAKE_CURRENT_SOURCE_DIR}/include")
      install(
        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
        COMPONENT devel
      )
    endif()

  endif()
  message(DEBUG "cactus_add_install_rules: END")
endfunction()


include(CMakePackageConfigHelpers)


set(_CACTUS_TEMPLATE_CMAKE_CONFIG_PATH ${CMAKE_CURRENT_LIST_DIR}/project-config-template.cmake)

function(cactus_add_project_install_rules namespaceName TEMPLATE_CONFIG_REQUIRED_TARGET)
    string(TOUPPER "INSTALL_${PROJECT_NAME}" INSTALL_SWITCH_VAR)
    message(DEBUG "cactus_add_project_install_rules: START (namespaceName=${namespaceName}, PROJECT_NAME=${PROJECT_NAME}, ${INSTALL_SWITCH_VAR}=${${INSTALL_SWITCH_VAR}})")

    if (NOT DEFINED CACTUS_CMAKE_CONFIG_COMPONENT_NAME)
        set(CACTUS_CMAKE_CONFIG_COMPONENT_NAME cmake)
    endif()

    if (${INSTALL_SWITCH_VAR} OR (NOT DEFINED ${INSTALL_SWITCH_VAR}))

        # Install config file containing exported targets
        message(DEBUG "cactus_add_project_install_rules: Generating ${PROJECT_NAME}Targets.cmake (for downstream packages to import installed targets)")
        install(EXPORT ${PROJECT_NAME}Targets
            FILE
                ${PROJECT_NAME}Targets.cmake
            NAMESPACE
                ${namespaceName}::
            DESTINATION
                ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
            COMPONENT
                ${CACTUS_CMAKE_CONFIG_COMPONENT_NAME}
        )

        set(CUSTOM_CMAKE_CONFIG_PATH ${PROJECT_SOURCE_DIR}/cmake/${PROJECT_NAME}-config.cmake)
        set(GENERATED_CMAKE_CONFIG_PATH ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config.cmake)
        if(EXISTS ${CUSTOM_CMAKE_CONFIG_PATH})
            message(STATUS "cactus_add_project_install_rules: Using ${CUSTOM_CMAKE_CONFIG_PATH} as '-config.cmake' file")
            configure_file(
                ${CUSTOM_CMAKE_CONFIG_PATH}
                ${GENERATED_CMAKE_CONFIG_PATH}
                COPYONLY
            )
        else()
            message(STATUS "cactus_add_project_install_rules: Autogenerating ${PROJECT_NAME}-config.cmake")
            set(TEMPLATE_CONFIG_NAMESPACE_NAME ${namespaceName})

            set(FIND_PACKAGE_ARGS_LIST ${ARGV})
            list(REMOVE_AT FIND_PACKAGE_ARGS_LIST 0 1)
            list(TRANSFORM FIND_PACKAGE_ARGS_LIST PREPEND "find_package(")
            list(TRANSFORM FIND_PACKAGE_ARGS_LIST APPEND ")")
            list(JOIN FIND_PACKAGE_ARGS_LIST "\n" TEMPLATE_CONFIG_FIND_PACKAGE_STATEMENTS)

            configure_file(
                ${_CACTUS_TEMPLATE_CMAKE_CONFIG_PATH}
                ${GENERATED_CMAKE_CONFIG_PATH}
                @ONLY
            )
        endif()

        # Generate the version file
        write_basic_package_version_file(
            "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake"
            VERSION
                "${${PROJECT_NAME}_VERSION_MAJOR}.${${PROJECT_NAME}_VERSION_MINOR}.${${PROJECT_NAME}_VERSION_PATCH}"
            COMPATIBILITY AnyNewerVersion
        )

        # Install the configuration files
        install(
            FILES
                ${GENERATED_CMAKE_CONFIG_PATH}
                ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake
            DESTINATION
                ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
            COMPONENT
                ${CACTUS_CMAKE_CONFIG_COMPONENT_NAME}
        )

    endif()
    message(DEBUG "cactus_add_project_install_rules: END")
endfunction()
