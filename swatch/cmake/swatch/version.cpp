

#include "swatch/phase2/PackageInfo.hpp"
#include "swatch/phase2/PackageRegistry.hpp"


namespace {

swatch::phase2::PackageInfo createPackageInfo() {
  using swatch::phase2::PackageInfo;
  PackageInfo lInfo;

  lInfo.name = "__PACKAGE_NAME__";
  lInfo.version.major = __PROJECT_VERSION_MAJOR__;
  lInfo.version.minor = __PROJECT_VERSION_MINOR__;
  lInfo.version.patch = __PROJECT_VERSION_PATCH__;
  // lInfo.prerelease = ; - if TWEAK non-zero

#if __IN_GIT_REPO__
  lInfo.vcs = PackageInfo::Git();
  lInfo.vcs->sha = "__GIT_SHA__";
  lInfo.vcs->clean = __GIT_IS_REPO_CLEAN__;

#if __GIT_IS_BRANCH_CHECKED_OUT__
  lInfo.vcs->ref = std::pair<PackageInfo::Git::RefType, std::string>(PackageInfo::Git::kBranch, "__GIT_BRANCH_NAME__");
#endif

#if __GIT_IS_TAG_CHECKED_OUT__
  lInfo.vcs->ref = std::pair<PackageInfo::Git::RefType, std::string>(PackageInfo::Git::kTag, "__GIT_TAG_NAME__");
#endif

#endif

#if __IN_GITLAB_CI_JOB__
  PackageInfo::GitLabBuild lBuild;
  lBuild.serverURL = "__CI_SERVER_URL__";
  lBuild.projectPath = "__CI_PROJECT_PATH__";
  lBuild.projectID = __CI_PROJECT_ID__;
  lBuild.pipelineID = __CI_PIPELINE_ID__;
  lBuild.jobID = __CI_JOB_ID__;
#else
  PackageInfo::LocalBuild lBuild;
#endif
  lBuild.time = std::chrono::system_clock::time_point(std::chrono::seconds(__BUILDTIME_SECONDS_SINCE_EPOCH__));
  lInfo.build = lBuild;

  return lInfo;
}

}

SWATCH_REGISTER_PACKAGE_INFO(createPackageInfo());
