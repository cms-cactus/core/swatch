
include(${CMAKE_CURRENT_LIST_DIR}/cactus-utilities/clangFormat.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cactus-utilities/doxygen.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cactus-utilities/install.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cactus-utilities/package.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/swatch.cmake)


function(cactus_add_component_rules componentName)
  cactus_add_install_rules(${componentName})
endfunction()


function(cactus_add_project_rules)

  # string(TOUPPER PROJECT_NAME UPPERCASE_PROJECT_NAME)
  # option(INSTALL_${UPPERCASE_PROJECT_NAME} "Enable installation of ${PROJECT_NAME}. (Projects embedding  ${PROJECT_NAME} may want to turn this OFF.)" ON)

  if(NOT CMAKE_BUILD_TYPE)
      set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel." FORCE)
  endif()

  cactus_add_clang_format_targets()

  cactus_add_project_install_rules(${ARGV})
  cactus_add_project_rpm_rules(${CPACK_COMPONENTS_ALL})
endfunction()


function(cactus_add_phase2_plugin_rules)

  # string(TOUPPER PROJECT_NAME UPPERCASE_PROJECT_NAME)
  # option(INSTALL_${UPPERCASE_PROJECT_NAME} "Enable installation of ${PROJECT_NAME}. (Projects embedding  ${PROJECT_NAME} may want to turn this OFF.)" ON)

  if(NOT CMAKE_BUILD_TYPE)
      set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel." FORCE)
  endif()

  cactus_add_install_rules(plugin)
  set(CPACK_RPM_MAIN_COMPONENT "plugin")
  set(CACTUS_CMAKE_CONFIG_COMPONENT_NAME "devel")

  if (DEFINED PLUGIN_CONFIG_FILE)
    install(
      FILES
        ${PLUGIN_CONFIG_FILE}
      RENAME
        herd.yml
      DESTINATION
        ${CMAKE_INSTALL_SYSCONFDIR}/${PROJECT_NAME}
      COMPONENT
        plugin
    )
  endif()

  cactus_add_clang_format_targets()

  cactus_add_project_install_rules(${ARGV})
  cactus_add_project_rpm_rules()

  if (NOT DEFINED SWATCH_PROJECT_INFO_PLUGIN_NAME)
    set(SWATCH_PROJECT_INFO_PLUGIN_NAME ${PROJECT_NAME})
  endif()
  list(GET ARGV 1 PLUGIN_LIBRARY_TARGET)
  swatch_embed_project_info(${SWATCH_PROJECT_INFO_PLUGIN_NAME} ${PLUGIN_LIBRARY_TARGET})

endfunction()
