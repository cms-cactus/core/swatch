
#######################################################
# Runner target: Declare - source files & dependencies
#######################################################

add_executable(swatch-logger-test-runner src/common/runLogger.cxx)

target_link_libraries(swatch-logger-test-runner 
    PUBLIC
        swatch_logger
        Boost::unit_test_framework
        log4cplus
    )


#######################################################
# Runner target: Build options
#######################################################

target_compile_features(swatch-logger-test-runner PUBLIC cxx_std_11)
target_compile_options(swatch-logger-test-runner PRIVATE -g -fPIC -O3 -Werror=return-type -Wall)


#######################################################
# Default rules (install, packaging, clang-format etc.)
#######################################################

include(../../cmake/cactus-utilities.cmake)
cactus_add_component_rules(logger-test)