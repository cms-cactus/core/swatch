
#include "swatch/phase2/test/DummyProcessor.hpp"


#include "swatch/core/Factory.hpp"


SWATCH_REGISTER_CLASS(swatch::phase2::test::DummyProcessor)


namespace swatch {
namespace phase2 {
namespace test {

DummyProcessor::DummyProcessor(const core::AbstractStub& aStub) :
  Processor(aStub)
{
  registerInputs();
  registerOutputs();
}


DummyProcessor::~DummyProcessor()
{
}


action::StateMachine& DummyProcessor::registerStateMachine(const std::string& aId, const std::string& aInitialState, const std::string& aErrorState)
{
  return ActionableObject::registerStateMachine(aId, aInitialState, aErrorState);
}


void DummyProcessor::retrieveMetricValues()
{
}

} // namespace test
} // namespace phase2
} // namespace swatch
