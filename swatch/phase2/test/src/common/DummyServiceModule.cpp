
#include "swatch/phase2/test/DummyServiceModule.hpp"


#include "swatch/core/Factory.hpp"


SWATCH_REGISTER_CLASS(swatch::phase2::test::DummyServiceModule)


namespace swatch {
namespace phase2 {
namespace test {

DummyServiceModule::DummyServiceModule(const core::AbstractStub& aStub) :
  ServiceModule(aStub)
{
}


DummyServiceModule::~DummyServiceModule()
{
}


action::StateMachine& DummyServiceModule::registerStateMachine(const std::string& aId, const std::string& aInitialState, const std::string& aErrorState)
{
  return ActionableObject::registerStateMachine(aId, aInitialState, aErrorState);
}


void DummyServiceModule::retrieveMetricValues()
{
}

} // namespace test
} // namespace phase2
} // namespace swatch
