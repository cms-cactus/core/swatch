
#ifndef __SWATCH_PHASE2_TEST_DUMMYSERVICEMODULE_HPP__
#define __SWATCH_PHASE2_TEST_DUMMYSERVICEMODULE_HPP__


#include "swatch/phase2/ServiceModule.hpp"


namespace swatch {
namespace phase2 {
namespace test {

class DummyServiceModule : public phase2::ServiceModule {
public:
  DummyServiceModule(const core::AbstractStub& aStub);
  virtual ~DummyServiceModule();

  // Expose registerFunctionoid template method as public for tests
  template <typename T>
  T& registerCommand(const std::string& aId)
  {
    return ActionableObject::registerCommand<T>(aId);
  }

  action::StateMachine& registerStateMachine(const std::string& aId, const std::string& aInitialState, const std::string& aErrorState);

private:
  void retrieveMetricValues();
};


} /* namespace test */
} /* namespace phase2 */
} /* namespace swatch */

#endif /* SWATCH_PHASE2_TEST_DUMMYPROCESSOR_HPP */
