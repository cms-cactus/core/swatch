
#include "swatch/phase2/PackageRegistry.hpp"


namespace swatch {
namespace phase2 {


std::unordered_map<std::string, PackageInfo> PackageRegistry::sPackages;


PackageRegistry::PackageRegistry()
{
}


PackageRegistry::~PackageRegistry()
{
}


void PackageRegistry::add(const PackageInfo& aPackage)
{
  if (sPackages.count(aPackage.name) > 0) {
    SWATCH_THROW(PackageAlreadyRegistered("Build & version info for package named '" + aPackage.name + "' has already been registered"));
  }

  sPackages[aPackage.name] = aPackage;
}


const PackageInfo& PackageRegistry::get(const std::string& aPackageName)
{
  const auto lIt = sPackages.find(aPackageName);
  if (lIt == sPackages.end()) {
    SWATCH_THROW(PackageNotFound("Build & version info for package named '" + aPackageName + "' could not be found"));
  }

  return lIt->second;
}

std::unordered_map<std::string, PackageInfo>::const_iterator PackageRegistry::begin()
{
  return sPackages.cbegin();
}


std::unordered_map<std::string, PackageInfo>::const_iterator PackageRegistry::end()
{
  return sPackages.cend();
}


} // namespace phase2
} // namespace swatch
