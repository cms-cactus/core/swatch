
#include "swatch/phase2/Board.hpp"


#include <sstream>

#include "log4cplus/logger.h" // for Logger
#include "log4cplus/loggingmacros.h"

#include "swatch/core/AbstractStub.hpp"
#include "swatch/core/Factory.hpp"

#include "swatch/logger/Logger.hpp"

#include "swatch/phase2/AlgoInterface.hpp"
#include "swatch/phase2/DeviceStub.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OpticalFibreConnector.hpp"
#include "swatch/phase2/OpticalFibreConnectorStub.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/Processor.hpp"
#include "swatch/phase2/ServiceModule.hpp"


namespace swatch {
namespace phase2 {

log4cplus::Logger Board::sLogger = swatch::logger::Logger::getInstance("swatch.phase2");

Board::Board(const std::string& aHardwareType, const DeviceStub& aServiceStub, const std::vector<DeviceStub>& aProcessorStubs, const std::vector<OpticalFibreConnectorStub>& aOptoConnectorStubs) :
  Board(aHardwareType, aServiceStub, aProcessorStubs, aOptoConnectorStubs, std::function<void(const action::ObjectFunctionoid&)>())
{
}


Board::Board(const std::string& aHardwareType, const DeviceStub& aServiceStub, const std::vector<DeviceStub>& aProcessorStubs, const std::vector<OpticalFibreConnectorStub>& aOptoConnectorStubs, const std::function<void(const action::ObjectFunctionoid&)>& aExtraPostActionCallback) :
  mHardwareType(aHardwareType),
  mServiceModule(core::Factory::get()->make<ServiceModule>(aServiceStub.creator, aServiceStub))
{
  addActionable(mServiceModule);
  if (aExtraPostActionCallback)
    mServiceModule->setPostActionCallback(aExtraPostActionCallback);

  // TODO: Check that all processor IDs different from service module
  for (const auto& lStub : aProcessorStubs) {
    Processor* lProc = NULL;
    try {
      lProc = core::Factory::get()->make<Processor>(lStub.creator, lStub);
      if (aExtraPostActionCallback)
        lProc->setPostActionCallback([this, lProc, aExtraPostActionCallback](const action::ObjectFunctionoid& aAction) { this->syncOpticsMonitoringSettingsWithFPGA(*lProc); aExtraPostActionCallback(aAction); });
      else
        lProc->setPostActionCallback([this, lProc](const action::ObjectFunctionoid&) { this->syncOpticsMonitoringSettingsWithFPGA(*lProc); });
    }
    catch (const ProcessorAbsent&) {
      LOG4CPLUS_INFO(sLogger, "Processor '" << lStub.id << "' has been declared as absent");
      continue;
    }

    if (lProc->mAlgo == NULL) {
      LOG4CPLUS_INFO(sLogger, "No algo interface registered in processor '" << lStub.id << "'; registering the empty algo interface base class.");
      lProc->registerAlgo(new AlgoInterface());
    }

    addActionable(lProc);
    mProcessors.emplace(lProc->getId(), lProc);
  }

  const auto lIt = sOpticalModuleCallbacks.find(mHardwareType);
  if (lIt != sOpticalModuleCallbacks.end()) {
    const auto lOpticsAndFrontPanelInfo = lIt->second();

    for (OpticalModule* lModule : lOpticsAndFrontPanelInfo.first) {
      if (lModule == nullptr)
        continue;

      mOpticalModules[lModule->getId()] = lModule;
      addMonitorable(lModule);
    }

    for (const auto& lConnectorSiteId : lOpticsAndFrontPanelInfo.second)
      mFrontPanelOptoConnectors[lConnectorSiteId] = std::unique_ptr<OpticalFibreConnector>();

    for (const auto& lStub : aOptoConnectorStubs) {
      if (mFrontPanelOptoConnectors.count(lStub.id) == 0) {
        std::ostringstream lMessage;
        lMessage << "Optical connector ID '" << lStub.id << "' does not match the ID of any of the ";
        lMessage << mFrontPanelOptoConnectors.size() << " declared front panel connector sites:";
        for (const auto& x : mFrontPanelOptoConnectors)
          lMessage << " " << x.first;
        SWATCH_THROW(InconsistentConnectorInfo(lMessage.str()));
      }

      if (mFrontPanelOptoConnectors.at(lStub.id))
        SWATCH_THROW(DuplicateConnectorId("Connector with ID '" + lStub.id + "' has been specified multiple times"));

      mFrontPanelOptoConnectors[lStub.id] = std::make_unique<OpticalFibreConnector>(lStub.id, lStub.type, lStub.channelCount, lStub.connectedOpticsMap);
    }
  }
  else if (not aOptoConnectorStubs.empty())
    SWATCH_THROW(InconsistentConnectorInfo("Optical connector specification non-empty, but no optical callback was defined"));

  // Verify supplied connection info: Front panel connectors -> optical modules
  for (const auto& x : mFrontPanelOptoConnectors) {
    // Skip entries for unoccupied front panel sites
    if (not x.second)
      continue;

    const auto& lOptoConnector = *x.second.get();
    for (const auto i : lOptoConnector.getChannels()) {
      if (lOptoConnector.getConnectedOpticsChannel(i) != boost::none) {
        const std::string lOpticsChannelIdPath = *lOptoConnector.getConnectedOpticsChannel(i);
        const std::string lOpticsModuleId = lOpticsChannelIdPath.substr(0, lOpticsChannelIdPath.find("."));
        const std::string lOpticsChannelId = lOpticsChannelIdPath.substr(lOpticsChannelIdPath.find(".") + 1);

        const auto lModuleIt = mOpticalModules.find(lOpticsModuleId);
        if (lModuleIt == mOpticalModules.end())
          SWATCH_THROW(InvalidConnectionInfo("No optical module matches the ID '" + lOpticsModuleId + "' declared for channel " + std::to_string(i) + " on front panel connector '" + lOptoConnector.getId() + "'"));

        const OpticalModule& lModule = *lModuleIt->second;
        const OpticalModuleChannel* lOptoChannel = NULL;
        try {
          lOptoChannel = &lModule.getObj<OpticalModuleChannel>(lOpticsChannelId);
        }
        catch (const std::exception&) {
          SWATCH_THROW(InvalidConnectionInfo("Optical module '" + lModule.getId() + "' connected to front panel connector '" + lOptoConnector.getId() + "' does not contain specified channel '" + lOpticsChannelId + "'"));
        }

        if (mOpticalFrontPanelChannelMap.count(lOptoChannel) > 0) {
          const auto& lOtherConnector = mOpticalFrontPanelChannelMap.at(lOptoChannel);
          std::ostringstream lMessage;
          lMessage << "Optical module channel '" << lOpticsChannelIdPath << "' is declared as connected to both frontpanel connectors ";
          lMessage << lOtherConnector.first->getId() << " ch" << std::to_string(lOtherConnector.second) << " and ";
          lMessage << lOptoConnector.getId() << " ch" << std::to_string(i);
          SWATCH_THROW(InvalidConnectionInfo(lMessage.str()));
        }

        mOpticalFrontPanelChannelMap[lOptoChannel] = std::make_pair(&lOptoConnector, i);
        mFrontPanelOpticalModuleMap[std::make_pair(&lOptoConnector, i)] = lOptoChannel;
      }
    }
  }

  // Verify supplied connection info: Processors -> optics
  for (const auto& x : mProcessors) {
    const auto& lProcessor = *x.second;

    // a) INPUT PORTS
    for (const auto* lPort : lProcessor.getInputPorts().getPorts()) {
      if (lPort->getConnection()) {
        const auto lOpticsIt = mOpticalModules.find(lPort->getConnection()->component);
        const auto lProcIt = mProcessors.find(lPort->getConnection()->component);
        if (lOpticsIt != mOpticalModules.end()) {
          const auto& lModule = *lOpticsIt->second;
          const size_t lIndex = lPort->getConnection()->index;
          if (not lOpticsIt->second->containsInput(lIndex))
            SWATCH_THROW(InvalidConnectionInfo("Optical module '" + lModule.getId() + "' connected to input port '" + lPort->getId() + "' on '" + lProcessor.getId() + "' does not contain specified channel (" + std::to_string(lIndex) + ")"));

          mOpticalInputPortMap[&lOpticsIt->second->getInput(lIndex)] = std::pair<const Processor*, const InputPort*>(&lProcessor, lPort);
        }
        else if (lProcIt != mProcessors.end()) {
          const auto& lOtherProc = *lProcIt->second;
          const size_t lIndex = lPort->getConnection()->index;
          if (not lOtherProc.getOutputPorts().contains(lIndex))
            SWATCH_THROW(InvalidConnectionInfo("Processor '" + lOtherProc.getId() + "' connected to input port '" + lPort->getId() + "' on '" + lProcessor.getId() + "' does not contain specified output channel (" + std::to_string(lIndex) + ")"));

          const auto& lOtherPort = lOtherProc.getOutputPorts().getPort(lIndex);
          if (not lOtherPort.getConnection()) {
            std::ostringstream lMessage;
            lMessage << "Input port '" << lPort->getId() << "' on '" << lProcessor.getId() << "' declared connection to '";
            lMessage << lOtherProc.getId() << ", output channel " + std::to_string(lIndex);
            lMessage << ". But that output port declared no connection.";
            SWATCH_THROW(InvalidConnectionInfo(lMessage.str()));
          }
          if (lOtherPort.getConnection()->component != lProcessor.getId() or lOtherPort.getConnection()->index != lPort->getIndex()) {
            std::ostringstream lMessage;
            lMessage << "Input port '" << lPort->getId() << "' on '" << lProcessor.getId() << "' declared connection to '";
            lMessage << lOtherProc.getId() << ", output channel " + std::to_string(lIndex);
            lMessage << ". But that output port declared connection to '" << lOtherPort.getConnection()->component;
            lMessage << "', input channel " << lOtherPort.getConnection()->index;
            SWATCH_THROW(InvalidConnectionInfo(lMessage.str()));
          }
        }
        else
          SWATCH_THROW(InvalidConnectionInfo("No FPGA or optical module matches the ID '" + lPort->getConnection()->component + "' of the component connected to input port '" + lPort->getId() + "' on '" + lProcessor.getId() + "'"));
      }
    }

    // b) OUTPUT PORTS
    for (const auto* lPort : lProcessor.getOutputPorts().getPorts()) {
      if (lPort->getConnection()) {
        const auto lOpticsIt = mOpticalModules.find(lPort->getConnection()->component);
        const auto lProcIt = mProcessors.find(lPort->getConnection()->component);
        if (lOpticsIt != mOpticalModules.end()) {
          const auto& lModule = *lOpticsIt->second;
          const size_t lIndex = lPort->getConnection()->index;
          if (not lOpticsIt->second->containsOutput(lIndex))
            SWATCH_THROW(InvalidConnectionInfo("Optical module '" + lModule.getId() + "' connected to output port '" + lPort->getId() + "' on '" + lProcessor.getId() + "' does not contain specified channel (" + std::to_string(lIndex) + ")"));

          mOpticalOutputPortMap[&lOpticsIt->second->getOutput(lIndex)] = std::pair<const Processor*, const OutputPort*>(&lProcessor, lPort);
        }
        else if (lProcIt != mProcessors.end()) {
          const auto& lOtherProc = *lProcIt->second;
          const size_t lIndex = lPort->getConnection()->index;
          if (not lOtherProc.getInputPorts().contains(lIndex))
            SWATCH_THROW(InvalidConnectionInfo("Processor '" + lOtherProc.getId() + "' connected to output port '" + lPort->getId() + "' on '" + lProcessor.getId() + "' does not contain specified input channel (" + std::to_string(lIndex) + ")"));

          const auto& lOtherPort = lOtherProc.getInputPorts().getPort(lIndex);
          if (not lOtherPort.getConnection()) {
            std::ostringstream lMessage;
            lMessage << "Output port '" << lPort->getId() << "' on '" << lProcessor.getId() << "' declared connection to '";
            lMessage << lOtherProc.getId() << ", input channel " + std::to_string(lIndex);
            lMessage << ". But that input port declared no connection.";
            SWATCH_THROW(InvalidConnectionInfo(lMessage.str()));
          }
          if (lOtherPort.getConnection()->component != lProcessor.getId() or lOtherPort.getConnection()->index != lPort->getIndex()) {
            std::ostringstream lMessage;
            lMessage << "Output port '" << lPort->getId() << "' on '" << lProcessor.getId() << "' declared connection to '";
            lMessage << lOtherProc.getId() << ", input channel " + std::to_string(lIndex);
            lMessage << ". But that input port declared connection to '" << lOtherPort.getConnection()->component;
            lMessage << "', output channel " << lOtherPort.getConnection()->index;
            SWATCH_THROW(InvalidConnectionInfo(lMessage.str()));
          }
        }
        else
          SWATCH_THROW(InvalidConnectionInfo("No FPGA or optical module matches the ID '" + lPort->getConnection()->component + "' of the component connected to output port '" + lPort->getId() + "' on '" + lProcessor.getId() + "'"));
      }
    }
  }

  // Verify supplied TCDS2 connection info
  for (const auto& x : mProcessors) {
    const auto& lProcessor = *x.second;

    if (lProcessor.getTCDS2Source()) {
      const std::string lSourceId = *lProcessor.getTCDS2Source();
      if (lSourceId == lProcessor.getId())
        SWATCH_THROW(InvalidConnectionInfo("TCDS2 source of processor '" + lProcessor.getId() + "' set as itself; if set, this must be another FPGA"));

      if ((mProcessors.count(lSourceId) == 0) and (lSourceId != mServiceModule->getId()))
        SWATCH_THROW(InvalidConnectionInfo("TCDS2 source, '" + lSourceId + "', of processor '" + lProcessor.getId() + "' does not match ID of any other FPGA on the board"));
    }
  }
}


Board::~Board()
{
}


const std::string& Board::getHardwareType() const
{
  return mHardwareType;
}


ServiceModule& Board::getServiceModule()
{
  return *mServiceModule;
}


const ServiceModule& Board::getServiceModule() const
{
  return *mServiceModule;
}


std::set<std::string> Board::getProcessors() const
{
  std::set<std::string> lResult;
  for (const auto& x : mProcessors)
    lResult.insert(x.second->getId());
  return lResult;
}


Processor& Board::getProcessor(const std::string& aId)
{
  auto it = mProcessors.find(aId);
  if (it == mProcessors.end())
    throw std::runtime_error("No processor with ID '" + aId + "'");
  else
    return *(it->second);
}


const Processor& Board::getProcessor(const std::string& aId) const
{
  auto it = mProcessors.find(aId);
  if (it == mProcessors.end())
    throw std::runtime_error("No processor with ID '" + aId + "'");
  else
    return *(it->second);
}


std::unordered_set<std::string> Board::getOpticalModules() const
{
  std::unordered_set<std::string> lResult;
  for (const auto& x : mOpticalModules)
    lResult.insert(x.second->getId());
  return lResult;
}


OpticalModule& Board::getOpticalModule(const std::string& aId)
{
  auto it = mOpticalModules.find(aId);
  if (it == mOpticalModules.end())
    throw std::runtime_error("No optical module with ID '" + aId + "' could be found");
  else
    return *(it->second);
}


const OpticalModule& Board::getOpticalModule(const std::string& aId) const
{
  auto it = mOpticalModules.find(aId);
  if (it == mOpticalModules.end())
    throw std::runtime_error("No optical module with ID '" + aId + "' could be found");
  else
    return *(it->second);
}


std::unordered_set<std::string> Board::getFrontPanelOptoConnectors() const
{
  std::unordered_set<std::string> lResult;
  for (const auto& x : mFrontPanelOptoConnectors)
    lResult.insert(x.first);
  return lResult;
}


const OpticalFibreConnector* Board::getFrontPanelOptoConnector(const std::string& aId) const
{
  auto it = mFrontPanelOptoConnectors.find(aId);
  if (it == mFrontPanelOptoConnectors.end())
    throw std::runtime_error("No front panel fibre connector with ID '" + aId + "' could be found");
  else
    return it->second.get();
}


const InputPort* Board::getConnectedInputPort(const OpticalModuleChannel& aChannel) const
{
  auto it = mOpticalInputPortMap.find(&aChannel);
  if (it == mOpticalInputPortMap.end())
    return NULL;
  else
    return it->second.second;
}


const OutputPort* Board::getConnectedOutputPort(const OpticalModuleChannel& aChannel) const
{
  auto it = mOpticalOutputPortMap.find(&aChannel);
  if (it == mOpticalOutputPortMap.end())
    return NULL;
  else
    return it->second.second;
}


std::pair<const OpticalFibreConnector*, size_t> Board::getConnectedFrontPanelChannel(const OpticalModuleChannel& aChannel) const
{
  auto lIt = mOpticalFrontPanelChannelMap.find(&aChannel);
  if (lIt == mOpticalFrontPanelChannelMap.end())
    return { NULL, 0 };
  else
    return lIt->second;
}


const OpticalModuleChannel* Board::getConnectedOpticalChannel(const InputPort& aPort) const
{
  if (aPort.getConnection())
    return &getOpticalModule(aPort.getConnection()->component).getInput(aPort.getConnection()->index);
  else
    return NULL;
}

const OpticalModuleChannel* Board::getConnectedOpticalChannel(const OutputPort& aPort) const
{
  if (aPort.getConnection())
    return &getOpticalModule(aPort.getConnection()->component).getOutput(aPort.getConnection()->index);
  else
    return NULL;
}


const OpticalModuleChannel* Board::getConnectedOpticalChannel(const OpticalFibreConnector& aConnector, const size_t aIndex) const
{
  const auto lIt = mFrontPanelOpticalModuleMap.find(std::make_pair(&aConnector, aIndex));
  if (lIt == mFrontPanelOpticalModuleMap.end())
    return NULL;
  else
    return lIt->second;
}


void Board::registerOpticalModuleCallback(const std::string& aHardwareType, const OpticalModuleCallback& aCallback)
{
  if (sOpticalModuleCallbacks.find(aHardwareType) != sOpticalModuleCallbacks.end()) {
    SWATCH_THROW(CallbackAlreadyDefined("Optical module callback already defined for hardware type '" + aHardwareType + "'"));
  }

  sOpticalModuleCallbacks[aHardwareType] = aCallback;
}


void Board::syncOpticsMonitoringSettingsWithFPGA(const Processor& aProcessor)
{
  log4cplus::Logger& lLogger = getActionable(aProcessor.getId()).getLogger();
  LOG4CPLUS_DEBUG(lLogger, "Running generic board post-action callback (synchronises optical module channel settings with I/O ports)");

  std::vector<core::MonitorableObject*> lChannelsToDisable, lChannelsToIgnore, lChannelsToEnable;

  for (const auto* lInputPort : aProcessor.getInputPorts().getPorts()) {
    if (lInputPort->getConnection()) {
      const auto lOpticsID = *lInputPort->getConnection();
      const auto lModuleIt = mOpticalModules.find(lOpticsID.component);
      if (lModuleIt != mOpticalModules.end()) {
        OpticalModuleChannel& lOpticsChannel = lModuleIt->second->getInput(lOpticsID.index);
        if ((aProcessor.getInputPorts().getMonitoringStatus() == core::monitoring::kDisabled) or (lInputPort->getMonitoringStatus() == core::monitoring::kDisabled) or lInputPort->isInLoopback())
          lChannelsToDisable.push_back(&lOpticsChannel);
        else if ((aProcessor.getInputPorts().getMonitoringStatus() == core::monitoring::kNonCritical) or (lInputPort->getMonitoringStatus() == core::monitoring::kNonCritical))
          lChannelsToIgnore.push_back(&lOpticsChannel);
        else
          lChannelsToEnable.push_back(&lOpticsChannel);
      }
    }
  }

  for (const auto* lOutputPort : aProcessor.getOutputPorts().getPorts()) {
    if (lOutputPort->getConnection()) {
      const auto lOpticsID = *lOutputPort->getConnection();
      const auto lModuleIt = mOpticalModules.find(lOpticsID.component);
      if (lModuleIt != mOpticalModules.end()) {
        OpticalModuleChannel& lOpticsChannel = lModuleIt->second->getOutput(lOpticsID.index);
        if ((aProcessor.getOutputPorts().getMonitoringStatus() == core::monitoring::kDisabled) or (lOutputPort->getMonitoringStatus() == core::monitoring::kDisabled) or lOutputPort->isInLoopback())
          lChannelsToDisable.push_back(&lOpticsChannel);
        else if ((aProcessor.getOutputPorts().getMonitoringStatus() == core::monitoring::kNonCritical) or (lOutputPort->getMonitoringStatus() == core::monitoring::kNonCritical))
          lChannelsToIgnore.push_back(&lOpticsChannel);
        else
          lChannelsToEnable.push_back(&lOpticsChannel);
      }
    }
  }

  // In addition to sync'ing with FPGA input port settings, disable monitoring on any
  // optical module input/output channels that aren't connected to anything.
  for (const auto& lModuleId : getOpticalModules()) {
    auto& lModule = getOpticalModule(lModuleId);

    for (auto* lInputChannel : lModule.getInputs()) {
      if (getConnectedInputPort(*lInputChannel) == NULL)
        lChannelsToDisable.push_back(lInputChannel);
    }

    for (auto* lOutputChannel : lModule.getOutputs()) {
      if (getConnectedOutputPort(*lOutputChannel) == NULL)
        lChannelsToDisable.push_back(lOutputChannel);
    }
  }

  const std::function<bool(core::MonitorableObject*, core::MonitorableObject*)> lSortComparator = [](const core::MonitorableObject* x, const core::MonitorableObject* y) { return x->getPath() < y->getPath(); };
  std::sort(lChannelsToEnable.begin(), lChannelsToEnable.end(), lSortComparator);
  std::sort(lChannelsToIgnore.begin(), lChannelsToIgnore.end(), lSortComparator);
  std::sort(lChannelsToDisable.begin(), lChannelsToDisable.end(), lSortComparator);

  if (lChannelsToEnable.empty() and lChannelsToIgnore.empty() and lChannelsToDisable.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not enabling, ignoring or disabling any optics channels");
  else if (lChannelsToEnable.empty() and lChannelsToIgnore.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not enabling or ignoring any optics channels");
  else if (lChannelsToEnable.empty() and lChannelsToDisable.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not enabling or disabling any optics channels");
  else if (lChannelsToIgnore.empty() and lChannelsToDisable.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not ignoring or disabling any optics channels");
  else if (lChannelsToEnable.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not enabling any optics channels");
  else if (lChannelsToIgnore.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not ignoring any optics channels");
  else if (lChannelsToDisable.empty())
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Not disabling any optics channels");

  if (not lChannelsToEnable.empty()) {
    std::ostringstream lChannelStr;
    for (const auto* x : lChannelsToEnable)
      lChannelStr << " " << x->getPath();
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Enabling " << lChannelsToEnable.size() << " optics channels: " << lChannelStr.str());
  }
  for (auto* x : lChannelsToEnable)
    x->setMonitoringStatus(core::monitoring::kEnabled);

  if (not lChannelsToIgnore.empty()) {
    std::ostringstream lChannelStr;
    for (const auto* x : lChannelsToIgnore)
      lChannelStr << " " << x->getPath();
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Ignoring " << lChannelsToIgnore.size() << " optics channels: " << lChannelStr.str());
  }
  for (auto* x : lChannelsToIgnore)
    x->setMonitoringStatus(core::monitoring::kNonCritical);

  if (not lChannelsToDisable.empty()) {
    std::ostringstream lChannelStr;
    for (const auto* x : lChannelsToDisable)
      lChannelStr << " " << x->getPath();
    LOG4CPLUS_INFO(lLogger, "Post-action monitoring sync: Disabling " << lChannelsToDisable.size() << " optics channels: " << lChannelStr.str());
  }
  for (auto* x : lChannelsToDisable)
    x->setMonitoringStatus(core::monitoring::kDisabled);
}


std::unordered_map<std::string, Board::OpticalModuleCallback> Board::sOpticalModuleCallbacks;

} // namespace phase2
} // namespace swatch
