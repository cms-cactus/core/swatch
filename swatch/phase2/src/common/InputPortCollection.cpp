
#include "swatch/phase2/InputPortCollection.hpp"


#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/Processor.hpp"


namespace swatch {
namespace phase2 {


InputPortCollection::InputPortCollection() :
  core::MonitorableObject(processorIds::kInputPorts, "Input ports")
{
  setMonitoringStatus(core::monitoring::kDisabled);
}


InputPortCollection::~InputPortCollection()
{
}


size_t InputPortCollection::getNumPorts() const
{
  return mPorts.size();
}


bool InputPortCollection::contains(const size_t aIndex) const
{
  return (mPortMap.count(aIndex) != 0);
}


const std::vector<const InputPort*>& InputPortCollection::getPorts() const
{
  return mConstPorts;
}


const std::vector<InputPort*>& InputPortCollection::getPorts()
{
  return mPorts;
}


const InputPort& InputPortCollection::getPort(const std::string& aId) const
{
  if (const InputPort* in = getObjPtr<InputPort>(aId))
    return *in;
  else
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port of ID \"" + aId + "\""));
}


InputPort& InputPortCollection::getPort(const std::string& aId)
{
  if (InputPort* in = getObjPtr<InputPort>(aId))
    return *in;
  else
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port of ID \"" + aId + "\""));
}


const InputPort& InputPortCollection::getPort(const size_t aIndex) const
{
  const auto lIt = mPortMap.find(aIndex);
  if (lIt == mPortMap.end())
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port with index " + std::to_string(aIndex)));

  return *lIt->second;
}


InputPort& InputPortCollection::getPort(const size_t aIndex)
{
  const auto lIt = mPortMap.find(aIndex);
  if (lIt == mPortMap.end())
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port with index " + std::to_string(aIndex)));

  return *lIt->second;
}


void InputPortCollection::addPort(InputPort* aInput)
{
  addMonitorable(aInput);
  mConstPorts.push_back(aInput);
  mPorts.push_back(aInput);
  mPortMap[aInput->getIndex()] = aInput;
}


} // namespace phase2
} // namespace swatch
