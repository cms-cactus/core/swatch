
#include "swatch/phase2/AlgoInterface.hpp"

#include "swatch/phase2/Processor.hpp"


namespace swatch {
namespace phase2 {

AlgoInterface::AlgoInterface() :
  MonitorableObject(processorIds::kAlgo, "Algorithm")
{
  setMonitoringStatus(core::monitoring::kDisabled);
}

AlgoInterface::~AlgoInterface()
{
}


void AlgoInterface::retrieveMetricValues()
{
}


} // namespace phase2
} // namespace swatch
