
#include "swatch/phase2/Processor.hpp"


#include "swatch/action/StateMachine.hpp"

#include "swatch/phase2/AlgoInterface.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace swatch {
namespace phase2 {

namespace fileTypes {
const std::string kRxBufferData = "processor-rx-buffer-data";
const std::string kTxBufferData = "processor-tx-buffer-data";
}

namespace processorIds {
const std::string kAlgo = "algo";
const std::string kInputPorts = "inputPorts";
const std::string kOutputPorts = "outputPorts";
const std::string kReadout = "readout";
const std::string kTTC = "ttc";
}

const std::string PayloadTestFSM::kId = "payloadTest";
const std::string PayloadTestFSM::kStateInitial = "Halted";
const std::string PayloadTestFSM::kStateError = "Error";
const std::string PayloadTestFSM::kStateReady = "Ready";

const std::string PayloadTestFSM::kTrSetup = "setup";
const std::string PayloadTestFSM::kTrPlayback = "playback";


PayloadTestFSM::PayloadTestFSM(action::StateMachine& aFSM) :
  fsm(addStates(aFSM)),
  setup(fsm.addTransition(kTrSetup, kStateInitial, kStateReady)),
  playback(fsm.addTransition(kTrPlayback, kStateReady, kStateReady))
{
}


action::StateMachine& PayloadTestFSM::addStates(action::StateMachine& aFSM)
{
  aFSM.addState(kStateReady);
  return aFSM;
}


const std::string LinkTestFSM::kId = "linkTest";
const std::string LinkTestFSM::kStateInitial = "Halted";
const std::string LinkTestFSM::kStateError = "Error";
const std::string LinkTestFSM::kStateSynchronized = "Synchronized";
const std::string LinkTestFSM::kStateTxConfigured = "TxConfigured";
const std::string LinkTestFSM::kStateRxConfigured = "RxConfigured";

const std::string LinkTestFSM::kTrSetup = "setup";
const std::string LinkTestFSM::kTrConfigureTx = "configureTx";
const std::string LinkTestFSM::kTrConfigureRx = "configureRx";
const std::string LinkTestFSM::kTrCapture = "capture";
const std::string LinkTestFSM::kTrContinue = "continue";

LinkTestFSM::LinkTestFSM(action::StateMachine& aFSM) :
  fsm(addStates(aFSM)),
  setup(fsm.addTransition(kTrSetup, kStateInitial, kStateSynchronized)),
  configureTx(fsm.addTransition(kTrConfigureTx, "configure tx", kStateSynchronized, kStateTxConfigured)),
  configureRx(fsm.addTransition(kTrConfigureRx, "configure rx", kStateTxConfigured, kStateRxConfigured)),
  capture(fsm.addTransition(kTrCapture, kStateRxConfigured, kStateRxConfigured))
{
  fsm.addTransition(kTrContinue, kStateRxConfigured, kStateSynchronized);
}

action::StateMachine& LinkTestFSM::addStates(action::StateMachine& aFSM)
{
  aFSM.addState(kStateSynchronized);
  aFSM.addState(kStateTxConfigured);
  aFSM.addState(kStateRxConfigured);
  return aFSM;
}


const std::string SliceTestFSM::kId = "sliceTest";
const std::string SliceTestFSM::kStateInitial = "Halted";
const std::string SliceTestFSM::kStateError = "Error";
const std::string SliceTestFSM::kStateSynchronized = "Synchronized";
const std::string SliceTestFSM::kStateConfigured = "Configured";

const std::string SliceTestFSM::kTrSetup = "setup";
const std::string SliceTestFSM::kTrConfigure = "configure";
const std::string SliceTestFSM::kTrCapture = "capture";
const std::string SliceTestFSM::kTrContinue = "continue";

SliceTestFSM::SliceTestFSM(action::StateMachine& aFSM) :
  fsm(addStates(aFSM)),
  setup(fsm.addTransition(kTrSetup, kStateInitial, kStateSynchronized)),
  configure(fsm.addTransition(kTrConfigure, kStateSynchronized, kStateConfigured)),
  capture(fsm.addTransition(kTrCapture, kStateConfigured, kStateConfigured))
{
  fsm.addTransition(kTrContinue, kStateConfigured, kStateSynchronized);
}

action::StateMachine& SliceTestFSM::addStates(action::StateMachine& aFSM)
{
  aFSM.addState(kStateSynchronized);
  aFSM.addState(kStateConfigured);
  return aFSM;
}


Processor::Processor(const swatch::core::AbstractStub& aStub, const size_t aCSPIndexOffset) :
  Device(aStub),
  mTTC(NULL),
  mReadout(NULL),
  mAlgo(NULL),
  mInputPorts(NULL),
  mOutputPorts(NULL),
  mCSPIndexOffset(aCSPIndexOffset),
  mPayloadTestFSM(registerStateMachine(PayloadTestFSM::kId, PayloadTestFSM::kStateInitial, PayloadTestFSM::kStateError)),
  mLinkTestFSM(registerStateMachine(LinkTestFSM::kId, LinkTestFSM::kStateInitial, LinkTestFSM::kStateError)),
  mSliceTestFSM(registerStateMachine(SliceTestFSM::kId, SliceTestFSM::kStateInitial, SliceTestFSM::kStateError))
{
}


Processor::~Processor()
{
}


const TTCInterface& Processor::getTTC() const
{
  if (mTTC == NULL)
    SWATCH_THROW(core::RuntimeError("Processor \"" + getPath() + "\" has not registered any TTC interface object"));
  else
    return *mTTC;
}


TTCInterface& Processor::getTTC()
{
  if (mTTC == NULL)
    SWATCH_THROW(core::RuntimeError("Processor \"" + getPath() + "\" has not registered any TTC interface object"));
  else
    return *mTTC;
}


const ReadoutInterface& Processor::getReadout() const
{
  if (mReadout == NULL)
    SWATCH_THROW(core::RuntimeError("Processor \"" + getPath() + "\" has not registered any readout interface object"));
  else
    return *mReadout;
}


ReadoutInterface& Processor::getReadout()
{
  if (mReadout == NULL)
    SWATCH_THROW(core::RuntimeError("Processor \"" + getPath() + "\" has not registered any readout interface object"));
  else
    return *mReadout;
}


const AlgoInterface& Processor::getAlgo() const
{
  if (mAlgo == NULL)
    SWATCH_THROW(core::RuntimeError("Processor \"" + getPath() + "\" has not registered any algo interface object"));
  else
    return *mAlgo;
}


AlgoInterface& Processor::getAlgo()
{
  if (mAlgo == NULL)
    SWATCH_THROW(InterfaceNotDefined("Processor \"" + getPath() + "\" has not registered any algo interface object"));
  else
    return *mAlgo;
}


const InputPortCollection& Processor::getInputPorts() const
{
  if (mInputPorts == NULL)
    SWATCH_THROW(InterfaceNotDefined("Processor \"" + getPath() + "\" has not registered any input port collection"));
  else
    return *mInputPorts;
}


InputPortCollection& Processor::getInputPorts()
{
  if (mInputPorts == NULL)
    SWATCH_THROW(InterfaceNotDefined("Processor \"" + getPath() + "\" has not registered any input port collection"));
  else
    return *mInputPorts;
}


const OutputPortCollection& Processor::getOutputPorts() const
{
  if (mOutputPorts == NULL)
    SWATCH_THROW(InterfaceNotDefined("Processor \"" + getPath() + "\" has not registered any output port collection"));
  else
    return *mOutputPorts;
}


size_t Processor::getCSPIndexOffset() const
{
  return mCSPIndexOffset;
}


const boost::optional<std::string>& Processor::getTCDS2Source() const
{
  return mTCDS2Source;
}


OutputPortCollection& Processor::getOutputPorts()
{
  if (mOutputPorts == NULL)
    SWATCH_THROW(InterfaceNotDefined("Processor \"" + getPath() + "\" has not registered any output port collection"));
  else
    return *mOutputPorts;
}


TTCInterface& Processor::registerTTC(TTCInterface* aTTCInterface)
{
  if (mTTC) {
    delete aTTCInterface;
    SWATCH_THROW(InterfaceAlreadyDefined("TTCInterface already defined for processor '" + getPath() + "'"));
  }
  this->addMonitorable(aTTCInterface);
  mTTC = aTTCInterface;
  return *mTTC;
}


ReadoutInterface& Processor::registerReadout(ReadoutInterface* aReadoutInterface)
{
  if (mReadout) {
    delete aReadoutInterface;
    SWATCH_THROW(InterfaceAlreadyDefined("ReadoutInterface already defined for processor '" + getPath() + "'"));
  }
  this->addMonitorable(aReadoutInterface);
  mReadout = aReadoutInterface;
  return *mReadout;
}


AlgoInterface& Processor::registerAlgo(AlgoInterface* aAlgoInterface)
{
  if (mAlgo) {
    delete aAlgoInterface;
    SWATCH_THROW(InterfaceAlreadyDefined("AlgoInterface already defined for processor '" + getPath() + "'"));
  }
  this->addMonitorable(aAlgoInterface);
  mAlgo = aAlgoInterface;
  return *mAlgo;
}


InputPortCollection& Processor::registerInputs()
{
  return registerInputs(new InputPortCollection());
}


InputPortCollection& Processor::registerInputs(InputPortCollection* aPortCollection)
{
  if (mInputPorts) {
    delete aPortCollection;
    SWATCH_THROW(InterfaceAlreadyDefined("InputPortCollection already defined for processor '" + getPath() + "'"));
  }
  this->addMonitorable(aPortCollection);
  mInputPorts = aPortCollection;
  return *mInputPorts;
}


OutputPortCollection& Processor::registerOutputs()
{
  return registerOutputs(new OutputPortCollection());
}


OutputPortCollection& Processor::registerOutputs(OutputPortCollection* aPortCollection)
{
  if (mOutputPorts) {
    delete aPortCollection;
    SWATCH_THROW(InterfaceAlreadyDefined("OutputPortCollection already defined for processor '" + getPath() + "'"));
  }
  this->addMonitorable(aPortCollection);
  mOutputPorts = aPortCollection;
  return *mOutputPorts;
}


void Processor::setTCDS2Source(const std::string& aId)
{
  if (mTCDS2Source)
    SWATCH_THROW(TCDS2SourceAlreadyDefined("The TCDS2 source of '" + getId() + "' has already been set to '" + *mTCDS2Source + "'; this should be set once on construction (if applicable), and never changed"));

  if (aId == getId())
    SWATCH_THROW(TCDS2SourceAlreadyDefined("The TCDS2 source of '" + getId() + "' has already been set to '" + *mTCDS2Source + "'; this should be set once on construction (if applicable), and never changed"));

  mTCDS2Source = aId;
}


PayloadTestFSM& Processor::getPayloadTestFSM()
{
  return mPayloadTestFSM;
}


LinkTestFSM& Processor::getLinkTestFSM()
{
  return mLinkTestFSM;
}


SliceTestFSM& Processor::getSliceTestFSM()
{
  return mSliceTestFSM;
}


} // namespace phase2
} // namespace swatch
