
#include "swatch/phase2/OutputPort.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace swatch {
namespace phase2 {


OutputPort::OutputPort(const size_t aIndex, const std::string& aId) :
  OutputPort(aIndex, aId, boost::none)
{
}


OutputPort::OutputPort(const size_t aIndex, const std::string& aId, const ChannelID& aConnectedChannel) :
  OutputPort(aIndex, aId, boost::optional<ChannelID>(aConnectedChannel))
{
}


OutputPort::OutputPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel) :
  AbstractPort(aIndex, aId, aConnectedChannel),
  mMetricIsOperating(registerMetric<bool>(kMetricIdIsOperating, "Is operating?", core::EqualCondition<bool>(false)))
{
}


OutputPort::~OutputPort()
{
}


const std::string OutputPort::kMetricIdIsOperating = "isOperating";


} // namespace phase2
} // namespace swatch
