
#include "swatch/phase2/InputPort.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace swatch {
namespace phase2 {

AbstractPort::AbstractPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel) :
  MaskableObject(aId, aId),
  mIndex(aIndex),
  mInLoopback(false),
  mPRBSMode(kPRBS31),
  mConnection(aConnectedChannel)
{
}


AbstractPort::~AbstractPort()
{
}


size_t AbstractPort::getIndex() const
{
  return mIndex;
}


bool AbstractPort::isInLoopback() const
{
  return mInLoopback;
}


void AbstractPort::setInLoopback(bool aInLoopback)
{
  mInLoopback = aInLoopback;
}


LinkOperatingMode AbstractPort::getOperatingMode() const
{
  return bool(mCSPSettings) ? kCSP : kPRBS;
}


PRBSMode AbstractPort::getPRBSMode() const
{
  if (not mPRBSMode)
    throw IncorrectPortMode("Port " + getId() + " is operating in CSP mode, but PRBS settings requested");

  return *mPRBSMode;
}


const CSPSettings& AbstractPort::getCSPSettings() const
{
  if (not mCSPSettings)
    throw IncorrectPortMode("Port " + getId() + " is operating in PRBS mode, but CSP settings requested");

  return *mCSPSettings;
}


void AbstractPort::setOperatingMode(const PRBSMode aMode)
{
  mPRBSMode = PRBSMode(aMode);
  mCSPSettings.reset();
}


void AbstractPort::setOperatingMode(const CSPSettings& aSettings)
{
  mPRBSMode.reset();
  mCSPSettings = CSPSettings(aSettings);
}


const boost::optional<ChannelID>& AbstractPort::getConnection() const
{
  return mConnection;
}


} // namespace phase2
} // namespace swatch
