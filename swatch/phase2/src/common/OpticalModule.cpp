
#include "swatch/phase2/OpticalModule.hpp"

#include <numeric>

#include "swatch/logger/Logger.hpp"


namespace swatch {
namespace phase2 {


OpticalModuleChannel::OpticalModuleChannel(const Direction aDirection, const size_t aIndex, const std::function<void(OpticalModuleChannel&)>& aRegistrar, const std::function<void(OpticalModuleChannel&)>& aMetricUpdater, const std::function<void(OpticalModuleChannel&)>& aPropertyUpdater) :
  PropertyHolder((aDirection == kRx ? "Rx" : "Tx") + std::to_string(aIndex)),
  mDirection(aDirection),
  mIndex(aIndex),
  mMetricUpdater(aMetricUpdater),
  mPropertyUpdater(aPropertyUpdater)
{
  setMonitoringStatus(core::monitoring::kDisabled);
  aRegistrar(*this);
}


OpticalModuleChannel::~OpticalModuleChannel()
{
}


OpticalModuleChannel::Direction OpticalModuleChannel::getDirection() const
{
  return mDirection;
}


size_t OpticalModuleChannel::getIndex() const
{
  return mIndex;
}


void OpticalModuleChannel::delegateMetricUpdate(const std::string& aId, MonitorableObject& aMonObj)
{
  MonitorableObject::delegateMetricUpdate(aId, aMonObj);
}


void OpticalModuleChannel::retrieveMetricValues()
{
  mMetricUpdater(*this);
}


bool OpticalModuleChannel::checkPresence() const
{
  return true;
}


void OpticalModuleChannel::retrievePropertyValues()
{
  mPropertyUpdater(*this);
}


OpticalModule::OpticalModule(const std::string& aId, size_t aNumInputChannels, size_t aNumOutputChannels) :
  OpticalModule(aId, aId, aNumInputChannels, aNumOutputChannels)
{
}


OpticalModule::OpticalModule(const std::string& aId, const std::string& aAlias, size_t aNumInputChannels, size_t aNumOutputChannels) :
  OpticalModule(aId, aAlias, getIndexVector(aNumInputChannels), getIndexVector(aNumOutputChannels))
{
}


OpticalModule::OpticalModule(const std::string& aId, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices) :
  OpticalModule(aId, aId, aInputIndices, aOutputIndices)
{
}


OpticalModule::OpticalModule(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices) :
  PropertyHolder(aId, aAlias),
  mStatus(),
  mLogger(swatch::logger::Logger::getInstance(aId))
{
  setMonitorableStatus(mStatus, mLogger);

  std::vector<size_t> lInputIndices(aInputIndices);
  std::sort(lInputIndices.begin(), lInputIndices.end());

  std::vector<size_t> lOutputIndices(aOutputIndices);
  std::sort(lInputIndices.begin(), lInputIndices.end());

  for (size_t i : lInputIndices) {
    if (mInputMap.count(i) > 0)
      SWATCH_THROW(DuplicateChannelIndex("Input channel index " + std::to_string(i) + " listed multiple times for optical module '" + getId() + "'"));

    auto* lInput = new OpticalModuleChannel(
        OpticalModuleChannel::kRx, i, [this](OpticalModuleChannel& x) { this->registerInputProperties(x); this->registerInputMetrics(x); }, [this](OpticalModuleChannel& x) { this->retrieveInputMetricValues(x); }, [this](OpticalModuleChannel& x) { this->retrieveInputPropertyValues(x); });
    addMonitorable(lInput);
    mInputMap[i] = lInput;
    mInputs.push_back(lInput);
    mConstInputs.push_back(lInput);
  }

  for (size_t i : lOutputIndices) {
    if (mOutputMap.count(i) > 0)
      SWATCH_THROW(DuplicateChannelIndex("Output channel index " + std::to_string(i) + " listed multiple times for optical module '" + getId() + "'"));

    auto* lOutput = new OpticalModuleChannel(
        OpticalModuleChannel::kTx, i, [this](OpticalModuleChannel& x) { this->registerOutputProperties(x); this->registerOutputMetrics(x); }, [this](OpticalModuleChannel& x) { this->retrieveOutputMetricValues(x); }, [this](OpticalModuleChannel& x) { this->retrieveOutputPropertyValues(x); });
    addMonitorable(lOutput);
    mOutputMap[i] = lOutput;
    mOutputs.push_back(lOutput);
    mConstOutputs.push_back(lOutput);
  }
}


OpticalModule::~OpticalModule()
{
}


const std::vector<const OpticalModuleChannel*>& OpticalModule::getInputs() const
{
  return mConstInputs;
}


const std::vector<OpticalModuleChannel*>& OpticalModule::getInputs()
{
  return mInputs;
}

bool OpticalModule::containsInput(size_t i) const
{
  return mInputMap.count(i);
}

const OpticalModuleChannel& OpticalModule::getInput(size_t i) const
{
  return *mInputMap.at(i);
}


OpticalModuleChannel& OpticalModule::getInput(size_t i)
{
  return *mInputMap.at(i);
}


const std::vector<const OpticalModuleChannel*>& OpticalModule::getOutputs() const
{
  return mConstOutputs;
}


const std::vector<OpticalModuleChannel*>& OpticalModule::getOutputs()
{
  return mOutputs;
}


bool OpticalModule::containsOutput(size_t i) const
{
  return mOutputMap.count(i);
}


const OpticalModuleChannel& OpticalModule::getOutput(size_t i) const
{
  return *mOutputMap.at(i);
}


OpticalModuleChannel& OpticalModule::getOutput(size_t i)
{
  return *mOutputMap.at(i);
}


void OpticalModule::registerInputMetrics(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::registerInputProperties(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::registerOutputMetrics(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::registerOutputProperties(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::retrieveInputMetricValues(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::retrieveInputPropertyValues(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::retrieveOutputMetricValues(OpticalModuleChannel& aChannel)
{
}


void OpticalModule::retrieveOutputPropertyValues(OpticalModuleChannel& aChannel)
{
}


std::vector<size_t> OpticalModule::getIndexVector(size_t aNumChannels)
{
  std::vector<size_t> indices(aNumChannels);
  std::iota(indices.begin(), indices.end(), 0);
  return indices;
}

} // namespace phase2
} // namespace swatch
