#include "swatch/phase2/ServiceModule.hpp"


namespace swatch {
namespace phase2 {


ServiceModule::ServiceModule(const swatch::core::AbstractStub& aStub) :
  Device(aStub),
  mMetricLogicalSlot(registerMetric<uint16_t>(kMetricIdLogicalSlot, "Logical slot")),
  mMetricShelfAddress(registerMetric<std::string>(kMetricIdShelfAddress, "ATCA shelf address"))
{
}


ServiceModule::~ServiceModule()
{
}


const std::string ServiceModule::kMetricIdLogicalSlot = "logicalSlot";
const std::string ServiceModule::kMetricIdShelfAddress = "shelfAddress";


} // namespace phase2
} // namespace swatch
