
#include "swatch/phase2/OutputPortCollection.hpp"


#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/Processor.hpp"


namespace swatch {
namespace phase2 {


OutputPortCollection::OutputPortCollection() :
  core::MonitorableObject(processorIds::kOutputPorts, "Output ports")
{
  setMonitoringStatus(core::monitoring::kDisabled);
}


OutputPortCollection::~OutputPortCollection()
{
}


size_t OutputPortCollection::getNumPorts() const
{
  return mPorts.size();
}


bool OutputPortCollection::contains(const size_t aIndex) const
{
  return (mPortMap.count(aIndex) != 0);
}


const std::vector<const OutputPort*>& OutputPortCollection::getPorts() const
{
  return mConstPorts;
}


const std::vector<OutputPort*>& OutputPortCollection::getPorts()
{
  return mPorts;
}


const OutputPort& OutputPortCollection::getPort(const std::string& aId) const
{
  if (const OutputPort* in = getObjPtr<OutputPort>(aId))
    return *in;
  else
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port of ID \"" + aId + "\""));
}


OutputPort& OutputPortCollection::getPort(const std::string& aId)
{
  if (OutputPort* in = getObjPtr<OutputPort>(aId))
    return *in;
  else
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any input port of ID \"" + aId + "\""));
}


const OutputPort& OutputPortCollection::getPort(const size_t aIndex) const
{
  const auto lIt = mPortMap.find(aIndex);
  if (lIt == mPortMap.end())
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any output port with index " + std::to_string(aIndex)));

  return *lIt->second;
}


OutputPort& OutputPortCollection::getPort(const size_t aIndex)
{
  const auto lIt = mPortMap.find(aIndex);
  if (lIt == mPortMap.end())
    SWATCH_THROW(core::RuntimeError("PortCollection \"" + this->getPath() + "\" does not contain any output port with index " + std::to_string(aIndex)));

  return *lIt->second;
}


void OutputPortCollection::addPort(OutputPort* aOutput)
{
  addMonitorable(aOutput);
  mConstPorts.push_back(aOutput);
  mPorts.push_back(aOutput);
  mPortMap[aOutput->getIndex()] = aOutput;
}


} // namespace phase2
} // namespace swatch
