
#include "swatch/phase2/EmptySocket.hpp"


namespace swatch {
namespace phase2 {

EmptySocket::EmptySocket(const std::string& aId, size_t aNumInputChannels, size_t aNumOutputChannels) :
  EmptySocket(aId, aId, aNumInputChannels, aNumOutputChannels)
{
}


EmptySocket::EmptySocket(const std::string& aId, const std::string& aAlias, size_t aNumInputChannels, size_t aNumOutputChannels) :
  OpticalModule(aId, aAlias, aNumInputChannels, aNumOutputChannels)
{
}


EmptySocket::EmptySocket(const std::string& aId, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices) :
  EmptySocket(aId, aId, aInputIndices, aOutputIndices)
{
}


EmptySocket::EmptySocket(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices) :
  OpticalModule(aId, aAlias, aInputIndices, aOutputIndices)
{
}


EmptySocket::~EmptySocket()
{
}


bool EmptySocket::checkPresence() const
{
  return false;
}


void EmptySocket::retrievePropertyValues()
{
}


void EmptySocket::retrieveMetricValues()
{
}

} // namespace phase2
} // namespace swatch