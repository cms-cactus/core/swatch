
#include "swatch/phase2/ObjectContainer.hpp"

#include "swatch/action/ActionableObject.hpp"
#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

std::set<std::string> ObjectContainer::getActionables() const
{
  std::set<std::string> lResult;
  for (const auto& x : mActionables)
    lResult.insert(x.second->getId());
  return lResult;
}


action::ActionableObject& ObjectContainer::getActionable(const std::string& aId)
{
  auto it = mActionables.find(aId);
  if (it == mActionables.end())
    throw std::runtime_error("No actionable with ID '" + aId + "'");
  else
    return *(it->second);
}


const action::ActionableObject& ObjectContainer::getActionable(const std::string& aId) const
{
  auto it = mActionables.find(aId);
  if (it == mActionables.end())
    throw std::runtime_error("No actionable with ID '" + aId + "'");
  else
    return *(it->second);
}


std::set<std::string> ObjectContainer::getMonitorables() const
{
  std::set<std::string> lResult;
  for (const auto& x : mMonitorables)
    lResult.insert(x.second->getId());
  return lResult;
}


core::MonitorableObject& ObjectContainer::getMonitorable(const std::string& aId)
{
  auto it = mMonitorables.find(aId);
  if (it == mMonitorables.end())
    throw std::runtime_error("No monitorable with ID '" + aId + "'");
  else
    return *(it->second);
}


const core::MonitorableObject& ObjectContainer::getMonitorable(const std::string& aId) const
{
  auto it = mMonitorables.find(aId);
  if (it == mMonitorables.end())
    throw std::runtime_error("No monitorable with ID '" + aId + "'");
  else
    return *(it->second);
}


void ObjectContainer::addActionable(action::ActionableObject* aActionableObj)
{
  const auto lId = aActionableObj->getId();
  if (mActionables.count(lId)) {
    delete aActionableObj;
    SWATCH_THROW(core::ObjectOfSameIdAlreadyExists("Cannot register actionable object '" + lId + "', as actionable object of same ID already exists"));
  }

  if (mMonitorables.count(lId)) {
    delete aActionableObj;
    SWATCH_THROW(core::ObjectOfSameIdAlreadyExists("Cannot register actionable object '" + lId + "', as monitorable object of same ID already exists"));
  }

  mActionables[lId] = aActionableObj;
  mMonitorables[lId] = std::shared_ptr<core::MonitorableObject>(aActionableObj, action::ActionableObject::Deleter());

  initializePresenceAndProperties(*aActionableObj);
}


void ObjectContainer::addMonitorable(core::MonitorableObject* aMonitorableObj)
{
  const auto lId = aMonitorableObj->getId();

  if (mMonitorables.count(lId)) {
    delete aMonitorableObj;
    SWATCH_THROW(core::ObjectOfSameIdAlreadyExists("Cannot register monitorable object '" + lId + "', as monitorable object of same ID already exists"));
  }

  mMonitorables[lId] = std::shared_ptr<core::MonitorableObject>(aMonitorableObj);

  initializePresenceAndProperties(*aMonitorableObj);
}


void ObjectContainer::initializePresenceAndProperties(core::MonitorableObject& aObj)
{
  if (auto* lPropertyHolder = dynamic_cast<action::PropertyHolder*>(&aObj)) {
    lPropertyHolder->updatePresence();
    if (lPropertyHolder->isPresent())
      lPropertyHolder->updateProperties();
    else
      return;
  }

  for (const auto& lChildId : aObj.getChildren()) {
    if (auto* lMonChild = dynamic_cast<core::MonitorableObject*>(&aObj.getObj(lChildId)))
      initializePresenceAndProperties(*lMonChild);
  }
}

} // namespace phase2
} // namespace swatch
