
#include "swatch/phase2/OpticalFibreConnectorStub.hpp"


namespace swatch {
namespace phase2 {


OpticalFibreConnectorStub::OpticalFibreConnectorStub(const std::string& aId, const std::string& aType, const size_t aChannelCount) :
  id(aId),
  type(aType),
  channelCount(aChannelCount)
{
}


OpticalFibreConnectorStub::~OpticalFibreConnectorStub()
{
}


} // namespace phase2
} // namespace swatch
