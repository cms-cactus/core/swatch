
#include "swatch/phase2/ReadoutInterface.hpp"


#include "swatch/phase2/Processor.hpp"


namespace swatch {
namespace phase2 {

ReadoutInterface::ReadoutInterface() :
  MonitorableObject(processorIds::kReadout, "Readout")
{
  setMonitoringStatus(core::monitoring::kDisabled);
}

ReadoutInterface::~ReadoutInterface()
{
}

} // namespace phase2
} // namespace swatch
