
#include "swatch/phase2/OpticalFibreConnector.hpp"


namespace swatch {
namespace phase2 {

OpticalFibreConnector::OpticalFibreConnector(const std::string& aId, const std::string& aType, const size_t aChannelCount, const std::map<size_t, std::string>& aConnectedOpticsMap) :
  mId(aId),
  mType(aType)
{
  // 1) Check that provided map doesn't contain any other channels outside of expected range
  for (const auto& x : aConnectedOpticsMap) {
    if ((x.first == 0) or (x.first > aChannelCount))
      throw InvalidOpticalFibreChannelIndex("Optical connector-module channel map supplied for connector " + getId() + " contains index " + std::to_string(x.first) + " which is outside of expected range (1 to " + std::to_string(aChannelCount) + ")");
  }

  // 2) Transfer map to member var
  for (size_t i = 1; i <= aChannelCount; i++) {
    mChannels.push_back(i);
    const auto lIt = aConnectedOpticsMap.find(i);
    if (lIt == aConnectedOpticsMap.end())
      mConnectedOpticsChannels[i] = boost::optional<std::string>();
    else
      mConnectedOpticsChannels[i] = boost::optional<std::string>(lIt->second);
  }
}


OpticalFibreConnector::~OpticalFibreConnector()
{
}


const std::string& OpticalFibreConnector::getId() const
{
  return mId;
}


const std::string& OpticalFibreConnector::getType() const
{
  return mType;
}


const std::vector<size_t>& OpticalFibreConnector::getChannels() const
{
  return mChannels;
}


const boost::optional<std::string>& OpticalFibreConnector::getConnectedOpticsChannel(const size_t i) const
{
  const auto lIt = mConnectedOpticsChannels.find(i);
  if (lIt == mConnectedOpticsChannels.end())
    SWATCH_THROW(InvalidOpticalFibreChannelIndex("Optical fibre connector " + getId() + " does not have channel of index " + std::to_string(i) + "  (valid range: 1 to " + std::to_string(mChannels.size()) + ")"));

  return lIt->second;
}


} // namespace phase2
} // namespace swatch