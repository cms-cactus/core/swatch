
#include "swatch/phase2/Device.hpp"


#include "swatch/core/AbstractStub.hpp"


namespace swatch {
namespace phase2 {


Device::Device(const swatch::core::AbstractStub& aStub) :
  ActionableObject(aStub.id, aStub.alias, aStub.loggerName),
  mStub(dynamic_cast<const DeviceStub&>(aStub)),
  mGateKeeperTables { getStub().id, getStub().role }
{
}


Device::~Device()
{
}


const DeviceStub& Device::getStub() const
{
  return mStub;
}


const std::vector<std::string>& Device::getGateKeeperContexts() const
{
  return mGateKeeperTables;
}


} // namespace phase2
} // namespace swatch
