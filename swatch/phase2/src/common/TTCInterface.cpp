
#include "swatch/phase2/TTCInterface.hpp"


#include "swatch/core/MetricConditions.hpp"
#include "swatch/phase2/Processor.hpp"


namespace swatch {
namespace phase2 {

TTCInterface::TTCInterface() :
  PropertyHolder(processorIds::kTTC, "TTC"),
  mMetricL1ACounter(registerMetric<uint32_t>("l1aCounter", "L1A count")),
  mMetricBunchCounter(registerMetric<uint32_t>("bunchCounter", "Bunch counter")),
  mMetricOrbitCounter(registerMetric<uint32_t>("orbitCounter", "Orbit counter")),
  mMetricIsClock40Locked(registerMetric<bool>("isClock40Locked", "Is clock locked?", core::EqualCondition<bool>(false))),
  mMetricHasClock40Stopped(registerMetric<bool>("hasClock40Stopped", "Has clock stopped?", core::EqualCondition<bool>(true))),
  mMetricIsBC0Locked(registerMetric<bool>("isBC0Locked", "Is BC0 stable?", core::EqualCondition<bool>(false)))
{
  setMonitoringStatus(core::monitoring::kDisabled);
}

TTCInterface::~TTCInterface()
{
}


} // namespace phase2
} // namespace swatch
