
#include "swatch/phase2/InputPort.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace swatch {
namespace phase2 {

InputPort::InputPort(const size_t aIndex, const std::string& aId) :
  InputPort(aIndex, aId, boost::none)
{
}


InputPort::InputPort(const size_t aIndex, const std::string& aId, const ChannelID& aConnectedChannel) :
  InputPort(aIndex, aId, boost::optional<ChannelID>(aConnectedChannel))
{
}


InputPort::InputPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel) :
  AbstractPort(aIndex, aId, aConnectedChannel),
  mMetricIsLocked(registerMetric<bool>(kMetricIdIsLocked, "Is PLL locked?", core::EqualCondition<bool>(false))),
  mMetricIsAligned(registerMetric<bool>(kMetricIdIsAligned, "Is aligned?", core::EqualCondition<bool>(false))),
  mMetricCRCErrors(registerMetric<uint32_t>(kMetricIdCRCErrors, "CRC error count")),
  mMetricSourceChannelId(registerMetric<uint16_t>("sourceChannel", "Source channel ID")),
  mMetricSourceSlotId(registerMetric<uint16_t>("sourceSlot", "Source slot ID")),
  mMetricSourceCrateId(registerMetric<uint16_t>("sourceCrate", "Source crate ID"))
{
  setWarningCondition(mMetricCRCErrors, core::GreaterThanCondition<uint32_t>(0));
}


InputPort::~InputPort()
{
}


const std::string InputPort::kMetricIdIsLocked = "isLocked";
const std::string InputPort::kMetricIdIsAligned = "isAligned";
const std::string InputPort::kMetricIdCRCErrors = "crcErrors";


} // namespace phase2
} // namespace swatch
