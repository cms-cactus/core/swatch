#ifndef __SWATCH_PHASE2_PRBSMODE_HPP__
#define __SWATCH_PHASE2_PRBSMODE_HPP__


namespace swatch {
namespace phase2 {


enum PRBSMode {
  kPRBS7,
  kPRBS9,
  kPRBS15,
  kPRBS23,
  kPRBS31
};


} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_PRBSMODE_HPP__ */