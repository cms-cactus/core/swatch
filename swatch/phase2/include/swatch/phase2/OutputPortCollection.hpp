
#ifndef __SWATCH_PHASE2_OUTPUTPORTCOLLECTION__
#define __SWATCH_PHASE2_OUTPUTPORTCOLLECTION__


#include <cstddef>
#include <map>
#include <string>
#include <vector>

#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

class OutputPort;

//! Class that aggregates the output ports of a processor
class OutputPortCollection : public core::MonitorableObject {
public:
  OutputPortCollection();

  virtual ~OutputPortCollection();

  //! Returns number of output channels
  size_t getNumPorts() const;

  bool contains(const size_t) const;

  const std::vector<const OutputPort*>& getPorts() const;

  const std::vector<OutputPort*>& getPorts();

  const OutputPort& getPort(const std::string& aId) const;

  OutputPort& getPort(const std::string& aId);

  const OutputPort& getPort(const size_t) const;

  OutputPort& getPort(const size_t);

  void addPort(OutputPort* aOutput);

protected:
  void retrieveMetricValues() {}

private:
  std::vector<const OutputPort*> mConstPorts;
  std::vector<OutputPort*> mPorts;
  std::map<size_t, OutputPort*> mPortMap;
};

} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_OUTPUTPORTCOLLECTION__ */