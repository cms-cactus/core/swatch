
#ifndef __SWATCH_PHASE2_SERVICEMODULE_HPP__
#define __SWATCH_PHASE2_SERVICEMODULE_HPP__


#include "swatch/phase2/Device.hpp"


namespace swatch {

namespace core {
template <typename DataType>
class SimpleMetric;
}

namespace phase2 {


class ServiceModule : public Device {
public:
  ServiceModule(const ::swatch::core::AbstractStub& aStub);
  virtual ~ServiceModule();

  static const std::string kMetricIdLogicalSlot;
  static const std::string kMetricIdShelfAddress;

protected:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;

  core::SimpleMetric<uint16_t>& mMetricLogicalSlot;
  core::SimpleMetric<std::string>& mMetricShelfAddress;
};


} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_SERVICEMODULE_HPP__ */