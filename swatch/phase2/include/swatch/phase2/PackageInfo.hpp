#ifndef __SWATCH_PHASE2_PACKAGEINFO_HPP__
#define __SWATCH_PHASE2_PACKAGEINFO_HPP__


#include <chrono>
#include <cstddef>
#include <string>
#include <vector>

#include <boost/optional.hpp>
#include <boost/variant.hpp>


namespace swatch {
namespace phase2 {

struct PackageInfo {

  struct Version {
    size_t major;
    size_t minor;
    size_t patch;
    boost::optional<std::string> prerelease;
  };

  struct Git {
    enum RefType {
      kTag,
      kBranch
    };

    std::string sha;
    bool clean;
    boost::optional<std::pair<RefType, std::string>> ref;
  };

  struct LocalBuild {
    std::chrono::system_clock::time_point time;
  };

  struct GitLabBuild {
    std::chrono::system_clock::time_point time;
    std::string serverURL;
    std::string projectPath;
    size_t projectID;
    size_t pipelineID;
    size_t jobID;
  };

  std::string name;
  Version version;

  // Information from version control system
  boost::optional<Git> vcs;

  // Build info
  boost::variant<LocalBuild, GitLabBuild> build;
};

} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_PACKAGEINFO_HPP__ */
