
#ifndef __SWATCH_PHASE2_OUTPUTPORT__
#define __SWATCH_PHASE2_OUTPUTPORT__


#include "swatch/phase2/AbstractPort.hpp"


namespace swatch {

namespace core {
template <typename DataType>
class SimpleMetric;
}

namespace phase2 {

class OutputPort : public AbstractPort {
protected:
  explicit OutputPort(const size_t aIndex, const std::string& aId);

  explicit OutputPort(const size_t aIndex, const std::string& aId, const ChannelID& aConnectedChannel);

  explicit OutputPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel);

public:
  virtual ~OutputPort();

  static const std::string kMetricIdIsOperating;

  // static const std::vector<std::string> kDefaultMetrics;

protected:
  core::SimpleMetric<bool>& mMetricIsOperating;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_OUTPUTPORT__ */
