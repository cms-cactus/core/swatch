
#ifndef __SWATCH_PHASE2_READOUTINTERFACE_HPP__
#define __SWATCH_PHASE2_READOUTINTERFACE_HPP__


#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

//! Abstract class defining the readout component interface of a processor
class ReadoutInterface : public core::MonitorableObject {
protected:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;

  ReadoutInterface();

public:
  virtual ~ReadoutInterface();

  // static const std::vector<std::string> kDefaultMetrics;

protected:
  // Eventually, phase-2 equivalent of: TTS state, 'ready', event counter
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_READOUTINTERFACE_HPP__ */