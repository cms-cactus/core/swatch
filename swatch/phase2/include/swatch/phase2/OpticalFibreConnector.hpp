
#ifndef __SWATCH_PHASE2_OPTICALFIBRECONNECTOR_HPP__
#define __SWATCH_PHASE2_OPTICALFIBRECONNECTOR_HPP__


#include <map>
#include <stddef.h>
#include <string>
#include <vector>

#include "boost/optional.hpp"

#include "swatch/core/exception.hpp"


namespace swatch {
namespace phase2 {

class OpticalFibreConnector {
public:
  OpticalFibreConnector(const std::string&, const std::string&, const size_t, const std::map<size_t, std::string>&);
  ~OpticalFibreConnector();

  const std::string& getId() const;

  const std::string& getType() const;

  const std::vector<size_t>& getChannels() const;

  const boost::optional<std::string>& getConnectedOpticsChannel(const size_t) const;

private:
  const std::string mId, mType;
  std::vector<size_t> mChannels;
  std::map<size_t, boost::optional<std::string>> mConnectedOpticsChannels;
};


SWATCH_DEFINE_EXCEPTION(InvalidOpticalFibreChannelIndex)

}
}


#endif /* __SWATCH_PHASE2_OPTICALFIBRECONNECTOR_HPP__ */