
#ifndef __SWATCH_PHASE2_ABSTRACTPORT__
#define __SWATCH_PHASE2_ABSTRACTPORT__


#include "boost/optional.hpp"

#include "swatch/action/MaskableObject.hpp"
#include "swatch/phase2/CSPSettings.hpp"
#include "swatch/phase2/ChannelID.hpp"
#include "swatch/phase2/LinkOperatingMode.hpp"
#include "swatch/phase2/PRBSMode.hpp"


namespace swatch {

namespace core {
template <typename DataType>
class SimpleMetric;
}

namespace phase2 {

class AbstractPort : public action::MaskableObject {
protected:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;
  template <typename DataType>
  using Property = swatch::action::Property<DataType>;

  explicit AbstractPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel);

public:
  virtual ~AbstractPort();

  size_t getIndex() const;

  bool isInLoopback() const;

  void setInLoopback(bool aLoopback = true);

  LinkOperatingMode getOperatingMode() const;

  PRBSMode getPRBSMode() const;

  const CSPSettings& getCSPSettings() const;

  void setOperatingMode(const PRBSMode);

  void setOperatingMode(const CSPSettings&);

  //! Returns information regarding the optical module or FPGA I/O port that this port is connected to (if any)
  const boost::optional<ChannelID>& getConnection() const;

protected:
  const size_t mIndex;
  bool mInLoopback;
  boost::optional<PRBSMode> mPRBSMode;
  boost::optional<CSPSettings> mCSPSettings;
  const boost::optional<ChannelID> mConnection;
};

SWATCH_DEFINE_EXCEPTION(IncorrectPortMode);

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_ABSTRACTPORT__ */
