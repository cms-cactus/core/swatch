
#ifndef __SWATCH_PHASE2_OBJECTCONTAINER_HPP__
#define __SWATCH_PHASE2_OBJECTCONTAINER_HPP__


#include <memory>
#include <set>
#include <string>
#include <unordered_map>


namespace swatch {
namespace action {
class ActionableObject;
}
namespace core {
class MonitorableObject;
}

namespace phase2 {

class ObjectContainer {
public:
  ObjectContainer() = default;

  ObjectContainer(const ObjectContainer&) = delete;

  ~ObjectContainer() = default;

  // Returns ID strings of all actionable objects
  std::set<std::string> getActionables() const;

  action::ActionableObject& getActionable(const std::string& aId);

  const action::ActionableObject& getActionable(const std::string& aId) const;

  // Returns ID strings of all monitorable objects (includes actionable objects)
  std::set<std::string> getMonitorables() const;

  core::MonitorableObject& getMonitorable(const std::string& aId);

  const core::MonitorableObject& getMonitorable(const std::string& aId) const;

protected:
  void addActionable(action::ActionableObject*);

  void addMonitorable(core::MonitorableObject*);

private:
  static void initializePresenceAndProperties(core::MonitorableObject&);

  std::unordered_map<std::string, action::ActionableObject*> mActionables;

  std::unordered_map<std::string, std::shared_ptr<core::MonitorableObject>> mMonitorables;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_OBJECTCONTAINER_HPP__ */