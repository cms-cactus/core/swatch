#ifndef __SWATCH_PHASE2_CSPSETTINGS_HPP__
#define __SWATCH_PHASE2_CSPSETTINGS_HPP__


#include <stddef.h>


namespace swatch {
namespace phase2 {


struct CSPSettings {
  enum IdleMethod {
    kIdleMethod1,
    kIdleMethod2
  };

  //! Number of data words per packet
  size_t packetLength;
  //! Number of clock cycles from start of one packet to the next (typically TMUX x clock cycles per BX)
  size_t packetPeriodicity;
  //! Idle method used (for use by output ports)
  IdleMethod idleMethod;
};


} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_CSPSETTINGS_HPP__ */