
#ifndef __SWATCH_PHASE2_INPUTPORT__
#define __SWATCH_PHASE2_INPUTPORT__


#include "swatch/phase2/AbstractPort.hpp"


namespace swatch {

namespace core {
template <typename DataType>
class SimpleMetric;
}

namespace phase2 {

class InputPort : public AbstractPort {
protected:
  explicit InputPort(const size_t aIndex, const std::string& aId);

  explicit InputPort(const size_t aIndex, const std::string& aId, const ChannelID& aConnectedChannel);

  explicit InputPort(const size_t aIndex, const std::string& aId, const boost::optional<ChannelID>& aConnectedChannel);

public:
  virtual ~InputPort();

  static const std::string kMetricIdIsLocked;
  static const std::string kMetricIdIsAligned;
  static const std::string kMetricIdCRCErrors;

  // static const std::vector<std::string> kDefaultMetrics;

protected:
  core::SimpleMetric<bool>& mMetricIsLocked;
  core::SimpleMetric<bool>& mMetricIsAligned;
  core::SimpleMetric<uint32_t>& mMetricCRCErrors;

  core::SimpleMetric<uint16_t>& mMetricSourceChannelId;
  core::SimpleMetric<uint16_t>& mMetricSourceSlotId;
  core::SimpleMetric<uint16_t>& mMetricSourceCrateId;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_INPUTPORT__ */
