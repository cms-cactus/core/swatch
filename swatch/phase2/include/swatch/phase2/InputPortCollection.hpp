
#ifndef __SWATCH_PHASE2_INPUTPORTCOLLECTION__
#define __SWATCH_PHASE2_INPUTPORTCOLLECTION__


#include <cstddef>
#include <map>
#include <string>
#include <vector>

#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

class InputPort;

//! Class that aggregates the input ports of a processor
class InputPortCollection : public core::MonitorableObject {
public:
  InputPortCollection();

  virtual ~InputPortCollection();

  //! Returns number of input channels
  size_t getNumPorts() const;

  bool contains(const size_t) const;

  const std::vector<const InputPort*>& getPorts() const;

  const std::vector<InputPort*>& getPorts();

  const InputPort& getPort(const std::string& aId) const;

  InputPort& getPort(const std::string& aId);

  const InputPort& getPort(const size_t aIndex) const;

  InputPort& getPort(const size_t aIndex);

  void addPort(InputPort* aInput);

protected:
  void retrieveMetricValues() {}

private:
  std::vector<const InputPort*> mConstPorts;
  std::vector<InputPort*> mPorts;
  std::map<size_t, InputPort*> mPortMap;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_INPUTPORTCOLLECTION__ */