
#ifndef __SWATCH_PHASE2_OPTICALMODULE__
#define __SWATCH_PHASE2_OPTICALMODULE__


#include <cstddef>
#include <functional>
#include <string>
#include <vector>

#include "swatch/action/ActionableStatus.hpp"
#include "swatch/action/PropertyHolder.hpp"
#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

class InputPort;

class OpticalModuleChannel : public action::PropertyHolder {
public:
  enum Direction {
    kRx,
    kTx
  };

  OpticalModuleChannel(const Direction, const size_t aIndex, const std::function<void(OpticalModuleChannel&)>& aRegistrar, const std::function<void(OpticalModuleChannel&)>& aMetricUpdater, const std::function<void(OpticalModuleChannel&)>& aPropertyUpdater);
  ~OpticalModuleChannel();

  Direction getDirection() const;
  size_t getIndex() const;

  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;
  template <typename DataType>
  using Property = swatch::action::Property<DataType>;

  template <typename DataType>
  SimpleMetric<DataType>& registerMetric(const std::string& aId)
  {
    return MonitorableObject::registerMetric<DataType>(aId);
  }

  template <typename DataType>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const char* aAlias)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aAlias);
  }

  template <typename DataType>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const std::string& aAlias)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aAlias);
  }

  template <typename DataType, class ErrorCondition>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const ErrorCondition& aErrorCondition)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aErrorCondition);
  }

  template <typename DataType, class ErrorCondition>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const char* aAlias, const ErrorCondition& aErrorCondition)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aAlias, aErrorCondition);
  }

  template <typename DataType, class ErrorCondition>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const std::string& aAlias, const ErrorCondition& aErrorCondition)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aAlias, aErrorCondition);
  }

  template <typename DataType, class ErrorCondition, class WarnCondition>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const ErrorCondition& aErrorCondition, const WarnCondition& aWarnCondition)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aErrorCondition, aWarnCondition);
  }

  template <typename DataType, class ErrorCondition, class WarnCondition>
  SimpleMetric<DataType>& registerMetric(const std::string& aId, const std::string& aAlias, const ErrorCondition& aErrorCondition, const WarnCondition& aWarnCondition)
  {
    return MonitorableObject::registerMetric<DataType>(aId, aAlias, aErrorCondition, aWarnCondition);
  }

  template <typename DataType>
  Property<DataType>& registerProperty(const std::string& aName)
  {
    return PropertyHolder::registerProperty<DataType>(aName);
  }

  template <typename DataType>
  Property<DataType>& registerProperty(const std::string& aName, const std::string& aGroup)
  {
    return PropertyHolder::registerProperty<DataType>(aName, aGroup);
  }

  void delegateMetricUpdate(const std::string&, MonitorableObject&);

private:
  bool checkPresence() const final;
  void retrieveMetricValues() final;
  void retrievePropertyValues() final;

  Direction mDirection;
  size_t mIndex;
  std::function<void(OpticalModuleChannel&)> mMetricUpdater;
  std::function<void(OpticalModuleChannel&)> mPropertyUpdater;
};


class OpticalModule : public action::PropertyHolder {
public:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;
  template <typename DataType>
  using Property = swatch::action::Property<DataType>;
  using Channel = OpticalModuleChannel;

protected:
  OpticalModule(const std::string& aId, size_t aNumInputChannels, size_t aNumOutputChannels);

  OpticalModule(const std::string& aId, const std::string& aAlias, size_t aNumInputChannels, size_t aNumOutputChannels);

  OpticalModule(const std::string& aId, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices);

  OpticalModule(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices);

public:
  virtual ~OpticalModule();

  const std::vector<const OpticalModuleChannel*>& getInputs() const;

  const std::vector<OpticalModuleChannel*>& getInputs();

  bool containsInput(size_t) const;

  const OpticalModuleChannel& getInput(size_t) const;

  OpticalModuleChannel& getInput(size_t);

  const std::vector<const OpticalModuleChannel*>& getOutputs() const;

  const std::vector<OpticalModuleChannel*>& getOutputs();

  bool containsOutput(size_t) const;

  const OpticalModuleChannel& getOutput(size_t) const;

  OpticalModuleChannel& getOutput(size_t);

private:
  virtual void registerInputMetrics(OpticalModuleChannel& aChannel);
  virtual void registerOutputMetrics(OpticalModuleChannel& aChannel);
  virtual void registerInputProperties(OpticalModuleChannel& aChannel);
  virtual void registerOutputProperties(OpticalModuleChannel& aChannel);

  virtual void retrieveInputMetricValues(OpticalModuleChannel& aChannel);
  virtual void retrieveOutputMetricValues(OpticalModuleChannel& aChannel);
  virtual void retrieveInputPropertyValues(OpticalModuleChannel& aChannel);
  virtual void retrieveOutputPropertyValues(OpticalModuleChannel& aChannel);

  static std::vector<size_t> getIndexVector(size_t);

  // FIXME: In long term ideally declare single common ActionableStatus instance for all optical
  //        modules - to protect against metrics of framework trying to update metrics of multiple
  //        modules in parallel
  action::ActionableStatus mStatus;
  log4cplus::Logger mLogger;

  std::map<size_t, OpticalModuleChannel*> mInputMap;
  std::map<size_t, OpticalModuleChannel*> mOutputMap;

  std::vector<OpticalModuleChannel*> mInputs;
  std::vector<OpticalModuleChannel*> mOutputs;

  std::vector<const OpticalModuleChannel*> mConstInputs;
  std::vector<const OpticalModuleChannel*> mConstOutputs;
};


SWATCH_DEFINE_EXCEPTION(DuplicateChannelIndex)


} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_OPTICALMODULE__ */
