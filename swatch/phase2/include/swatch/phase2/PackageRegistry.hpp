#ifndef __SWATCH_PHASE2_PACKAGEREGISTRY_HPP__
#define __SWATCH_PHASE2_PACKAGEREGISTRY_HPP__


#include <string>
#include <unordered_map>

#include "swatch/core/exception.hpp"
#include "swatch/phase2/PackageInfo.hpp"


namespace swatch {
namespace phase2 {

class PackageRegistry {
public:
  PackageRegistry();

  ~PackageRegistry();

  static void add(const PackageInfo& aPackage);

  static const PackageInfo& get(const std::string&);

  static std::unordered_map<std::string, PackageInfo>::const_iterator begin();

  static std::unordered_map<std::string, PackageInfo>::const_iterator end();

private:
  static std::unordered_map<std::string, PackageInfo> sPackages;
};


template <const size_t CountT>
class PackageRegistrationHelper {
  static bool sInitialised;

  static bool init(const PackageInfo& aPackage)
  {
    PackageRegistry::add(aPackage);
    return true;
  }
};


SWATCH_DEFINE_EXCEPTION(PackageAlreadyRegistered)
SWATCH_DEFINE_EXCEPTION(PackageNotFound)

} // namespace phase2
} // namespace swatch


#define _SWATCH_REGISTER_PACKAGE_INFO(index, packageInfo) \
  template <>                                             \
  bool ::swatch::phase2::PackageRegistrationHelper<index>::sInitialised = ::swatch::phase2::PackageRegistrationHelper<index>::init(packageInfo);


#define SWATCH_REGISTER_PACKAGE_INFO(packageInfo) \
  _SWATCH_REGISTER_PACKAGE_INFO(__COUNTER__, packageInfo)


#endif /* __SWATCH_PHASE2_PACKAGEREGISTRY_HPP__ */
