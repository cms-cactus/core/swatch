
#ifndef __SWATCH_PHASE2_BOARD_HPP__
#define __SWATCH_PHASE2_BOARD_HPP__


#include <functional>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "swatch/phase2/ObjectContainer.hpp"
#include "swatch/phase2/OpticalModule.hpp"


namespace swatch {
namespace action {
class ActionableObject;
class ObjectFunctionoid;
}

namespace phase2 {

class DeviceStub;
class InputPort;
class OpticalFibreConnector;
class OpticalFibreConnectorStub;
class OpticalModule;
class OutputPort;
class Processor;
class ServiceModule;

class Board : public ObjectContainer {
public:
  Board(const std::string&, const DeviceStub&, const std::vector<DeviceStub>&, const std::vector<OpticalFibreConnectorStub>& aOptoConnectorStubs);

  Board(const std::string&, const DeviceStub&, const std::vector<DeviceStub>&, const std::vector<OpticalFibreConnectorStub>& aOptoConnectorStubs, const std::function<void(const action::ObjectFunctionoid&)>& aAdditionalPostActionCallback);

  Board(const Board&) = delete;
  Board& operator=(const Board&) = delete;

  ~Board();

  const std::string& getHardwareType() const;

  ServiceModule& getServiceModule();

  const ServiceModule& getServiceModule() const;

  std::set<std::string> getProcessors() const;

  Processor& getProcessor(const std::string& aId);

  const Processor& getProcessor(const std::string& aId) const;

  std::unordered_set<std::string> getOpticalModules() const;

  OpticalModule& getOpticalModule(const std::string& aId);

  const OpticalModule& getOpticalModule(const std::string& aId) const;

  std::unordered_set<std::string> getFrontPanelOptoConnectors() const;

  const OpticalFibreConnector* getFrontPanelOptoConnector(const std::string& aId) const;

  const InputPort* getConnectedInputPort(const OpticalModuleChannel&) const;

  const OutputPort* getConnectedOutputPort(const OpticalModuleChannel&) const;

  std::pair<const OpticalFibreConnector*, size_t> getConnectedFrontPanelChannel(const OpticalModuleChannel&) const;

  const OpticalModuleChannel* getConnectedOpticalChannel(const InputPort&) const;

  const OpticalModuleChannel* getConnectedOpticalChannel(const OutputPort&) const;

  const OpticalModuleChannel* getConnectedOpticalChannel(const OpticalFibreConnector&, const size_t) const;

  typedef std::function<std::pair<std::vector<OpticalModule*>, std::vector<std::string>>()> OpticalModuleCallback;
  static void registerOpticalModuleCallback(const std::string& aHardwareType, const OpticalModuleCallback&);

  void syncOpticsMonitoringSettingsWithFPGA(const Processor&);

  template <const size_t CountT>
  class RegistrationHelper {
    static bool sInitialised;

    static bool init(const std::string& aHwType, const OpticalModuleCallback& aFunction)
    {
      Board::registerOpticalModuleCallback(aHwType, aFunction);
      return true;
    }
  };

private:
  static log4cplus::Logger sLogger;
  static std::unordered_map<std::string, OpticalModuleCallback> sOpticalModuleCallbacks;

  const std::string mHardwareType;

  ServiceModule* mServiceModule;

  std::unordered_map<std::string, Processor*> mProcessors;

  std::unordered_map<std::string, OpticalModule*> mOpticalModules;

  std::unordered_map<std::string, std::unique_ptr<OpticalFibreConnector>> mFrontPanelOptoConnectors;

  std::map<const OpticalModuleChannel*, std::pair<const OpticalFibreConnector*, size_t>> mOpticalFrontPanelChannelMap;
  std::map<const OpticalModuleChannel*, std::pair<const Processor*, const InputPort*>> mOpticalInputPortMap;
  std::map<const OpticalModuleChannel*, std::pair<const Processor*, const OutputPort*>> mOpticalOutputPortMap;

  std::map<std::pair<const OpticalFibreConnector*, size_t>, const OpticalModuleChannel*> mFrontPanelOpticalModuleMap;
};


SWATCH_DEFINE_EXCEPTION(CallbackAlreadyDefined)
SWATCH_DEFINE_EXCEPTION(DuplicateConnectorId)
SWATCH_DEFINE_EXCEPTION(DuplicateModuleId)
SWATCH_DEFINE_EXCEPTION(InconsistentConnectorInfo)
SWATCH_DEFINE_EXCEPTION(InvalidConnectionInfo)


#define _SWATCH_REGISTER_PHASE2_OPTICS_CREATOR(index, hwType, func) \
  template <>                                                       \
  bool ::swatch::phase2::Board::RegistrationHelper<index>::sInitialised = ::swatch::phase2::Board::RegistrationHelper<index>::init(hwType, func);


#define SWATCH_REGISTER_PHASE2_OPTICS_CREATOR(hwType, func) \
  _SWATCH_REGISTER_PHASE2_OPTICS_CREATOR(__COUNTER__, hwType, func)


} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_BOARD_HPP__ */
