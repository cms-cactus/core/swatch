
#ifndef __SWATCH_PHASE2_PROCESSOR_HPP__
#define __SWATCH_PHASE2_PROCESSOR_HPP__


#include "swatch/phase2/Device.hpp"


namespace swatch {
namespace action {
class StateMachine;
class Transition;
}


namespace phase2 {

namespace fileTypes {
extern const std::string kRxBufferData;
extern const std::string kTxBufferData;
}

namespace processorIds {
extern const std::string kAlgo;
extern const std::string kInputPorts;
extern const std::string kOutputPorts;
extern const std::string kReadout;
extern const std::string kTTC;
}

struct PayloadTestFSM {
  //! FSM ID string
  static const std::string kId;
  //! Initial state (i.e. halted)
  static const std::string kStateInitial;
  //! Error state
  static const std::string kStateError;
  //! Ready state
  static const std::string kStateReady;

  //! ID string for the 'setup' transition (initial state to ready state)
  static const std::string kTrSetup;
  //! ID string for the 'playback' transition (ready state to ready state)
  static const std::string kTrPlayback;

  //! The run control FSM object
  action::StateMachine& fsm;
  //! The 'setup' transition (initial state to ready state)
  action::Transition& setup;
  //! The 'playback' transition (ready state to ready state)
  action::Transition& playback;

  PayloadTestFSM(const PayloadTestFSM&) = delete;
  PayloadTestFSM& operator=(const PayloadTestFSM&) = delete;
  PayloadTestFSM(action::StateMachine& aFSM);

private:
  static action::StateMachine& addStates(action::StateMachine& aFSM);
};



struct LinkTestFSM {
  //! FSM ID string
  static const std::string kId;
  //! Initial state (i.e. halted)
  static const std::string kStateInitial;
  //! Error state
  static const std::string kStateError;
  //! Synchronized state
  static const std::string kStateSynchronized;
  //! Tx configured state
  static const std::string kStateTxConfigured;
  //! Rx configured state
  static const std::string kStateRxConfigured;

  //! ID string for the 'setup' transition (initial state to synchronized state)
  static const std::string kTrSetup;
  //! ID string for the 'configure TX' transition (synchronized state to tx configured state)
  static const std::string kTrConfigureTx;
  //! ID string for the 'configure RX' transition (tx configured state to rx configured state)
  static const std::string kTrConfigureRx;
  //! ID string for the 'capture' transition (configured state to configured state)
  static const std::string kTrCapture;
  //! ID string for the 'continue' transition (configured state to configured state)
  static const std::string kTrContinue; // Alt name: proceed, advance, return?

  //! The run control FSM object
  action::StateMachine& fsm;
  //! The 'setup' transition (initial state to synchronized state)
  action::Transition& setup;
  //! The 'configure TX' transition (synchronized state to tx configured state)
  action::Transition& configureTx;
  //! The 'configure RX' transition (tx configured state to rx configured state)
  action::Transition& configureRx;
  //! The 'capture' transition (configured state to configured state)
  action::Transition& capture;

  LinkTestFSM(const LinkTestFSM&) = delete;
  LinkTestFSM& operator=(const LinkTestFSM&) = delete;
  LinkTestFSM(action::StateMachine& aFSM);

private:
  static action::StateMachine& addStates(action::StateMachine& aFSM);
};


struct SliceTestFSM {
  //! FSM ID string
  static const std::string kId;
  //! Initial state (i.e. halted)
  static const std::string kStateInitial;
  //! Error state
  static const std::string kStateError;
  //! Synchronized state
  static const std::string kStateSynchronized;
  //! Configured state
  static const std::string kStateConfigured;

  //! ID string for the 'setup' transition (initial state to synchronized state)
  static const std::string kTrSetup;
  //! ID string for the 'configure' transition (synchronized state to configured state)
  static const std::string kTrConfigure;
  //! ID string for the 'capture' transition (configured state to configured state)
  static const std::string kTrCapture;
  //! ID string for the 'continue' transition (configured state to configured state)
  static const std::string kTrContinue; // Alt name: proceed, advance, return?

  //! The run control FSM object
  action::StateMachine& fsm;
  //! The 'setup' transition (initial state to synchronized state)
  action::Transition& setup;
  //! The 'configure' transition (synchronized state to configured state)
  action::Transition& configure;
  //! The 'capture' transition (configured state to configured state)
  action::Transition& capture;

  SliceTestFSM(const SliceTestFSM&) = delete;
  SliceTestFSM& operator=(const SliceTestFSM&) = delete;
  SliceTestFSM(action::StateMachine& aFSM);

private:
  static action::StateMachine& addStates(action::StateMachine& aFSM);
};


// Forward declarations
class AlgoInterface;
class InputPortCollection;
class OpticalModule;
class OutputPortCollection;
class ReadoutInterface;
class TTCInterface;

class Processor : public Device {
public:
  Processor(const ::swatch::core::AbstractStub& aStub, const size_t aCSPIndexOffset = 0);
  virtual ~Processor();

  // NOTE: Getters below possibly not required - TBC

  //! Returns this processor's TTC interface
  const TTCInterface& getTTC() const;

  //! Returns this processor's TTC interface
  TTCInterface& getTTC();

  //! Returns this processor's readout interface
  const ReadoutInterface& getReadout() const;

  //! Returns this processor's readout interface
  ReadoutInterface& getReadout();

  //! Returns this processor's algo interface
  const AlgoInterface& getAlgo() const;

  //! Returns this processor's algo interface
  AlgoInterface& getAlgo();

  //! Returns this processor's input port collection
  const InputPortCollection& getInputPorts() const;

  //! Returns this processor's input port collection
  InputPortCollection& getInputPorts();

  //! Returns this processor's output port collection
  const OutputPortCollection& getOutputPorts() const;

  //! Returns this processor's output port collection
  OutputPortCollection& getOutputPorts();

  size_t getCSPIndexOffset() const;

  const boost::optional<std::string>& getTCDS2Source() const;

protected:
  //! Register the supplied (heap-allocated) TTC interface in this processor; the processor base class takes ownership of the TTC interface instance.
  TTCInterface& registerTTC(TTCInterface* aTTCInterface);

  //! Register the supplied (heap-allocated) readout interface in this processor; the processor base class takes ownership of the readout interface instance.
  ReadoutInterface& registerReadout(ReadoutInterface* aReadoutInterface);

  //! Register the supplied (heap-allocated) algo interface in this processor; the processor base class takes ownership of the algo interface instance.
  AlgoInterface& registerAlgo(AlgoInterface* aAlgoInterface);

  InputPortCollection& registerInputs();

  //! Register the supplied (heap-allocated) input port collection in this processor; the processor base class takes ownership of the supplied port collection.
  InputPortCollection& registerInputs(InputPortCollection* aPortCollection);

  OutputPortCollection& registerOutputs();

  //! Register the supplied (heap-allocated) output port collection in this processor; the processor base class takes ownership of the supplied port collection.
  OutputPortCollection& registerOutputs(OutputPortCollection* aPortCollection);

  //! Set the ID of the FPGA acting as a board-local TCDS2 master, if this processor does not receive the TCDS2 stream directly from the backplane
  void setTCDS2Source(const std::string& aId);

  PayloadTestFSM& getPayloadTestFSM();

  LinkTestFSM& getLinkTestFSM();

  SliceTestFSM& getSliceTestFSM();

private:
  //! TTC control interface
  TTCInterface* mTTC;

  //! Readout control interface
  ReadoutInterface* mReadout;

  //! Algorithm control interface
  AlgoInterface* mAlgo;

  //! Optical input ports
  InputPortCollection* mInputPorts;

  //! Collection of optical output ports
  OutputPortCollection* mOutputPorts;

  size_t mCSPIndexOffset;

  boost::optional<std::string> mTCDS2Source;

  mutable std::vector<std::string> mGateKeeperTables;

  PayloadTestFSM mPayloadTestFSM;
  LinkTestFSM mLinkTestFSM;
  SliceTestFSM mSliceTestFSM;

  friend class Board;
};


SWATCH_DEFINE_EXCEPTION(InterfaceAlreadyDefined)
SWATCH_DEFINE_EXCEPTION(InterfaceNotDefined)
SWATCH_DEFINE_EXCEPTION(TCDS2SourceAlreadyDefined)
SWATCH_DEFINE_EXCEPTION(ProcessorAbsent)


} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_PROCESSOR_HPP__ */
