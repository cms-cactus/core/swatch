#ifndef __SWATCH_PHASE2_OPTICALFIBRECONNECTORSTUB_HPP__
#define __SWATCH_PHASE2_OPTICALFIBRECONNECTORSTUB_HPP__


#include <map>
#include <stddef.h>
#include <string>


namespace swatch {
namespace phase2 {


class OpticalFibreConnectorStub {
public:
  OpticalFibreConnectorStub(const std::string&, const std::string&, const size_t);
  ~OpticalFibreConnectorStub();

  std::string id;
  std::string type;
  size_t channelCount;
  std::map<size_t, std::string> connectedOpticsMap;
};


}
}


#endif /* __SWATCH_PHASE2_OPTICALFIBRECONNECTOR_HPP__ */