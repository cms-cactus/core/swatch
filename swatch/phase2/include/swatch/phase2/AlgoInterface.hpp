
#ifndef __SWATCH_PHASE2_ALGOINTERFACE__
#define __SWATCH_PHASE2_ALGOINTERFACE__


#include "swatch/core/MonitorableObject.hpp"


namespace swatch {
namespace phase2 {

class AlgoInterface : public core::MonitorableObject {
public:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;

  AlgoInterface();

  virtual ~AlgoInterface();

  void retrieveMetricValues();
};

} // namespace phase2
} // namespace swatch


#endif /* __SWATCH_PHASE2_ALGOINTERFACE__ */