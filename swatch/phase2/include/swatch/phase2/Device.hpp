
#ifndef __SWATCH_PHASE2_DEVICE_HPP__
#define __SWATCH_PHASE2_DEVICE_HPP__


// Standard headers
#include <stdint.h> // for uint32_t, uint64_t
#include <string> // for string
#include <vector> // for vector

// SWATCH headers
#include "swatch/action/ActionableObject.hpp"
#include "swatch/core/exception.hpp"
#include "swatch/phase2/DeviceStub.hpp"


namespace swatch {

namespace core {
class AbstractStub;
}

namespace phase2 {


class Device : public action::ActionableObject {

protected:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;
  template <typename DataType>
  using Property = swatch::action::Property<DataType>;

  Device(const swatch::core::AbstractStub&);

public:
  Device(const Device&) = delete;
  Device& operator=(const Device&) = delete;

  virtual ~Device();

  const DeviceStub& getStub() const;

  virtual const std::vector<std::string>& getGateKeeperContexts() const;

private:
  const DeviceStub mStub;

  mutable std::vector<std::string> mGateKeeperTables;
};


} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_DEVICE_HPP__ */
