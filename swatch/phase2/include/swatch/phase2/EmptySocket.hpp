
#ifndef __SWATCH_PHASE2_EMPTYSOCKET__
#define __SWATCH_PHASE2_EMPTYSOCKET__


#include "swatch/phase2/OpticalModule.hpp"


namespace swatch {
namespace phase2 {

class EmptySocket : public OpticalModule {
public:
  EmptySocket(const std::string& aId, size_t aNumInputChannels, size_t aNumOutputChannels);
  EmptySocket(const std::string& aId, const std::string& aAlias, size_t aNumInputChannels, size_t aNumOutputChannels);
  EmptySocket(const std::string& aId, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices);
  EmptySocket(const std::string& aId, const std::string& aAlias, const std::vector<size_t>& aInputIndices, const std::vector<size_t>& aOutputIndices);

  virtual ~EmptySocket();

private:
  bool checkPresence() const;
  void retrievePropertyValues() override;
  void retrieveMetricValues() override;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_EMPTYSOCKET__ */