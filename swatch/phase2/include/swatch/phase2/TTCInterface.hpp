
#ifndef __SWATCH_PHASE2_TTCINTERFACE_HPP__
#define __SWATCH_PHASE2_TTCINTERFACE_HPP__


#include "swatch/action/PropertyHolder.hpp"


namespace swatch {

namespace core {
template <typename DataType>
class SimpleMetric;
}

namespace phase2 {

//! Abstract class defining the TTC component interface of a processor
class TTCInterface : public action::PropertyHolder {
protected:
  template <typename DataType>
  using SimpleMetric = swatch::core::SimpleMetric<DataType>;

  TTCInterface();

public:
  virtual ~TTCInterface();

protected:
  //! Metric containing l1a counter values
  core::SimpleMetric<uint32_t>& mMetricL1ACounter;

  //! Metric containing bunch counter value
  core::SimpleMetric<uint32_t>& mMetricBunchCounter;

  //! Metric containing orbit counter value
  core::SimpleMetric<uint32_t>& mMetricOrbitCounter;

  //! Metric indicating if clk40 is locked
  core::SimpleMetric<bool>& mMetricIsClock40Locked;

  //! Metric indicating if clk40 has stopped
  core::SimpleMetric<bool>& mMetricHasClock40Stopped;

  //! Metric indicating if BC0 is locked
  core::SimpleMetric<bool>& mMetricIsBC0Locked;

public:
  // static const std::vector<std::string> kDefaultMetrics;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_TTCINTERFACE_HPP__ */
