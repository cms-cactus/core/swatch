#ifndef __SWATCH_PHASE2_LINKOPERATINGMODE_HPP__
#define __SWATCH_PHASE2_LINKOPERATINGMODE_HPP__


namespace swatch {
namespace phase2 {


enum LinkOperatingMode {
  kCSP,
  kPRBS
};


} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_LINKOPERATINGMODE_HPP__ */