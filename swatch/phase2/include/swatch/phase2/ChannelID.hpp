#ifndef __SWATCH_PHASE2_CHANNELID_HPP__
#define __SWATCH_PHASE2_CHANNELID_HPP__


#include <stddef.h>
#include <string>


namespace swatch {
namespace phase2 {

//! Board-local identifier for I/O channel (e.g. FPGA I/O port or optical module channel)
struct ChannelID {
  //! ID string of FPGA device or optical module
  std::string component;
  //! Index of channel (local to FPGA device or optical module)
  size_t index;
};

} // namespace phase2
} // namespace swatch

#endif /* __SWATCH_PHASE2_CHANNELID_HPP__ */