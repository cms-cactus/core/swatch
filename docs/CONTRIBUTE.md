# Contribution guidelines

## Implementing a feature or fixing a bug

We follow the standard git feature branch workflow, use GitLab merge requests for reviewing changes, and GitLab CI jobs for automated testing.

 1. Make sure that your local copy of the repository is up to date:
    ```bash
    git checkout master && git pull
    ```
 2. Create a branch:
    ```bash
    # If an issue has already been opened (with X replaced by the issue number)
    git checkout -b <username>-issue-X
    # Otherwise
    git checkout -b <username>-my-cool-feature
    ```
 3. Commit your changes:
    ```bash
    git add file1 file2 ...
    git commit -m "Short description of commit"
    git push origin my-cool-feature
    ```
 4. [Open a merge request](https://gitlab.cern.ch/cms-cactus/core/swatch//-/merge_requests/new)

    * If you haven't finished your work on this branch, then start the title with `WIP:` to ensure that it doesn't get merged straight away.


## Creating a new release

 1. Change the version variables of updated packages (in their Makefiles) accordingly

 2. Create the new tag:

```
git checkout master
git pull
git tag -a -m "Version X.Y.Z" vX.Y.Z HEAD
```
 3. Update the [user guide](http://cactus.web.cern.ch/cactus/release/swatch/latest_doc/index.html)

    * Source: https://gitlab.cern.ch/cms-cactus/core/swatch-docs/
    * Note that the `master` branch of the docs are deployed to the above URL. If you want to test some changes first, then commit them to another branch; the results will be deployed under http://cactus.web.cern.ch/cactus/release/swatch/pipelines/